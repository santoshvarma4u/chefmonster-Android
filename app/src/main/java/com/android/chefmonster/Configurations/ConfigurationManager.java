package com.android.chefmonster.Configurations;

/**
 * Created by SantoshT on 6/1/2017.
 */

public class ConfigurationManager {
    private static ConfigurationManager sInstance = null;
    public static ConfigurationManager getInstance() {
        if (sInstance == null) {
            sInstance = new ConfigurationManager();
        }

        return sInstance;
    }
    private static final String APIURL = "api";
}
