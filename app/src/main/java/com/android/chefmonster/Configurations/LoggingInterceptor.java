package com.android.chefmonster.Configurations;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by SantoshT on 6/1/2017.
 */

class LoggingInterceptor implements Interceptor {

    HttpLoggingInterceptor.Logger logger;
    @Override public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();


        long t1 = System.nanoTime();
        logger.log(String.format("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.headers()));

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        logger.log(String.format("Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6d, response.headers()));
        return response;
    }
}
