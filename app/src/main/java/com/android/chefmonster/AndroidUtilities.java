package com.android.chefmonster;

//8106555293

public class AndroidUtilities {
	private static boolean waitingForSms = false;
	private static final Object smsLock = new Object();

	public static void runOnUIThread(Runnable runnable) {
		runOnUIThread(runnable, 0);
	}

	public static void runOnUIThread(Runnable runnable, long delay) {
		if (delay == 0) {
			ApplicationLoader.applicationHandler.post(runnable);
		} else {
			ApplicationLoader.applicationHandler.postDelayed(runnable, delay);
		}
	}

	public static boolean isWaitingForSms() {
		boolean value = false;
		synchronized (smsLock) {
			value = waitingForSms;
		}
		return value;
	}

	public static void setWaitingForSms(boolean value) {
		synchronized (smsLock) {
			waitingForSms = value;
		}
	}

	class SMS {
		boolean flag = false;

		public synchronized void Question(String msg) {
			if (flag) {
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println(msg);
			flag = true;
			notify();
		}

		public synchronized void Answer(String msg) {
			if (!flag) {
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			System.out.println(msg);
			flag = false;
			notify();
		}
	}
}
