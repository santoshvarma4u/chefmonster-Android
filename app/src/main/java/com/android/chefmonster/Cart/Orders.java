package com.android.chefmonster.Cart;

/**
 * Created by SantoshT on 4/17/2017.
 */

public class Orders {

    String Order_ID,OrderDate,OrderInfo,OrderItemName,OrderItemQty,OrderItemCost,OrderStatus,OrderItemInvCost;

    public String getOrder_ID() {
        return Order_ID;
    }

    public void setOrder_ID(String order_ID) {
        Order_ID = order_ID;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getOrderInfo() {
        return OrderInfo;
    }

    public void setOrderInfo(String orderInfo) {
        OrderInfo = orderInfo;
    }

    public String getOrderItemName() {
        return OrderItemName;
    }

    public void setOrderItemName(String orderItemName) {
        OrderItemName = orderItemName;
    }

    public String getOrderItemQty() {
        return OrderItemQty;
    }

    public void setOrderItemQty(String orderItemQty) {
        OrderItemQty = orderItemQty;
    }

    public String getOrderItemCost() {
        return OrderItemCost;
    }

    public void setOrderItemCost(String orderItemCost) {
        OrderItemCost = orderItemCost;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getOrderItemInvCost() {
        return OrderItemInvCost;
    }

    public void setOrderItemInvCost(String orderItemInvCost) {
        OrderItemInvCost = orderItemInvCost;
    }
}
