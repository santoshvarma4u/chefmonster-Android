package com.android.chefmonster.Cart;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Checkout.Checkout;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ChakravartyG on 9/28/2016.
 */

public class ActivityCartNew extends AppCompatActivity {
    private static final String userLogin = "userLogin";
    private static final String s_username = "username";
    private static final String s_Email = "email";
    private static final String s_Phone = "phone";
    // declare static variables to store tax and currency data
    static double Tax;
    //static String Currency;
    final int CLEAR_ALL_ORDER = 0;
    final int CLEAR_ONE_ORDER = 1;
    List<Order> lOrdersDel, lOrdersPickup;
    //
    DBHelper_New db;
    RecyclerView rvDelivery, rvPickup;
    ProgressBar prgLoading;
    TextView txtTotalLabel, txtTotal, txtAlert;
    Button btnClear, Checkout;

    // declate dbhelper and adapter objects
    //DBHelper_New dbhelper;
    double Total_price;
    int FLAG;
    int ID;
    String TaxCurrencyAPI;
    int IOConnect = 0;

    //SmartUser currentUser=null;
    Geocoder geocoder;
    List<Address> addresses;
    int PLACE_PICKER_REQUEST = 4879;
    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");
    ArrayList<ArrayList<Object>> data;
    SharedPreferences sharedPreferences;
    private boolean mUserLoggedIn = false;
    //
    TextView tvPickNodata,tvNodelData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_cart_new);/*
        //
        tvNodelData=(TextView)findViewById(R.id.tvNodelData);
        tvPickNodata=(TextView)findViewById(R.id.tvPickNodata);

        Checkout = (Button) findViewById(R.id.Checkout);
        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        txtTotalLabel = (TextView) findViewById(R.id.txtTotalLabel);
        txtTotal = (TextView) findViewById(R.id.txtTotal);
        txtAlert = (TextView) findViewById(R.id.txtAlert);
        btnClear = (Button) findViewById(R.id.btnClear);

        tvDeliveryClear=(TextView)findViewById(R.id.tvDeliveryClear);
        tvPickClear=(TextView)findViewById(R.id.tvPickClear);
        tvDeliveryClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeliveryDataClear(v);
            }
        });
        tvPickClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPickUpDataClear(v);
            }
        });
        imageLoader = new ImageLoader(ActivityCartNew.this);


        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbarr) ;
        setSupportActionBar(toolbar);
        ImageView iv_menu_nav=(ImageView) findViewById(R.id.iv_menu_nav);
        TextView tv_title=(TextView) findViewById(R.id.txtTitle);
        tv_title.setText("My Cart");
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               startActivity(new Intent(ActivityCartNew.this,MainActivity.class));
                finish();
            }
        });

        //
        db = new DBHelper_New(this);
        //dbhelper= new DBHelper_New(this);
        lOrdersDel = new ArrayList<>();
        lOrdersPickup = new ArrayList<>();
        //
        //
        rvDelivery = (RecyclerView) findViewById(R.id.rvDelivery);
        RecyclerView.LayoutManager mLayoutManagerDel = new LinearLayoutManager(this);
        rvDelivery.setLayoutManager(mLayoutManagerDel);
        rvDelivery.setItemAnimator(new DefaultItemAnimator());
        //
        rvPickup = (RecyclerView) findViewById(R.id.rvPickup);
        RecyclerView.LayoutManager mLayoutManagerPickk = new LinearLayoutManager(this);
        rvPickup.setLayoutManager(mLayoutManagerPickk);
        rvPickup.setItemAnimator(new DefaultItemAnimator());
        //


*//*
        btnClear.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                // show confirmation dialog
                showClearDialog(CLEAR_ALL_ORDER, 1111);
            }
        });
*//*



        //test by chakry
        // event listener to handle list when clicked
        *//*listOrder.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                // show confirmation dialog
                System.out.println("in click");
                showClearDialog(CLEAR_ONE_ORDER, Menu_ID.get(position));
            }
        });*//*


        Checkout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                db.close();
                Intent iReservation = new Intent(ActivityCartNew.this, Checkout.class);
                startActivity(iReservation);


            }
        });
        new ActivityCartNew.getDataTask().execute();*/

    }

  /*  // method to create dialog
    void showClearDialog(int flag, int id) {
        FLAG = flag;
        ID = id;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirm);
        switch (FLAG) {
            case 0:
                builder.setMessage(getString(R.string.clear_all_order));
                break;
            case 1:
                builder.setMessage(getString(R.string.clear_one_order));
                break;
        }
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (FLAG) {
                    case 0:
                        // clear all menu in order table
                        //dbhelper.openDataBase();
                        db.deleteAllData();
                        //listOrder.invalidateViews();
                        clearData();
                        new ActivityCartNew.getDataTask().execute();
                        break;
                    case 1:
                        // clear selected menu in order table
                        //	dbhelper.openDataBase();
                        db.deleteData(ID);
                        // listOrder.invalidateViews();
                        clearData();
                        new ActivityCartNew.getDataTask().execute();
                        break;
                }

            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                // close dialog
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    // clear arraylist variables before used
    void clearData() {
        lOrdersDel.clear();

        lOrdersPickup.clear();
    }

    public double getDataFromDatabase(Context ctx){

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        data = db.getAllData();

        // store data to arraylist variables
        for(int i=0;i<data.size();i++){
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price +=Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));

        return Total_price;
    }


    // method to get data from server
    public void getDataFromDatabase() {

        Total_price = 0;
        clearData();
        data = db.getAllData();

        if(data.size() > 0)
        {
            // store data to arraylist variables
            for (int i = 0; i < data.size(); i++) {
                ArrayList<Object> row = data.get(i);
                //
                Order order = new Order();
                order.setMenu_ID(Integer.parseInt(row.get(0).toString()));
                order.setMenu_name(row.get(1).toString());
                order.setQuantity(Integer.parseInt(row.get(2).toString()));
                order.setSub_total_price(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
                order.setOrderType(row.get(4).toString());
                order.setOrderFrom(row.get(5).toString());
                order.setItemImage(row.get(6).toString());
                Total_price += order.getSub_total_price();
                //
                if(row.get(4).toString().equalsIgnoreCase("DELIVERY")){
                    lOrdersDel.add(order);
                }else{
                    lOrdersPickup.add(order);
                }

            }

            // count total order
            Total_price -= (Total_price * (Tax / 100));
            Total_price = Double.parseDouble(formatData.format(Total_price));
        }
        else
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ActivityCartNew.this,"Hmmm!...No Items Found in your bowl!",Toast.LENGTH_LONG).show();
                }
            });

            startActivity(new Intent(ActivityCartNew.this, MainActivity.class));
            finish();
        }


    }

    // when back button pressed close database and back to previous page
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        //dbhelper.close();
        finish();
        //overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }

    public ImageLoader imageLoader;
    private boolean didUserLoggedin() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mUserLoggedIn = sharedPreferences.getBoolean(userLogin, false);
        return mUserLoggedIn;
    }

    public class OrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        List<Order> orders;
        boolean isDelvery;

        public OrdersAdapter(List<Order> orders,boolean isDelvery) {
            this.orders = orders;
            this.isDelvery=isDelvery;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater li = LayoutInflater.from(parent.getContext());
            View view = li.inflate(R.layout.order_list_item, parent, false);
            RecyclerView.ViewHolder vHoder;
            if(isDelvery)
             vHoder = new ViewholderOrderDel(view);
            else
                vHoder = new ViewholderOrderPick(view);
            return vHoder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


            if(holder instanceof ViewholderOrderDel){
                ViewholderOrderDel viewholderOrder = (ViewholderOrderDel) holder;
                viewholderOrder.txtMenuIDcart.setText(orders.get(position).getMenu_ID()+"");
                viewholderOrder.txtMenuName.setText(orders.get(position).getMenu_name());
                viewholderOrder.txtQuantity.setText(orders.get(position).getQuantity()+"");
                viewholderOrder.txtPrice.setText(orders.get(position).getSub_total_price()+" ₹".trim());
                viewholderOrder.txtDealType.setText(orders.get(position).getOrderFrom());
                imageLoader.DisplayImage(Constants.ImagesUrl+orders.get(position).getItemImage(), viewholderOrder.ivItemImage);
            }else
            {
                ViewholderOrderPick viewholderOrder = (ViewholderOrderPick) holder;
                viewholderOrder.txtMenuIDcart.setText(orders.get(position).getMenu_ID()+"");
                viewholderOrder.txtMenuName.setText(orders.get(position).getMenu_name());
                viewholderOrder.txtQuantity.setText(orders.get(position).getQuantity()+"");
                viewholderOrder.txtPrice.setText(orders.get(position).getSub_total_price()+" ₹".trim());
                viewholderOrder.txtDealType.setText(orders.get(position).getOrderFrom());
                imageLoader.DisplayImage(Constants.ImagesUrl+orders.get(position).getItemImage(), viewholderOrder.ivItemImage);
            }



        }

        @Override
        public int getItemCount() {
            return orders.size();
        }
    }

    class ViewholderOrderDel extends RecyclerView.ViewHolder {
        TextView txtMenuName, txtQuantity, txtPrice, txtMenuIDcart,txtDealType,tv_delete;
        Button btninc, btndec;
        ImageView ivItemImage,iv_delete;



        public ViewholderOrderDel(final View convertView) {
            super(convertView);
            txtMenuName = (TextView) convertView.findViewById(R.id.txtMenuName);
            txtQuantity = (TextView) convertView.findViewById(R.id.txtQuantity);
            txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
            txtMenuIDcart = (TextView) convertView.findViewById(R.id.txtMenuCartID);
            btninc = (Button) convertView.findViewById(R.id.btninccart);
            btndec = (Button) convertView.findViewById(R.id.btndeccart);
            txtDealType=(TextView) convertView.findViewById(R.id.dealType);
            ivItemImage=(ImageView) convertView.findViewById(R.id.clientImgView);
            iv_delete=(ImageView) convertView.findViewById(R.id.iv_delete);
            iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onDeliveryItemDelete(lOrdersDel.get(getAdapterPosition()).getMenu_ID(),lOrdersDel.get(getAdapterPosition()).getOrderFrom(),getAdapterPosition());

                }
            });
            btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //System.out.println("hello in inc");
                    int qty = Integer.parseInt(txtQuantity.getText().toString());
                    double basePrice=Double.parseDouble(txtPrice.getText().toString().replace("₹","").trim())/qty;
                    if(qty>=1){
                        qty++;
                        double changedPrice=basePrice*qty;
                        txtQuantity.setText(String.valueOf(qty));
                        txtPrice.setText(String.valueOf(changedPrice));
                          db.updateData(lOrdersDel.get(getAdapterPosition()).getMenu_ID(), qty, (basePrice*qty),lOrdersDel.get(getAdapterPosition()).getOrderFrom()
                                  ,"Home Chef","DELIVERY");



                    }
                    else{



                    }
                    txtTotal.setText(getDataFromDatabase(ActivityCartNew.this)+" ₹");
                }
            });
            btndec.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty = Integer.parseInt(txtQuantity.getText().toString());
                    double basePrice=Double.parseDouble(txtPrice.getText().toString())/qty;
                    if(qty<=1){
                    }
                    else{
                        qty--;
                        double changedPrice=basePrice*qty;
                        txtQuantity.setText(String.valueOf(qty));
                        txtPrice.setText(String.valueOf(changedPrice));
                        db.updateData(lOrdersDel.get(getAdapterPosition()).getMenu_ID(), qty, (basePrice*qty),lOrdersDel.get(getAdapterPosition()).getOrderFrom(),"Home Chef","PICKUP");

                    }
                    txtTotal.setText(getDataFromDatabase(ActivityCartNew.this)+" ₹");
                }
            });
        }
    }

    class ViewholderOrderPick extends RecyclerView.ViewHolder {
        TextView txtMenuName, txtQuantity, txtPrice, txtMenuIDcart,txtDealType,tv_delete;
        Button btninc, btndec;
        ImageView ivItemImage;


        public ViewholderOrderPick(final View convertView) {
            super(convertView);
            txtMenuName = (TextView) convertView.findViewById(R.id.txtMenuName);
            txtQuantity = (TextView) convertView.findViewById(R.id.txtQuantity);
            txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
            txtMenuIDcart = (TextView) convertView.findViewById(R.id.txtMenuCartID);
            btninc = (Button) convertView.findViewById(R.id.btninccart);
            btndec = (Button) convertView.findViewById(R.id.btndeccart);
            txtDealType=(TextView) convertView.findViewById(R.id.dealType);
            ivItemImage=(ImageView) convertView.findViewById(R.id.clientImgView);
            tv_delete=(TextView) convertView.findViewById(R.id.tv_delete);
            tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onPickUpItemDelete(lOrdersPickup.get(getAdapterPosition()).getMenu_ID(),lOrdersPickup.get(getAdapterPosition()).getOrderFrom(),getAdapterPosition());

                }
            });

            btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //System.out.println("hello in inc");
                    int qty = Integer.parseInt(txtQuantity.getText().toString());
                    double basePrice=Double.parseDouble(txtPrice.getText().toString().replace("₹","").trim())/qty;
                    if(qty>=1){
                        qty++;
                        double changedPrice=basePrice*qty;
                        txtQuantity.setText(String.valueOf(qty));
                        txtPrice.setText(String.valueOf(changedPrice));

                        db.updateData(lOrdersPickup.get(getAdapterPosition()).getMenu_ID(), qty, (basePrice*qty),lOrdersPickup.get(getAdapterPosition()).getOrderFrom(),"Home Chef","PICKUP");

                    }
                    else{


                    }
                    txtTotal.setText(getDataFromDatabase(ActivityCartNew.this)+" ₹");
                }
            });
            btndec.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty = Integer.parseInt(txtQuantity.getText().toString());
                    double basePrice=Double.parseDouble(txtPrice.getText().toString())/qty;
                    if(qty<=1){
                    }
                    else{
                        qty--;
                        double changedPrice=basePrice*qty;
                        txtQuantity.setText(String.valueOf(qty));
                        txtPrice.setText(String.valueOf(changedPrice));
                        db.updateData(lOrdersPickup.get(getAdapterPosition()).getMenu_ID(), qty, (basePrice*qty),lOrdersPickup.get(getAdapterPosition()).getOrderFrom(),"Home Chef","PICKUP");
                        //db.updateData(Long.parseLong(holder.txtMenuIDcart.getText().toString()), qty, changedPrice);
                    }
                    txtTotal.setText(getDataFromDatabase(ActivityCartNew.this)+" ₹");
                }
            });
        }
    }


    // asynctask class to handle parsing json in background
    public class getDataTask extends AsyncTask<Void, Void, Void> {

        // show progressbar first
        getDataTask() {
            if (!prgLoading.isShown()) {
                prgLoading.setVisibility(View.VISIBLE);
                //lytOrder.setVisibility(View.INVISIBLE);
                txtAlert.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // get data from database
            getDataFromDatabase();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            // show data
            txtTotal.setText(Total_price + " ₹");
            txtTotalLabel.setText(getString(R.string.total_order) + " (Tax " + Tax + "%)");
            prgLoading.setVisibility(View.INVISIBLE);
            // if data available show data on list
            // otherwise, show alert text
            if (lOrdersPickup.size() > 0 || lOrdersDel.size()>0) {
                //lytOrder.setVisibility(View.VISIBLE);
                //listOrder.setAdapter(mola);
                oaDel=new OrdersAdapter(lOrdersDel,true);
                rvDelivery.setAdapter(oaDel);
                if(lOrdersDel.size()==0)
                    tvNodelData.setVisibility(View.VISIBLE);

                odpickup=new OrdersAdapter(lOrdersPickup,false);
                rvPickup.setAdapter(odpickup);
                if(lOrdersPickup.size()==0)
                    tvPickNodata.setVisibility(View.VISIBLE);

            } else {
                txtAlert.setVisibility(View.VISIBLE);
            }

        }
    }
    TextView tvDeliveryClear,tvPickClear;

    OrdersAdapter odpickup,oaDel;
    public void onPickUpDataClear(View view){
        lOrdersPickup.clear();
        odpickup.notifyDataSetChanged();
        db.deleteDataByQuery("tbl_order","delivery_type='PICKUP'");
        txtTotal.setText(String.valueOf(getDataFromDatabase(ActivityCartNew.this)));
    }
    public void onPickUpItemDelete(int MenuID,String From,int pos){

        db.deleteDataByQuery("tbl_order","delivery_type='PICKUP' and order_from='"+From+"' and id="+MenuID);
        lOrdersPickup.remove(pos);
        odpickup.notifyDataSetChanged();
        txtTotal.setText(String.valueOf(getDataFromDatabase(ActivityCartNew.this)));
    }
    public void onDeliveryDataClear(View view){
        lOrdersDel.clear();
        oaDel.notifyDataSetChanged();
        db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY'");
        txtTotal.setText(String.valueOf(getDataFromDatabase(ActivityCartNew.this)));
    }
    public void onDeliveryItemDelete(int MenuID,String From,int pos){

        db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY' and order_from='"+From+"' and id="+MenuID);
        lOrdersDel.remove(pos);
        oaDel.notifyDataSetChanged();
        txtTotal.setText(String.valueOf(getDataFromDatabase(ActivityCartNew.this)));
    }

*/
}
