package com.android.chefmonster.Cart;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.Checkout.Checkout;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.UI.MainActivity;

import java.util.ArrayList;

import tarek360.animated.icons.AnimatedIconView;
import tarek360.animated.icons.IconFactory;

public class CartTabbedActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    Button btnCheckout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_tabbed);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle=(TextView)findViewById(R.id.txtTitle);
        txtTitle.setText("Cart");

        btnCheckout=(Button)findViewById(R.id.btnCheckout);
        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iReservation = new Intent(CartTabbedActivity.this, Checkout.class);
                String transitionName="carttocheckout";
                //btnCheckout
                View viewStart = findViewById(R.id.btnCheckout);
                ActivityOptionsCompat options =

                        ActivityOptionsCompat.makeSceneTransitionAnimation(CartTabbedActivity.this,
                                viewStart,   // Starting view
                                transitionName    // The String
                        );
                ActivityCompat.startActivity(CartTabbedActivity.this,iReservation, options.toBundle());
            }
        });

        ImageView iv_menu_nav=(ImageView) findViewById(R.id.iv_menu_nav);
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(CartTabbedActivity.this,HomePage.class));
                finish();
            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Delivery Items"));
        tabLayout.addTab(tabLayout.newTab().setText("Pickup Items"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        LinearLayout tabOne = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
     /*   tabOne.setText("Delivery Items");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.delivery,0, 0, 0);*/
        tabLayout.getTabAt(0).setCustomView(tabOne);
        LinearLayout tabTwo = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.pickup_cart_custom_tab, null);
        tabLayout.getTabAt(1).setCustomView(tabTwo);



        viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        getDataFromDatabase();
    }


    ArrayList<ArrayList<Object>> data;

    public void getDataFromDatabase() {

        DBHelper_New db=new DBHelper_New(CartTabbedActivity.this);

        data = db.getAllData();

        String itemFound="NA" ;
        if(data.size() > 0)
        {
            // store data to arraylist variables
            for (int i = 0; i < data.size(); i++) {
                ArrayList<Object> row = data.get(i);
                    //
                if(row.get(4).toString().equalsIgnoreCase("DELIVERY")){
                    itemFound="DELIVERY";
                }
                else {

                }
            }


        }

        if(data.size() > 0 && itemFound.equalsIgnoreCase("NA"))
        {
            selectPage(1);
        }
    }


    void selectPage(int pageIndex){
        tabLayout.setScrollPosition(pageIndex,0f,true);
        viewPager.setCurrentItem(pageIndex);
    }
}
