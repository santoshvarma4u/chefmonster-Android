package com.android.chefmonster.Cart;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/*import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;*/

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.chefmonster.Checkout.ActivityCheckout;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.R;

public class ActivityCart extends Activity {
	
	// declare view objects
//	ImageButton imgNavBack;
	ListView listOrder;
	ProgressBar prgLoading;
	TextView txtTotalLabel, txtTotal, txtAlert;
	Button btnClear, Checkout;
	RelativeLayout lytOrder;
	
	// declate dbhelper and adapter objects
	DBHelper_New dbhelper;
	AdapterCart mola;
	
	
	// declare static variables to store tax and currency data
	static double Tax;
	static String Currency;

	// declare arraylist variable to store data
	ArrayList<ArrayList<Object>> data;
	static ArrayList<Integer> Menu_ID = new ArrayList<Integer>();
	static ArrayList<String> Menu_name = new ArrayList<String>();
	static ArrayList<Integer> Quantity = new ArrayList<Integer>();
	static ArrayList<Double> Sub_total_price = new ArrayList<Double>();
	
	double Total_price;
	final int CLEAR_ALL_ORDER = 0;
	final int CLEAR_ONE_ORDER = 1;
	int FLAG;
	int ID;
	String TaxCurrencyAPI;
	int IOConnect = 0;

	Geocoder geocoder;
	List<Address> addresses;
	int PLACE_PICKER_REQUEST = 4879;

	// create price format
	DecimalFormat formatData = new DecimalFormat("#.##");

	//SmartUser currentUser=null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mycart);


        Checkout = (Button) findViewById(R.id.Checkout);
        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        listOrder = (ListView) findViewById(R.id.listOrder);
        txtTotalLabel = (TextView) findViewById(R.id.txtTotalLabel);
        txtTotal = (TextView) findViewById(R.id.txtTotal);
        txtAlert = (TextView) findViewById(R.id.txtAlert);
        btnClear = (Button) findViewById(R.id.btnClear);
        lytOrder = (RelativeLayout) findViewById(R.id.lytOrder);
        

        mola = new AdapterCart(this,txtTotal);
        dbhelper = new DBHelper_New(this);


		btnClear.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// show confirmation dialog
				showClearDialog(CLEAR_ALL_ORDER, 1111);
			}
		});
		
        // event listener to handle list when clicked
		listOrder.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// show confirmation dialog
				System.out.println("in click");
				showClearDialog(CLEAR_ONE_ORDER, Menu_ID.get(position));
			}
		});

        
        Checkout.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {

				dbhelper.close();
				Intent iReservation = new Intent(ActivityCart.this, ActivityCheckout.class);
				startActivity(iReservation);


			}
		});
		new getDataTask().execute();
    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cartmenu, menu);
		//setNavigationList(menu);
		return true;
	}

	private boolean didUserLocationSaved() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		boolean mUserLocationSaved  = sharedPreferences.getBoolean("Location", false);
		return mUserLocationSaved;
	}



	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
			
		case android.R.id.home:
            // app icon in action bar clicked; go home
        	this.finish();
        	//overridePendingTransition(R.anim.open_main, R.anim.close_next);
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
    
    // method to create dialog
    void showClearDialog(int flag, int id){
    	FLAG = flag;
    	ID = id;
		AlertDialog.Builder builder = 	new AlertDialog.Builder(this);
		builder.setTitle(R.string.confirm);
		switch(FLAG){
		case 0:
			builder.setMessage(getString(R.string.clear_all_order));
			break;
		case 1:
			builder.setMessage(getString(R.string.clear_one_order));
			break;
		}
		builder.setCancelable(false);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				switch(FLAG){
				case 0:
					// clear all menu in order table
					//dbhelper.openDataBase();
					dbhelper.deleteAllData();
	    			listOrder.invalidateViews();
	    			clearData();
					new getDataTask().execute();
					break;
				case 1:
					// clear selected menu in order table
				//	dbhelper.openDataBase();
					dbhelper.deleteData(ID);
	    			listOrder.invalidateViews();
	    			clearData();
					new getDataTask().execute();
					break;
				}
				
			}
		});
		
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// close dialog
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
    }
    


	// clear arraylist variables before used
    void clearData(){
    	Menu_ID.clear();
    	Menu_name.clear();
    	Quantity.clear();
    	Sub_total_price.clear();
    }
    
    // asynctask class to handle parsing json in background
    public class getDataTask extends AsyncTask<Void, Void, Void>{
    	
    	// show progressbar first
    	getDataTask(){
    		if(!prgLoading.isShown()){
    			prgLoading.setVisibility(View.VISIBLE);
    			lytOrder.setVisibility(View.INVISIBLE);
    			txtAlert.setVisibility(View.INVISIBLE);
    		}
    	}
    	
    	@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
    		// get data from database
    		getDataFromDatabase();
			return null;
		}
    	
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			// show data
			txtTotal.setText( Total_price+" "+Currency);
			txtTotalLabel.setText(getString(R.string.total_order)+" (Tax "+Tax+"%)");
			prgLoading.setVisibility(View.INVISIBLE);
			// if data available show data on list
			// otherwise, show alert text
			if(Menu_ID.size() > 0){
				lytOrder.setVisibility(View.VISIBLE);
				listOrder.setAdapter(mola);
			}else{
				txtAlert.setVisibility(View.VISIBLE);
			}
			
		}
    }
    
    // method to get data from server
    public void getDataFromDatabase(){
    	
    	Total_price = 0;
    	clearData();
    	data = dbhelper.getAllData();
    	
    	// store data to arraylist variables
    	for(int i=0;i<data.size();i++){
    		ArrayList<Object> row = data.get(i);
    		
    		Menu_ID.add(Integer.parseInt(row.get(0).toString()));
    		Menu_name.add(row.get(1).toString());
    		Quantity.add(Integer.parseInt(row.get(2).toString()));
    		Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
    		Total_price += Sub_total_price.get(i);
    	}
    	
    	// count total order
    	Total_price -= (Total_price * (Tax/100));
    	Total_price = Double.parseDouble(formatData.format(Total_price));
    }
    
    // when back button pressed close database and back to previous page
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	super.onBackPressed();
    	//dbhelper.close();
    	finish();
    	//overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
    
	 

	SharedPreferences sharedPreferences;
	private boolean mUserLoggedIn = false;
	private static  final String userLogin="userLogin";
	private static  final String s_username="username";
	private static  final String s_Email="email";
	private static  final String s_Phone="phone";

	private boolean didUserLoggedin()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mUserLoggedIn=sharedPreferences.getBoolean(userLogin,false);
		return mUserLoggedIn;
	}

    
}
