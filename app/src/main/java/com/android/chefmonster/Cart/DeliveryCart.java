package com.android.chefmonster.Cart;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Checkout.ActivityCheckout;
import com.android.chefmonster.Checkout.Checkout;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.LoadingDrawble.loadingdrawable.LoadingView;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.google.android.gms.wallet.Cart;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class DeliveryCart extends Fragment {


    private static final String userLogin = "userLogin";
    private static final String s_username = "username";
    private static final String s_Email = "email";
    private static final String s_Phone = "phone";
    // declare static variables to store tax and currency data
    static double Tax;
    //static String Currency;
    final int CLEAR_ALL_ORDER = 0;
    final int CLEAR_ONE_ORDER = 1;
    List<Order> lOrdersDel, lOrdersPickup;
    //
    DBHelper_New db;
    RecyclerView rvDelivery, rvPickup;
    LoadingView loading_view;
    TextView txtTotalLabel, txtTotal;
    LinearLayout txtAlert;
    Button btnClear, btnCheckout;

     // declate dbhelper and adapter objects
    //DBHelper_New dbhelper;
    double Total_price;
    int FLAG;
    int ID;
    String TaxCurrencyAPI;
    int IOConnect = 0;

    //SmartUser currentUser=null;
    Geocoder geocoder;
    List<Address> addresses;
    int PLACE_PICKER_REQUEST = 4879;
    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");
    ArrayList<ArrayList<Object>> data;
    SharedPreferences sharedPreferences;
    private boolean mUserLoggedIn = false;
    //
    LinearLayout tvPickNodata,tvNodelData;

    ImageLoader imageLoader;

    public DeliveryCart() {
        // Required empty public constructor
    }
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_delivery_cart, container, false);


        tvNodelData=(LinearLayout)view.findViewById(R.id.tvNodelData);
        tvPickNodata=(LinearLayout)view.findViewById(R.id.tvPickNodata);

        btnCheckout = (Button) view.findViewById(R.id.Checkout);
        loading_view = (LoadingView)view.findViewById(R.id.loading_view);
        txtTotalLabel = (TextView)view.findViewById(R.id.txtTotalLabel);
        txtTotal = (TextView)view.findViewById(R.id.txtTotal);
        txtAlert = (LinearLayout) view.findViewById(R.id.txtAlert);
        btnClear = (Button)view.findViewById(R.id.btnClear);

        tvDeliveryClear=(TextView)view.findViewById(R.id.tvDeliveryClear);

        tvDeliveryClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeliveryDataClear(v);
            }
        });
        imageLoader = new ImageLoader(getActivity());


        //
        db = new DBHelper_New(getActivity());
        //dbhelper= new DBHelper_New(this);
        lOrdersDel = new ArrayList<>();
        lOrdersPickup = new ArrayList<>();
        //
        //
        rvDelivery = (RecyclerView) view.findViewById(R.id.rvDelivery);
        RecyclerView.LayoutManager mLayoutManagerDel = new LinearLayoutManager(getActivity());
        rvDelivery.setLayoutManager(mLayoutManagerDel);
        rvDelivery.setItemAnimator(new DefaultItemAnimator());

      /*  rvPickup = (RecyclerView) findViewById(R.id.rvPickup);
        RecyclerView.LayoutManager mLayoutManagerPickk = new LinearLayoutManager(this);
        rvPickup.setLayoutManager(mLayoutManagerPickk);
        rvPickup.setItemAnimator(new DefaultItemAnimator());*/

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                db.close();
                if(lOrdersDel.size()>0)
                {
                    Intent iReservation = new Intent(getActivity(), Checkout.class);
                    String transitionName="carttocheckout";
                    //btnCheckout
                    View viewStart = view.findViewById(R.id.Checkout);
                    ActivityOptionsCompat options =

                            ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                                    viewStart,   // Starting view
                                    transitionName    // The String
                            );
                    ActivityCompat.startActivity(getActivity(),iReservation, options.toBundle());
                }
            }
        });
        new getDataTask().execute();

        return view;
    }

    // clear arraylist variables before used
    void clearData() {
        lOrdersDel.clear();
    }


    public double getDataFromDatabase(Context ctx){

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        data = db.getAllData();

        // store data to arraylist variables
        for(int i=0;i<data.size();i++){
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price +=Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));

        return Total_price;
    }


    // method to get data from server
    public void getDataFromDatabase() {

        Total_price = 0;
        clearData();
        data = db.getAllData();

        if(data.size() > 0)
        {
            // store data to arraylist variables
            for (int i = 0; i < data.size(); i++) {
                ArrayList<Object> row = data.get(i);
                //
                Order order = new Order();
                order.setMenu_ID(Integer.parseInt(row.get(0).toString()));
                order.setMenu_name(row.get(1).toString());
                order.setQuantity(Integer.parseInt(row.get(2).toString()));
                order.setSub_total_price(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
                order.setOrderType(row.get(4).toString());
                order.setOrderFrom(row.get(5).toString());
                order.setItemImage(row.get(6).toString());
                order.setMenuType(row.get(7).toString());
                order.setMenuStockQty(row.get(8).toString());
                Total_price += order.getSub_total_price();
                //
                if(row.get(4).toString().equalsIgnoreCase("DELIVERY")){
                    lOrdersDel.add(order);
                }
            }

            // count total order
            Total_price -= (Total_price * (Tax / 100));
            Total_price = Double.parseDouble(formatData.format(Total_price));
        }
        else
        {

            btnCheckout.setVisibility(View.GONE);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity()," No Items Found in cart. ",Toast.LENGTH_LONG).show();
                }
            });/*

            startActivity(new Intent(ActivityCartNew.this, MainActivity.class));
            finish();*/
        }


    }


    public class OrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        List<Order> orders;
        boolean isDelvery;

        public OrdersAdapter(List<Order> orders,boolean isDelvery) {
            this.orders = orders;
            this.isDelvery=isDelvery;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater li = LayoutInflater.from(parent.getContext());
            View view = li.inflate(R.layout.order_list_item, parent, false);
            RecyclerView.ViewHolder vHoder;
            if(isDelvery)
                vHoder = new ViewholderOrderDel(view);
            else
                vHoder = new ViewholderOrderPick(view);
            return vHoder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


            if(holder instanceof ViewholderOrderDel){
                ViewholderOrderDel viewholderOrder = (ViewholderOrderDel) holder;
                viewholderOrder.txtMenuIDcart.setText(orders.get(position).getMenu_ID()+"");
                viewholderOrder.txtMenuName.setText(orders.get(position).getMenu_name());
                viewholderOrder.txtQuantity.setText(orders.get(position).getQuantity()+"");
                viewholderOrder.txtPrice.setText(orders.get(position).getSub_total_price()+" ₹".trim());
                viewholderOrder.txtDealType.setText(orders.get(position).getOrderFrom());
                imageLoader.DisplayImage(Constants.ImagesUrl+orders.get(position).getItemImage(), viewholderOrder.ivItemImage);
            }else
            {
                ViewholderOrderPick viewholderOrder = (ViewholderOrderPick) holder;
                viewholderOrder.txtMenuIDcart.setText(orders.get(position).getMenu_ID()+"");
                viewholderOrder.txtMenuName.setText(orders.get(position).getMenu_name());
                viewholderOrder.txtQuantity.setText(orders.get(position).getQuantity()+"");
                viewholderOrder.txtPrice.setText(orders.get(position).getSub_total_price()+" ₹".trim());
                viewholderOrder.txtDealType.setText(orders.get(position).getOrderFrom());
                imageLoader.DisplayImage(Constants.ImagesUrl+orders.get(position).getItemImage(), viewholderOrder.ivItemImage);
            }



        }

        @Override
        public int getItemCount() {
            return orders.size();
        }
    }

    class ViewholderOrderDel extends RecyclerView.ViewHolder {
        TextView txtMenuName, txtQuantity, txtPrice, txtMenuIDcart,txtDealType,tv_delete;
        Button btninc, btndec;
        ImageView ivItemImage,iv_delete;



        public ViewholderOrderDel(final View convertView) {
            super(convertView);
            txtMenuName = (TextView) convertView.findViewById(R.id.txtMenuName);
            txtQuantity = (TextView) convertView.findViewById(R.id.txtQuantity);
            txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
            txtMenuIDcart = (TextView) convertView.findViewById(R.id.txtMenuCartID);
            btninc = (Button) convertView.findViewById(R.id.btninccart);
            btndec = (Button) convertView.findViewById(R.id.btndeccart);
            txtDealType=(TextView) convertView.findViewById(R.id.dealType);
            ivItemImage=(ImageView) convertView.findViewById(R.id.clientImgView);
            iv_delete=(ImageView) convertView.findViewById(R.id.iv_delete);
            iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onDeliveryItemDelete(lOrdersDel.get(getAdapterPosition()).getMenu_ID(),lOrdersDel.get(getAdapterPosition()).getOrderFrom(),getAdapterPosition());

                }
            });
            btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //System.out.println("hello in inc");
                    int qty = Integer.parseInt(txtQuantity.getText().toString());
                    if(qty<Integer.parseInt(lOrdersDel.get(getAdapterPosition()).getMenuStockQty().toString()))
                    {
                        double basePrice=Double.parseDouble(txtPrice.getText().toString().replace("₹","").trim())/qty;
                        if(qty>=1){
                            qty++;
                            double changedPrice=ApplicationLoader.round((basePrice*qty),2);
                            txtQuantity.setText(String.valueOf(qty));
                            txtPrice.setText(String.valueOf(changedPrice));
                            if(lOrdersDel.get(getAdapterPosition()).getMenuType().toLowerCase().equals("combo"))
                            {
                                db.updateData(lOrdersDel.get(getAdapterPosition()).getMenu_ID(), qty, (ApplicationLoader.round((basePrice*qty),2)),lOrdersDel.get(getAdapterPosition()).getOrderFrom()
                                        ,"Home Chef","DELIVERY","COMBO",lOrdersDel.get(getAdapterPosition()).getMenuStockQty().toString());
                            }
                            else
                            {
                                db.updateData(lOrdersDel.get(getAdapterPosition()).getMenu_ID(), qty, (ApplicationLoader.round((basePrice*qty),2)),lOrdersDel.get(getAdapterPosition()).getOrderFrom()
                                        ,"Home Chef","DELIVERY","Single",lOrdersDel.get(getAdapterPosition()).getMenuStockQty().toString());
                            }


                        }
                        else{



                        }
                        txtTotal.setText(getDataFromDatabase(getActivity())+" ₹");
                    }
                    else
                    {
                        Toast.makeText(getActivity(),"No More Quantity Available",Toast.LENGTH_SHORT).show();
                    }

                }
            });
            btndec.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int qty = Integer.parseInt(txtQuantity.getText().toString());
                    double basePrice=Double.parseDouble(txtPrice.getText().toString().replace("₹","").trim())/qty;
                    if(qty<=1){
                        final int[] finalQty = {qty};
                        new MaterialDialog.Builder(getActivity())
                                .autoDismiss(false)
                                .title("Are you sure want delete this item?")
                                .positiveText("Confirm")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        onDeliveryItemDelete(lOrdersDel.get(getAdapterPosition()).getMenu_ID(),lOrdersDel.get(getAdapterPosition()).getOrderFrom(),getAdapterPosition());
                                        dialog.dismiss();
                                    }
                                })
                                .negativeText("Cancel")
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                        dialog.dismiss();
                                    }
                                })
                                .show();

                    }
                    else{
                        qty--;
                        double changedPrice=ApplicationLoader.round((basePrice*qty),2);
                        txtQuantity.setText(String.valueOf(qty));
                        txtPrice.setText(String.valueOf(changedPrice));
                        if(lOrdersDel.get(getAdapterPosition()).getMenuType().toLowerCase().equals("combo"))
                        {
                            db.updateData(lOrdersDel.get(getAdapterPosition()).getMenu_ID(), qty, (ApplicationLoader.round((basePrice*qty),2)),lOrdersDel.get(getAdapterPosition()).getOrderFrom(),"Home Chef","DELIVERY","COMBO",lOrdersDel.get(getAdapterPosition()).getMenuStockQty().toString());

                        }
                        else
                        {
                            db.updateData(lOrdersDel.get(getAdapterPosition()).getMenu_ID(), qty, (ApplicationLoader.round((basePrice*qty),2)),lOrdersDel.get(getAdapterPosition()).getOrderFrom(),"Home Chef","DELIVERY","Single",lOrdersDel.get(getAdapterPosition()).getMenuStockQty().toString());
                        }
                    }
                    txtTotal.setText(getDataFromDatabase(getActivity())+" ₹");
                }
            });
        }
    }

    class ViewholderOrderPick extends RecyclerView.ViewHolder {
        TextView txtMenuName, txtQuantity, txtPrice, txtMenuIDcart,txtDealType,tv_delete;
        Button btninc, btndec;
        ImageView ivItemImage;


        public ViewholderOrderPick(final View convertView) {
            super(convertView);
            txtMenuName = (TextView) convertView.findViewById(R.id.txtMenuName);
            txtQuantity = (TextView) convertView.findViewById(R.id.txtQuantity);
            txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
            txtMenuIDcart = (TextView) convertView.findViewById(R.id.txtMenuCartID);
            btninc = (Button) convertView.findViewById(R.id.btninccart);
            btndec = (Button) convertView.findViewById(R.id.btndeccart);
            txtDealType=(TextView) convertView.findViewById(R.id.dealType);
            ivItemImage=(ImageView) convertView.findViewById(R.id.clientImgView);
            tv_delete=(TextView) convertView.findViewById(R.id.tv_delete);
            tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onPickUpItemDelete(lOrdersPickup.get(getAdapterPosition()).getMenu_ID(),lOrdersPickup.get(getAdapterPosition()).getOrderFrom(),getAdapterPosition());

                }
            });

            btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //System.out.println("hello in inc");
                    int qty = Integer.parseInt(txtQuantity.getText().toString());
                    double basePrice=Double.parseDouble(txtPrice.getText().toString().replace("₹","").trim())/qty;
                    if(qty>=1){
                        qty++;
                        double changedPrice=ApplicationLoader.round((basePrice*qty),2);
                        txtQuantity.setText(String.valueOf(qty));
                        txtPrice.setText(String.valueOf(changedPrice));
                        if(lOrdersPickup.get(getAdapterPosition()).getMenuType().toLowerCase().equals("combo"))
                        {
                            db.updateData(lOrdersPickup.get(getAdapterPosition()).getMenu_ID(), qty, ApplicationLoader.round((basePrice*qty),2),lOrdersPickup.get(getAdapterPosition()).getOrderFrom(),"Home Chef","PICKUP","COMBO",lOrdersDel.get(getAdapterPosition()).getMenuStockQty().toString());

                        }
                        else
                        {
                            db.updateData(lOrdersPickup.get(getAdapterPosition()).getMenu_ID(), qty, ApplicationLoader.round((basePrice*qty),2),lOrdersPickup.get(getAdapterPosition()).getOrderFrom(),"Home Chef","PICKUP","Single",lOrdersDel.get(getAdapterPosition()).getMenuStockQty().toString());

                        }

                    }
                    else{


                    }
                    txtTotal.setText(getDataFromDatabase(getActivity())+" ₹");
                }
            });
            btndec.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty = Integer.parseInt(txtQuantity.getText().toString());
                    double basePrice=Double.parseDouble(txtPrice.getText().toString())/qty;
                    if(qty<=1){
                    }
                    else{
                        qty--;
                        double changedPrice=ApplicationLoader.round((basePrice*qty),2);
                        txtQuantity.setText(String.valueOf(qty));
                        txtPrice.setText(String.valueOf(changedPrice));
                        if(lOrdersPickup.get(getAdapterPosition()).getMenuType().toLowerCase().equals("combo")) {
                            db.updateData(lOrdersPickup.get(getAdapterPosition()).getMenu_ID(), qty, ApplicationLoader.round((basePrice*qty),2), lOrdersPickup.get(getAdapterPosition()).getOrderFrom(), "Home Chef", "PICKUP","COMBO",lOrdersDel.get(getAdapterPosition()).getMenuStockQty().toString());
                        }
                        else
                        {
                            db.updateData(lOrdersPickup.get(getAdapterPosition()).getMenu_ID(), qty, ApplicationLoader.round((basePrice*qty),2), lOrdersPickup.get(getAdapterPosition()).getOrderFrom(), "Home Chef", "PICKUP","Single",lOrdersDel.get(getAdapterPosition()).getMenuStockQty().toString());

                        }
                        //db.updateData(Long.parseLong(holder.txtMenuIDcart.getText().toString()), qty, changedPrice);
                    }
                    txtTotal.setText(getDataFromDatabase(getActivity())+" ₹");
                }
            });
        }
    }


    // asynctask class to handle parsing json in background
    public class getDataTask extends AsyncTask<Void, Void, Void> {

        // show progressbar first
        getDataTask() {
            if (!loading_view.isShown()) {
                loading_view.setVisibility(View.VISIBLE);
                //lytOrder.setVisibility(View.INVISIBLE);
                txtAlert.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // get data from database
            getDataFromDatabase();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            // show data
            txtTotal.setText(Total_price + " ₹");
            txtTotalLabel.setText(getString(R.string.total_order) + " (Tax " + Tax + "%)");
            loading_view.setVisibility(View.GONE);
            // if data available show data on list
            // otherwise, show alert text
            if (lOrdersPickup.size() > 0 || lOrdersDel.size()>0) {
                //lytOrder.setVisibility(View.VISIBLE);
                //listOrder.setAdapter(mola);
                oaDel=new OrdersAdapter(lOrdersDel,true);
                rvDelivery.setAdapter(oaDel);
                if(lOrdersDel.size()==0)
                    tvNodelData.setVisibility(View.VISIBLE);

                odpickup=new OrdersAdapter(lOrdersPickup,false);
                //rvPickup.setAdapter(odpickup);
                if(lOrdersPickup.size()==0)
                    tvPickNodata.setVisibility(View.VISIBLE);

            } else {
                txtAlert.setVisibility(View.VISIBLE);
            }

        }
    }
    TextView tvDeliveryClear,tvPickClear;

    OrdersAdapter odpickup,oaDel;
    public void onPickUpDataClear(View view){
        lOrdersPickup.clear();
        odpickup.notifyDataSetChanged();
        db.deleteDataByQuery("tbl_order","delivery_type='PICKUP'");
        txtTotal.setText(String.valueOf(getDataFromDatabase(getActivity())));
    }
    public void onPickUpItemDelete(int MenuID,String From,int pos){

        db.deleteDataByQuery("tbl_order","delivery_type='PICKUP' and order_from='"+From+"' and id="+MenuID);
        lOrdersPickup.remove(pos);
        odpickup.notifyDataSetChanged();
        txtTotal.setText(String.valueOf(getDataFromDatabase(getActivity())));
    }
    public void onDeliveryDataClear(View view){
        btnCheckout.setVisibility(View.GONE);
        lOrdersDel.clear();
        oaDel.notifyDataSetChanged();
        db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY'");
        txtTotal.setText(String.valueOf(getDataFromDatabase(getActivity())));
        txtAlert.setVisibility(View.VISIBLE);
    }
    public void onDeliveryItemDelete(int MenuID,String From,int pos){

        db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY' and order_from='"+From+"' and id="+MenuID);
        lOrdersDel.remove(pos);
        oaDel.notifyDataSetChanged();
        txtTotal.setText(String.valueOf(getDataFromDatabase(getActivity())));
        if(lOrdersDel.size()==0)
            txtAlert.setVisibility(View.VISIBLE);

    }
}
