package com.android.chefmonster.Cart;

import java.util.ArrayList;

/**
 * Created by ChakravartyG on 9/28/2016.
 */

public class Order {
    int Menu_ID, Quantity;
    String Menu_name;
    double Sub_total_price;
    String orderType;
    String MenuType;
    String MenuStockQty;

    public String getMenuStockQty() {
        return MenuStockQty;
    }

    public void setMenuStockQty(String menuStockQty) {
        MenuStockQty = menuStockQty;
    }

    public String getMenuType() {
        return MenuType;
    }

    public void setMenuType(String menuType) {
        MenuType = menuType;
    }

    public String getItemImage() {
        return ItemImage;
    }

    public void setItemImage(String itemImage) {
        ItemImage = itemImage;
    }

    String ItemImage;

    public String getOrderFrom() {
        return OrderFrom;
    }

    public void setOrderFrom(String orderFrom) {
        OrderFrom = orderFrom;
    }

    String OrderFrom;
    //test

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public int getMenu_ID() {
        return Menu_ID;
    }

    public void setMenu_ID(int menu_ID) {
        Menu_ID = menu_ID;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public String getMenu_name() {
        return Menu_name;
    }

    public void setMenu_name(String menu_name) {
        Menu_name = menu_name;
    }

    public double getSub_total_price() {
        return Sub_total_price;
    }

    public void setSub_total_price(double sub_total_price) {
        Sub_total_price = sub_total_price;
    }
}
