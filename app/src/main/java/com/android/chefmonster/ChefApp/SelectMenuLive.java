package com.android.chefmonster.ChefApp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.LiveVideo.RtcActivity;
import com.android.chefmonster.R;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SelectMenuLive extends AppCompatActivity {

    private RecyclerView rvMenus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_menu_live);
        initView();
    }

    private void initView() {
        rvMenus = (RecyclerView) findViewById(R.id.rvMenus);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectMenuLive.this,LinearLayoutManager.VERTICAL,false);
        rvMenus.setLayoutManager(mLayoutManager);
        rvMenus.setItemAnimator(new DefaultItemAnimator());
        cml=new ChefMenuList();
        chefMenus=new ArrayList<>();
    }


    List<ChefMenu> chefMenus;


    ChefMenuList cml;

    public class ChefMenuList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_menus_chef, parent, false);
            viewHolder = new ViewHolderRow(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderRow vh2 = (ViewHolderRow) holder;
            configureViewHolderRow(vh2, position);
        }

        @Override
        public int getItemCount() {
            return chefMenus.size();
        }

        ChefMenu listChefMenu;
        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {
            listChefMenu=chefMenus.get(position);
            holder.MenuId.setText(listChefMenu.getMenuID());
            holder.MenuName.setText(listChefMenu.getMenuName());
        }
    }
    class ViewHolderRow extends RecyclerView.ViewHolder{

        TextView MenuId,MenuName;

        public ViewHolderRow(View itemView) {
            super(itemView);
            MenuId=(TextView)itemView.findViewById(R.id.chef_menu_id);
            MenuName=(TextView)itemView.findViewById(R.id.chef_menu_name);
            MenuId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(SelectMenuLive.this, RtcActivity.class);
                    intent.putExtra("chefLive","chefLive");
                    intent.putExtra("menuid",chefMenus.get(getAdapterPosition()).getMenuID().toString());
                    intent.putExtra("menuname",chefMenus.get(getAdapterPosition()).getMenuName().toString());
                    startActivity(intent);
                }
            });
            MenuName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(SelectMenuLive.this, RtcActivity.class);
                    intent.putExtra("chefLive","chefLive");
                    intent.putExtra("menuid",chefMenus.get(getAdapterPosition()).getMenuID().toString());
                    intent.putExtra("menuname",chefMenus.get(getAdapterPosition()).getMenuName().toString());
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataFromServer("2");
      /*  if(!ApplicationLoader.getChefId().isEmpty())
        {
            getDataFromServer(ApplicationLoader.getChefId());
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Please login to post live food preparation",Toast.LENGTH_SHORT).show();
        }*/

    }

    public void getDataFromServer(String chefId)
    {
        String mUrl="http://128.199.173.98/api/getChefMenus.php?chefid="+chefId+"&accesskey=12345";
        System.out.println(mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {

                //closeDialog();
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                   runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });
    }
    public void parseJSONData(String strResponse) {
        try
        {
            System.out.println("******"+strResponse.toString());
            JSONObject json = new JSONObject(strResponse.toString());
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            //  cusineList.clear();
            for (int ii = 0; ii < data.length(); ii++) {
                ChefMenu clist = new ChefMenu();
                JSONObject object = data.getJSONObject(ii);
                JSONObject cmenu = object.getJSONObject("cmenus");
                clist.setMenuID(cmenu.getString("Menu_ID"));
                clist.setMenuName(cmenu.getString("Menu_Name"));
                chefMenus.add(clist);
            }
            rvMenus.setAdapter(cml);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
