package com.android.chefmonster.ChefApp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.UI.MainActivity;
import com.ivankocijan.magicviews.views.MagicTextView;

public class ChefSideMenu extends AppCompatActivity implements View.OnClickListener {

    protected ImageView ivCloseMenu;
    protected MagicTextView tvMenuAddress;
    protected MagicTextView tvAccounts;
    protected MagicTextView tvManageOrders;
    protected MagicTextView tvManageMenus;
    protected MagicTextView tvLiveCook;
    protected MagicTextView tvReferEarn;
    protected MagicTextView tvHelp;
    protected MagicTextView tvSignout;
    protected MagicTextView tvSwitchToCustomer;
    protected RelativeLayout activityChefSideMenu;
    protected MagicTextView tvHome;

    TelephonyManager tm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_chef_side_menu);
        tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        initView();

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(ChefSideMenu.this, ViewFragment.class);

        if (view.getId() == R.id.ivCloseMenu) {
            finish();

        } else if (view.getId() == R.id.tvMenuAddress) {

        } else if (view.getId() == R.id.tv_accounts) {

            if (ApplicationLoader.getChefId().isEmpty()) {
                intent.putExtra("url", "http://chefmonster.com:3000/users/login/" + tm.getDeviceId());

            } else {
                intent.putExtra("url", "http://chefmonster.com:3000/users/info/" + ApplicationLoader.getChefId());
            }

            startActivity(intent);
            finish();


        } else if (view.getId() == R.id.tv_manage_orders) {

            if (ApplicationLoader.getChefId().isEmpty()) {
                intent.putExtra("url", "http://chefmonster.com:3000/users/login/" + tm.getDeviceId());

            } else {
                intent.putExtra("url", "http://chefmonster.com:3000/orders/list/" + ApplicationLoader.getChefId());
            }
            startActivity(intent);
            finish();


        } else if (view.getId() == R.id.tv_manage_menus) {

            if (ApplicationLoader.getChefId().isEmpty()) {
                intent.putExtra("url", "http://chefmonster.com:3000/users/login/" + tm.getDeviceId());

            } else {
                intent.putExtra("url", "http://chefmonster.com:3000/menu/view-menus/" + ApplicationLoader.getChefId());
            }
            startActivity(intent);
            finish();

        } else if (view.getId() == R.id.tv_live_cook) {

            startActivity(new Intent(ChefSideMenu.this, SelectMenuLive.class));
            finish();

        } else if (view.getId() == R.id.tv_refer_earn) {

        } else if (view.getId() == R.id.tv_help) {

        } else if (view.getId() == R.id.tv_signout) {
            ApplicationLoader.removeFromSharedPreferences("chefid");
            startActivity(new Intent(ChefSideMenu.this, MainActivity.class));
            finish();

        } else if (view.getId() == R.id.tv_switch_to_customer) {
            startActivity(new Intent(ChefSideMenu.this, HomePage.class));
            finish();
        } else if (view.getId() == R.id.tv_home) {
            intent = new Intent(ChefSideMenu.this, ChefLauncher.class);
            startActivity(intent);
            finish();
        }
    }

    private void initView() {
        ivCloseMenu = (ImageView) findViewById(R.id.ivCloseMenu);
        ivCloseMenu.setOnClickListener(ChefSideMenu.this);
        tvMenuAddress = (MagicTextView) findViewById(R.id.tvMenuAddress);
        tvMenuAddress.setOnClickListener(ChefSideMenu.this);
        tvMenuAddress.setText(ApplicationLoader.getUserCity() + "," + ApplicationLoader.getUserLocation());
        tvAccounts = (MagicTextView) findViewById(R.id.tv_accounts);
        tvAccounts.setOnClickListener(ChefSideMenu.this);
        tvManageOrders = (MagicTextView) findViewById(R.id.tv_manage_orders);
        tvManageOrders.setOnClickListener(ChefSideMenu.this);
        tvManageMenus = (MagicTextView) findViewById(R.id.tv_manage_menus);
        tvManageMenus.setOnClickListener(ChefSideMenu.this);
        tvLiveCook = (MagicTextView) findViewById(R.id.tv_live_cook);
        tvLiveCook.setOnClickListener(ChefSideMenu.this);
        tvReferEarn = (MagicTextView) findViewById(R.id.tv_refer_earn);
        tvReferEarn.setOnClickListener(ChefSideMenu.this);
        tvHelp = (MagicTextView) findViewById(R.id.tv_help);
        tvHelp.setOnClickListener(ChefSideMenu.this);
        tvSignout = (MagicTextView) findViewById(R.id.tv_signout);
        tvSignout.setOnClickListener(ChefSideMenu.this);
        tvSwitchToCustomer = (MagicTextView) findViewById(R.id.tv_switch_to_customer);
        tvSwitchToCustomer.setOnClickListener(ChefSideMenu.this);
        activityChefSideMenu = (RelativeLayout) findViewById(R.id.activity_chef_side_menu);

        if (ApplicationLoader.getChefId().isEmpty()) {
            tvAccounts.setVisibility(View.GONE);
            tvManageOrders.setVisibility(View.GONE);
            tvLiveCook.setVisibility(View.GONE);
            tvReferEarn.setVisibility(View.GONE);
            tvSignout.setVisibility(View.GONE);
        }
        tvHome = (MagicTextView) findViewById(R.id.tv_home);
        tvHome.setOnClickListener(ChefSideMenu.this);
    }
}
