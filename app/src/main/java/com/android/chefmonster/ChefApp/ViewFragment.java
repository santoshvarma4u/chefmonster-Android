package com.android.chefmonster.ChefApp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.android.chefmonster.R;
import com.android.chefmonster.UI.MenuActivity;

/**
 * Created by tskva on 9/17/2016.
 */
public class ViewFragment extends AppCompatActivity implements View.OnClickListener {

    protected ImageView ivMenuNav;
    /* private static final String ARG_PARAM1 = "param1";
        private static final String ARG_PARAM2 = "param2";*/
    WebView chef_view;

    // TODO: Rename and change types of parameters
   /* private String mParam1;
    private String mParam2;
    private ViewFragment.OnFragmentInteractionListener mListener;


    public ViewFragment() {
        // Required empty public constructor
    }

    public static ViewFragment newInstance(String param1, String param2) {
        ViewFragment fragment = new ViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.fragment_chef);

        chef_view = (WebView) findViewById(R.id.wv_chef);
        chef_view.getSettings().setJavaScriptEnabled(true);
        WebSettings mWebSettings = chef_view.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowContentAccess(true);
        mWebSettings.setLoadWithOverviewMode(true);


        // Javascript inabled on webview
        mWebSettings.setJavaScriptEnabled(true);
        // Other webview options
        mWebSettings.setLoadWithOverviewMode(true);

        chef_view.setScrollbarFadingEnabled(false);
        mWebSettings.setBuiltInZoomControls(true);
        mWebSettings.setPluginState(WebSettings.PluginState.ON);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setRenderPriority(
                WebSettings.RenderPriority.HIGH);
        mWebSettings.setCacheMode(
                WebSettings.LOAD_NO_CACHE);
        mWebSettings.setAppCacheEnabled(true);
        chef_view.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings
                .setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setSavePassword(true);
        mWebSettings.setSaveFormData(true);
        mWebSettings.setEnableSmoothTransition(true);


        String strtext = getIntent().getExtras().getString("url");

        initView();
        startWebView(strtext);
       /* chef_view.getSettings().setJavaScriptEnabled(true);


        //Load url in webview
        chef_view.loadUrl(strtext);*/


    }


    private void startWebView(String url) {


        chef_view.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(ViewFragment.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onTooManyRedirects(WebView view, Message cancelMsg, Message continueMsg) {
                super.onTooManyRedirects(view, cancelMsg, continueMsg);
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });

        // Javascript inabled on webview
        chef_view.getSettings().setJavaScriptEnabled(true);

        //Load url in webview
        chef_view.loadUrl(url);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_menu_nav) {
            startActivity(new Intent(ViewFragment.this,ChefSideMenu.class));
        }
    }

    private void initView() {
        ivMenuNav = (ImageView) findViewById(R.id.iv_menu_nav);
        ivMenuNav.setOnClickListener(ViewFragment.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ViewFragment.this, ChefLauncher.class);
        startActivity(intent);
        finish();
    }
}
