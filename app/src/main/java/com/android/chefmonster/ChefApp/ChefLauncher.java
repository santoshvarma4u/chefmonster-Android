package com.android.chefmonster.ChefApp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.Login.UserProfile;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.UI.Menu.DeliveryListFragment;
import com.android.chefmonster.UI.MenuActivity;
import com.android.chefmonster.Utills.Constants;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import java.io.IOException;

public class ChefLauncher extends AppCompatActivity implements ChefFragment.OnFragmentInteractionListener {

    private Toolbar mToolbar;
    private Context mContext;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    TextView im_cust;
    TelephonyManager tm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chef_launcher);
        init(savedInstanceState);
    }
    private void init(@Nullable final Bundle savedInstanceState)
    {
        bindResources();
        setUpToolbar();
       // setUpDrawer();
     //   restoreState(savedInstanceState);
    }
    private void bindResources()
    {
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_activity_DrawerLayout);
       /* mNavigationView = (NavigationView) findViewById(R.id.activity_main_navigation_view);
        im_cust=(TextView) findViewById(R.id.im_chef);
        im_cust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(ChefLauncher.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });*/

        tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
 //       TelephonyManager tmp=new TelephonyManager();

        //checkthe user plug saving

        if(ApplicationLoader.getChefId().isEmpty())
        {

            tm.getDeviceId();
            OkHttpClient client = new OkHttpClient();
            FormBody.Builder formBody;

            formBody = new FormBody.Builder()
                    .add("deviceid", tm.getDeviceId())
                    .add("checkChef", "true");

            RequestBody formBodyA = formBody.build();
            final Request request = new Request.Builder()
                    .url(Constants.checkChef)
                    .post(formBodyA)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call request, IOException e) {

                }

                @Override
                public void onResponse(Call call,Response response) throws IOException {
                    if (response.isSuccessful()) {

                        result = Integer.parseInt(response.body().string().toString());
                     runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             Toast.makeText(getApplicationContext(),"Please Login to get notified",Toast.LENGTH_SHORT).show();
                             String urlChef="http://chefmonster.com:3000/users/login/"+tm.getDeviceId();
                             if(result!=0)
                             {
                                 ApplicationLoader.setChefId(String.valueOf(result));
                                 showFragmanet(1,new ChefFragment(),urlChef);
                             }
                             else
                             {
                                 Toast.makeText(getApplicationContext(),"Unable to fetch data , try again",Toast.LENGTH_SHORT).show();
                                 Intent intent = new Intent(ChefLauncher.this, ChefLauncher.class);
                                 startActivity(intent);
                                 finish();
                             }


                           /*  if(result>0)
                             {
                                 Toast.makeText(getApplicationContext(),"Found Previous Login",Toast.LENGTH_SHORT).show();
                                 ApplicationLoader.setChefId(String.valueOf(result));
                                 showFragmanet(1,new ChefFragment(),"http://chefmonster.com:3000/users/profile/"+ApplicationLoader.getChefId());
                             }
                             else if(result==0)
                             {
                                 Toast.makeText(getApplicationContext(),"Please Login to get notified",Toast.LENGTH_SHORT).show();
                                 String urlChef="http://chefmonster.com:3000/users/login/"+tm.getDeviceId();
                                 showFragmanet(1,new ChefFragment(),urlChef);
                             }*/
                         }
                     });

                    }
                }
            });
        }
        else
        {
            if(ApplicationLoader.getChefId().equalsIgnoreCase("0"))
            {
                ApplicationLoader.setChefId("");
                ApplicationLoader.removeFromSharedPreferences("chefid");

                Toast.makeText(getApplicationContext(),"Unable to fetch data , try again",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ChefLauncher.this, ChefLauncher.class);
                startActivity(intent);
                finish();
            }
            else
            {
                if(getIntent().getExtras().containsKey("toorders"))
                {
                    showFragmanet(1,new ChefFragment(),"http://chefmonster.com:3000/orders/view/"+getIntent().getExtras().getString("toorders"));
                }
                else
                    showFragmanet(1,new ChefFragment(),"http://chefmonster.com:3000/users/profile/"+ApplicationLoader.getChefId());
            }
        }
    }
    int result=0;
    private void setUpToolbar() {

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbarr) ;
        setSupportActionBar(toolbar);
        TextView City=(TextView) findViewById(R.id.txtLocalCity);
        ImageView iv_menu_nav=(ImageView) findViewById(R.id.iv_menu_nav);
        City.setText("Chef Monster - Chef");
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   mDrawerLayout.openDrawer(GravityCompat.START);
                startActivity(new Intent(ChefLauncher.this,ChefSideMenu.class));

            }
        });
    }

    private void setUpDrawer()
    {/*
        mNavigationView
                .getHeaderView(0)
                .findViewById(R.id.navigation_drawer_header_clickable)
                .setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                        //check is the user already logged in or not
                        startActivityWithDelay(UserProfile.class);
                    }
                });

        mNavigationView.setNavigationItemSelectedListener
                (
                        new NavigationView.OnNavigationItemSelectedListener()
                        {
                            @Override
                            public boolean onNavigationItemSelected(final MenuItem item)
                            {
                                mDrawerLayout.closeDrawer(GravityCompat.START);

                                switch (item.getItemId())
                                {
                                    case R.id.navigation_view_item_home:
                                        item.setChecked(true);
                                        setToolbarTitle("Home");
                                       // showFragmanet(1,new AddMenuFragment(),"http://chefmonster.com:3000/users/profile/"+ApplicationLoader.getChefId());
                                        item.setChecked(true);
                                        break;

                                    case R.id.navigation_view_item_deals:
                                        setToolbarTitle("Add Menus");
                                        showFragmanet(1,new AddMenuFragment(),"http://chefmonster.com:3000/menu/add-menu/"+ApplicationLoader.getChefId());
                                        break;

                                    case R.id.navigation_view_item_cart:
                                        setToolbarTitle("Manage Menus");
                                        showFragmanet(1,new ViewFragment(),"http://chefmonster.com:3000/menu/view-menus/"+ApplicationLoader.getChefId());
                                        break;

                                   *//* case R.id.navigation_view_item_stock:
                                        setToolbarTitle("Manage Stock");
                                        showFragmanet(1,new ViewFragment(),"http://chefmonster.com:3000/menu/add-stock/152");
                                        break;
*//*
                                    case R.id.navigation_view_item_orders:
                                        setToolbarTitle("Manage Orders");
                                        showFragmanet(1,new ViewFragment(),"http://chefmonster.com:3000/orders/list/"+ApplicationLoader.getChefId());
                                        break;

                                    case R.id.navigation_view_item_account:
                                        setToolbarTitle("My Account");
                                        showFragmanet(1,new ViewFragment(),"http://chefmonster.com:3000/users/profile/"+ApplicationLoader.getChefId());
                                        break;
                                    case R.id.navigation_view_item_logout:
                                        ApplicationLoader.removeFromSharedPreferences("chefid");
                                        startActivity(new Intent(ChefLauncher.this, MainActivity.class));
                                        finish();
                                        break;
                                    case  R.id.navigation_view_item_live:
                                        startActivity(new Intent(ChefLauncher.this, SelectMenuLive.class));
                                        finish();
                                        break;

                                }

                                return true;
                            }
                        }
                );*/
    }



    private void restoreState(final @Nullable Bundle savedInstanceState)
    {

        if (savedInstanceState == null)
        {
            showFragmanet(1,new ChefFragment(),"http://chefmonster.com:3000/users/login/"+tm.getDeviceId());
        }
        else
        {
            setToolbarTitle((String) savedInstanceState.getCharSequence("Chef Monster"));
        }
        showFragmanet(1,new ChefFragment(),"http://chefmonster.com:3000/users/login/"+tm.getDeviceId());
    }

    private void showDefaultFragment()
    {
        setToolbarTitle("Chef Monster");
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_activity_content_frame, new ChefFragment());
        tx.commit();
        // showImageFragment(MenuActivity.sIMAGE_NEO);
    }
    private void setToolbarTitle(@StringRes final String title)
    {
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(title);
        }
    }



    Bundle bundle;
    public void showFragmanet(int flag,Fragment fm,String URL)
    {

        bundle = new Bundle();

        if(ApplicationLoader.getChefId().isEmpty())
        {
            bundle.putString("url", "http://chefmonster.com:3000/users/login/"+tm.getDeviceId());

        }
        else
        {
            bundle.putString("url", URL);
        }

        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        if(flag==1)
            tx.add(R.id.main_activity_content_frame_chef,fm);
        else
            tx.replace(R.id.main_activity_content_frame_chef,fm);

        fm.setArguments(bundle);
        tx.addToBackStack("ChefMonster");
        tx.commit();
    }
    private void setToolbarTitle(@StringRes final int string)
    {
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(string);
        }
    }

    private String getToolbarTitle()
    {
        if (getSupportActionBar() != null)
        {
            return (String) getSupportActionBar().getTitle();
        }

        return getString(R.string.app_name);
    }

    /**
     * start this activities with delay to avoid junk while closing the drawer
     */
    private void startActivityWithDelay(@NonNull final Class activity)
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                startActivity(new Intent(mContext, activity));
            }
        }, 500);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
