package com.android.chefmonster.ChefApp;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * Created by SantoshT on 11/7/2016.
 */

public class WebAppInterface  {

    Context ctx;

    public WebAppInterface(Context ctx) {
        this.ctx=ctx;
    }
    @JavascriptInterface
    public void ImageCallBack()
    {
        Toast.makeText(ctx, "Call back call", Toast.LENGTH_SHORT).show();
    }
}
