package com.android.chefmonster.ChefApp;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;

/**
 * Created by tskva on 9/17/2016.
 */
public class AddMenuFragment extends AppCompatActivity {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    WebView chef_view;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_chef);

        chef_view=(WebView)findViewById(R.id.wv_chef);
        chef_view.getSettings().setJavaScriptEnabled(true);

        WebSettings mWebSettings = chef_view.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowContentAccess(true);
        mWebSettings.setLoadWithOverviewMode(true);


        // Javascript inabled on webview
        mWebSettings.setJavaScriptEnabled(true);
        // Other webview options
        mWebSettings.setLoadWithOverviewMode(true);

        chef_view.setScrollbarFadingEnabled(false);
        mWebSettings.setBuiltInZoomControls(true);
        mWebSettings.setPluginState(WebSettings.PluginState.ON);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setRenderPriority(
                WebSettings.RenderPriority.HIGH);
        mWebSettings.setCacheMode(
                WebSettings.LOAD_NO_CACHE);
        mWebSettings.setAppCacheEnabled(true);
        chef_view.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings
                .setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setSavePassword(true);
        mWebSettings.setSaveFormData(true);
        mWebSettings.setEnableSmoothTransition(true);

        //startWebView("http://128.199.173.98:3000/menu/add-menu/995");
        // Inflate the layout for this fragment
        chef_view.loadUrl("http://128.199.173.98:3000/menu/add-menu/"+ ApplicationLoader.getChefId());
    }


    private void startWebView(String url) {


        chef_view.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;


            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource (WebView view, String url) {

                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(AddMenuFragment.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }
            public void onPageFinished(WebView view, String url) {
                try{
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                    progressDialog.dismiss();
                }catch(Exception exception){
                    progressDialog.dismiss();
                    exception.printStackTrace();
                }
            }

        });

        // Javascript inabled on webview
        //chef_view.getSettings().setJavaScriptEnabled(true);

        //Load url in webview
        chef_view.loadUrl(url);

    }



}
