package com.android.chefmonster.ChefApp;

/**
 * Created by SantoshT on 1/31/2017.
 */

public class ChefMenu {

    String MenuID;
    String MenuName;

    public String getMenuID() {
        return MenuID;
    }

    public void setMenuID(String menuID) {
        MenuID = menuID;
    }

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }
}
