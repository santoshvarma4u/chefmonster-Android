package com.android.chefmonster.Searchbar;

import java.io.Serializable;

/**
 * Created by SantoshT on 12/5/2016.
 */

public class SearchIndex implements Serializable{

    String menuID,menuName,categoryName;

    public String getMenuID() {
        return menuID;
    }

    public void setMenuID(String menuID) {
        this.menuID = menuID;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
