package com.android.chefmonster;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.os.StrictMode;

import com.android.chefmonster.Services.ScreenReceiver;
import com.ivankocijan.magicviews.MagicViews;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SantoshT on 9/9/2016.
 */
public class ApplicationLoader extends Application {


    public static volatile Context applicationContext;
    public static volatile Handler applicationHandler;
    public static volatile boolean isScreenOn = false;
    private static volatile boolean applicationInited = false;
    @Override
    public void onCreate() {

        super.onCreate();

        applicationContext = getApplicationContext();
        applicationHandler = new Handler(applicationContext.getMainLooper());

        MagicViews.setFontFolderPath(this, "fonts");
        MagicViews.setDefaultTypeFace(this, "RNSSanz-Medium.otf");

/*
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Quicksand-Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());*/
    }

    @Override
    protected void attachBaseContext(Context base) {

        super.attachBaseContext(base);
       // super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }


    public static void postInitApplication() {
        if (applicationInited) {
            return;
        }

        applicationInited = true;

        try {
            final IntentFilter filter = new IntentFilter(
                    Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            final BroadcastReceiver mReceiver = new ScreenReceiver();
            applicationContext.registerReceiver(mReceiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PowerManager pm = (PowerManager) ApplicationLoader.applicationContext
                    .getSystemService(Context.POWER_SERVICE);
            isScreenOn = pm.isScreenOn();
            // FileLog.e("tmessages", "screen state = " + isScreenOn);
        } catch (Exception e) {
            // FileLog.e("tmessages", e);
        }

        // ApplicationLoader app = (ApplicationLoader)
        // ApplicationLoader.applicationContext;
        // app.initPlayServices();

    }

    public static void setUserCity(String City) {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        SharedPreferences.Editor et = preferences.edit();
        et.putString("UserCity", City);
        et.commit();
    }
    public static void setUserLocation(String Location,String lat,String lan) {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        SharedPreferences.Editor et = preferences.edit();
        et.putString("UserLocation", Location);
        et.putString("UserLat",lat);
        et.putString("UserLan",lan);

        et.commit();
    }

    public static void setUserNameEmail(String username,String Email,String profilePic) {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        SharedPreferences.Editor et = preferences.edit();
        et.putString("UserName", username);
        et.putString("Email",Email);
        et.putString("ProfilePic",profilePic);
        et.commit();
    }
    public static String getUserProfilePic() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String city = preferences.getString("ProfilePic", "");
        return city;
    }

    public static String getUserName() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String city = preferences.getString("UserName", "");
        return city;
    }
    public static String getUserEmail() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String city = preferences.getString("Email", "");
        return city;
    }

    public static String getUserCity() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String city = preferences.getString("UserCity", "");
        return city;
    }
    public static String getUserLocation() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String Location = preferences.getString("UserLocation", "");
        return Location;
    }
    public static String getUserLat() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String latLan="";
        if(preferences.contains("UserLat"))
        {
            latLan = preferences.getString("UserLat", "");
        }
        return latLan;
    }

    public static String getUserLon() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String latLan="";
        if(preferences.contains("UserLan"))
        {
            latLan = preferences.getString("UserLan", "");
        }
        return latLan;
    }
    public static String getProchefID() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String latLan="";
        if(preferences.contains("ProchefID"))
        {
            latLan = preferences.getString("ProchefID", "");
        }
        return latLan;
    }

    public static void setProchefID(String chefid) {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        SharedPreferences.Editor et = preferences.edit();
        et.putString("ProchefID", chefid);
        et.commit();
    }

    public static void setUserPhone(String phoneNumber) {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        SharedPreferences.Editor et = preferences.edit();
        et.putString("PhoneNumber", phoneNumber);
        et.commit();
    }
    public static String getUserPhone() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String latLan="";
        if(preferences.contains("PhoneNumber"))
        {
            latLan = preferences.getString("PhoneNumber", "");
        }
        return latLan;
    }

    public static void setFCMToken(String phoneNumber) {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        SharedPreferences.Editor et = preferences.edit();
        et.putString("FCMToken", phoneNumber);
        et.commit();
    }

    public static String getFCMToken() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String latLan="";
        if(preferences.contains("FCMToken"))
        {
            latLan = preferences.getString("FCMToken", "");
        }
        return latLan;
    }
    public static void setChefId(String chefid) {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        SharedPreferences.Editor et = preferences.edit();
        et.putString("chefid", chefid);
        et.commit();
    }

    public static String getChefId() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String chefid="";
        if(preferences.contains("chefid"))
        {
            chefid = preferences.getString("chefid", "");
        }
        return chefid;
    }


    public static void setDeliveryType(String deliverytype) {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        SharedPreferences.Editor et = preferences.edit();
        et.putString("deliverytype", deliverytype);
        et.commit();
    }

    public static String getDeliveryType() {
        SharedPreferences preferences = ApplicationLoader.applicationContext
                .getSharedPreferences("ChefMonster", Activity.MODE_PRIVATE);
        String deliverytype="";
        if(preferences.contains("deliverytype"))
        {
            deliverytype = preferences.getString("deliverytype", "");
        }
        return deliverytype;
    }





    public static void removeFromSharedPreferences(String key) {
        if (ApplicationLoader.applicationContext != null) {
            SharedPreferences mSharedPreferences = ApplicationLoader.applicationContext.getSharedPreferences("ChefMonster", 0);
            if (mSharedPreferences != null)
                mSharedPreferences.edit().remove(key).commit();
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}
