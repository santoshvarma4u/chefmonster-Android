package com.android.chefmonster.Checkout;

/**
 * Created by SantoshT on 5/1/2017.
 */

public class CheckoutOrder {

    public String MenuName,MenuQty,MenuPrice;

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }

    public String getMenuQty() {
        return MenuQty;
    }

    public void setMenuQty(String menuQty) {
        MenuQty = menuQty;
    }

    public String getMenuPrice() {
        return MenuPrice;
    }

    public void setMenuPrice(String menuPrice) {
        MenuPrice = menuPrice;
    }
}




