package com.android.chefmonster.Checkout.Coupons;

/**
 * Created by tskva on 3/22/2017.
 */

public class Coupon {

    int id;
    String date_time,couponname,coupontype,couponvalue,expirydate,repeatcount,response;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRepeatcount() {
        return repeatcount;
    }

    public void setRepeatcount(String repeatcount) {
        this.repeatcount = repeatcount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getCouponname() {
        return couponname;
    }

    public void setCouponname(String couponname) {
        this.couponname = couponname;
    }

    public String getCoupontype() {
        return coupontype;
    }

    public void setCoupontype(String coupontype) {
        this.coupontype = coupontype;
    }

    public String getCouponvalue() {
        return couponvalue;
    }

    public void setCouponvalue(String couponvalue) {
        this.couponvalue = couponvalue;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }
}
