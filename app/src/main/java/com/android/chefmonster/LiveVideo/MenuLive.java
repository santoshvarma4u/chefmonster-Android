package com.android.chefmonster.LiveVideo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.android.chefmonster.R;

public class MenuLive extends AppCompatActivity {


    protected WebView wvLiveVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_menu_live);
        initView();
    }

    private void initView() {
        wvLiveVideo = (WebView) findViewById(R.id.wvLiveVideo);
        WebSettings mWebSettings = wvLiveVideo.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowContentAccess(true);
        mWebSettings.setLoadWithOverviewMode(true);


        // Javascript inabled on webview
        mWebSettings.setJavaScriptEnabled(true);
        // Other webview options
        mWebSettings.setLoadWithOverviewMode(true);

        wvLiveVideo.setScrollbarFadingEnabled(false);
        mWebSettings.setBuiltInZoomControls(true);
        mWebSettings.setPluginState(WebSettings.PluginState.ON);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setRenderPriority(
                WebSettings.RenderPriority.HIGH);
        mWebSettings.setCacheMode(
                WebSettings.LOAD_NO_CACHE);
        mWebSettings.setAppCacheEnabled(true);
        wvLiveVideo.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings
                .setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setSavePassword(true);
        mWebSettings.setSaveFormData(true);
        mWebSettings.setEnableSmoothTransition(true);
        Bundle extras = getIntent().getExtras();
        if(extras.containsKey("liveURL"))
        {
            wvLiveVideo.loadUrl(extras.getString("liveURL").toString());
        }
    }
}
