package com.android.chefmonster.Utills;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.widget.Toast;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by SantoshT on 9/8/2016.
 */
public class Constants {

    public static String AdminPageURL = "http://128.199.173.98/admin";
    public static String ImagesSlideURL="http://128.199.173.98/admin/images/sliders/";
    public static String CategoryAPI = "http://128.199.173.98/api/get-all-category-data.php";
    public static String MenuAPI = "http://128.199.173.98/api/get-menu-data-by-category-id.php";
    public static String TaxCurrencyAPI = "http://128.199.173.98/api/get-tax-and-currency.php";
    public static String MenuDetailAPI = "http://128.199.173.98/api/get-menu-detail.php";
    public static String SendDataAPI = "http://128.199.173.98/api/add-reservation.php";
    public static String SaveDevice = "http://128.199.173.98/api/saveDevice.php";
    public static String GetOrders = "http://128.199.173.98/api/getOrders.php";
    public static String checkChef = "http://128.199.173.98/api/deliveryApi.php";
    public static String searchUrl = "http://128.199.173.98/api/getSearchResults.php";
    public static String ImagesUrl="http://128.199.173.98/admin";
    public static String ProchefBranchesAPI="http://128.199.173.98/api/getProChefInfo.php";
    public static String googleMapUrl="https://maps.googleapis.com/maps/api/staticmap?size=120x120&maptype=roadmap&path=color:0xff0000ff|weight:2|";
    public static String checkCouponUrl="http://128.199.173.98/api/checkCoupon.php";
    public static String OtpURL="http://128.199.173.98/api/smsapi.php";
    public static String SubmitFeedback="http://128.199.173.98/api/feedback.php";
    // change this access similar with accesskey in admin panel for security reason
    public static String AccessKey = "12345";
//https://maps.googleapis.com/maps/api/staticmap?size=120x120&maptype=roadmap&path=color:0xff0000ff|weight:2|17.8044365,83.3900578|17.717216608867627,83.31019431262393&key=AIzaSyCI_e36oRpU-iHWYt0-1GY77k9GGKI51BY
    // database path configuration
    public  static String DBPath = "/data/data/com.android.chefmonster/databases/";

    // method to check internet connection
    public static boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // method to handle images from server
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }

    public static boolean isGPSON(Context ctx) {
        boolean res = false;
        String provider = Settings.Secure.getString(ctx
                        .getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {
            res = true;
        }
        System.out.println("GPS status:" + res);
        return res;
    }


}
