package com.android.chefmonster.Utills;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.chefmonster.R;
import com.android.chefmonster.Splash;

public class NoConnection extends Activity {

    Button btnRetry;
    ProgressBar pb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);
        if(isNetworkAvailable(NoConnection.this))
            finish();

        pb=(ProgressBar)findViewById(R.id.internetProgressbar);
        btnRetry=(Button)findViewById(R.id.btnRetryInternet);
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pb.setVisibility(View.VISIBLE);
                new CountDownTimer(5000,1000) {

                    /** This method will be invoked on finishing or expiring the timer */
                    @Override
                    public void onFinish() {
                        /** Creates an intent to start new activity */

                        pb.setVisibility(View.GONE);
                       if(isNetworkAvailable(NoConnection.this))
                       {
                           finish();
                       }
                        else
                       {
                           Toast.makeText(getApplicationContext(),"Oops,No Internet Connection Found",Toast.LENGTH_LONG).show();
                       }
                    }
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                }.start();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public static boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
