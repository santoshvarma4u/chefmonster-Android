package com.android.chefmonster.AppIntro;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Locations.SelectLocationActivity;
import com.android.chefmonster.R;
import com.android.chefmonster.Splash;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.UI.OTP.OTPActivity;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.NoConnection;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;

public class IntroActivity extends AppIntro {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(new IntroSlider1());
       // addSlide(AppIntroFragment.newInstance("Chef Monster", "", R.drawable.intro, Color.parseColor("#ffcd02")));
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        init();
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        init();
        // Do something when users tap on Done button.
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }

    String result, username;
    String tmDevice;

    public void init()
    {

        //getDBBackUp("com.android.chefmonster", DBHelper_New.DB_NAME, DBHelper_New.DB_NAME);

        if(Constants.isNetworkAvailable(IntroActivity.this))
        {
          if(ApplicationLoader.getUserLat().equals(""))
            {
                //Intent to location
                Intent intent=new Intent(IntroActivity.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","true");
                startActivity(intent);
                finish();
            }
          else  if(ApplicationLoader.getUserPhone().equals(""))
            {
                Intent intent=new Intent(IntroActivity.this, OTPActivity.class);
                intent.putExtra("AvailabilityStatus","Success");
                startActivity(intent);
                finish();
            }

            else {
                //Intent to menu
                // startActivity(new Intent(IntroActivity.this, MainActivity.class));
                 startActivity(new Intent(IntroActivity.this, HomePage.class));
                finish();
            }
        }
        else
        {
            //No internet error
            startActivityForResult(new Intent(IntroActivity.this,NoConnection.class),2452);
        }
    }

}
