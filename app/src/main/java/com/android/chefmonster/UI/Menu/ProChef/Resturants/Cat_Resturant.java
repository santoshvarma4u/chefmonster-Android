package com.android.chefmonster.UI.Menu.ProChef.Resturants;

/**
 * Created by SantoshT on 11/30/2016.
 */

public class Cat_Resturant {

    String chefId;
    String resturantLogo;
    String latitude;
    String longitude;

    public String getResturantName() {
        return resturantName;
    }

    public void setResturantName(String resturantName) {
        this.resturantName = resturantName;
    }

    String resturantName;

    public String getChefId() {
        return chefId;
    }

    public void setChefId(String chefId) {
        this.chefId = chefId;
    }

    public String getResturantLogo() {
        return resturantLogo;
    }

    public void setResturantLogo(String resturantLogo) {
        this.resturantLogo = resturantLogo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
