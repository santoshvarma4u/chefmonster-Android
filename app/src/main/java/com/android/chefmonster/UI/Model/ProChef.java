package com.android.chefmonster.UI.Model;

import com.android.chefmonster.UI.Menu.ProChef.Branches;

import java.util.List;

/**
 * Created by ChakravartyG on 9/17/2016.
 */

public class ProChef {

    String retaurentname, logo, minorder, paymenttype, sepkitchen, username, userreview, deltype, latitude, longitude, distance, duration, delivery_time, pickupImageUrl,chefid;
    String available_food_types;
    List<Branches> branchsList;
    public String getAvailable_food_types() {
        return available_food_types;
    }

    public void setAvailable_food_types(String available_food_types) {
        this.available_food_types = available_food_types;
    }

    public String getRetaurentname() {
        return retaurentname;
    }

    public void setRetaurentname(String retaurentname) {
        this.retaurentname = retaurentname;
    }

    public String getChefid() {
        return chefid;
    }

    public List<Branches> getBranchsList() {
        return branchsList;
    }

    public void setBranchsList(List<Branches> branchsList) {
        this.branchsList = branchsList;
    }

    public void setChefid(String chefid) {
        this.chefid = chefid;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMinorder() {
        return minorder;
    }

    public void setMinorder(String minorder) {
        this.minorder = minorder;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public String getSepkitchen() {
        return sepkitchen;
    }

    public void setSepkitchen(String sepkitchen) {
        this.sepkitchen = sepkitchen;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserreview() {
        return userreview;
    }

    public void setUserreview(String userreview) {
        this.userreview = userreview;
    }

    public String getDeltype() {
        return deltype;
    }

    public void setDeltype(String deltype) {
        this.deltype = deltype;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getPickupImageUrl() {
        return pickupImageUrl;
    }

    public void setPickupImageUrl(String pickupImageUrl) {
        this.pickupImageUrl = pickupImageUrl;
    }
}
