package com.android.chefmonster.UI.Login;

/**
 * Created by SantoshT on 6/20/2017.
 */

public class Trans {

    String TransactionID,Modeofpayment,TransDate,TransAmount,TransStatus;

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }

    public String getModeofpayment() {
        return Modeofpayment;
    }

    public void setModeofpayment(String modeofpayment) {
        Modeofpayment = modeofpayment;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransAmount() {
        return TransAmount;
    }

    public void setTransAmount(String transAmount) {
        TransAmount = transAmount;
    }

    public String getTransStatus() {
        return TransStatus;
    }

    public void setTransStatus(String transStatus) {
        TransStatus = transStatus;
    }
}
