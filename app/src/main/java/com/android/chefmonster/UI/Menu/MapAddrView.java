package com.android.chefmonster.UI.Menu;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.squareup.picasso.Picasso;

import java.net.URLDecoder;

import static android.net.Uri.decode;

public class MapAddrView extends AppCompatActivity {

    ImageView iv_pickupImage;
    Button btnGetDirections;
    TextView pickupAddr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_addr_view);
        Intent intent=getIntent();
        String ImageUrl=intent.getExtras().getString("pimage").replace("120x120","500x600");
        String Location=intent.getExtras().getString("paddress");
        final String PickupLatitude=intent.getExtras().getString("platitude");
        final String PickupLongitude=intent.getExtras().getString("plongitude");

        pickupAddr=(TextView)findViewById(R.id.addr_map);
        btnGetDirections=(Button)findViewById(R.id.btnGetAddress);
        iv_pickupImage=(ImageView)findViewById(R.id.map_iv_addr);

        System.out.println(ImageUrl);
        pickupAddr.setText(Location);
        ImageLoader imageLoader=new ImageLoader(getApplicationContext());
        imageLoader.DisplayImage(ImageUrl,iv_pickupImage);
        //Picasso.with(MapAddrView.this).load(ImageUrl).into(iv_pickupImage);
        btnGetDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("geo:" + PickupLatitude
                                    + "," + PickupLongitude
                                    + "?q=" + ApplicationLoader.getUserLat()
                                    + "," + ApplicationLoader.getUserLon()
                                    + "(Chefmonster)"));
                    intent.setComponent(new ComponentName(
                            "com.google.android.apps.maps",
                            "com.google.android.maps.MapsActivity"));
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {

                    try {
                        startActivity(new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("market://details?id=com.google.android.apps.maps")));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.apps.maps")));
                    }

                    e.printStackTrace();
                }
            }
        });

        //check this menu availa in myorders or not and then enable


         btnGetDirections.setEnabled(false);

    }
}
