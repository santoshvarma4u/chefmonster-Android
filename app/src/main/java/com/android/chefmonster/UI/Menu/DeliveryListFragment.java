package com.android.chefmonster.UI.Menu;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.ActivityCartNew;
import com.android.chefmonster.Cart.CartTabbedActivity;
import com.android.chefmonster.ChefApp.SelectMenuLive;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.InfinateViewpager.InfiniteViewPager;
import com.android.chefmonster.LiveVideo.MenuLive;
import com.android.chefmonster.LiveVideo.RtcActivity;
import com.android.chefmonster.LoadingDrawble.loadingdrawable.LoadingView;
import com.android.chefmonster.R;
import com.android.chefmonster.Searchbar.MaterialSearchBar;
import com.android.chefmonster.UI.Menu.Filters.Cusines;
import com.android.chefmonster.UI.Menu.Filters.FilterBottomSheet;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.circlepageidicator.CirclePageIndicator;
import com.konifar.fab_transformation.FabTransformation;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;
import static com.android.chefmonster.R.id.listMenu;
import static com.android.chefmonster.R.id.txtQty;
import static com.android.chefmonster.R.id.txtStockQty;

/**
 * Created by ChakravartyG on 9/10/2016.
 */

public class DeliveryListFragment extends Fragment implements View.OnClickListener, FilterListener,MaterialSearchBar.OnSearchActionListener {
    View view;
    RecyclerView recycler_view;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public ImageLoader imageLoader;
    DecimalFormat formatData;
    private TextView mTxtAmountAdapter;
    private TextView txtItems;
    int ItemCount = 0;
    RelativeLayout checkout;
    LinearLayout optionsLayout;
    EditText mSearch;
    boolean dealsApply;
    TextView tv_checkout;
    //
    private List<String> lastSearches;
    private MaterialSearchBar searchBar;

    Button btnPickup,btnDeals,btnCombos;
    Button btnCartAdapter;
    Button btnFilter;

     List<Menu> menuList;
    SwipeRevealLayout swipeRevealLayout;

    Button mainFragFilter;

    TextView tv_type_delivery,tv_type_pickup,tv_type_deliverypickup;

    static String Currency="₹";

    FloatingActionButton fab;
    Toolbar toolbarFooter;
    LoadingView loading_view;

    BottomSheetDialog bottomSheetDialog ;
    JSONObject filterResult;
    View view1;

    public String searchKey="";

    SwipeRefreshLayout mSwipeRefreshLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.menu_deliverylist_frag, container, false);
        //

        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);

        tv_checkout=(TextView)view.findViewById(R.id.tv_checkout);
        tv_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(getActivity(), CartTabbedActivity.class);
                startActivity(iMyOrder);
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.smoothScrollToPosition(0);

        loading_view=(LoadingView)view.findViewById(R.id.loading_view);
        //filter bottom sheet
        filterResult = new JSONObject();
        mainFragFilter=(Button) view.findViewById(R.id.mainFragFilter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);

        view1 = getActivity().getLayoutInflater().inflate(R.layout.fragment_filter_bottom_sheet, null);

        bottomSheetDialog = new BottomSheetDialog(getActivity());
        bottomSheetDialog.setContentView(view1);

        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                bottomSheetDialog.dismiss();
                bottomSheetDialog.hide();
                Toast.makeText(getActivity(), "Dismissed", Toast.LENGTH_SHORT).show();
            }
        });



        initFilterView(view1);

        mainFragFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottomSheetDialog.show();

            }
        });


        menuList=new ArrayList<>();

        imageLoader = new ImageLoader(getActivity());

        formatData = new DecimalFormat("#.##");
//

        toolbarFooter=(Toolbar) view.findViewById(R.id.toolbar_footer);
         fab=(FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(getActivity(), CartTabbedActivity.class);
                startActivity(iMyOrder);
            }
        });
        mTxtAmountAdapter = (TextView) view.findViewById(R.id.txtCartPriceAdapter);
        mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));
        checkout = (RelativeLayout) view.findViewById(R.id.lytCheckout);
        btnCartAdapter=(Button) view.findViewById(R.id.btnCartAdapter);
        searchBar = (MaterialSearchBar) view.findViewById(R.id.edtKeyword);
        searchBar.setOnSearchActionListener(this);
        lastSearches = loadSearchSuggestionFromDisk();
       // searchBar.setLastSuggestions(lastSearches);
        swipeRevealLayout=(SwipeRevealLayout) view.findViewById(R.id.swipeRevealLayout);
        tv_type_delivery=(TextView) view.findViewById(R.id.typedelivery);
        tv_type_pickup=(TextView) view.findViewById(R.id.typepickup);
        tv_type_deliverypickup=(TextView) view.findViewById(R.id.typepickupndelivery);

        btnDeals=(Button) view.findViewById(R.id.btnDeals);
        btnDeals.setOnClickListener(this);
        btnCombos=(Button)view.findViewById(R.id.btnCombos);
        btnCombos.setOnClickListener(this);

        btnCartAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(getActivity(), ActivityCartNew.class);
                startActivity(iMyOrder);
            }
        });
        //
        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState==SCROLL_STATE_IDLE)
                    fab.setVisibility(View.VISIBLE);
                else
                    fab.setVisibility(View.GONE);
            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                if (fab.getVisibility() != View.VISIBLE) {
                    FabTransformation.with(fab).transformFrom(toolbarFooter);
                }
              //  fab.setVisibility(View.GONE);
            }
        });
        linearLayoutManager = (LinearLayoutManager) recycler_view.getLayoutManager();

        //
        optionsLayout = (LinearLayout) view.findViewById(R.id.linearLayout10);
        //
        txtItems=(TextView) view.findViewById(R.id.txtItems);
        txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());

        btnPickup = (Button) view.findViewById(R.id.btnPickup);
        btnPickup.setOnClickListener(this);
        //
        dla = new DeliveryListAdapter();

        // recycler_view.setAdapter(dla);view

        btnFilter=(Button)view.findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(this);
        //

        mSwipeRefreshLayout.setColorSchemeResources(R.color.appColor,R.color.optionColor);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                typeOfDelivery="Delivery & Pickup";
                loadSliderImages();
                getFilterDataFromServer();
            }
        });

        //taken from onresume

        loadSliderImages();
        getFilterDataFromServer();

        return view;
    }


    public static DeliveryListFragment newInstance() {

        return (new DeliveryListFragment());

    }

    boolean searchEnable=false;
    String  searchKeyFromHome="";
    public void filterHit()
    {

        bottomSheetDialog.dismiss();
        bottomSheetDialog.cancel();
        bottomSheetDialog.show();
  }
    public void searchRequest(String sMenuId,String sMenuName){
        Log.i("sMenuId",sMenuId);
        Log.i("sMenuName",sMenuName);
        searchKey=sMenuName;
        searchKeyFromHome=sMenuName;
        searchEnable=true;

        loadSliderImages();
        getFilterDataFromServer();

        //handle new layout

       // rlSearchLayout

       // getDataFromServer(sMenuName,"Delivery & Pickup");
    }

    public void TypeofDeliveryRequest(String tod){

        typeOfDelivery=tod;
        loadSliderImages();
        getFilterDataFromServer();
    }




    DeliveryListAdapter dla;

    public LinearLayoutManager linearLayoutManager;
    PickUpListFragment pickUpListFragment;
    CombosFragment combosFragment;

    public void setPickUpFragment() {
        pickUpListFragment = new PickUpListFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.main_activity_content_frame, pickUpListFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

    }
    public void setComboFragment() {
        combosFragment = new CombosFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.main_activity_content_frame, combosFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    RecyclerView rv_cusines;
    TextView filter_apply,filter_reset;
    CheckBox cb_distanceFilter,cb_ratingFilter,cb_vegFilter,cb_splRecipeFilter,cb_100Range,cb_200Range,cb_300Range,cb_AboveRange;
    RadioButton rb_distanceFilter,rb_ratingFilter,rb_vegFilter,rb_splRecipeFilter;
    RadioGroup sortGroup;
    LinearLayout ll_distanceFilter,ll_ratingFilter,ll_vegFilter,ll_splRecipeFilter,ll_100Range,ll_200Range,ll_300Range,ll_AboveRange;
    public void initFilterView(View view)
    {
        rv_cusines=(RecyclerView)view.findViewById(R.id.rvcusines);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_cusines.setLayoutManager(mLayoutManager);
        rv_cusines.setItemAnimator(new DefaultItemAnimator());
        // rv_cusines.smoothScrollToPosition(0);

        // TextView filter_apply,filter_reset;
        //CheckBox cb_distanceFilter,cb_ratingFilter,cb_vegFilter,cb_splRecipeFilter,cb_100Range,cb_200Range,cb_300Range,cb_AboveRange;

        filter_apply=(TextView)view.findViewById(R.id.filter_apply);
        filter_reset=(TextView)view.findViewById(R.id.filter_reset);
        cb_distanceFilter=(CheckBox)view.findViewById(R.id.cb_distanceFilter);
        cb_ratingFilter=(CheckBox)view.findViewById(R.id.cb_ratingFilter);
        cb_vegFilter=(CheckBox)view.findViewById(R.id.cb_vegFilter);
        cb_splRecipeFilter=(CheckBox)view.findViewById(R.id.cb_splRecipeFilter);
        cb_100Range=(CheckBox)view.findViewById(R.id.cb_100Range);
        cb_200Range=(CheckBox)view.findViewById(R.id.cb_200Range);
        cb_300Range=(CheckBox)view.findViewById(R.id.cb_300Range);
        cb_AboveRange=(CheckBox)view.findViewById(R.id.cb_AboveRange);


        ll_distanceFilter=(LinearLayout)view.findViewById(R.id.ll_distanceFilter);
        ll_ratingFilter=(LinearLayout)view.findViewById(R.id.ll_ratingFilter);
        ll_vegFilter=(LinearLayout)view.findViewById(R.id.ll_vegFilter);
        ll_splRecipeFilter=(LinearLayout)view.findViewById(R.id.ll_splRecipeFilter);
        ll_100Range=(LinearLayout)view.findViewById(R.id.ll_100Range);
        ll_200Range=(LinearLayout)view.findViewById(R.id.ll_200Range);
        ll_300Range=(LinearLayout)view.findViewById(R.id.ll_300Range);
        ll_AboveRange=(LinearLayout)view.findViewById(R.id.ll_AboveRange);

        ll_distanceFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_distanceFilter.performClick();
            }
        });
        ll_ratingFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_ratingFilter.performClick();
            }
        });
        ll_vegFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_vegFilter.performClick();
            }
        });
        ll_splRecipeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_splRecipeFilter.performClick();
            }
        });
        ll_100Range.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_100Range.performClick();
            }
        });
        ll_200Range.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_200Range.performClick();
            }
        });
        ll_300Range.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_300Range.performClick();
            }
        });
        ll_AboveRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_AboveRange.performClick();
            }
        });

        sortGroup=(RadioGroup)view.findViewById(R.id.sortGroup);

        rb_distanceFilter=(RadioButton) view.findViewById(R.id.rb_distanceFilter);
        rb_ratingFilter=(RadioButton)view.findViewById(R.id.rb_ratingFilter);
        rb_vegFilter=(RadioButton)view.findViewById(R.id.rb_vegFilter);
        rb_splRecipeFilter=(RadioButton)view.findViewById(R.id.rb_splRecipeFilter);



        cb_distanceFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {

                    if (isChecked) {

                        filterResult.put("sort", "distance");
                       // cb_distanceFilter.setButtonDrawable(R.drawable.filter_checked);
                        cb_ratingFilter.setChecked(false);
                        cb_vegFilter.setChecked(false);
                        cb_splRecipeFilter.setChecked(false);

                        filterResult.remove("rating");
                        filterResult.remove("veg");
                        filterResult.remove("spl");

                    } else {

                        filterResult.put("sort", "");
                       // cb_distanceFilter.setButtonDrawable(R.drawable.filter_unchecked);
                        cb_distanceFilter.setChecked(false);
                        cb_ratingFilter.setChecked(false);
                        cb_vegFilter.setChecked(false);
                        cb_splRecipeFilter.setChecked(false);

                    }
                }
                catch (JSONException ex)
                {

                }

            }
        });
        cb_ratingFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("sort", "rating");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                   // cb_ratingFilter.setButtonDrawable(R.drawable.filter_checked);

                    cb_distanceFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                    filterResult.remove("dist");
                    filterResult.remove("veg");
                    filterResult.remove("spl");
                }
                else
                {
                    try {
                        filterResult.put("sort", "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                  //  cb_ratingFilter.setButtonDrawable(R.drawable.filter_unchecked);
                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                }
            }
        });
        cb_vegFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("sort", "Veg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                  //  cb_vegFilter.setButtonDrawable(R.drawable.filter_checked);
//
                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                    filterResult.remove("rating");
                    filterResult.remove("dist");
                    filterResult.remove("spl");
                }
                else
                {
                    try {
                        filterResult.put("sort", "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //cb_vegFilter.setButtonDrawable(R.drawable.filter_unchecked);
                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                }
            }
        });
        cb_splRecipeFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("sort", "SplRecipe");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                   // cb_splRecipeFilter.setButtonDrawable(R.drawable.filter_checked);

                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    filterResult.remove("rating");
                    filterResult.remove("veg");
                    filterResult.remove("dist");
                }
                else
                {
                    try {
                        filterResult.put("spl", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                   // cb_splRecipeFilter.setButtonDrawable(R.drawable.filter_unchecked);
                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                }
            }
        });
        cb_100Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("range", "100");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                   // cb_100Range.setButtonDrawable(R.drawable.filter_checked);
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_AboveRange.setChecked(false);

                }
                else
                {
                    try {
                        filterResult.put("range", "0");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_100Range.setChecked(false);
                    cb_AboveRange.setChecked(false);
                }
            }
        });
        cb_200Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("range", "200");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_100Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_AboveRange.setChecked(false);

                }
                else
                {
                    try {
                        filterResult.put("range", "0");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_100Range.setChecked(false);
                    cb_AboveRange.setChecked(false);
                }
            }
        });
        cb_300Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("range", "300");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_200Range.setChecked(false);
                    cb_100Range.setChecked(false);
                    cb_AboveRange.setChecked(false);
                }
                else
                {
                    try {
                        filterResult.put("range", "0");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_100Range.setChecked(false);
                    cb_AboveRange.setChecked(false);
                }
            }
        });
        cb_AboveRange.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("range", "301");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_100Range.setChecked(false);
                }
                else
                {
                    try {
                        filterResult.put("range", "0");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_100Range.setChecked(false);
                    cb_AboveRange.setChecked(false);
                }
            }
        });


        filter_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    filterResult.put("cusinefilter",checkedList.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i("cusinefilter",checkedList.toString());
                getDataFromServer("","",filterResult.toString());

               bottomSheetDialog.dismiss();

            }
        });

        filter_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getDataFromServer("","","");
                bottomSheetDialog.dismiss();
            }
        });

        cusineList=new ArrayList<>();
        fla=new CuisineListAdapter();
    }

    List<Cusines> cusineList;


    CuisineListAdapter fla;
    List<String> checkedList = new ArrayList<>();

    public class CuisineListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_cusine, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return cusineList.size();
        }
        Cusines listCusine;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCusine=cusineList.get(position);
            holder.CusineName.setText(listCusine.getCuisineName());
            holder.CusineId=listCusine.getCuisineId();


        }
    }


    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView CusineName;
        String CusineId;
        CheckBox cb_select;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            CusineName=(TextView)itemView.findViewById(R.id.tv_cusine);
            cb_select=(CheckBox)itemView.findViewById(R.id.cb_cusineitem);
            cb_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        cb_select.setButtonDrawable(R.drawable.filter_checked);
                        checkedList.add(cusineList.get(getAdapterPosition()).getCuisineId());
                    }
                    else {

                        cb_select.setButtonDrawable(R.drawable.filter_unchecked);
                        checkedList.remove(cusineList.get(getAdapterPosition()).getCuisineId());
                    }

                }
            });
        }
    }



    public void getFilterDataFromServer()
    {
        String mUrl="http://128.199.173.98/api/getCuisine.php?cuisine=true&accesskey=12345";
        System.out.println(mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {

                //closeDialog();
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                parseFilterJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });
    }
    public void parseFilterJSONData(String strResponse) {
        try
        {
            System.out.println("******"+strResponse.toString());
            JSONObject json = new JSONObject(strResponse.toString());
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
                cusineList.clear();
            for (int ii = 0; ii < data.length(); ii++) {
                Cusines clist = new Cusines();
                JSONObject object = data.getJSONObject(ii);
                JSONObject cmenu = object.getJSONObject("cusine");
                clist.setCuisineId(cmenu.getString("id"));
                clist.setCuisineName(cmenu.getString("cuisine"));
                cusineList.add(clist);
            }
            rv_cusines.setAdapter(fla);
            fla.notifyDataSetChanged();

            mSwipeRefreshLayout.setRefreshing(false);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPickup:
                setPickUpFragment();
                break;
            case R.id.btnFilter:
                FilterFragment.setFilterListener(this);
                startActivity(new Intent(getActivity(),FilterFragment.class));
                break;
            case R.id.btnDeals:

                Log.e("dealsApply"," "+dealsApply);
                if(dealsApply)
                {
                    dealsApply=false;
                    loadSliderImages();
                }
                else
                {
                    dealsApply=true;
                    loadSliderImages();
                }

                break;
            case R.id.btnCombos:
                setComboFragment();
                break;
            default:
                break;
        }
    }

    @Override
    public void onApplyFilter(JSONObject result) {
        Log.i("result",result.toString());
        Toast.makeText(getActivity(), result.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSearchStateChanged(boolean enabled) {

        if(enabled)
        {
            recycler_view.setVisibility(View.GONE);
        }
        else
        {
            recycler_view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSearchConfirmed(CharSequence text) {

        //getdata with search

       //  getDataFromServer(text.toString());


    }

    @Override
    public void onButtonClicked(int buttonCode) {


    }
    public List<String> loadSearchSuggestionFromDisk()
    {
        List<String> ll=new ArrayList<>();
        ll.add(0,"Biryani");
        ll.add(1,"Chilli Chicken");
        ll.add(2,"Dum Biryani");


        return ll;
       /* DBHelper_New db=new DBHelper_New(getActivity());
        List<List<String>> results=db.getDataByQuery("SELECT "+db.FOOD_NAME+" "+db.TABLE_RECENTSEARCH);
        return results;*/
    }


    public class DeliveryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        final int SLIDER = 0, ROW = 1;
        private final int VIEW_TYPE_LOADING = 3;

        private OnLoadMoreListener mOnLoadMoreListener;

        private boolean isLoading;
        private int visibleThreshold = 5;
        private int lastVisibleItem, totalItemCount;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            switch (viewType) {
                case SLIDER:
                    View vSlider = inflater.inflate(R.layout.item_viewpager, viewGroup, false);
                    viewHolder = new ViewHolderSlider(vSlider);
                    break;

                case ROW:
                    View vRow = inflater.inflate(R.layout.menu_items_new, viewGroup, false);
                    viewHolder = new ViewHolderRow(vRow);
                    break;
                default:
                    break;
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

            switch (viewHolder.getItemViewType()) {
                case SLIDER:
                    ViewHolderSlider vh1 = (ViewHolderSlider) viewHolder;
                    configureViewHolderSlider(vh1, position);
                    break;
                case ROW:
                    ViewHolderRow vh2 = (ViewHolderRow) viewHolder;
                    configureViewHolderRow(vh2, position);
                    break;
                default:

                    break;
            }
        }
        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return menuList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? SLIDER : ROW ;
        }


        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        public void configureViewHolderSlider(final ViewHolderSlider viewHolder, int position) {

                viewHolder.pager.setAdapter(new SlidingImageAdapter(getActivity(), sliderImages));
                viewHolder.minipager.setAdapter(new SlidingImageMenuAdapter(getActivity(),imageLoader));

            viewHolder.pager.setCurrentItem(1, true);

        }

        int ItemCount = 0;
        Menu listMenu;
        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {

            Double finalPrice=0.0;
            listMenu=menuList.get(position);
            holder.txtText.setText(listMenu.getMenu_name());

            holder.txtfoodtype.setText(listMenu.getMenu_food_type());
            holder.txtStockQty.setText(listMenu.getMenu_stock_qty());
            holder.txtweigthingms.setText(listMenu.getMenu_weigth_grms()+" gms");
            holder.txtAreaNamePickup.setText(listMenu.getMenu_pickup_location());
            if(position > 1)
            {
                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0,40,0,0);
                holder.menuCard.setLayoutParams(params);
            }
            if(listMenu.getMenu_Deal_price() > 0)
            {
                holder.ivDealBanner.setVisibility(View.VISIBLE);
                holder.txtMainValue.setVisibility(View.VISIBLE);
                holder.txtMainValue.setText(listMenu.getMenu_price() + " " + DeliveryListFragment.Currency);
                holder.txtMainValue.setPaintFlags(holder.txtMainValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                Double afterDiscount=listMenu.getMenu_price()-listMenu.getMenu_Deal_price();
                holder.txtSubText.setText(String.valueOf(afterDiscount) + " " + DeliveryListFragment.Currency);
                menuList.get(position).setFinal_price(afterDiscount);
                //finalPrice=afterDiscount;
            }
            else
            {
                holder.txtSubText.setText(listMenu.getMenu_price() + " " + DeliveryListFragment.Currency);
                finalPrice=listMenu.getMenu_price();
                menuList.get(position).setFinal_price(finalPrice);
            }

            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
//                holder.itemLayout.setVisibility(View.VISIBLE);//commented as per jii request
                holder.pickupimage.setVisibility(View.VISIBLE);
               // imageLoader.DisplayImage(Constants.googleMapUrl+listMenu.getMenu_pickup_image(), holder.pickupimage);
                Picasso.with(getActivity()).load(Constants.googleMapUrl+listMenu.getMenu_pickup_image()).into(holder.imgThumb);

                Log.d("testUrl",Constants.googleMapUrl+listMenu.getMenu_pickup_image());
               // imageLoader.DisplayImage(listMenu.getMenu_pickup_image(), holder.pickupimage);
            }

            holder.pickupimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getActivity(),MapAddrView.class);
                    i.putExtra("pimage",Constants.googleMapUrl+listMenu.getMenu_pickup_image());
                    i.putExtra("paddress",listMenu.getMenu_pickup_location().toString());
                    i.putExtra("platitude",listMenu.getLatitude());
                    i.putExtra("plongitude",listMenu.getLongitude());
                    startActivity(i);
                }
            });


            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
                holder.txtDeliverytime.setText(listMenu.getMenu_pickup_distance());
                holder.txtviewdelivery.setText("Distance");
            }
            else
            {
                holder.txtDeliverytime.setText(listMenu.getMenu_delivery_time());
                holder.txtviewdelivery.setText("Delivery Time");
            }

            if(listMenu.isMenu_image_avail())
                Picasso.with(getActivity()).load(Constants.ImagesUrl + listMenu.getMenu_image()).placeholder(R.drawable.loading).into(holder.imgThumb);
            else
                Picasso.with(getActivity()).load(R.drawable.photonotavail).placeholder(R.drawable.loading).into(holder.imgThumb);

            final Double finalPrice1 = finalPrice;

            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
                holder.txtQty.setText(new DBHelper_New(getActivity()).getMenuQty(String.valueOf(listMenu.getMenu_ID()),"PICKUP"));

                if(Integer.parseInt(holder.txtQty.getText().toString())>0)
                   holder.morefromchef.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.txtQty.setText(new DBHelper_New(getActivity()).getMenuQty(String.valueOf(listMenu.getMenu_ID()),"DELIVERY"));

                if(Integer.parseInt(holder.txtQty.getText().toString())>0)
                    holder.morefromchef.setVisibility(View.VISIBLE);
            }
        }
    }

    class ViewHolderSlider extends RecyclerView.ViewHolder {
        ViewPager pager,minipager;
        InfiniteViewPager mViewPager;
        CirclePageIndicator indicator;
        Button btnDelivery,btnPickup,btnDeals,btnCombos;
        SearchView searchViewPager;
        RelativeLayout rlSearchLayout,pagerLayout;
        TextView searchWord,availchoices;
        ImageView clearSearch;

        public ViewHolderSlider(View itemView) {
            super(itemView);
            pager = (ViewPager) itemView.findViewById(R.id.pager);
            pager.setClipToPadding(false);
            // set padding manually, the more you set the padding the more you see of prev & next page
            pager.setPadding(60, 20, 60, 20);
            // sets a margin b/w individual pages to ensure that there is a gap b/w them
            pager.setPageMargin(20);


            //menu pager

            minipager=(ViewPager) itemView.findViewById(R.id.minipager);

            minipager.setClipToPadding(false);
            minipager.setPadding(20,0,70,0);
            minipager.setPageMargin(10);


            minipager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                private float mLastPositionOffset = 0f;
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    if(positionOffset < mLastPositionOffset && positionOffset < 0.9) {
                        minipager.setCurrentItem(position);
                    } else if(positionOffset > mLastPositionOffset && positionOffset > 0.1) {
                        minipager.setCurrentItem(position+1);
                    }
                    mLastPositionOffset = positionOffset;
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
           /* private class MyPageChangeListener implements OnPageChangeListener {
                private float mLastPositionOffset = 0f;
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }
            }
*/
            /*indicator = (CirclePageIndicator) itemView.findViewById(R.id.indicator);*/

            rlSearchLayout=(RelativeLayout)itemView.findViewById(R.id.rlSearchLayout);
            pagerLayout=(RelativeLayout)itemView.findViewById(R.id.pagerLayout);
            searchWord=(TextView)itemView.findViewById(R.id.searchWord);
            clearSearch=(ImageView)itemView.findViewById(R.id.clearSearch);
            availchoices=(TextView)itemView.findViewById(R.id.availchoices);
            btnPickup = (Button) itemView.findViewById(R.id.btnPickup);
            btnCombos=(Button) itemView.findViewById(R.id.btnCombos);
            btnCombos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setComboFragment();
                }
            });
            btnPickup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setPickUpFragment();
                }
            });
            btnDeals=(Button) itemView.findViewById(R.id.btnDeals);
            clearSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchEnable=false;
                    searchKey="";
                    pagerLayout.setVisibility(View.VISIBLE);
                    pager.setVisibility(View.VISIBLE);
                    minipager.setVisibility(View.VISIBLE);
                    rlSearchLayout.setVisibility(View.GONE);
                    availchoices.setText("Available Choices Near You");
                    searchWord.setText("");
                    loadSliderImages();
                    getFilterDataFromServer();
                }
            });


            if(searchEnable)
            {
                pagerLayout.setVisibility(View.GONE);
                pager.setVisibility(View.GONE);
                minipager.setVisibility(View.GONE);
                rlSearchLayout.setVisibility(View.VISIBLE);
                availchoices.setText("Search results for");
                if(!searchKey.isEmpty())
                    searchWord.setText(searchKey);

            }
            else
            {
                searchEnable=false;
                searchKey="";
                pagerLayout.setVisibility(View.VISIBLE);
                pager.setVisibility(View.VISIBLE);
                minipager.setVisibility(View.VISIBLE);
                rlSearchLayout.setVisibility(View.GONE);
                availchoices.setText("Available Choices Near You");
               // searchWord.setText("");
            }

            if(dealsApply)
                btnDeals.setBackgroundResource(R.drawable.borderbackgroundselected);

            btnDeals.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e("dealsApply"," "+dealsApply);

                    if(dealsApply)
                    {
                        dealsApply=false;
                        loadSliderImages();
                    }
                    else
                    {
                        dealsApply=true;
                        loadSliderImages();
                    }
                }
            });

            btnDelivery=(Button) itemView.findViewById(R.id.btnDelivery);
            btnDelivery.setBackgroundResource(R.drawable.borderbackgroundselected);
            btnDelivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            btnCombos=(Button) itemView.findViewById(R.id.btnDeals);
            btnCombos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dealsApply=true;
                    loadSliderImages();
                }
            });
        }
    }

    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty, txtDeliverytime, txtweigthingms, txtfoodtype,txtMainValue,txtviewdelivery,txtAreaNamePickup,btn_live;
        ImageView imgThumb,ivDealBanner,pickupimage;
        LinearLayout itemLayout,morefromchef;
        Button btninc, btndrc;
        RatingBar rb;
        CardView menuCard;

        public ViewHolderRow(View convertView) {
            super(convertView);
            txtText = (TextView) convertView.findViewById(R.id.txtText);
            txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
            imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
            pickupimage = (ImageView) convertView.findViewById(R.id.pickupimage);
            txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgms);
            txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQty);
            txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtype);
            txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtime);
            txtQty = (TextView) convertView.findViewById(R.id.txtQty);
            ivDealBanner=(ImageView) convertView.findViewById(R.id.ivDealBanner);
            btninc = (Button) convertView.findViewById(R.id.btninc);
            btndrc = (Button) convertView.findViewById(R.id.btndrc);
            txtMainValue=(TextView) convertView.findViewById(R.id.txtMainValue);
            txtviewdelivery=(TextView) convertView.findViewById(R.id.txtviewdelivery);
            txtAreaNamePickup=(TextView) convertView.findViewById(R.id.txtAreaNamePickup);
            itemLayout=(LinearLayout) convertView.findViewById(R.id.itemLayout);
            menuCard=(CardView) convertView.findViewById(R.id.menuCard);
            morefromchef=(LinearLayout)convertView.findViewById(R.id.morefromchef);
            btn_live=(TextView)convertView.findViewById(R.id.btn_live);


            Log.e("Adapter Postion",getAdapterPosition()+""+menuList.size());
          // btninc.setOnClickListener(new btnIncListener(0.0,getAdapterPosition(),txtQty));

            btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Adapter Postion",getAdapterPosition()+""+menuList.size());


                    if(menuList.get(getAdapterPosition()).getMenu_delivery().toLowerCase().contains("pickup"))
                    {
                        int qty = Integer.parseInt(txtQty.getText().toString());

                        if (qty < Integer.parseInt(txtStockQty.getText().toString()) )
                        {

                            if(qty <=0){
                                View adv = getActivity().getLayoutInflater().inflate(R.layout.layout_pickup_alert, null, false);
                                TextView tvdistance=(TextView)adv.findViewById(R.id.tvalertDistance);
                                TextView tvcost=(TextView)adv.findViewById(R.id.tvalertCost);
                                TextView tvTime=(TextView)adv.findViewById(R.id.tvalertTime);


                                tvdistance.setText(menuList.get(getAdapterPosition()).getMenu_pickup_distance());
                                tvcost.setText(menuList.get(getAdapterPosition()).getMenu_price()+" "+Currency);
                                tvTime.setText(menuList.get(getAdapterPosition()).getMenu_delivery_time());


                                new MaterialDialog.Builder(getActivity())
                                        .autoDismiss(false)
                                        .title("Confirm Pickup")
                                        .customView(adv,true)
                                        .positiveText("Confirm")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                btnInc(getAdapterPosition(),txtQty,txtStockQty,morefromchef,btn_live);
                                                dialog.dismiss();
                                            }
                                        })
                                        .negativeText("Cancel")
                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                                dialog.dismiss();
                                            }
                                        })
                                        .show();
                            }
                            else {
                                btnInc(getAdapterPosition(),txtQty,txtStockQty,morefromchef,btn_live);
                            }

                        }
                            else
                            {
                                Toast.makeText(getActivity(), "No More Qunatity available", Toast.LENGTH_SHORT).show();
                            }
                    }
                    else
                    {
                        btnInc(getAdapterPosition(),txtQty,txtStockQty,morefromchef,btn_live);
                    }



                }
            });


            //   final Double finalPrice2 = finalPrice;
            btndrc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnDec(getAdapterPosition(),txtQty,txtStockQty,morefromchef);
                }
            });
        }
    }

    // clear arraylist variables before used
    void clearData() {
      menuList.clear();
    }


    ProgressDialog pd;



    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
        loading_view.setVisibility(View.VISIBLE);
      /*  if (pd == null)
            pd = new ProgressDialog(getActivity());
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();*/
    }

    public void closeDialog() {
        // do something wih the result
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(loading_view.getVisibility()==View.VISIBLE)
                            loading_view.setVisibility(View.GONE);
            }
        });

       /* if (pd != null)
            pd.dismiss();*/
    }

    public void getDataFromServer(String keyword,String DeliveryType,String filterKey) {
        //

        //showProgressDialog();

        String mUrl="";

/*
        if(DeliveryType.equals("Delivery"))
        {
            if(ApplicationLoader.getProchefID().isEmpty() && keyword.isEmpty())
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=delivery&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else if(ApplicationLoader.getProchefID().isEmpty() && keyword.length() > 0)
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=delivery&searchkeyword="+keyword+"&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else
                mUrl="http://128.199.173.98/api/get-menu-prochef.php?accesskey=12345&category_id=00&chefid="+ApplicationLoader.getProchefID()+"&deliverytype=delivery&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();

        }
        else if(DeliveryType.equals("Pickup"))
        {

            if(ApplicationLoader.getProchefID().isEmpty() && keyword.isEmpty())
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else if(ApplicationLoader.getProchefID().isEmpty() && keyword.length() > 0)
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=pickup&searchkeyword="+keyword+"&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else
                mUrl="http://128.199.173.98/api/get-menu-prochef.php?accesskey=12345&category_id=00&chefid="+ApplicationLoader.getProchefID()+"&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        }
        else
        {
            if(ApplicationLoader.getProchefID().isEmpty() && keyword.isEmpty())
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=deliverynpickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else if(ApplicationLoader.getProchefID().isEmpty() && keyword.length() > 0)
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=deliverynpickup&searchkeyword="+keyword+"&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else
                mUrl="http://128.199.173.98/api/get-menu-prochef.php?accesskey=12345&category_id=00&chefid="+ApplicationLoader.getProchefID()+"&deliverytype=deliverynpickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        }*/

/*
        mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php";

        Log.i("murl",mUrl);
        System.out.println(mUrl+"");
        RequestBody formBody;
        if( filterKey.length() > 0)
        {
            if(DeliveryType.equals("Delivery"))
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","delivery")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","pickup")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
            else  if(DeliveryType.equals("Pickup"))
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","pickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("chefid","00")
                            .add("deliverytype","delivery")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
            else
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","deliverynpickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","deliverynpickup")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }

        }
        else //not filters
        {
            if(DeliveryType.equals("Delivery"))
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","delivery")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","pickup")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
            else  if(DeliveryType.equals("Pickup"))
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","pickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("chefid","00")
                            .add("deliverytype","delivery")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
            else
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","deliverynpickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","deliverynpickup")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
        }

        System.out.println(mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Builder()
                .url(mUrl)
                .post(formBody)
                .build();




        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {

                closeDialog();


                final String responseString=response.body().string();
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            parseJSONData(responseString);
                        }
                    });

                }
            }
        });*/
    }


    // method to parse json data from server
    public void parseJSONData(String strResponse) {

        clearData();

        try {
            menuList.add(null);

            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int ii = 0; ii < data.length(); ii++) {

                Menu menuItem=new Menu();
                JSONObject object = data.getJSONObject(ii);

                JSONObject menu = object.getJSONObject("Menu");

                Log.e("Menu", menu.toString());

                Log.e("dealPrice"," "+menu.getString("deal_price"));
                if(dealsApply)
                {
                    if(Integer.parseInt(menu.getString("deal_price"))>0)
                    {

                        menuItem.setMenu_ID(Long.parseLong(menu.getString("Menu_ID")));
                        menuItem.setMenu_name(menu.getString("Menu_name"));
                        menuItem.setMenu_Deal_price(Double.valueOf(menu.getString("deal_price")));
                        menuItem.setMenu_delivery(menu.getString("delivery_type"));
                        menuItem.setMenu_delivery_time(menu.getString("delivery_time"));
                        menuItem.setMenu_food_type(menu.getString("type_of_food"));
                        menuItem.setMenu_image(menu.getString("main_image"));
                        menuItem.setLatitude(menu.getString("latitude"));
                        menuItem.setLongitude(menu.getString("longitude"));
                        menuItem.setLive_url(menu.getString("live_url"));
                        menuItem.setLive_status(menu.getString("live_status"));
                        if(menu.getString("main_image").equals(""))
                        {
                            menuItem.setMenu_image_avail(false);
                        }
                        else
                        {
                            menuItem.setMenu_image_avail(true);
                        }

                        if(menu.getString("delivery_type").equals("Pickup"))
                        {
                            menuItem.setPickup_image_avail(true);
                            menuItem.setMenu_stock_qty(menu.getString("pickup_quantity"));
                            menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("pickup_total"))));
                        }
                        else
                        {
                            menuItem.setPickup_image_avail(false);
                            menuItem.setMenu_stock_qty(menu.getString("Quantity"));
                            menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                        }
                        menuItem.setMenu_pickup_distance(menu.getString("distance"));
                        menuItem.setMenu_pickup_location(menu.getString("pickuplocation"));
                        menuItem.setMenu_weigth_grms(menu.getString("weight_in_grams"));
                        menuItem.setMenu_serves(menu.getString("no_of_serves"));
                        menuItem.setMenu_pickup_image(menu.getString("pickupImageUrl"));

                    }
                }
                else
                {
                    menuItem.setMenu_ID(Long.parseLong(menu.getString("Menu_ID")));
                    menuItem.setMenu_name(menu.getString("Menu_name"));
                    menuItem.setMenu_Deal_price(Double.valueOf(menu.getString("deal_price")));
                    menuItem.setMenu_delivery(menu.getString("delivery_type"));
                    menuItem.setMenu_delivery_time(menu.getString("delivery_time"));
                    menuItem.setMenu_food_type(menu.getString("type_of_food"));
                    menuItem.setMenu_image(menu.getString("main_image"));
                    menuItem.setMenu_pickup_distance(menu.getString("distance"));
                    menuItem.setLatitude(menu.getString("latitude"));
                    menuItem.setLongitude(menu.getString("longitude"));
                    menuItem.setLive_url(menu.getString("live_url"));
                    menuItem.setLive_status(menu.getString("live_status"));

                    if(menu.getString("main_image").equals(""))
                    {
                        menuItem.setMenu_image_avail(false);
                    }
                    else
                    {
                        menuItem.setMenu_image_avail(true);
                    }

                    if(menu.getString("delivery_type").equals("Pickup"))
                    {
                        menuItem.setPickup_image_avail(true);
                        menuItem.setMenu_stock_qty(menu.getString("pickup_quantity"));
                        menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("pickup_total"))));
                    }
                    else
                    {
                        menuItem.setPickup_image_avail(false);
                        menuItem.setMenu_stock_qty(menu.getString("Quantity"));
                        menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                    }

                    menuItem.setMenu_pickup_location(menu.getString("pickuplocation"));
                    menuItem.setMenu_weigth_grms(menu.getString("weight_in_grams"));
                    menuItem.setMenu_serves(menu.getString("no_of_serves"));
                    menuItem.setMenu_pickup_image(menu.getString("pickupImageUrl"));

                }
                menuList.add(menuItem);
            }


            dla.notifyDataSetChanged();
            recycler_view.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper_New dbhelper = new DBHelper_New(ctx);
        //dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }
        // count total order
        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }




    public void loadSliderImages() {
        showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://chefmonster.com/api/getSliders.php").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {

            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                // Log.i("response",response.body());
                //Log.i("response",response.body().string());
                closeDialog();
                if (response.isSuccessful()) {
                    //processSliderImages(response.body().string());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processSliderImages(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });

    }


    private void showBottomSheetFragment(){

        DeliveryTypeSelectionBS deliveryTypeSelectionBS = new DeliveryTypeSelectionBS();
        deliveryTypeSelectionBS.show(getFragmentManager(),"BottomSheet Fragment");
    }


  /*  List<Category> categoryList;
*/
/*
    public void getCategoryDataServer() {
        //
        showProgressDialog();

        String mUrl="";
        mUrl="http://128.199.173.98/api/getCategoryResturant.php?accesskey=12345&getCatsonly=true";
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                closeDialog();

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                            try {
                                processCategorySlider(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                }
            }
        });
    }

    public void processCategorySlider(String response){

        preLoadedCategoryMenus();

        try {

            JSONObject json = new JSONObject(response);
            JSONArray data = json.getJSONArray("data");
            Log.e("CategoryData", data.toString());
            for (int ii = 0; ii < data.length(); ii++) {
                Category category=new Category();
                JSONObject object = data.getJSONObject(ii);

                JSONObject cats = object.getJSONObject("Categories");
                category.setCategoryId(cats.getString("Category_ID"));
                category.setCategoryName(cats.getString("Category_name"));
                category.setCategoryImage(cats.getString("Category_image"));
                category.setCategoryDiscription(cats.getString("Category_discription"));

                categoryList.add(category);

                Log.e("Cats", cats.toString());
            }

            getDataFromServer("","Delivery & Pickup");
        }
        catch (JSONException jse)
        {
            jse.printStackTrace();

        }

    }


    public void preLoadedCategoryMenus()
    {
        categoryList.clear();

        Category preMenus=new Category();


        preMenus.setCategoryId("TU");
        preMenus.setCategoryName("Today's Unique");
        preMenus.setCategoryImage("Today's Unique");
        preMenus.setCategoryDiscription("Today's Unique");

        categoryList.add(preMenus);

        preMenus.setCategoryId("TD");
        preMenus.setCategoryName("Trending");
        preMenus.setCategoryImage("Trending");
        preMenus.setCategoryDiscription("Trending");

        categoryList.add(preMenus);


      *//*  preMenus.setCategoryId("DL");
        preMenus.setCategoryName("Deals");
        preMenus.setCategoryImage("Deals");
        preMenus.setCategoryDiscription("Deals");


        categoryList.add(preMenus);

        preMenus.setCategoryId("CO");
        preMenus.setCategoryName("Combos");
        preMenus.setCategoryImage("Combos");
        preMenus.setCategoryDiscription("Combos");


        categoryList.add(preMenus);*//*

    }*/

    public void processSliderImages(String response) {
        //{"slides": ["slide.jpg","slide.jpg","slide.jpg"]}
        sliderImages.clear();
        try {
            JSONObject jobj = new JSONObject(response);
            JSONArray jSlides = jobj.getJSONArray("slides");
            for (int i = 0; i < jSlides.length(); i++) {
                sliderImages.add(jSlides.getString(i));
            }
            //
            //slidingImageAdapter = new SlidingImageAdapter(getActivity(), sliderImages, imageLoader);
            recycler_view.setAdapter(dla);
            //
            showProgressDialog();
            new CountDownTimer(2000,1000) {


                /** This method will be invoked on finishing or expiring the timer */
                @Override
                public void onFinish() {
                    /** Creates an intent to start new activity */
                    if(searchKey.length()>1)
                    {
                        getDataFromServer(URLEncoder.encode(searchKey),typeOfDelivery,"");
                    }
                    else
                    {
                        getDataFromServer("",typeOfDelivery,"");
                    }
                }

                /** This method will be invoked in every 1000 milli seconds until
                 * this timer is expired.Because we specified 1000 as tick time
                 * while creating this CountDownTimer
                 */
                @Override
                public void onTick(long millisUntilFinished) {

                }
            }.start();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    String typeOfDelivery="Delivery & Pickup";

    ArrayList<String> sliderImages = new ArrayList<>();
    SlidingImageAdapter slidingImageAdapter;

    public class SlidingImageMenuAdapter extends PagerAdapter {
        private ArrayList<String> IMAGES;
        private LayoutInflater inflater;
        private Context context;
        private ImageLoader imageLoader;

        public SlidingImageMenuAdapter(Context context, ImageLoader imageLoader) {
            this.context = context;
            inflater = LayoutInflater.from(context);
            this.imageLoader=imageLoader;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.sliding_category, view, false);

            //assert imageLayout != null;



            final TextView btnMiniSlider1=(TextView)imageLayout.findViewById(R.id.btnMiniSlider1);
            final TextView btnMiniSlider2=(TextView)imageLayout.findViewById(R.id.btnMiniSlider2);

            btnMiniSlider1.setText("Today's Unique");
            btnMiniSlider2.setText("Be more with Combos");


    /*    imageView1.setImageResource(R.drawable.dealsslider);
        imageView2.setImageResource(R.drawable.tranding);*/

            if(position!=0) {

                btnMiniSlider1.setText("Deals");
                btnMiniSlider2.setText("Tranding");
                btnMiniSlider1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("Deals clicked");
                    }
                });
                btnMiniSlider2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("Tranding clicked");
                    }
                });
        /*    imageView1.setImageResource(R.drawable.todayunique);
            imageView2.setImageResource(R.drawable.combomore);*/
            }
            else {
                btnMiniSlider1.setText("Today's Unique");
                btnMiniSlider2.setText("Be more with Combos");
                btnMiniSlider1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("Today's Unique");
                    }
                });
                btnMiniSlider2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("Be more with Combos");
                    }
                });
            }

            // imageView.setImageResource(IMAGES.get(position));
     /*   Log.i("img", Constants.ImagesSlideURL + IMAGES.get(position));
        //Picasso.with(context).load(Constants.ImagesSlideURL + IMAGES.get(position)).resize(200,200).into(imageView);

        imageLoader.DisplayImage(Constants.ImagesSlideURL + IMAGES.get(position), imageView);
        view.addView(imageLayout, 0);*/
            view.addView(imageLayout, 0);

            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }


        public void btnInc(final int adapterPos, TextView txtQty,TextView txtStock, LinearLayout liveLayout, TextView btnLive) {

            Log.e("apos",adapterPos+""+menuList.size());
            double finalPrice=menuList.get(adapterPos).getFinal_price();

            btnLive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(getActivity(), MenuLive.class);
                    intent.putExtra("customerLive","customerLive");
                    intent.putExtra("menuid",menuList.get(adapterPos).getMenu_ID());
                    intent.putExtra("menuname",menuList.get(adapterPos).getMenu_name());
                    intent.putExtra("liveURL",menuList.get(adapterPos).getLive_url());
                    startActivity(intent);


                }
            });

            int qty = Integer.parseInt(txtQty.getText().toString());
            if (qty < 0) {
               // liveLayout.setVisibility(View.GONE);

                //ItemCount--;
            } else {

                    if (qty < Integer.parseInt(txtStock.getText().toString())) {
                        ItemCount++;
                        liveLayout.setVisibility(View.VISIBLE);
                        //    checkout.setVisibility(View.VISIBLE);
                        qty++;
                        double price = finalPrice;
                        double changedPrice = ApplicationLoader.round(price * qty,2);
                        //holder.txtSubText.setText(changedPrice+" "+ActivityMenuList.Currency);
                        txtQty.setText(String.valueOf(qty));
                        //    checkout.setVisibility(View.VISIBLE);
                /*    qty++;
                    double price = finalPrice;
                    double changedPrice = price * qty;*/
                        //holder.txtSubText.setText(changedPrice+" "+ActivityMenuList.Currency);
                        txtQty.setText(String.valueOf(qty));

                        DBHelper_New db = new DBHelper_New(getActivity());
                        //db.openDataBase();
                        long menuid = menuList.get(adapterPos).getMenu_ID();
                        System.out.println(menuid + "**************" + menuList.get(adapterPos).getMenu_delivery().toString());
                        double menuprice = finalPrice;
                        String menuname = menuList.get(adapterPos).getMenu_name();
                        String menuImage = menuList.get(adapterPos).getMenu_image();

                        if (menuList.get(adapterPos).getMenu_Deal_price() > 0) {
                            if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                                if (db.isDataExist(menuid, "DEAL", "DELIVERY", "Single")) {
                                    db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "DELIVERY", "Single",txtStock.getText().toString());
                                } else {
                                    db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "DELIVERY", menuImage, "Single",txtStock.getText().toString());
                                }
                            } else if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {
                                if (db.isDataExist(menuid, "DEAL", "PICKUP", "Single")) {
                                    db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "PICKUP", "Single",txtStock.getText().toString());
                                } else {
                                    db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "PICKUP", menuImage, "Single",txtStock.getText().toString());
                                }
                            }
                        } else {
                            if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                                if (db.isDataExist(menuid, "Home Chef", "DELIVERY", "Single")) {
                                    db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "DELIVERY", "Single",txtStock.getText().toString());
                                } else {
                                    db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "DELIVERY", menuImage, "Single",txtStock.getText().toString());
                                }
                            } else if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {
                                if (db.isDataExist(menuid, "Home Chef", "PICKUP", "Single")) {
                                    db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "PICKUP", "Single",txtStock.getText().toString());
                                } else {
                                    db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "PICKUP", menuImage, "Single",txtStock.getText().toString());
                                }
                            }
                        }


                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        double ChangedPrice = currentCartPrice + menuprice;

                        txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());
                        mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));

                        menuList.get(adapterPos).setItemCount(qty);

                        if (fab.getVisibility() == View.VISIBLE) {
                            FabTransformation.with(fab).transformTo(toolbarFooter);
                        }
                        else
                        {
                            fab.setVisibility(View.VISIBLE);
                        }
                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                     //   db.close();
                    } else {
                        Toast.makeText(getActivity(), "No More Qunatity available", Toast.LENGTH_SHORT).show();
                    }

                    //txtItems.setText(String.valueOf(ItemCount));
                }
            }


        public void btnDec(int adapterPos,TextView txtQty,TextView txtStock, LinearLayout liveLayout) {

            double finalPrice=menuList.get(adapterPos).getFinal_price();


            int qty = Integer.parseInt(txtQty.getText().toString());

            if (qty <= 0) {

                liveLayout.setVisibility(View.GONE);

                double price = finalPrice;
                //	holder.txtSubText.setText(price+" "+DeliveryListFragment.Currency);
                txtQty.setText("0");

                long menuid =  menuList.get(adapterPos).getMenu_ID();
                DBHelper_New db = new DBHelper_New(getActivity());
                //db.openDataBase();

                if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery"))
                    db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY' and id="+menuid);
                else
                    db.deleteDataByQuery("tbl_order","delivery_type='PICKUP' and id="+menuid);

               db.close();

                double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                if (currentCartPrice > 0) {
                    double ChangedPrice = currentCartPrice - price;
                    //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                }

            } else {
                ItemCount--;
                if (qty == 1) {
                    double price = finalPrice;
                    txtQty.setText("0");

                    liveLayout.setVisibility(View.GONE);


                    long menuid =  menuList.get(adapterPos).getMenu_ID();
                    DBHelper_New db = new DBHelper_New(getActivity());
                    //db.openDataBase();

                    if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery"))
                        db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY' and id="+menuid);
                    else
                        db.deleteDataByQuery("tbl_order","delivery_type='PICKUP' and id="+menuid);

                    System.out.println("delivery_type='PICKUP' and id="+menuid);

                   // db.close();

                    double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                    if (currentCartPrice > 0) {
                        double ChangedPrice = currentCartPrice - price;
                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                    }



                    //holder.txtSubText.setText(price+" "+DeliveryListFragment.Currency);
                  /*  DBHelper_New db = new DBHelper_New(getActivity());
                    //db.openDataBase();
                    long menuid =  menuList.get(adapterPos).getMenu_ID();
                    double menuprice = finalPrice;
                    String menuname =  menuList.get(adapterPos).getMenu_name();
                    String menuimage= menuList.get(adapterPos).getMenu_image();
                    if( menuList.get(adapterPos).getMenu_Deal_price() > 0)
                    {
                        if (db.isDataExist(menuid,"DEAL","DELIVERY","Single")) {
                            db.updateData(menuid, qty, (menuprice * qty),"DEAL","Home Chef","DELIVERY","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, (menuprice * qty),"DEAL","Home Chef","DELIVERY",menuimage,"Single",txtStock.getText().toString());
                        }
                    }
                    else
                    {
                        if (db.isDataExist(menuid,"Home Chef","DELIVERY","Single")) {
                            db.updateData(menuid, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY",menuimage,"Single",txtStock.getText().toString());
                        }
                    }
                    double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                    double ChangedPrice = currentCartPrice - price;*/

                    //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                } else {
                    qty--;
                    double price = finalPrice;
                    double changedPrice = price * qty;
                    //holder.txtSubText.setText(changedPrice+" "+DeliveryListFragment.Currency);
                    txtQty.setText(String.valueOf(qty));
                    DBHelper_New db = new DBHelper_New(getActivity());
                    //db.openDataBase();
                    long menuid =  menuList.get(adapterPos).getMenu_ID();
                    double menuprice = finalPrice;
                    String menuname = menuList.get(adapterPos).getMenu_name();
                    String menuImage= menuList.get(adapterPos).getMenu_image();
                    if( menuList.get(adapterPos).getMenu_Deal_price() > 0)
                    {
                        if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                            if (db.isDataExist(menuid,"DEAL","DELIVERY","Single")) {
                                db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","DELIVERY","Single",txtStock.getText().toString());
                            } else {
                                db.addData(menuid, menuname, qty,ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","DELIVERY",menuImage,"Single",txtStock.getText().toString());
                            }
                        }
                        else  if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {

                            if (db.isDataExist(menuid,"DEAL","PICKUP","Single")) {
                                db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","PICKUP","Single",txtStock.getText().toString());
                            } else {
                                db.addData(menuid, menuname, qty,ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","PICKUP",menuImage,"Single",txtStock.getText().toString());
                            }
                        }
                    }
                    else
                    {
                        if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                            if (db.isDataExist(menuid,"Home Chef","DELIVERY","Single")) {
                                db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","DELIVERY","Single",txtStock.getText().toString());
                            } else {
                                db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","DELIVERY",menuImage,"Single",txtStock.getText().toString());
                            }
                        }
                        else  if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {
                            if (db.isDataExist(menuid,"Home Chef","PICKUP","Single")) {
                                db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","PICKUP","Single",txtStock.getText().toString());
                            } else {
                                db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","PICKUP",menuImage,"Single",txtStock.getText().toString());
                            }
                        }
                    }
                    double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                    double ChangedPrice = currentCartPrice - price;

                    //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                    //db.close();
                }
            }
            mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));
            txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());
            //txtItems.setText(String.valueOf(ItemCount));
            menuList.get(adapterPos).setItemCount(qty);
            if (fab.getVisibility() == View.VISIBLE) {
                FabTransformation.with(fab).transformTo(toolbarFooter);
            }

        }

}
