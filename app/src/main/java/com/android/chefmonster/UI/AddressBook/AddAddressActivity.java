package com.android.chefmonster.UI.AddressBook;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.UI.OTP.OTPActivity;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AddAddressActivity  extends AppCompatActivity implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{


    Button btnShowMenu, autodetectlocation;
    EditText etLocation, etCity;
    Button ivCaptureLocation,btnChangeLocaation;
    LinearLayout layoutLocationSelection;
    LinearLayout layoutLocationPick;
    String address, city, state, country, postalCode, knownName;
    LatLng ll;
    List<Address> addresses;
    Geocoder geocoder;
    Double autoLatitude = 0.0, autoLongitude = 0.0;
    LocationManager locman;
    int PLACE_PICKER_REQUEST = 1789;
    Boolean firsttime=true;
    private GoogleApiClient googleApiClient;
    ProgressDialog pd;

    private static final String TAG = "LocationActivity";
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;

    Double finalLatitude,finalLongitude;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        bindUI();
        geocoder = new Geocoder(this, Locale.getDefault());


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle=(TextView)findViewById(R.id.txtTitle);
        txtTitle.setText("Add Address");


        ImageView iv_menu_nav=(ImageView) findViewById(R.id.iv_menu_nav);
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getIntent().getExtras().containsKey("forOtp"))
                {
                    finish();
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }
                else
                {
                    startActivity(new Intent(AddAddressActivity.this,HomePage.class));
                    finish();
                }

            }
        });


        //setting dummy data for My oneplus error

       /* if(Constants.isGPSON(AddAddressActivity.this))
        {

        }
        else
        {
            Toast.makeText(
                    getApplicationContext(),
                    "Please Select Use GPS satellites Option and then click Back button",
                    Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent, 123);
        }
*/

        createLocationRequest();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .enableAutoManage(this, this)
                .build();


        layoutLocationSelection.setVisibility(View.GONE);
        layoutLocationPick.setVisibility(View.VISIBLE);

        autodetectlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(autoLatitude==0.0)
                {
                    Toast.makeText(getApplicationContext(),"Unable to capture location,Tryagain..",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    showProgress("Getting Location Please Wait");

                    try {
                        setLocation(autoLatitude, autoLongitude);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    hideProgress();
                }
            }
        });
        btnShowMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //dummy code for my mobile

               /* etCity.setText("Vizag");
                etLocation.setText("MVP Colony, 6th Sector");*/
                if (etLocation.getText().length() > 0) {

                    startActivity(new Intent(AddAddressActivity.this,HomePage.class));
                    finish();

                  /*  startActivity(new Intent(AddAddressActivity.this, HomePage.class));
                    finish();*/
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Location,Please Try again", Toast.LENGTH_SHORT).show();
                }

            }
        });
        etCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        ivCaptureLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("inclick", "-----------------");
                callGoogleClinet();
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(AddAddressActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });
        btnChangeLocaation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("inclick", "-----------------");
                callGoogleClinet();
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(AddAddressActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });


    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart fired ..............");
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop fired ..............");
        mGoogleApiClient.disconnect();
        Log.d(TAG, "isConnected ...............: " + mGoogleApiClient.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }




    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void callGoogleClinet()
    {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getApplicationContext()).addApi(LocationServices.API).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            PendingResult result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback() {
                @Override
                public void onResult(Result result) {
                    final Status status = result.getStatus();
                    //final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(AddAddressActivity.this, 1000);

                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }

            });

        }
        googleApiClient = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }
    public void showProgress(String title) {
        pd.setMessage(title);
        pd.setIndeterminate(true);
        pd.show();

    }

    public void hideProgress() {
        pd.dismiss();
    }


    public void bindUI() {
        btnShowMenu = (Button) findViewById(R.id.btnShowMenu);
        autodetectlocation = (Button) findViewById(R.id.autodetectlocation);
        pd = new ProgressDialog(AddAddressActivity.this);
        etLocation = (EditText) findViewById(R.id.etLocation);
        etCity = (EditText) findViewById(R.id.etCity);
        ivCaptureLocation = (Button) findViewById(R.id.ivPickLocation);
        btnChangeLocaation = (Button) findViewById(R.id.btnChangeLocaation);
        layoutLocationSelection = (LinearLayout) findViewById(R.id.layoutLocationSelection);
        layoutLocationPick = (LinearLayout) findViewById(R.id.layoutLocationPick);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
               // mGoogleApiClient.disconnect();

                Place place = PlacePicker.getPlace(data, this);
                ll = place.getLatLng();
                finalLatitude=ll.latitude;
                finalLongitude=ll.longitude;
                try {
                    addresses = geocoder.getFromLocation(ll.latitude, ll.longitude, 1);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                address = addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getAddressLine(1);
                city = addresses.get(0).getLocality();
                state = addresses.get(0).getAdminArea();
                country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                knownName = addresses.get(0).getFeatureName();
                final String adrString = address + " ," + city + " ," + state + " ," + country;
                Log.d("Address", addresses.get(0).getAddressLine(0) + "*********" + addresses.get(0).getAddressLine(1) + "******" + addresses.get(0).getAddressLine(2));
                etCity.setText(city);
                etLocation.setText(address);
                layoutLocationSelection.setVisibility(View.VISIBLE);
                layoutLocationPick.setVisibility(View.GONE);
                mGoogleApiClient.disconnect();
            }
        }
        else if(requestCode == 123)
        {
            createLocationRequest();
        }
    }

    public void setLocation(double lat, double lon) throws IOException {
        addresses = geocoder.getFromLocation(lat, lon, 1);
        address = addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getAddressLine(1);
        city = addresses.get(0).getLocality();
        state = addresses.get(0).getAdminArea();
        country = addresses.get(0).getCountryName();
        postalCode = addresses.get(0).getPostalCode();
        knownName = addresses.get(0).getFeatureName();
        final String adrString = address + " ," + city + " ," + state + " ," + country;
        Log.d("Address", addresses.get(0).getAddressLine(0) + "*********" + addresses.get(0).getAddressLine(1) + "******" + addresses.get(0).getAddressLine(2));
        etCity.setText(city);
        etLocation.setText(address);
        layoutLocationSelection.setVisibility(View.VISIBLE);
        layoutLocationPick.setVisibility(View.GONE);
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        autoLatitude = location.getLatitude();
        autoLongitude = location.getLongitude();
        finalLatitude=autoLatitude;
        finalLongitude=autoLongitude;

            try {
                setLocation(autoLatitude, autoLongitude);
                // btnShowMenu.performClick();

            } catch (IOException e) {
                e.printStackTrace();
            }

    }
}
