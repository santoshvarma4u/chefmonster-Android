package com.android.chefmonster.UI.Menu;

import org.json.JSONObject;

/**
 * Created by ChakravartyG on 9/22/2016.
 */

public interface FilterListener {
    public void onApplyFilter(JSONObject result);
}
