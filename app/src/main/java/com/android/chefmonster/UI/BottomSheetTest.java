package com.android.chefmonster.UI;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.chefmonster.R;
import com.android.chefmonster.UI.Menu.DeliveryTypeSelectionBS;

public class BottomSheetTest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_sheet_test);

        Button btnBottomSheetFragment = (Button)findViewById(R.id.button_bottomsheetfragment) ;
        btnBottomSheetFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showBottomSheetFragment();
            }
        });
    }

    private void showBottomSheetFragment(){

        DeliveryTypeSelectionBS deliveryTypeSelectionBS = new DeliveryTypeSelectionBS();
        deliveryTypeSelectionBS.show(getSupportFragmentManager(),"BottomSheet Fragment");
    }


}
