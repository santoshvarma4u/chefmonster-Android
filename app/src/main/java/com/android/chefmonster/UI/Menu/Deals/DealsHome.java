package com.android.chefmonster.UI.Menu.Deals;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.ActivityCart;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.Menu.FilterFragment;
import com.android.chefmonster.UI.Menu.FilterListener;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class DealsHome extends AppCompatActivity implements View.OnClickListener, FilterListener {

    RecyclerView recycler_view;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public ImageLoader imageLoader;
    DecimalFormat formatData;
    private TextView mTxtAmountAdapter;
    private TextView txtItems;
    int ItemCount = 0;
    RelativeLayout checkout;
    LinearLayout optionsLayout;
    //
    Button btnPickup;
    Button btnCartAdapter;
    Button btnFilter;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals_home);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setUpToolbar();

        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        imageLoader = new ImageLoader(getApplicationContext());
        //
        formatData = new DecimalFormat("#.##");
//
        mTxtAmountAdapter = (TextView) findViewById(R.id.txtCartPriceAdapter);
        checkout = (RelativeLayout) findViewById(R.id.lytCheckout);
        btnCartAdapter = (Button) findViewById(R.id.btnCartAdapter);
        btnCartAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(DealsHome.this, ActivityCart.class);
                startActivity(iMyOrder);
            }
        });
        linearLayoutManager = (LinearLayoutManager) recycler_view.getLayoutManager();
        optionsLayout = (LinearLayout) findViewById(R.id.linearLayout10);
        txtItems = (TextView) findViewById(R.id.txtItems);
        btnPickup = (Button) findViewById(R.id.btnDealPickup);
        btnPickup.setOnClickListener(this);
        dla = new DealsDeliveryListAdapter();
        recycler_view.setAdapter(dla);
        getDataFromServer();
    }

    DealsDeliveryListAdapter dla;

    public class DealsDeliveryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        final int ROW = 1;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            switch (viewType) {
                case ROW:
                    View vRow = inflater.inflate(R.layout.dealsitem, viewGroup, false);
                    viewHolder = new ViewHolderRow(vRow);
                    break;
                default:

                    break;
            }
            return viewHolder;


        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            switch (viewHolder.getItemViewType()) {
                case ROW:
                    ViewHolderRow vh2 = (ViewHolderRow) viewHolder;
                    configureViewHolderRow(vh2, position);
                    break;
                default:
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return DealsHome.Menu_ID.size();
        }

        @Override
        public int getItemViewType(int position) {
            return ROW;
        }


        int ItemCount = 0;

        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {
            holder.txtText.setText(DealsHome.Menu_Deal_Name.get(position));
            holder.txtSubText.setText(DealsHome.Currency + " " + DealsHome.Menu_Deal_Price.get(position));
            holder.txtDiscount.setText(DealsHome.Menu_price.get(position) + " ");
            holder.txtDiscount.setPaintFlags(holder.txtDiscount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.txtDeliverytime.setText(DealsHome.Menu_delivery_time.get(position));
            holder.txtfoodtype.setText(DealsHome.Menu_food_type.get(position));
            holder.txtStockQty.setText(DealsHome.Menu_stock_qty.get(position));
            holder.txtweigthingms.setText(DealsHome.Menu_weigth_grms.get(position));
            holder.txtItems.setText("Items: " + DealsHome.Menu_name.get(position));

            imageLoader.DisplayImage(Constants.ImagesUrl + DealsHome.Menu_image.get(position), holder.imgThumb);
            //
            holder.btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    if (qty < 0) {

                        //ItemCount--;
                    } else {
                        ItemCount++;

                        checkout.setVisibility(View.VISIBLE);
                        qty++;
                        double price = Double.parseDouble(DealsHome.Menu_Deal_Price.get(position));
                        double changedPrice = price * qty;
                        //holder.txtSubText.setText(changedPrice+" "+ActivityMenuList.Currency);
                        holder.txtQty.setText(String.valueOf(qty));

                        DBHelper_New db = new DBHelper_New(DealsHome.this);
                        //db.openDataBase();
                        long menuid = DealsHome.Menu_ID.get(position);
                        double menuprice = Double.parseDouble(DealsHome.Menu_Deal_Price.get(position));
                        String menuname = DealsHome.Menu_name.get(position);
                      /*  if (db.isDataExist(menuid,"DEAL","DELIVERY")) {
                            db.updateData(menuid, qty, (menuprice * qty), "DEAL", "Home Chef", "DELIVERY");
                        } else {
                            db.addData(menuid, menuname, qty, (menuprice * qty), "DEAL", "Home Chef", "DELIVERY","dfdsf");
                        }
*/
                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        double ChangedPrice = currentCartPrice + menuprice;

                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        db.close();
                    }
                    txtItems.setText(new DBHelper_New(DealsHome.this).getItemsCount());
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(DealsHome.this)));
                }
            });
            holder.btndrc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    qty--;
/*
                    if (qty > 0)
                        ItemCount--;*/

                    if (qty <= 0) {


                        double price = Double.parseDouble(DealsHome.Menu_Deal_Price.get(position));
                        //	holder.txtSubText.setText(price+" "+DealsHome.Currency);
                        holder.txtQty.setText("0");

                        long menuid = DealsHome.Menu_ID.get(position);
                        DBHelper_New db = new DBHelper_New(DealsHome.this);
                        //db.openDataBase();
                        db.deleteData(menuid);
                        db.close();

                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        if (currentCartPrice > 0) {
                            double ChangedPrice = currentCartPrice - price;
                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        }

                    } else {
                        ItemCount--;
                        if (qty == 1) {
                            double price = Double.parseDouble(DealsHome.Menu_Deal_Price.get(position));
                            holder.txtQty.setText("0");
                            //holder.txtSubText.setText(price+" "+DealsHome.Currency);
                            DBHelper_New db = new DBHelper_New(DealsHome.this);
                            //db.openDataBase();
                            long menuid = DealsHome.Menu_ID.get(position);
                            double menuprice = Double.parseDouble(DealsHome.Menu_Deal_Price.get(position));
                            String menuname = DealsHome.Menu_name.get(position);
                           /* if (db.isDataExist(menuid,"DEAL","DELIVERY")) {
                                db.updateData(menuid, qty, (menuprice * qty), "DEAL", "Home Chef", "DELIVERY");
                            } else {
                                db.addData(menuid, menuname, qty, (menuprice * qty), "DEAL", "Home Chef", "DELIVERY","");
                            }*/
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        } else {

                            double price = Double.parseDouble(DealsHome.Menu_Deal_Price.get(position));
                            double changedPrice = price * qty;
                            //holder.txtSubText.setText(changedPrice+" "+DealsHome.Currency);
                            holder.txtQty.setText(String.valueOf(qty));
                            DBHelper_New db = new DBHelper_New(DealsHome.this);
                            //db.openDataBase();
                            long menuid = DealsHome.Menu_ID.get(position);
                            double menuprice = Double.parseDouble(DealsHome.Menu_Deal_Price.get(position));
                            String menuname = DealsHome.Menu_name.get(position);
                          /*  if (db.isDataExist(menuid,"DEAL","DELIVERY")) {
                                db.updateData(menuid, qty, (menuprice * qty), "DEAL", "Home Chef", "DELIVERY");
                            } else {
                                db.addData(menuid, menuname, qty, (menuprice * qty), "DEAL", "Home Chef", "DELIVERY","");
                            }*/
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        }
                    }
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(DealsHome.this)));
                    txtItems.setText(new DBHelper_New(DealsHome.this).getItemsCount());

                }
            });

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDealPickup:
                startActivity(new Intent(DealsHome.this, DealsPickup.class));
                finish();
                break;
            case R.id.btnFilter:
                FilterFragment.setFilterListener(this);
                startActivity(new Intent(DealsHome.this, FilterFragment.class));

                break;
            default:
                break;
        }
    }

    @Override
    public void onApplyFilter(JSONObject result) {
        Log.i("result", result.toString());
        Toast.makeText(DealsHome.this, result.toString(), Toast.LENGTH_SHORT).show();
    }

    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty, txtDeliverytime, txtweigthingms, txtfoodtype, txtDiscount, txtItems;
        ImageView imgThumb;
        Button btninc, btndrc;
        RatingBar rb;

        public ViewHolderRow(View convertView) {
            super(convertView);
            txtText = (TextView) convertView.findViewById(R.id.txtText);
            txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
            imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
            txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgms);
            txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQty);
            txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtype);
            txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtime);
            txtQty = (TextView) convertView.findViewById(R.id.txtQty);
            btninc = (Button) convertView.findViewById(R.id.btninc);
            btndrc = (Button) convertView.findViewById(R.id.btndrc);
            txtDiscount = (TextView) convertView.findViewById(R.id.txtDiscount);
            txtItems = (TextView) convertView.findViewById(R.id.txtItemname);


        }
    }

    void clearData() {
        Menu_ID.clear();
        Menu_name.clear();
        Menu_price.clear();
        Menu_image.clear();
        Menu_stock_qty.clear();
        Menu_delivery.clear();
        Menu_delivery_time.clear();
        Menu_food_type.clear();
        Menu_serves.clear();
        Menu_weigth_grms.clear();
        Menu_Discount.clear();
        Menu_Deal_Price.clear();
        Menu_Deal_Name.clear();
        Menu_Deal_Type.clear();
    }


    ProgressDialog pd;

    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
        if (pd == null)
            pd = new ProgressDialog(DealsHome.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();
    }

    public void closeDialog() {
        // do something wih the result
        if (pd != null)
            pd.dismiss();
    }

    public void getDataFromServer() {/*

        showProgressDialog();

        String mUrl = "";

        mUrl = "http://128.199.173.98/api/get-deals-combos.php?accesskey=12345&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();


        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                closeDialog();

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });*/
    }


    // method to parse json data from server
    public void parseJSONData(String strResponse) {

        clearData();

        try {
            ///dummy data
/*
            Menu_ID.add(null);
            Menu_name.add(null);
            Menu_price.add(null);
            Menu_image.add(null);//Menu_image
            Menu_stock_qty.add(null);
            Menu_delivery.add(null);
            Menu_delivery_time.add(null);
            Menu_food_type.add(null);
            Menu_serves.add(null);
            Menu_weigth_grms.add(null);
            Menu_Discount.add(null);
            Menu_Deal_Name.add(null);
            Menu_Deal_Price.add(null);
            Menu_Deal_Type.add(null);*/


            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int ii = 0; ii < data.length(); ii++) {
                int i = 0;
                JSONObject object = data.getJSONObject(i);

                JSONObject menu = object.getJSONObject("MenuActivity");

                Log.e("MenuActivity", menu.toString());

                Menu_ID.add(Long.parseLong(menu.getString("Menu_ID")));
                Menu_name.add(menu.getString("Menu_name"));
                Menu_price.add(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                Menu_image.add(menu.getString("deal_image"));//Menu_image
                Menu_stock_qty.add(menu.getString("Quantity"));
                Menu_delivery.add(menu.getString("delivery_type"));
                Menu_delivery_time.add(menu.getString("delivery_time"));
                Menu_food_type.add(menu.getString("type_of_food"));
                Menu_serves.add(menu.getString("no_of_serves"));
                Menu_weigth_grms.add(menu.getString("weight_in_grams"));
                Menu_Deal_Type.add(menu.getString("deal_type"));
                Menu_Deal_Price.add(menu.getString("deal_price"));
                Menu_Deal_Name.add(menu.getString("deal_name"));
                Menu_Discount.add(menu.getString("discount_type"));

            }
            dla.notifyDataSetChanged();


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    static ArrayList<Long> Menu_ID = new ArrayList<Long>();
    static ArrayList<String> Menu_name = new ArrayList<String>();
    static ArrayList<Double> Menu_price = new ArrayList<Double>();
    static ArrayList<String> Menu_image = new ArrayList<String>();
    static ArrayList<String> Menu_stock_qty = new ArrayList<String>();
    static ArrayList<String> Menu_food_type = new ArrayList<String>();
    static ArrayList<String> Menu_delivery = new ArrayList<String>();
    static ArrayList<String> Menu_weigth_grms = new ArrayList<String>();
    static ArrayList<String> Menu_serves = new ArrayList<String>();
    static ArrayList<String> Menu_delivery_time = new ArrayList<String>();
    static ArrayList<String> Menu_Discount = new ArrayList<String>();
    static ArrayList<String> Menu_Deal_Name = new ArrayList<String>();
    static ArrayList<String> Menu_Deal_Type = new ArrayList<String>();
    static ArrayList<String> Menu_Deal_Price = new ArrayList<String>();


    static String Currency = "₹";

    @Override
    public void onResume() {
        super.onResume();
        // loadSliderImages();


    }

    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper_New dbhelper = new DBHelper_New(ctx);
        //dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }


    public LinearLayoutManager linearLayoutManager;

    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left_black_48dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


}
