package com.android.chefmonster.UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.CartTabbedActivity;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.InfinateViewpager.InfiniteViewPager;
import com.android.chefmonster.LiveVideo.MenuLive;
import com.android.chefmonster.Locations.SelectLocationActivity;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.Menu.DeliveryTypeSelection;
import com.android.chefmonster.UI.Menu.Filters.Cusines;
import com.android.chefmonster.UI.Menu.MapAddrView;
import com.android.chefmonster.UI.Menu.Menu;
import com.android.chefmonster.UI.Menu.MoreFromThisChef;
import com.android.chefmonster.UI.Menu.OnLoadMoreListener;
import com.android.chefmonster.UI.Menu.Search.SearchActivity;
import com.android.chefmonster.UI.Menu.SlidingImageAdapter;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.circlepageidicator.CirclePageIndicator;
import com.konifar.fab_transformation.FabTransformation;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.iwgang.countdownview.CountdownView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;

public class HomePage extends AppCompatActivity {

    RecyclerView recycler_view;
    public static String Currency="₹";
    List<Menu> menuList;
    DecimalFormat formatData;
    boolean dealsApply;
    DeliveryListAdapter dla;
    View view;
    int ItemCount = 0;
    private TextView mTxtAmountAdapter;
    private TextView txtItems;
    private TextView tv_imchef, tv_username, tv_userEmail, tv_btmhomechef,tv_btmprochef,tv_btmcancel;
    TextView txtChefSelection;
    private BottomSheetBehavior mBottomSheetBehavior;
    TextView btndelivery,btnpickup,btmcancel;
    View filterView;
    JSONObject filterResult;
    BottomSheetDialog mfilterBottomSheet ;
    FloatingActionButton fab;
    BottomSheetDialog bottomSheetDialog;
    Toolbar toolbarFooter;
    TextView tv_checkout;
    SwipeRefreshLayout mSwipeRefreshLayout;
    LinearLayout lytnoitems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        initView();
    }
    
    private void initView()
    {
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HomePage.this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.smoothScrollToPosition(0);

        lytnoitems=(LinearLayout)findViewById(R.id.lytnoitems);
        lytnoitems.setVisibility(View.GONE);
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swiperefresh);


        tv_checkout=(TextView)findViewById(R.id.tv_checkout);
        tv_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(HomePage.this, CartTabbedActivity.class);
                startActivity(iMyOrder);
            }
        });


        menuList=new ArrayList<>();//init Menu
        formatData = new DecimalFormat("#.##");
        dealsApply=false;
        dla = new DeliveryListAdapter();

        mTxtAmountAdapter = (TextView) findViewById(R.id.txtCartPriceAdapter);
        mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(HomePage.this)));

        txtItems=(TextView) findViewById(R.id.txtItems);
        txtItems.setText(new DBHelper_New(HomePage.this).getItemsCount());

        toolbarFooter=(Toolbar) findViewById(R.id.toolbar_footer);
        fab=(FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(HomePage.this, CartTabbedActivity.class);
                startActivity(iMyOrder);
            }
        });



        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState==SCROLL_STATE_IDLE)
                    fab.setVisibility(View.VISIBLE);
                else
                    fab.setVisibility(View.GONE);
            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (fab.getVisibility() != View.VISIBLE) {
                    FabTransformation.with(fab).transformFrom(toolbarFooter);
                }
                //  fab.setVisibility(View.GONE);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(R.color.appColor,R.color.optionColor);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                typeOfDelivery="Delivery & Pickup";
                txtChefSelection.setText(typeOfDelivery);
                loadSliderImages();
                if(mSwipeRefreshLayout.isRefreshing())
                {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });


       /* View view1 =  getLayoutInflater().inflate(R.layout.deliverytype_bottomsheet, null);

        bottomSheetDialog = new BottomSheetDialog(HomePage.this);
        bottomSheetDialog.setContentView(view1);

        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        btndelivery=(TextView)view1.findViewById(R.id.btndelivery) ;
        btnpickup=(TextView)view1.findViewById(R.id.btnpickup) ;
        btmcancel=(TextView)view1.findViewById(R.id.btmcancel) ;


        if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
        {
            btndelivery.setText("Delivery");
            btnpickup.setText("Pickup");
        }
        else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
        {
            btndelivery.setText("Delivery & Pickup");
            btnpickup.setText("Pickup");
        }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
        {
            btndelivery.setText("Delivery");
            btnpickup.setText("Delivery & Pickup");
        }

        btndelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                {
                    ApplicationLoader.setDeliveryType("delivery");
                    txtChefSelection.setText("Delivery");
                    btndelivery.setText("Delivery");
                    btnpickup.setText("Pickup");
                }
                else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
                {
                    ApplicationLoader.setDeliveryType("deliverynpickup");
                    txtChefSelection.setText("Delivery & Pickup");
                    btndelivery.setText("Delivery");
                    btnpickup.setText("Pickup");

                }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
                {
                    ApplicationLoader.setDeliveryType("delivery");
                    txtChefSelection.setText("Delivery");
                    btndelivery.setText("Delivery");
                    btnpickup.setText("Delivery & Pickup");

                }
                bottomSheetDialog.dismiss();
            }
        });
        btnpickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                {
                    ApplicationLoader.setDeliveryType("pickup");
                    txtChefSelection.setText("Pickup");
                    btndelivery.setText("Delivery");
                    btnpickup.setText("Delivery & Pickup");
                }
                else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
                {
                    ApplicationLoader.setDeliveryType("pickup");
                    txtChefSelection.setText("Pickup");
                    btndelivery.setText("Delivery");
                    btnpickup.setText("Delivery & Pickup");

                }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
                {
                    ApplicationLoader.setDeliveryType("deliverynpickup");
                    txtChefSelection.setText("Delivery & Pickup");
                    btndelivery.setText("Delivery");
                    btnpickup.setText("Pickup");
                }
                bottomSheetDialog.dismiss();
            }
        });
        btmcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });
*/
        setUpToolbar();
        //loadSliderImages();

        filterResult = new JSONObject();
        //loading filters
        final View viewBottomSheet = findViewById(R.id.bottom_sheet) ;
        bottomSheetBehavior = BottomSheetBehavior.from(viewBottomSheet) ;
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        initFilterView(viewBottomSheet);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //check either user submitted last order feedback or not

        if(!ApplicationLoader.getUserEmail().isEmpty())
        {



            HttpLoggingInterceptor interceptor;
            interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build();

            FormBody.Builder formBody;

            formBody = new FormBody.Builder()
                    .add("checkFeedback", "true")
                    .add("useremail", ApplicationLoader.getUserEmail());

            RequestBody formBodyA = formBody.build();
            Request request = new Request.Builder()
                    .url(Constants.SubmitFeedback)
                    .post(formBodyA)
                    .build();
            showProgressDialog();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    closeDialog();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if(response.isSuccessful())
                    {
                        closeDialog();
                        String orderid=response.body().string();
                        if(!orderid.equalsIgnoreCase("0"))
                        {
                            //redirect feedback activity with orderid
                            Intent intent=new Intent(HomePage.this,FeedbackActivity.class);
                            intent.putExtra("orderid",orderid);
                            startActivityForResult(intent,2454);
                        }
                        else
                        {
                          runOnUiThread(new Runnable() {
                              @Override
                              public void run() {
                                  loadSliderImages();
                              }
                          });

                        }
                    }
                }
            });
        }
        else
        {
            loadSliderImages();
        }

    }

    private BottomSheetBehavior bottomSheetBehavior ;

    public int SEARCH_REQ_CODE=2454;

    private void setUpToolbar() {

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);
        TextView City=(TextView) findViewById(R.id.txtLocalCity);
        TextView AddressLocal=(TextView) findViewById(R.id.txtLocalAddress);
        ImageView iv_menu_nav=(ImageView) findViewById(R.id.iv_menu_nav);
        txtChefSelection=(TextView) findViewById(R.id.txtChefSelection);
        City.setText(ApplicationLoader.getUserCity());
        AddressLocal.setText(ApplicationLoader.getUserLocation());
        ImageView ab_filter=(ImageView) findViewById(R.id.ab_filter);
        ImageView imgSearch=(ImageView)findViewById(R.id.imgSearch);
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(HomePage.this, SearchActivity.class),SEARCH_REQ_CODE);
            }
        });

        ab_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             //   Toast.makeText(HomePage.this, "FilterCick", Toast.LENGTH_SHORT).show();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        tv_btmhomechef=(TextView) findViewById(R.id.btmhomechef);
        tv_btmprochef=(TextView) findViewById(R.id.btmprochef);
        tv_btmcancel=(TextView) findViewById(R.id.btmcancel);


        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(HomePage.this,MenuActivity.class);
                startActivity(intent);
                // mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
        City.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomePage.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });
        AddressLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomePage.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });
        txtChefSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(HomePage.this, DeliveryTypeSelection.class),DEL_CODE);
              //  bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            }
        });


        if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
        {
            txtChefSelection.setText("Delivery & Pickup");

        }
        else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
        {
            txtChefSelection.setText("Delivery");

        }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
        {
            txtChefSelection.setText("Pickup");
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        HomePage.this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1234&& resultCode==RESULT_OK){


        }else if(requestCode == SEARCH_REQ_CODE && resultCode == RESULT_OK){
            //pass data to fragment from here
            String menuId= data.getExtras().getString("MenuId").toString();
            String menuName=data.getExtras().getString("MenuName").toString();
            searchRequest(menuId,menuName,"","");

        }
        else if(requestCode == DEL_CODE && resultCode == RESULT_OK){
            //pass data to fragment from here

            String typeofdelivery= data.getExtras().getString("typeofdelivery").toString();

            if(typeofdelivery.equalsIgnoreCase("Delivery & Pickup"))
            {
                ApplicationLoader.setDeliveryType("deliverynpickup");
            }
            else if(typeofdelivery.equalsIgnoreCase("Delivery"))
            {
                ApplicationLoader.setDeliveryType("delivery");
            }else if(typeofdelivery.equalsIgnoreCase("Pickup"))
            {
                ApplicationLoader.setDeliveryType("pickup");
            }

            txtChefSelection.setText(typeofdelivery);
           // loadSliderImages();
            getDataFromServer("",ApplicationLoader.getDeliveryType(),"","","");

        }
    }

    boolean searchEnable=false;
    String  searchKeyFromHome="";
    public String searchKey="",sChefid="";


    public void searchRequest(String sMenuId,String sMenuName,String sChefId,String sliderRequest){

        if(sMenuName.length() >= 3){
            searchKey=sMenuName;
            searchKeyFromHome=sMenuName;
            searchEnable=true;
            //loadSliderImages();
            getDataFromServer(sMenuName,"Delivery & Pickup","","","");
        }
        else if(sChefId.length() > 0){
         /*   searchKey=sMenuName;
            searchKeyFromHome=sMenuName;*/
            searchEnable=true;
            sChefid=sChefId;
            //loadSliderImages();
            getDataFromServer("","Delivery & Pickup","",sChefId,"");
        }
        else if(sliderRequest.length() > 0){
         /*   searchKey=sMenuName;
            searchKeyFromHome=sMenuName;*/
            searchEnable=true;
            sChefid=sChefId;
            isSliderRequest=sliderRequest;
            //loadSliderImages();
            getDataFromServer("","Delivery & Pickup","","",sliderRequest);
        }

        else
        {
           // loadSliderImages();
            getDataFromServer("","Delivery & Pickup","","","");
        }
    }


    String isSliderRequest="";

    int DEL_CODE=5;

    public class DeliveryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        final int SLIDER = 0, ROW = 1;
        private final int VIEW_TYPE_LOADING = 3;

        private OnLoadMoreListener mOnLoadMoreListener;

        private boolean isLoading;
        private int visibleThreshold = 5;
        private int lastVisibleItem, totalItemCount;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            switch (viewType) {
                case SLIDER:
                    View vSlider = inflater.inflate(R.layout.item_viewpager, viewGroup, false);
                    viewHolder = new ViewHolderSlider(vSlider);
                    break;

                case ROW:
                    View vRow = inflater.inflate(R.layout.menu_items_new, viewGroup, false);
                    viewHolder = new ViewHolderRow(vRow);
                    break;
                default:
                    break;
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

            switch (viewHolder.getItemViewType()) {
                case SLIDER:
                    ViewHolderSlider vh1 = (ViewHolderSlider) viewHolder;
                    configureViewHolderSlider(vh1, position);
                    break;
                case ROW:
                    ViewHolderRow vh2 = (ViewHolderRow) viewHolder;
                    configureViewHolderRow(vh2, position);
                    break;
                default:

                    break;
            }
        }
        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return menuList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? SLIDER : ROW ;
        }


        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        public void configureViewHolderSlider(final ViewHolderSlider viewHolder, int position) {

           viewHolder.pager.setAdapter(new SlidingImageAdapter(HomePage.this, sliderImages));
            viewHolder.minipager.setAdapter(new SlidingImageMenuAdapter(HomePage.this));

            viewHolder.pager.setCurrentItem(1, true);

        }

        int ItemCount = 0;
        Menu listMenu;
        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {

            Double finalPrice=0.0;
            listMenu=menuList.get(position);


            holder.txtText.setText(listMenu.getMenu_name());

            holder.txtfoodtype.setText(listMenu.getMenu_food_type());
            holder.txtStockQty.setText(listMenu.getMenu_stock_qty());
            holder.txtweigthingms.setText(listMenu.getMenu_weigth_grms()+" gms");
            holder.txtAreaNamePickup.setText(listMenu.getMenu_pickup_location());

            if(position > 1)
            {
                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0,10,0,0);
                holder.menuCard.setLayoutParams(params);
            }
            if(listMenu.getMenu_Deal_price() > 0)
            {
                holder.ivDealBanner.setVisibility(View.VISIBLE);
                holder.txtMainValue.setVisibility(View.VISIBLE);
                holder.txtMainValue.setText(listMenu.getMenu_price() + " " + Currency);
                holder.txtMainValue.setPaintFlags(holder.txtMainValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                Double afterDiscount=listMenu.getMenu_price()-listMenu.getMenu_Deal_price();
                holder.txtSubText.setText(String.valueOf(ApplicationLoader.round(afterDiscount,2)) + " " + Currency);
                menuList.get(position).setFinal_price(ApplicationLoader.round(afterDiscount,2));
                //finalPrice=afterDiscount;
            }
            else
            {
                holder.txtSubText.setText(listMenu.getMenu_price() + " " + Currency);
                finalPrice=listMenu.getMenu_price();
                menuList.get(position).setFinal_price(finalPrice);
            }

            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
//                holder.itemLayout.setVisibility(View.VISIBLE);//commented as per jii request
                holder.pickupimage.setVisibility(View.VISIBLE);

                String mapurl=Constants.googleMapUrl+listMenu.getMenu_pickup_image();

                ImageLoader iml=new ImageLoader(HomePage.this);

                iml.DisplayImage(mapurl, holder.pickupimage);
                   // Picasso.with(HomePage.this).load(mapurl).into(holder.pickupimage);

                Log.d("testUrl", mapurl);
                //imageLoader.DisplayImage(listMenu.getMenu_pickup_image(), holder.pickupimage);
            }

            holder.pickupimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(HomePage.this,MapAddrView.class);
                    i.putExtra("pimage",Constants.googleMapUrl+listMenu.getMenu_pickup_image());
                    i.putExtra("paddress",listMenu.getMenu_pickup_location().toString());
                    i.putExtra("platitude",listMenu.getLatitude());
                    i.putExtra("plongitude",listMenu.getLongitude());
                    startActivity(i);
                }
            });


            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
                holder.txtDeliverytime.setText(listMenu.getMenu_pickup_distance());
                holder.txtviewdelivery.setText("Distance");
            }
            else
            {
                holder.txtDeliverytime.setText(listMenu.getMenu_delivery_time());
                holder.txtviewdelivery.setText("Delivery Time");
            }

            if(listMenu.isMenu_image_avail())
                Picasso.with(HomePage.this).load(Constants.ImagesUrl + listMenu.getMenu_image()).placeholder(R.drawable.loading).into(holder.imgThumb);
            else
                Picasso.with(HomePage.this).load(R.drawable.photonotavail).placeholder(R.drawable.loading).into(holder.imgThumb);

            if(listMenu.getMenu_food_type().equalsIgnoreCase("veg"))
                Picasso.with(HomePage.this).load(R.drawable.leaf).placeholder(R.drawable.loading).into(holder.vegicon);
            else
                Picasso.with(HomePage.this).load(R.drawable.nonvegnew).placeholder(R.drawable.loading).into(holder.vegicon);




            final Double finalPrice1 = finalPrice;

            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
                holder.txtQty.setText(new DBHelper_New(HomePage.this).getMenuQty(String.valueOf(listMenu.getMenu_ID()),"PICKUP"));

                if(Integer.parseInt(holder.txtQty.getText().toString())>0)
                    holder.morefromchef.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.txtQty.setText(new DBHelper_New(HomePage.this).getMenuQty(String.valueOf(listMenu.getMenu_ID()),"DELIVERY"));

                if(Integer.parseInt(holder.txtQty.getText().toString())>0)
                    holder.morefromchef.setVisibility(View.VISIBLE);
            }


            //mins*60000=milliseconds


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String timenow = simpleDateFormat.format(new Date());


            SimpleDateFormat timeToCookDateFormat = new SimpleDateFormat("dd-MM-yyyy");

            String timetocook="";


            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            try {
                Date date = parseFormat.parse(listMenu.getTimetocook());
                timetocook=timeToCookDateFormat.format(new Date())+" "+displayFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            System.out.println(timenow + " timenow ");
            System.out.println(timetocook + " timetocook, ");

            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            Date d1 = null;
            Date d2 = null;

            try {
                d1 = format.parse(timenow);
                d2 = format.parse(timetocook);

                //in milliseconds
                long diff = d2.getTime() - d1.getTime();

                long diffSeconds = diff / 1000 % 60;
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000) % 24;
                long diffDays = diff / (24 * 60 * 60 * 1000);

                System.out.println(diffDays + " days, ");
                System.out.println(diffHours + " hours, ");
                System.out.println(diffMinutes + " minutes, ");
                System.out.println(diffSeconds + " seconds.");


                if(diffMinutes>0 || diffHours>0)
                {
                    holder.timerlayout.setVisibility(View.VISIBLE);
                    if(diffHours>0)
                    {
                        long convertedmins=diffHours*60;

                        long totalmins=convertedmins+diffMinutes;

                        long totalMilliseconds=totalmins*60000;
                        holder.cdv.start(totalMilliseconds);
                    }
                    else if(diffMinutes > 0)
                    {
                        long totalMilliseconds=diffMinutes*60000;
                        holder.cdv.start(totalMilliseconds);

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
      }

        }





    }

    class ViewHolderSlider extends RecyclerView.ViewHolder {
        ViewPager pager,minipager;
        InfiniteViewPager mViewPager;
        CirclePageIndicator indicator;
        Button btnDelivery,btnPickup,btnDeals,btnCombos;
        SearchView searchViewPager;
        RelativeLayout rlSearchLayout,pagerLayout;
        TextView searchWord,availchoices;
        ImageView clearSearch;


        public ViewHolderSlider(View itemView) {
            super(itemView);
            pager = (ViewPager) itemView.findViewById(R.id.pager);
            pager.setClipToPadding(false);
            // set padding manually, the more you set the padding the more you see of prev & next page
            pager.setPadding(60, 20, 60, 20);
            // sets a margin b/w individual pages to ensure that there is a gap b/w them
            pager.setPageMargin(20);


            //menu pager

            minipager=(ViewPager) itemView.findViewById(R.id.minipager);

            minipager.setClipToPadding(false);
            minipager.setPadding(20,0,70,0);
            minipager.setPageMargin(10);


            minipager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                private float mLastPositionOffset = 0f;
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    if(positionOffset < mLastPositionOffset && positionOffset < 0.9) {
                        minipager.setCurrentItem(position);
                    } else if(positionOffset > mLastPositionOffset && positionOffset > 0.1) {
                        minipager.setCurrentItem(position+1);
                    }
                    mLastPositionOffset = positionOffset;
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            rlSearchLayout=(RelativeLayout)itemView.findViewById(R.id.rlSearchLayout);
            pagerLayout=(RelativeLayout)itemView.findViewById(R.id.pagerLayout);
            searchWord=(TextView)itemView.findViewById(R.id.searchWord);
            clearSearch=(ImageView)itemView.findViewById(R.id.clearSearch);
            availchoices=(TextView)itemView.findViewById(R.id.availchoices);
            btnPickup = (Button) itemView.findViewById(R.id.btnPickup);
            btnCombos=(Button) itemView.findViewById(R.id.btnCombos);
            btnDeals=(Button) itemView.findViewById(R.id.btnDeals);

            clearSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchEnable=false;
                    searchKey="";
                    pagerLayout.setVisibility(View.VISIBLE);
                    pager.setVisibility(View.VISIBLE);
                    minipager.setVisibility(View.VISIBLE);
                    rlSearchLayout.setVisibility(View.GONE);
                    availchoices.setText("Available Choices Near You");
                    searchWord.setText("");
                    loadSliderImages();
                    getFilterDataFromServer();
                }
            });


            if(searchEnable)
            {
                pagerLayout.setVisibility(View.GONE);
                pager.setVisibility(View.GONE);
                minipager.setVisibility(View.GONE);
                rlSearchLayout.setVisibility(View.VISIBLE);
                if(sChefid.length() > 0)
                {
                    availchoices.setText("");
                    searchWord.setText("Menu from selected chef");
                }
                else if(isSliderRequest.length()>0)
                {
                    availchoices.setText("Filter Results");
                    searchWord.setText(isSliderRequest.toString().toUpperCase());
                }
                else
                {
                    availchoices.setText("Search results");
                    if(!searchKey.isEmpty())
                        searchWord.setText(searchKey);
                }
            }
            else
            {
                searchEnable=false;
                searchKey="";
                pagerLayout.setVisibility(View.VISIBLE);
                pager.setVisibility(View.VISIBLE);
                minipager.setVisibility(View.VISIBLE);
                rlSearchLayout.setVisibility(View.GONE);
                availchoices.setText("Available Choices Near You");
                // searchWord.setText("");
            }

            btnDelivery=(Button) itemView.findViewById(R.id.btnDelivery);
            btnDelivery.setBackgroundResource(R.drawable.borderbackgroundselected);
            btnDelivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            btnCombos=(Button) itemView.findViewById(R.id.btnDeals);
            btnCombos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                 /*   dealsApply=true;
                   */
                 loadSliderImages();
                }
            });
        }
    }

    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty,
                txtDeliverytime, txtweigthingms, txtfoodtype,txtMainValue,txtviewdelivery,txtAreaNamePickup,btn_live;
        ImageView imgThumb,ivDealBanner,pickupimage,vegicon;
        LinearLayout itemLayout,morefromchef,timerlayout;
        Button btninc, btndrc;
        RatingBar rb;
        CardView menuCard;

        CountdownView cdv;


        public ViewHolderRow(View convertView) {
            super(convertView);
            txtText = (TextView) convertView.findViewById(R.id.txtText);
            txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
            imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
            pickupimage = (ImageView) convertView.findViewById(R.id.pickupimage);
            txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgms);
            txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQty);
            txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtype);
            txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtime);
            txtQty = (TextView) convertView.findViewById(R.id.txtQty);
            ivDealBanner=(ImageView) convertView.findViewById(R.id.ivDealBanner);
            vegicon=(ImageView) convertView.findViewById(R.id.vegicon);
            btninc = (Button) convertView.findViewById(R.id.btninc);
            btndrc = (Button) convertView.findViewById(R.id.btndrc);
            txtMainValue=(TextView) convertView.findViewById(R.id.txtMainValue);
            txtviewdelivery=(TextView) convertView.findViewById(R.id.txtviewdelivery);
            txtAreaNamePickup=(TextView) convertView.findViewById(R.id.txtAreaNamePickup);
            itemLayout=(LinearLayout) convertView.findViewById(R.id.itemLayout);
            menuCard=(CardView) convertView.findViewById(R.id.menuCard);
            morefromchef=(LinearLayout)convertView.findViewById(R.id.morefromchef);
            timerlayout=(LinearLayout)convertView.findViewById(R.id.timerlayout);

            btn_live=(TextView)convertView.findViewById(R.id.btn_live);

            cdv = (CountdownView)convertView.findViewById(R.id.countdownview);


            Log.e("Adapter Postion",getAdapterPosition()+""+menuList.size());
            // btninc.setOnClickListener(new btnIncListener(0.0,getAdapterPosition(),txtQty));

            btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Adapter Postion",getAdapterPosition()+""+menuList.size());


                    if(menuList.get(getAdapterPosition()).getMenu_delivery().toLowerCase().contains("pickup"))
                    {
                        int qty = Integer.parseInt(txtQty.getText().toString());

                        if (qty < Integer.parseInt(txtStockQty.getText().toString()) )
                        {

                            if(qty <=0){
                                View adv = HomePage.this.getLayoutInflater().inflate(R.layout.layout_pickup_alert, null, false);
                                TextView tvdistance=(TextView)adv.findViewById(R.id.tvalertDistance);
                                TextView tvcost=(TextView)adv.findViewById(R.id.tvalertCost);
                                TextView tvTime=(TextView)adv.findViewById(R.id.tvalertTime);
                                TextView pickupwarning=(TextView)adv.findViewById(R.id.pickupwarning);


                                tvdistance.setText(menuList.get(getAdapterPosition()).getMenu_pickup_distance());
                                tvcost.setText(menuList.get(getAdapterPosition()).getMenu_price()+" "+Currency);
                                tvTime.setText(menuList.get(getAdapterPosition()).getMenu_delivery_time());

                                if(Double.parseDouble(menuList.get(getAdapterPosition()).getDistanceValue().toString()) > 5)
                                    pickupwarning.setVisibility(View.VISIBLE);


                                new MaterialDialog.Builder(HomePage.this)
                                        .autoDismiss(false)
                                        .title("Confirm Pickup")
                                        .customView(adv,true)
                                        .positiveText("Confirm")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                btnInc(getAdapterPosition(),txtQty,txtStockQty,morefromchef,btn_live,menuList.get(getAdapterPosition()).getMenu_Chef_id());
                                                dialog.dismiss();
                                            }
                                        })
                                        .negativeText("Cancel")
                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                                dialog.dismiss();
                                            }
                                        })
                                        .show();
                            }
                            else {
                                btnInc(getAdapterPosition(),txtQty,txtStockQty,morefromchef,btn_live,menuList.get(getAdapterPosition()).getMenu_Chef_id());
                            }
                        }
                        else
                        {
                            Toast.makeText(HomePage.this, "No More Quantity available", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        btnInc(getAdapterPosition(),txtQty,txtStockQty,morefromchef,btn_live,menuList.get(getAdapterPosition()).getMenu_Chef_id());
                    }
                }
            });

            //   final Double finalPrice2 = finalPrice;
            btndrc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnDec(getAdapterPosition(),txtQty,txtStockQty,morefromchef);
                }
            });
        }
    }
    public void getDataFromServer(String keyword,String DeliveryType,String filterKey,String filterChefId,String sliderFilter) {
        //
        Log.i("Delivery Type",DeliveryType);
        Log.i("keyword",keyword);

        showProgressDialog();

        String mUrl = Constants.MenuAPI;

        Log.i("murl", mUrl);
        FormBody.Builder formBody =new FormBody.Builder();

        if (filterKey.length() > 0) {
            if (DeliveryType.equalsIgnoreCase("Delivery")) {
                if (keyword.isEmpty() && sliderFilter.isEmpty()) {

                    formBody.add("keyword", filterKey)
                            .add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "delivery")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
                else if (!sliderFilter.isEmpty()) {

                    if(sliderFilter.equalsIgnoreCase("unique"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("unique", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("trending"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("trending", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("deals"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("deals", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }else if(sliderFilter.equalsIgnoreCase("combos"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("combos", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }

                }

                else {

                    formBody.add("keyword", filterKey)
                            .add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "pickup")
                            .add("searchkeyword", keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
            } else if (DeliveryType.equalsIgnoreCase("Pickup")) {
                if (keyword.isEmpty() && sliderFilter.isEmpty()) {
                    formBody.add("keyword", filterKey)
                            .add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "pickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
                else if (!sliderFilter.isEmpty()) {

                    if(sliderFilter.equalsIgnoreCase("unique"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("unique", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("trending"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("trending", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("deals"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("deals", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }else if(sliderFilter.equalsIgnoreCase("combos"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("combos", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }

                }
                else {

                    formBody.add("keyword", filterKey)
                            .add("accesskey", "12345")
                            .add("chefid", "00")
                            .add("deliverytype", "delivery")
                            .add("searchkeyword", keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
            } else {
                if (keyword.isEmpty()) {
                    formBody.add("keyword", filterKey)
                            .add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "deliverynpickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
                else if (!sliderFilter.isEmpty()) {

                    if(sliderFilter.equalsIgnoreCase("unique"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("unique", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("trending"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("trending", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("deals"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("deals", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }else if(sliderFilter.equalsIgnoreCase("combos"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("combos", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }

                }
                else {

                    formBody.add("keyword", filterKey)
                            .add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "deliverynpickup")
                            .add("searchkeyword", keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
            }

        }
        else if(filterChefId.length() > 0) //not filters
        {
            if (DeliveryType.equalsIgnoreCase("Delivery")) {
                if (keyword.isEmpty()) {
                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "delivery")
                            .add("filterchefid", filterChefId)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                } else {

                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "pickup")
                            .add("filterchefid", filterChefId)
                            .add("searchkeyword", keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
            } else if (DeliveryType.equalsIgnoreCase("Pickup")) {
                if (keyword.isEmpty()) {
                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("filterchefid", filterChefId)
                            .add("deliverytype", "pickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                } else {

                    formBody.add("accesskey", "12345")
                            .add("chefid", "00")
                            .add("filterchefid", filterChefId)
                            .add("deliverytype", "delivery")
                            .add("searchkeyword", keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
            } else {
                if (keyword.isEmpty()) {
                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("filterchefid", filterChefId)
                            .add("deliverytype", "deliverynpickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                } else {

                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("filterchefid", filterChefId)
                            .add("deliverytype", "deliverynpickup")
                            .add("searchkeyword", keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
            }
        } else //not filters
        {
            if (DeliveryType.equalsIgnoreCase("Delivery")) {
                if (keyword.isEmpty() && sliderFilter.isEmpty()) {
                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "delivery")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
                else if (!sliderFilter.isEmpty()) {

                    if(sliderFilter.equalsIgnoreCase("unique"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("unique", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("trending"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("trending", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("deals"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("deals", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }else if(sliderFilter.equalsIgnoreCase("combos"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "delivery")
                                .add("combos", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }

                }
                else {

                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "pickup")
                            .add("searchkeyword", keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
            } else if (DeliveryType.equalsIgnoreCase("Pickup")) {
                if (keyword.isEmpty() &&  sliderFilter.isEmpty()) {
                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "pickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
                else if (!sliderFilter.isEmpty()) {

                    if(sliderFilter.equalsIgnoreCase("unique"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "pickup")
                                .add("unique", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("trending"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "pickup")
                                .add("trending", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("deals"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "pickup")
                                .add("deals", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }else if(sliderFilter.equalsIgnoreCase("combos"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "pickup")
                                .add("combos", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }

                }
                else {

                    formBody.add("accesskey", "12345")
                            .add("chefid", "00")
                            .add("deliverytype", "delivery")
                            .add("searchkeyword", keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
            } else {
                if (keyword.isEmpty() && sliderFilter.isEmpty()) {
                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "deliverynpickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
                else if (!sliderFilter.isEmpty()) {

                    if(sliderFilter.equalsIgnoreCase("unique"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "deliverynpickup")
                                .add("unique", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("trending"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "deliverynpickup")
                                .add("trending", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }
                    else if(sliderFilter.equalsIgnoreCase("deals"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "deliverynpickup")
                                .add("deals", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }else if(sliderFilter.equalsIgnoreCase("combos"))
                    {
                        formBody.add("keyword", filterKey)
                                .add("accesskey", "12345")
                                .add("category_id", "00")
                                .add("deliverytype", "deliverynpickup")
                                .add("combos", "1")
                                .add("latitude", ApplicationLoader.getUserLat())
                                .add("longitude", ApplicationLoader.getUserLon());
                    }

                }
                else {

                    formBody.add("accesskey", "12345")
                            .add("category_id", "00")
                            .add("deliverytype", "deliverynpickup")
                            .add("searchkeyword", keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude", ApplicationLoader.getUserLon());
                }
            }
        }

       HttpLoggingInterceptor interceptor;
        interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
        RequestBody formBodyA = formBody.build();
        Request request = new Request.Builder()
                .url(mUrl)
                .post(formBodyA)
                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                               // Log.e("response", response.body().string());
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });
    }

    void clearData() {
        menuList.clear();
    }


    public void parseJSONData(String strResponse) {

        clearData();

        try {
            menuList.add(null);

            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            if(data.length() <= 0){
                lytnoitems.setVisibility(View.VISIBLE);
            }

            for (int ii = 0; ii < data.length(); ii++) {

                Menu menuItem=new Menu();
                JSONObject object = data.getJSONObject(ii);

                JSONObject menu = object.getJSONObject("Menu");

                Log.e("Menu", menu.toString());

                Log.e("dealPrice"," "+menu.getString("deal_price"));
                if(dealsApply)
                {
                    if(Integer.parseInt(menu.getString("deal_price"))>0)
                    {
                        menuItem.setMenu_ID(Long.parseLong(menu.getString("Menu_ID")));
                        menuItem.setMenu_name(menu.getString("Menu_name"));
                        menuItem.setMenu_Deal_price(Double.valueOf(menu.getString("deal_price")));
                        menuItem.setMenu_delivery(menu.getString("delivery_type"));
                        menuItem.setMenu_delivery_time(menu.getString("delivery_time"));
                        menuItem.setMenu_food_type(menu.getString("type_of_food"));
                        menuItem.setMenu_image(menu.getString("main_image"));
                        menuItem.setLatitude(menu.getString("latitude"));
                        menuItem.setLongitude(menu.getString("longitude"));
                        menuItem.setLive_url(menu.getString("live_url"));
                        menuItem.setMenu_Chef_id(menu.getString("chefid"));
                        menuItem.setDistanceValue(menu.getString("distanceValue"));
                        menuItem.setLive_status(menu.getString("live_status"));
                        menuItem.setTimetocook(menu.getString("timetocook"));
                        if(menu.getString("main_image").equals(""))
                        {
                            menuItem.setMenu_image_avail(false);
                        }
                        else
                        {
                            menuItem.setMenu_image_avail(true);
                        }

                        if(menu.getString("delivery_type").equals("Pickup"))
                        {
                            menuItem.setPickup_image_avail(true);
                            menuItem.setMenu_stock_qty(menu.getString("pickup_quantity"));
                            menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("pickup_total"))));
                        }
                        else
                        {
                            menuItem.setPickup_image_avail(false);
                            menuItem.setMenu_stock_qty(menu.getString("Quantity"));
                            menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                        }
                        menuItem.setMenu_pickup_distance(menu.getString("distance"));
                        menuItem.setMenu_pickup_location(menu.getString("pickuplocation"));
                        menuItem.setMenu_weigth_grms(menu.getString("weight_in_grams"));
                        menuItem.setMenu_serves(menu.getString("no_of_serves"));
                        menuItem.setMenu_pickup_image(menu.getString("pickupImageUrl"));

                    }
                }
                else
                {
                    menuItem.setMenu_ID(Long.parseLong(menu.getString("Menu_ID")));
                    menuItem.setMenu_name(menu.getString("Menu_name"));
                    menuItem.setMenu_Deal_price(Double.valueOf(menu.getString("deal_price")));
                    menuItem.setMenu_delivery(menu.getString("delivery_type"));
                    menuItem.setMenu_delivery_time(menu.getString("delivery_time"));
                    menuItem.setMenu_food_type(menu.getString("type_of_food"));
                    menuItem.setMenu_image(menu.getString("main_image"));
                    menuItem.setMenu_pickup_distance(menu.getString("distance"));
                    menuItem.setLatitude(menu.getString("latitude"));
                    menuItem.setMenu_Chef_id(menu.getString("chefid"));
                    menuItem.setLongitude(menu.getString("longitude"));
                    menuItem.setLive_url(menu.getString("live_url"));
                    menuItem.setLive_status(menu.getString("live_status"));
                    menuItem.setTimetocook(menu.getString("timetocook"));
                    menuItem.setDistanceValue(menu.getString("distanceValue"));

                    if(menu.getString("main_image").equals(""))
                    {
                        menuItem.setMenu_image_avail(false);
                    }
                    else
                    {
                        menuItem.setMenu_image_avail(true);
                    }

                    if(menu.getString("delivery_type").equals("Pickup"))
                    {
                        menuItem.setPickup_image_avail(true);
                        menuItem.setMenu_stock_qty(menu.getString("pickup_quantity"));
                        menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("pickup_total"))));
                    }
                    else
                    {
                        menuItem.setPickup_image_avail(false);
                        menuItem.setMenu_stock_qty(menu.getString("Quantity"));
                        menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                    }

                    menuItem.setMenu_pickup_location(menu.getString("pickuplocation"));
                    menuItem.setMenu_weigth_grms(menu.getString("weight_in_grams"));
                    menuItem.setMenu_serves(menu.getString("no_of_serves"));
                    menuItem.setMenu_pickup_image(menu.getString("pickupImageUrl"));

                }
                menuList.add(menuItem);
            }

            recycler_view.setAdapter(dla);
            dla.notifyDataSetChanged();
            recycler_view.setVisibility(View.VISIBLE);
       /*     InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow();*/
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        closeDialog();
    }
    ProgressDialog pd;

    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
      //  loading_view.setVisibility(View.VISIBLE);
        if (pd == null)
            pd = new ProgressDialog(HomePage.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();
    }

    public void closeDialog() {
        // do something wih the result
      /*  getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(loading_view.getVisibility()==View.VISIBLE)
                    loading_view.setVisibility(View.GONE);
            }
        });*/

        if (pd != null)
            pd.dismiss();
    }

    String typeOfDelivery="Delivery & Pickup";

    ArrayList<String> sliderImages = new ArrayList<>();
    SlidingImageAdapter slidingImageAdapter;

    public class SlidingImageMenuAdapter extends PagerAdapter {
        private ArrayList<String> IMAGES;
        private LayoutInflater inflater;
        private Context context;
        private ImageLoader imageLoader;

        public SlidingImageMenuAdapter(Context context) {
            this.context = context;
            inflater = LayoutInflater.from(context);

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.sliding_category, view, false);

            //assert imageLayout != null;

            final LinearLayout llout1=(LinearLayout)imageLayout.findViewById(R.id.llout1) ;
            final LinearLayout llout2=(LinearLayout)imageLayout.findViewById(R.id.llout2) ;

            final TextView btnMiniSlider1=(TextView)imageLayout.findViewById(R.id.btnMiniSlider1);
            final TextView btnMiniSlider2=(TextView)imageLayout.findViewById(R.id.btnMiniSlider2);

            btnMiniSlider1.setText("Today's Unique");
            btnMiniSlider2.setText("Be more with Combos");



            if(position!=0) {

                btnMiniSlider1.setText("Deals");
                btnMiniSlider2.setText("Trending");

        /*    imageView1.setImageResource(R.drawable.todayunique);
            imageView2.setImageResource(R.drawable.combomore);*/
            }
            else {
                btnMiniSlider1.setText("Today's Unique");
                btnMiniSlider2.setText("Be more with Combos");

            }

            btnMiniSlider1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(btnMiniSlider1.getText().toString().equalsIgnoreCase("Deals"))
                    {

                      //  Toast.makeText(HomePage.this, "Deals", Toast.LENGTH_SHORT).show();
                        HomePage.this.searchRequest("","","","deals");
                    }
                    else  if(btnMiniSlider1.getText().toString().equalsIgnoreCase("Today's Unique"))
                    {
                       // Toast.makeText(HomePage.this, "Today's Unique", Toast.LENGTH_SHORT).show();
                        HomePage.this.searchRequest("","","","unique");
                    }
                }
            });


            btnMiniSlider2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(btnMiniSlider2.getText().toString().equalsIgnoreCase("Trending"))
                    {
                        //Toast.makeText(HomePage.this, "Trending", Toast.LENGTH_SHORT).show();
                        HomePage.this.searchRequest("","","","trending");
                    }
                    else  if(btnMiniSlider2.getText().toString().equalsIgnoreCase("Be more with Combos"))
                    {
                       // Toast.makeText(HomePage.this, "Be more with Combos", Toast.LENGTH_SHORT).show();
                        HomePage.this.searchRequest("","","","combos");
                    }
                }
            });


            llout1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(btnMiniSlider1.getText().toString().equalsIgnoreCase("Deals"))
                    {
                      //  Toast.makeText(HomePage.this, "Deals", Toast.LENGTH_SHORT).show();
                        HomePage.this.searchRequest("","","","deals");
                    }
                    else  if(btnMiniSlider1.getText().toString().equalsIgnoreCase("Today's Unique"))
                    {
                       // Toast.makeText(HomePage.this, "Today's Unique", Toast.LENGTH_SHORT).show();
                        HomePage.this.searchRequest("","","","unique");
                    }
                }
            });

            llout2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(btnMiniSlider1.getText().toString().equalsIgnoreCase("Trending"))
                    {
                       // Toast.makeText(HomePage.this, "Deals", Toast.LENGTH_SHORT).show();
                        HomePage.this.searchRequest("","","","trending");
                    }
                    else  if(btnMiniSlider1.getText().toString().equalsIgnoreCase("Be more with Combos"))
                    {
                       // Toast.makeText(HomePage.this, "Today's Unique", Toast.LENGTH_SHORT).show();
                        HomePage.this.searchRequest("","","","combos");
                    }
                }
            });
            view.addView(imageLayout, 0);

            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }


    public void btnInc(final int adapterPos, TextView txtQty, TextView txtStock, LinearLayout liveLayout, TextView btnLive, final String chefid) {

        Log.e("apos",adapterPos+""+menuList.size());
        double finalPrice=menuList.get(adapterPos).getFinal_price();

        btnLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*   Intent intent=new Intent(HomePage.this, MenuLive.class);
                intent.putExtra("customerLive","customerLive");
                intent.putExtra("menuid",menuList.get(adapterPos).getMenu_ID());
                intent.putExtra("menuname",menuList.get(adapterPos).getMenu_name());
                intent.putExtra("liveURL",menuList.get(adapterPos).getLive_url());
                startActivity(intent);*/

                Toast.makeText(HomePage.this, "Live not presenting by chef", Toast.LENGTH_SHORT).show();


            }
        });

        liveLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent=new Intent(HomePage.this, MoreFromThisChef.class);
                intent.putExtra("chefid",menuList.get(adapterPos).getMenu_Chef_id());
                startActivity(intent);*/
                //Toast.makeText(HomePage.this, chefid, Toast.LENGTH_SHORT).show();
               searchRequest("","",chefid,"");
            }
        });

        int qty = Integer.parseInt(txtQty.getText().toString());
        if (qty < 0) {

        } else {

            if (qty < Integer.parseInt(txtStock.getText().toString())) {
                ItemCount++;
                liveLayout.setVisibility(View.VISIBLE);

                qty++;
                double price = finalPrice;
                double changedPrice = ApplicationLoader.round(price * qty,2);

                txtQty.setText(String.valueOf(qty));

                txtQty.setText(String.valueOf(qty));

                DBHelper_New db = new DBHelper_New(HomePage.this);
                //db.openDataBase();
                long menuid = menuList.get(adapterPos).getMenu_ID();
                System.out.println(menuid + "**************" + menuList.get(adapterPos).getMenu_delivery().toString());
                double menuprice = finalPrice;
                String menuname = menuList.get(adapterPos).getMenu_name();
                String menuImage = menuList.get(adapterPos).getMenu_image();

                if (menuList.get(adapterPos).getMenu_Deal_price() > 0) {
                    if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                        if (db.isDataExist(menuid, "DEAL", "DELIVERY", "Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "DELIVERY", "Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "DELIVERY", menuImage, "Single",txtStock.getText().toString());
                        }
                    } else if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {
                        if (db.isDataExist(menuid, "DEAL", "PICKUP", "Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "PICKUP", "Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "PICKUP", menuImage, "Single",txtStock.getText().toString());
                        }
                    }
                } else {
                    if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                        if (db.isDataExist(menuid, "Home Chef", "DELIVERY", "Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "DELIVERY", "Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "DELIVERY", menuImage, "Single",txtStock.getText().toString());
                        }
                    } else if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {
                        if (db.isDataExist(menuid, "Home Chef", "PICKUP", "Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "PICKUP", "Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "PICKUP", menuImage, "Single",txtStock.getText().toString());
                        }
                    }
                }




                 double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());
                 double ChangedPrice = currentCartPrice + menuprice;

                txtItems.setText(new DBHelper_New(HomePage.this).getItemsCount());
                mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(HomePage.this)));

                menuList.get(adapterPos).setItemCount(qty);
                if (fab.getVisibility() == View.VISIBLE) {
                    FabTransformation.with(fab).transformTo(toolbarFooter);
                }
                else
                {
                    fab.setVisibility(View.VISIBLE);
                }


            } else {
                Toast.makeText(HomePage.this, "No More Quantity available", Toast.LENGTH_SHORT).show();
            }

        }
    }


    public void btnDec(int adapterPos,TextView txtQty,TextView txtStock, LinearLayout liveLayout) {

        double finalPrice=menuList.get(adapterPos).getFinal_price();


        int qty = Integer.parseInt(txtQty.getText().toString());

        if (qty <= 0) {

            liveLayout.setVisibility(View.GONE);

            double price = finalPrice;

            txtQty.setText("0");

            long menuid =  menuList.get(adapterPos).getMenu_ID();
            DBHelper_New db = new DBHelper_New(HomePage.this);

            if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery"))
                db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY' and id="+menuid);
            else
                db.deleteDataByQuery("tbl_order","delivery_type='PICKUP' and id="+menuid);

            db.close();

            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

            if (currentCartPrice > 0) {
                double ChangedPrice = currentCartPrice - price;
                //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
            }

        } else {
            ItemCount--;
            if (qty == 1) {
                double price = finalPrice;
                txtQty.setText("0");

                liveLayout.setVisibility(View.GONE);


                long menuid =  menuList.get(adapterPos).getMenu_ID();
                DBHelper_New db = new DBHelper_New(HomePage.this);
                //db.openDataBase();

                if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery"))
                    db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY' and id="+menuid);
                else
                    db.deleteDataByQuery("tbl_order","delivery_type='PICKUP' and id="+menuid);

                System.out.println("delivery_type='PICKUP' and id="+menuid);


                double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                if (currentCartPrice > 0) {
                    double ChangedPrice = currentCartPrice - price;

                }

            } else {
                qty--;
                double price = finalPrice;
                double changedPrice = price * qty;

                txtQty.setText(String.valueOf(qty));
                DBHelper_New db = new DBHelper_New(HomePage.this);
                //db.openDataBase();
                long menuid =  menuList.get(adapterPos).getMenu_ID();
                double menuprice = finalPrice;
                String menuname = menuList.get(adapterPos).getMenu_name();
                String menuImage= menuList.get(adapterPos).getMenu_image();
                if( menuList.get(adapterPos).getMenu_Deal_price() > 0)
                {
                    if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                        if (db.isDataExist(menuid,"DEAL","DELIVERY","Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","DELIVERY","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty,ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","DELIVERY",menuImage,"Single",txtStock.getText().toString());
                        }
                    }
                    else  if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {

                        if (db.isDataExist(menuid,"DEAL","PICKUP","Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","PICKUP","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty,ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","PICKUP",menuImage,"Single",txtStock.getText().toString());
                        }
                    }
                }
                else
                {
                    if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                        if (db.isDataExist(menuid,"Home Chef","DELIVERY","Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","DELIVERY","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","DELIVERY",menuImage,"Single",txtStock.getText().toString());
                        }
                    }
                    else  if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {
                        if (db.isDataExist(menuid,"Home Chef","PICKUP","Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","PICKUP","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","PICKUP",menuImage,"Single",txtStock.getText().toString());
                        }
                    }
                }
                double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                double ChangedPrice = currentCartPrice - price;

                //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                //db.close();
            }
        }
        mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(HomePage.this)));
        txtItems.setText(new DBHelper_New(HomePage.this).getItemsCount());
        //txtItems.setText(String.valueOf(ItemCount));
        menuList.get(adapterPos).setItemCount(qty);
        if (fab.getVisibility() == View.VISIBLE) {
            FabTransformation.with(fab).transformTo(toolbarFooter);
        }

    }

    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper_New dbhelper = new DBHelper_New(ctx);
        //dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }
        // count total order
        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }

    public void loadSliderImages() {
        showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://chefmonster.com/api/getSliders.php").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if (response.isSuccessful()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processSliderImages(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }


        });

    }

    public void processSliderImages(String response) {

        //{"slides": ["slide.jpg","slide.jpg","slide.jpg"]}
        sliderImages.clear();
        try {
            JSONObject jobj = new JSONObject(response);
            JSONArray jSlides = jobj.getJSONArray("slides");
            for (int i = 0; i < jSlides.length(); i++) {
                sliderImages.add(jSlides.getString(i));
            }
            //
            //slidingImageAdapter = new SlidingImageAdapter(getActivity(), sliderImages, imageLoader);
            recycler_view.setAdapter(dla);
            //

            new CountDownTimer(2000,1000) {


                /** This method will be invoked on finishing or expiring the timer */
                @Override
                public void onFinish() {

                    getDataFromServer("",typeOfDelivery,"","","");
                    /** Creates an intent to start new activity */
                  /*  if(searchKey.length()>1)
                    {
                        getDataFromServer(URLEncoder.encode(searchKey),typeOfDelivery);
                    }
                    else
                    {
                        getDataFromServer("",typeOfDelivery);
                    }*/
                }

                /** This method will be invoked in every 1000 milli seconds until
                 * this timer is expired.Because we specified 1000 as tick time
                 * while creating this CountDownTimer
                 */
                @Override
                public void onTick(long millisUntilFinished) {

                }
            }.start();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    RecyclerView rv_cusines;
    TextView filter_apply,filter_reset;
    CheckBox cb_distanceFilter,cb_ratingFilter,cb_vegFilter,cb_splRecipeFilter,cb_100Range,cb_200Range,cb_300Range,cb_AboveRange;
    RadioButton rb_distanceFilter,rb_ratingFilter,rb_vegFilter,rb_splRecipeFilter;
    RadioGroup sortGroup;
    LinearLayout ll_distanceFilter,ll_ratingFilter,ll_vegFilter,ll_splRecipeFilter,ll_100Range,ll_200Range,ll_300Range,ll_AboveRange;
    public void initFilterView(View view)
    {
        rv_cusines=(RecyclerView)view.findViewById(R.id.rvcusines);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HomePage.this);
        rv_cusines.setLayoutManager(mLayoutManager);
        rv_cusines.setItemAnimator(new DefaultItemAnimator());
        // rv_cusines.smoothScrollToPosition(0);

        // TextView filter_apply,filter_reset;
        //CheckBox cb_distanceFilter,cb_ratingFilter,cb_vegFilter,cb_splRecipeFilter,cb_100Range,cb_200Range,cb_300Range,cb_AboveRange;

        filter_apply=(TextView)view.findViewById(R.id.filter_apply);
        filter_reset=(TextView)view.findViewById(R.id.filter_reset);
        cb_distanceFilter=(CheckBox)view.findViewById(R.id.cb_distanceFilter);
        cb_ratingFilter=(CheckBox)view.findViewById(R.id.cb_ratingFilter);
        cb_vegFilter=(CheckBox)view.findViewById(R.id.cb_vegFilter);
        cb_splRecipeFilter=(CheckBox)view.findViewById(R.id.cb_splRecipeFilter);
        cb_100Range=(CheckBox)view.findViewById(R.id.cb_100Range);
        cb_200Range=(CheckBox)view.findViewById(R.id.cb_200Range);
        cb_300Range=(CheckBox)view.findViewById(R.id.cb_300Range);
        cb_AboveRange=(CheckBox)view.findViewById(R.id.cb_AboveRange);


        ll_distanceFilter=(LinearLayout)view.findViewById(R.id.ll_distanceFilter);
        ll_ratingFilter=(LinearLayout)view.findViewById(R.id.ll_ratingFilter);
        ll_vegFilter=(LinearLayout)view.findViewById(R.id.ll_vegFilter);
        ll_splRecipeFilter=(LinearLayout)view.findViewById(R.id.ll_splRecipeFilter);
        ll_100Range=(LinearLayout)view.findViewById(R.id.ll_100Range);
        ll_200Range=(LinearLayout)view.findViewById(R.id.ll_200Range);
        ll_300Range=(LinearLayout)view.findViewById(R.id.ll_300Range);
        ll_AboveRange=(LinearLayout)view.findViewById(R.id.ll_AboveRange);

        ll_distanceFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_distanceFilter.performClick();
            }
        });
        ll_ratingFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_ratingFilter.performClick();
            }
        });
        ll_vegFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_vegFilter.performClick();
            }
        });
        ll_splRecipeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_splRecipeFilter.performClick();
            }
        });
        ll_100Range.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_100Range.performClick();
            }
        });
        ll_200Range.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_200Range.performClick();
            }
        });
        ll_300Range.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_300Range.performClick();
            }
        });
        ll_AboveRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_AboveRange.performClick();
            }
        });

        sortGroup=(RadioGroup)view.findViewById(R.id.sortGroup);

        rb_distanceFilter=(RadioButton) view.findViewById(R.id.rb_distanceFilter);
        rb_ratingFilter=(RadioButton)view.findViewById(R.id.rb_ratingFilter);
        rb_vegFilter=(RadioButton)view.findViewById(R.id.rb_vegFilter);
        rb_splRecipeFilter=(RadioButton)view.findViewById(R.id.rb_splRecipeFilter);



        cb_distanceFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {

                    if (isChecked) {

                        filterResult.put("sort", "distance");
                        // cb_distanceFilter.setButtonDrawable(R.drawable.filter_checked);
                        cb_ratingFilter.setChecked(false);
                        cb_vegFilter.setChecked(false);
                        cb_splRecipeFilter.setChecked(false);

                        filterResult.remove("rating");
                        filterResult.remove("veg");
                        filterResult.remove("spl");

                    } else {
                        filterResult.remove("sort");

                        filterResult.put("sort", "");
                        // cb_distanceFilter.setButtonDrawable(R.drawable.filter_unchecked);
                        cb_distanceFilter.setChecked(false);
                        cb_ratingFilter.setChecked(false);
                        cb_vegFilter.setChecked(false);
                        cb_splRecipeFilter.setChecked(false);

                    }
                }
                catch (JSONException ex)
                {

                }

            }
        });
        cb_ratingFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("sort", "rating");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // cb_ratingFilter.setButtonDrawable(R.drawable.filter_checked);

                    cb_distanceFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                    filterResult.remove("dist");
                    filterResult.remove("veg");
                    filterResult.remove("spl");
                }
                else
                {
                    try {
                        filterResult.put("sort", "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //  cb_ratingFilter.setButtonDrawable(R.drawable.filter_unchecked);
                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                }
            }
        });
        cb_vegFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("sort", "Veg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //  cb_vegFilter.setButtonDrawable(R.drawable.filter_checked);
//
                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                    filterResult.remove("rating");
                    filterResult.remove("dist");
                    filterResult.remove("spl");
                }
                else
                {
                    try {
                        filterResult.remove("sort");
                        filterResult.remove("veg");
                        filterResult.remove("Veg");
                        filterResult.put("sort", "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //cb_vegFilter.setButtonDrawable(R.drawable.filter_unchecked);
                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                }
            }
        });
        cb_splRecipeFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("sort", "SplRecipe");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // cb_splRecipeFilter.setButtonDrawable(R.drawable.filter_checked);

                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    filterResult.remove("rating");
                    filterResult.remove("veg");
                    filterResult.remove("dist");
                }
                else
                {
                    try {
                        filterResult.remove("sort");
                        filterResult.put("spl", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // cb_splRecipeFilter.setButtonDrawable(R.drawable.filter_unchecked);
                    cb_distanceFilter.setChecked(false);
                    cb_ratingFilter.setChecked(false);
                    cb_vegFilter.setChecked(false);
                    cb_splRecipeFilter.setChecked(false);
                }
            }
        });
        cb_100Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                {
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_AboveRange.setChecked(false);
                    cb_100Range.setChecked(false);
                    Toast.makeText(HomePage.this, "Make sure the delivery type as Pickup or Delivery", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(isChecked)
                    {
                        try {
                            filterResult.put("range", "100");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // cb_100Range.setButtonDrawable(R.drawable.filter_checked);
                        cb_200Range.setChecked(false);
                        cb_300Range.setChecked(false);
                        cb_AboveRange.setChecked(false);

                    }
                    else
                    {
                        try {
                            filterResult.put("range", "0");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cb_200Range.setChecked(false);
                        cb_300Range.setChecked(false);
                        cb_100Range.setChecked(false);
                        cb_AboveRange.setChecked(false);
                    }
                }

            }
        });
        cb_200Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                {
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_AboveRange.setChecked(false);
                    cb_100Range.setChecked(false);
                    Toast.makeText(HomePage.this, "Make sure the delivery type as Pickup or Delivery", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(isChecked)
                    {
                        try {
                            filterResult.put("range", "200");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cb_100Range.setChecked(false);
                        cb_300Range.setChecked(false);
                        cb_AboveRange.setChecked(false);

                    }
                    else
                    {
                        try {
                            filterResult.put("range", "0");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cb_200Range.setChecked(false);
                        cb_300Range.setChecked(false);
                        cb_100Range.setChecked(false);
                        cb_AboveRange.setChecked(false);
                    }
                }

            }
        });
        cb_300Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                {
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_AboveRange.setChecked(false);
                    cb_100Range.setChecked(false);
                    Toast.makeText(HomePage.this, "Make sure the delivery type as Pickup or Delivery", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(isChecked)
                    {
                        try {
                            filterResult.put("range", "300");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cb_200Range.setChecked(false);
                        cb_100Range.setChecked(false);
                        cb_AboveRange.setChecked(false);
                    }
                    else
                    {
                        try {
                            filterResult.put("range", "0");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cb_200Range.setChecked(false);
                        cb_300Range.setChecked(false);
                        cb_100Range.setChecked(false);
                        cb_AboveRange.setChecked(false);
                    }
                }

            }
        });
        cb_AboveRange.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                {
                    cb_200Range.setChecked(false);
                    cb_300Range.setChecked(false);
                    cb_AboveRange.setChecked(false);
                    cb_100Range.setChecked(false);
                    Toast.makeText(HomePage.this, "Make sure the delivery type as Pickup or Delivery", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(isChecked)
                    {
                        try {
                            filterResult.put("range", "301");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cb_200Range.setChecked(false);
                        cb_300Range.setChecked(false);
                        cb_100Range.setChecked(false);
                    }
                    else
                    {
                        try {
                            filterResult.put("range", "0");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cb_200Range.setChecked(false);
                        cb_300Range.setChecked(false);
                        cb_100Range.setChecked(false);
                        cb_AboveRange.setChecked(false);
                    }
                }

            }
        });


        filter_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    filterResult.put("cusinefilter",checkedList.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i("cusinefilter",checkedList.toString());
                getDataFromServer("","",filterResult.toString(),"","");

                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

            }
        });

        filter_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                txtChefSelection.setText("Delivery & Pickup");
                startActivity(new Intent(HomePage.this,HomePage.class));
                finish();

            }
        });

        getFilterDataFromServer();
        cusineList=new ArrayList<>();
        fla=new CuisineListAdapter();
    }

    List<Cusines> cusineList;


    CuisineListAdapter fla;
    List<String> checkedList = new ArrayList<>();

    public class CuisineListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_cusine, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return cusineList.size();
        }
        Cusines listCusine;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCusine=cusineList.get(position);
            holder.CusineName.setText(listCusine.getCuisineName());
            holder.CusineId=listCusine.getCuisineId();


        }
    }


    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView CusineName;
        String CusineId;
        CheckBox cb_select;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            CusineName=(TextView)itemView.findViewById(R.id.tv_cusine);
            cb_select=(CheckBox)itemView.findViewById(R.id.cb_cusineitem);
            cb_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        cb_select.setButtonDrawable(R.drawable.filter_checked);
                        checkedList.add(cusineList.get(getAdapterPosition()).getCuisineId());
                    }
                    else {

                        cb_select.setButtonDrawable(R.drawable.filter_unchecked);
                        checkedList.remove(cusineList.get(getAdapterPosition()).getCuisineId());
                    }

                }
            });
        }
    }
    public void getFilterDataFromServer()
    {
        String mUrl="http://128.199.173.98/api/getCuisine.php?cuisine=true&accesskey=12345";
        System.out.println(mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                parseFilterJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });
    }
    public void parseFilterJSONData(String strResponse) {
        try
        {
            System.out.println("******"+strResponse.toString());
            JSONObject json = new JSONObject(strResponse.toString());
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            cusineList.clear();
            for (int ii = 0; ii < data.length(); ii++) {
                Cusines clist = new Cusines();
                JSONObject object = data.getJSONObject(ii);
                JSONObject cmenu = object.getJSONObject("cusine");
                clist.setCuisineId(cmenu.getString("id"));
                clist.setCuisineName(cmenu.getString("cuisine"));
                cusineList.add(clist);
            }
            rv_cusines.setAdapter(fla);
            fla.notifyDataSetChanged();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }


 /*   public class ViewHolderDeliveryType extends ViewHolder
    {

        public ViewHolderDeliveryType(View contentView) {
            super(contentView);
             contentView=getLayoutInflater().inflate(R.layout.deliverytype_bottomsheet, null);

            btndelivery=(TextView)contentView.findViewById(R.id.btndelivery) ;
            btnpickup=(TextView)contentView.findViewById(R.id.btnpickup) ;
            btmcancel=(TextView)contentView.findViewById(R.id.btmcancel) ;

            btndelivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                    {
                        ApplicationLoader.setDeliveryType("delivery");
                        txtChefSelection.setText("Delivery");
                        btndelivery.setText("Delivery");
                        btnpickup.setText("Pickup");
                    }
                    else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
                    {
                        ApplicationLoader.setDeliveryType("deliverynpickup");
                        txtChefSelection.setText("Delivery & Pickup");
                        btndelivery.setText("Delivery");
                        btnpickup.setText("Pickup");

                    }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
                    {
                        ApplicationLoader.setDeliveryType("delivery");
                        txtChefSelection.setText("Delivery");
                        btndelivery.setText("Delivery");
                        btnpickup.setText("Delivery & Pickup");

                    }
                }
            });
            btnpickup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                    {
                        ApplicationLoader.setDeliveryType("pickup");
                        txtChefSelection.setText("Pickup");
                        btndelivery.setText("Delivery");
                        btnpickup.setText("Delivery & Pickup");
                    }
                    else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
                    {
                        ApplicationLoader.setDeliveryType("pickup");
                        txtChefSelection.setText("Pickup");
                        btndelivery.setText("Delivery");
                        btnpickup.setText("Delivery & Pickup");

                    }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
                    {
                        ApplicationLoader.setDeliveryType("deliverynpickup");
                        txtChefSelection.setText("Delivery & Pickup");
                        btndelivery.setText("Delivery");
                        btnpickup.setText("Pickup");
                    }

                }
            });
            btmcancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });


        }
    }*/
}
