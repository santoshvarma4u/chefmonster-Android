package com.android.chefmonster.UI.OTP;

import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;


import com.android.chefmonster.AndroidUtilities;
import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Locations.SelectLocationActivity;
import com.android.chefmonster.R;
import com.android.chefmonster.Services.SMSListener;
import com.android.chefmonster.Splash;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.Utills.Constants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;


@SuppressLint("NewApi")
public class PinValidation extends AppCompatActivity implements SMSListener.SMSOTPListener{
	Toolbar toolbar;
	TextView tv_mobile,countdowntimer;
	ImageView backnavigation;
	Button btnAcCont;
	EditText pin;
	String METHOD_NAME = "CheckOTP";
	String[] keys = {"MobileNo", "OTP", "Date"};

	String METHOD_NAMEP = "GenerateOTP";

	public static final String SMS_KEY = "android.provider.Telephony.SMS_RECEIVED";
	private static PinValidation inst;


	public static PinValidation getInstance() {
		return inst;
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pinlayout);
	/*	toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setTitle("OTP Verification");
*/
		tv_mobile = (TextView) findViewById(R.id.tv_mobile);
		countdowntimer = (TextView) findViewById(R.id.countdowntimer);
		tv_mobile.setText(ApplicationLoader.getUserPhone());
		pin = (EditText) findViewById(R.id.pin);
		backnavigation=(ImageView)findViewById(R.id.backnavigation);
		btnAcCont=(Button) findViewById(R.id.btnAcCont);
		Intent in=getIntent();
		key=in.getExtras().get("pin").toString();

		btnAcCont.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Toast.makeText(PinValidation.this, "Resending OTP,Please wait", Toast.LENGTH_SHORT).show();
				Intent in=getIntent();
				String phone=in.getExtras().get("phone").toString();


				Random random=new Random();

				final String randomNumber=String.format("%04d", random.nextInt(10000));
				FormBody.Builder formBody;
				formBody = new FormBody.Builder()
						.add("smsApi","true")
						.add("to", "91"+phone)
						.add("otp",randomNumber);

				HttpLoggingInterceptor interceptor;
				interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

				OkHttpClient client = new OkHttpClient.Builder()
						.addInterceptor(interceptor)
						.build();
				RequestBody formBodyA = formBody.build();
				Request request = new Request.Builder()
						.url(Constants.OtpURL)
						.post(formBodyA)
						.build();

				client.newCall(request).enqueue(new Callback() {
					@Override
					public void onFailure(Call call, IOException e) {

					}

					@Override
					public void onResponse(Call call, Response response) throws IOException {
						if (response.isSuccessful()) {

							key = randomNumber;
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									Toast.makeText(PinValidation.this, "OTP Sent Successfully", Toast.LENGTH_SHORT).show();
								}
							});

						}
					}
				});
			}
		});

		pin.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
										  KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					// do something

					Intent in=getIntent();
					String phone=in.getExtras().get("phone").toString();
					validate(key,phone);
				}
				return false;
			}
		});
		///
		SMSListener.addSMSOTPListener(this);

		backnavigation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				Intent intent = new Intent(PinValidation.this, OTPActivity.class);
				startActivity(intent);
				finish();
			}
		});

		AndroidUtilities.setWaitingForSms(true);

		new CountDownTimer(60000,1000) {

			/** This method will be invoked on finishing or expiring the timer */
			@Override
			public void onFinish() {
				/** Creates an intent to start new activity */
				btnAcCont.setVisibility(View.VISIBLE);
				countdowntimer.setVisibility(View.GONE);
			}

			/** This method will be invoked in every 1000 milli seconds until
			 * this timer is expired.Because we specified 1000 as tick time
			 * while creating this CountDownTimer
			 */
			@Override
			public void onTick(long millisUntilFinished) {

				long ElapsedTime=millisUntilFinished/1000;
				countdowntimer.setText("OTP will receive with in "+ElapsedTime+" seconds");
			}
		}.start();
	}

	/*public void validation(View v) {
		validate();
	}*/
	String key="";
	public void validate(String key,String phone) {



		if(key.equals(pin.getText().toString()))
		{

			ApplicationLoader.setUserPhone(phone) ;

			if(ApplicationLoader.getUserLat().equals(""))
			{
				//Intent to menu
				Intent intent=new Intent(PinValidation.this, SelectLocationActivity.class);
				intent.putExtra("firstTime","true");
				startActivity(intent);
				finish();
			}
			else {
				//Intent to menu
				startActivity(new Intent(PinValidation.this, HomePage.class));
				finish();
			}
		}

	}

	ProgressDialog pd;

	private Handler handPin = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();

		}

		;
	};

	public void updatePin(final String smsMessage) {
		pin.setText(smsMessage);
		AndroidUtilities.setWaitingForSms(false);

		Intent in=getIntent();

		String phone=in.getExtras().get("phone").toString();
		validate(key,phone);

	}



	@Override
	public void onSmsRecieved(String otp) {
		pin.setText(otp);
		AndroidUtilities.setWaitingForSms(false);
		Intent in=getIntent();
		String phone=in.getExtras().get("phone").toString();
		validate(key,phone);
		SMSListener.removeSMSOTPListener(this);
	}
}
