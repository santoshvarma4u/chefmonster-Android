package com.android.chefmonster.UI.Menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.chefmonster.R;

import org.json.JSONException;
import org.json.JSONObject;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by ChakravartyG on 9/22/2016.
 */

public class FilterFragment extends AppCompatActivity {

    Toolbar toolbar;
    SegmentedGroup sortmenu, budget, foodtypenv, foodtypeveg;
    RadioButton veg, nonveg;
    public static final String KEY_RESULT="FILTER_DATA";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_filter);

        //
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        veg=(RadioButton)findViewById(R.id.veg);
        nonveg=(RadioButton)findViewById(R.id.nonveg);
        sortmenu = (SegmentedGroup) findViewById(R.id.sortmenu);
        budget = (SegmentedGroup) findViewById(R.id.budget);
        foodtypeveg = (SegmentedGroup) findViewById(R.id.foodtypeveg);
        //foodtypenv = (SegmentedGroup) findViewById(R.id.foodtypenv);

    }

    public void onApplyFilter(View view) {
        try {
            JSONObject result = new JSONObject();
            //
            int menu = sortmenu.getCheckedRadioButtonId();
            RadioButton rbMenu = (RadioButton) findViewById(menu);
            String sort = rbMenu.getTag().toString();
            result.put("sort", sort);
            //
            int budgetVal = budget.getCheckedRadioButtonId();
            RadioButton rbBudget = (RadioButton) findViewById(budgetVal);
            String budgetV = rbBudget.getTag().toString();
            result.put("budget", budgetV);
            //
            if(veg.isChecked())
                result.put("veg", true);
            else
                result.put("veg", false);
            //
            if(nonveg.isChecked())
                result.put("nonveg", true);
            else
                result.put("nonveg", false);

            if(veg.isChecked()==false && nonveg.isChecked()==false)
                result.put("both",true);

            if(filterListener!=null){
                filterListener.onApplyFilter(result);
                finish();
            }

//            Intent intent=new Intent();
//            intent.putExtra(KEY_RESULT,result.toString());
//            setResult(RESULT_OK,intent);
//            finish();



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static FilterListener filterListener;

    public static void setFilterListener(FilterListener filterListener){
        FilterFragment.filterListener=filterListener;
    }



}
