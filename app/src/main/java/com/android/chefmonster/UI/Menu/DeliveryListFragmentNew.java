package com.android.chefmonster.UI.Menu;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.ActivityCartNew;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.InfinateViewpager.InfiniteViewPager;
import com.android.chefmonster.R;
import com.android.chefmonster.Searchbar.MaterialSearchBar;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.circlepageidicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ChakravartyG on 9/10/2016.
 */

public class DeliveryListFragmentNew extends Fragment implements View.OnClickListener, FilterListener,MaterialSearchBar.OnSearchActionListener {
    @Override
    public void onClick(View view) {

    }

    @Override
    public void onApplyFilter(JSONObject result) {

    }

    @Override
    public void onSearchStateChanged(boolean enabled) {

    }

    @Override
    public void onSearchConfirmed(CharSequence text) {

    }

    @Override
    public void onButtonClicked(int buttonCode) {

    }/*

    //not using this
    View view;
    RecyclerView recycler_view;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public ImageLoader imageLoader;
    DecimalFormat formatData;
    private TextView mTxtAmountAdapter;
    private TextView txtItems;
    int ItemCount = 0;
    RelativeLayout checkout;
    LinearLayout optionsLayout;
    EditText mSearch;
    boolean dealsApply;
    //
    private List<String> lastSearches;
    private MaterialSearchBar searchBar;

    Button btnPickup,btnDeals,btnCombos;
    Button btnCartAdapter;
    Button btnFilter;

    SwipeRevealLayout swipeRevealLayout;

    TextView tv_type_delivery,tv_type_pickup,tv_type_deliverypickup;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.menu_deliverylist_frag, container, false);
        //
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());

        imageLoader = new ImageLoader(getActivity());
        //
        formatData = new DecimalFormat("#.##");
//
        mTxtAmountAdapter = (TextView) view.findViewById(R.id.txtCartPriceAdapter);

        mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));

        checkout = (RelativeLayout) view.findViewById(R.id.lytCheckout);
        btnCartAdapter=(Button) view.findViewById(R.id.btnCartAdapter);

        searchBar = (MaterialSearchBar) view.findViewById(R.id.edtKeyword);
        searchBar.setOnSearchActionListener(this);

        lastSearches = loadSearchSuggestionFromDisk();
        //searchBar.setLastSuggestions(lastSearches);
        swipeRevealLayout=(SwipeRevealLayout) view.findViewById(R.id.swipeRevealLayout);

        tv_type_delivery=(TextView) view.findViewById(R.id.typedelivery);
        tv_type_pickup=(TextView) view.findViewById(R.id.typepickup);
        tv_type_deliverypickup=(TextView) view.findViewById(R.id.typepickupndelivery);

        tv_type_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tv_type_deliverypickup.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery");
                    tv_type_delivery.setText("Delivery & Pickup");
                    //get only delivery items
                }
                else if(tv_type_deliverypickup.getText().equals("Delivery") && tv_type_delivery.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery & Pickup");
                    tv_type_delivery.setText("Delivery");
                    //get all items
                }
                else if(tv_type_deliverypickup.getText().equals("Pickup") && tv_type_delivery.getText().equals("Delivery"))
                {
                    tv_type_deliverypickup.setText("Delivery");
                    tv_type_delivery.setText("Pickup");
                    //get delivery items only
                }
                else if(tv_type_deliverypickup.getText().equals("Delivery") && tv_type_delivery.getText().equals("Pickup"))
                {
                    tv_type_deliverypickup.setText("Pickup");
                    tv_type_delivery.setText("Delivery");
                    //get pickup items only
                }
                else if(tv_type_deliverypickup.getText().equals("Pickup") && tv_type_delivery.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery & Pickup");
                    tv_type_pickup.setText("Pickup");
                    //get all items
                }
                swipeRevealLayout.close(true);
            }
        });

        tv_type_pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tv_type_deliverypickup.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Pickup");
                    tv_type_pickup.setText("Delivery & Pickup");
                    //get only pickup items
                }
                else if(tv_type_deliverypickup.getText().equals("Pickup") && tv_type_pickup.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery & Pickup");
                    tv_type_pickup.setText("Pickup");
                    //get all items
                }
                else if(tv_type_deliverypickup.getText().equals("Delivery") && tv_type_pickup.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery & Pickup");
                    tv_type_pickup.setText("Delivery");
                    //get all items
                }
                else if(tv_type_deliverypickup.getText().equals("Delivery") && tv_type_pickup.getText().equals("Pickup"))
                {
                    tv_type_deliverypickup.setText("Pickup");
                    tv_type_pickup.setText("Delivery");
                    //get pickup items only
                }
                else if(tv_type_deliverypickup.getText().equals("Pickup") && tv_type_pickup.getText().equals("Delivery"))
                {
                    tv_type_deliverypickup.setText("Delivery");
                    tv_type_delivery.setText("Pickup");
                    //get delivery items only
                }
                swipeRevealLayout.close(true);
            }
        });


        //searchBar.getLastSuggestions();

      *//*  mSearch=(EditText)view.findViewById(R.id.edtKeyword);
        mSearch.setFocusable(false);
        mSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearch.setFocusable(true);
            }
        });*//*

        btnDeals=(Button) view.findViewById(R.id.btnDeals);
        btnDeals.setOnClickListener(this);
        btnCombos=(Button)view.findViewById(R.id.btnCombos);
        btnCombos.setOnClickListener(this);

        btnCartAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(getActivity(), ActivityCartNew.class);
                startActivity(iMyOrder);
            }
        });
        //
        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                //totalItemCount = linearLayoutManager.getItemCount();
                //lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
              *//*  if (linearLayoutManager.findFirstVisibleItemPosition() > 0) {
                   if(optionsLayout.getVisibility()==View.GONE)
                    optionsLayout.setVisibility(View.VISIBLE);
                } else {
                    optionsLayout.setVisibility(View.GONE);
                }*//*
            }
        });
        linearLayoutManager = (LinearLayoutManager) recycler_view.getLayoutManager();

        //
        optionsLayout = (LinearLayout) view.findViewById(R.id.linearLayout10);
        //
        txtItems=(TextView) view.findViewById(R.id.txtItems);
        txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());

        btnPickup = (Button) view.findViewById(R.id.btnPickup);
        btnPickup.setOnClickListener(this);
        //
        dla = new DeliveryListAdapter();

        // recycler_view.setAdapter(dla);

        btnFilter=(Button)view.findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(this);
        //

        return view;
    }

    DeliveryListAdapter dla;

    public LinearLayoutManager linearLayoutManager;
    PickUpListFragment pickUpListFragment;
    CombosFragment combosFragment;

    public void setPickUpFragment() {
        pickUpListFragment = new PickUpListFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.main_activity_content_frame, pickUpListFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

    }
    public void setComboFragment() {
        combosFragment = new CombosFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.main_activity_content_frame, combosFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPickup:
                setPickUpFragment();
                break;
            case R.id.btnFilter:
                FilterFragment.setFilterListener(this);
                startActivity(new Intent(getActivity(),FilterFragment.class));
                break;
            case R.id.btnDeals:

                Log.e("dealsApply"," "+dealsApply);
                if(dealsApply)
                {
                    dealsApply=false;
                    loadSliderImages();
                }
                else
                {
                    dealsApply=true;
                    loadSliderImages();
                }

                break;
            case R.id.btnCombos:
                setComboFragment();
                break;
            default:
                break;
        }
    }

    @Override
    public void onApplyFilter(JSONObject result) {
        Log.i("result",result.toString());
        Toast.makeText(getActivity(), result.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSearchStateChanged(boolean enabled) {

        if(enabled)
        {
            recycler_view.setVisibility(View.GONE);
        }
        else
        {
            recycler_view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSearchConfirmed(CharSequence text) {

        //getdata with search

        getDataFromServer(text.toString());


    }

    @Override
    public void onButtonClicked(int buttonCode) {


    }
    public List<String> loadSearchSuggestionFromDisk()
    {
        List<String> ll=new ArrayList<>();
        ll.add(0,"Biryani");
        ll.add(1,"Chilli Chicken");
        ll.add(2,"Dum Biryani");


        return ll;
       *//* DBHelper_New db=new DBHelper_New(getActivity());
        List<List<String>> results=db.getDataByQuery("SELECT "+db.FOOD_NAME+" "+db.TABLE_RECENTSEARCH);
        return results;*//*
    }

    public class DeliveryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        final int SLIDER = 0, ROW = 1;


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            switch (viewType) {
                case SLIDER:
                    View vSlider = inflater.inflate(R.layout.item_viewpager, viewGroup, false);
                    viewHolder = new ViewHolderSlider(vSlider);
                    break;
                case ROW:
                    View vRow = inflater.inflate(R.layout.menu_items_new, viewGroup, false);
                    viewHolder = new ViewHolderRow(vRow);
                    break;
                default:

                    break;
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {



            switch (viewHolder.getItemViewType()) {
                case SLIDER:
                    ViewHolderSlider vh1 = (ViewHolderSlider) viewHolder;
                    configureViewHolderSlider(vh1, position);
                    break;
                case ROW:
                    ViewHolderRow vh2 = (ViewHolderRow) viewHolder;
                    configureViewHolderRow(vh2, position);
                    break;
                default:

                    break;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return DeliveryListFragmentNew.Menu_ID.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? SLIDER : ROW;
        }

        public void configureViewHolderSlider(final ViewHolderSlider viewHolder, int position) {


                viewHolder.pager.setAdapter(new SlidingImageAdapter(getActivity(), sliderImages));
              //  viewHolder.indicator.setViewPager(viewHolder.pager);




            final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
           // viewHolder.indicator.setRadius(2 * density);

         NUM_PAGES = sliderImages.size();



           *//* // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    viewHolder.pager.setCurrentItem(currentPage++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 5000, 5000);*//*

        }

        int ItemCount = 0;

        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {

            Double finalPrice=0.0;
            holder.txtText.setText(DeliveryListFragmentNew.Menu_name.get(position));


            holder.txtfoodtype.setText(DeliveryListFragmentNew.Menu_food_type.get(position));
            holder.txtStockQty.setText(DeliveryListFragmentNew.Menu_stock_qty.get(position));
            holder.txtweigthingms.setText(DeliveryListFragmentNew.Menu_weigth_grms.get(position));
            holder.txtAreaNamePickup.setText(DeliveryListFragmentNew.Menu_pickup_location.get(position));

            if(DeliveryListFragmentNew.Menu_Deal_price.get(position) > 0)
            {
                holder.ivDealBanner.setVisibility(View.VISIBLE);
                holder.txtMainValue.setVisibility(View.VISIBLE);
                holder.txtMainValue.setText(DeliveryListFragmentNew.Menu_price.get(position) + " " + DeliveryListFragmentNew.Currency);
                holder.txtMainValue.setPaintFlags(holder.txtMainValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                Double afterDiscount= DeliveryListFragmentNew.Menu_price.get(position)- DeliveryListFragmentNew.Menu_Deal_price.get(position);
                holder.txtSubText.setText(String.valueOf(afterDiscount) + " " + DeliveryListFragmentNew.Currency);
                finalPrice=afterDiscount;
            }
            else
            {
                holder.txtSubText.setText(DeliveryListFragmentNew.Menu_price.get(position) + " " + DeliveryListFragmentNew.Currency);
                finalPrice= DeliveryListFragmentNew.Menu_price.get(position);

            }

            if(DeliveryListFragmentNew.Menu_delivery.get(position).toString().toLowerCase().contains("pickup"))
            {
                holder.itemLayout.setVisibility(View.VISIBLE);
                holder.pickupimage.setVisibility(View.VISIBLE);
                imageLoader.DisplayImage(DeliveryListFragmentNew.Menu_pickup_image.get(position), holder.pickupimage);
            }


            if(DeliveryListFragmentNew.Menu_delivery.get(position).toString().toLowerCase().contains("pickup"))
            {
                holder.txtDeliverytime.setText(DeliveryListFragmentNew.Menu_pickup_distance.get(position));
                holder.txtviewdelivery.setText("Distance");
            }
            else
            {
                holder.txtDeliverytime.setText(DeliveryListFragmentNew.Menu_delivery_time.get(position));
                holder.txtviewdelivery.setText("Delivery Time");
            }

            imageLoader.DisplayImage(Constants.ImagesUrl + DeliveryListFragmentNew.Menu_image.get(position), holder.imgThumb);
            //
            final Double finalPrice1 = finalPrice;
            holder.btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    if (qty < 0) {

                        //ItemCount--;
                    } else {
                        ItemCount++;

                        checkout.setVisibility(View.VISIBLE);
                        qty++;
                        double price = finalPrice1;
                        double changedPrice = price * qty;
                        //holder.txtSubText.setText(changedPrice+" "+ActivityMenuList.Currency);
                        holder.txtQty.setText(String.valueOf(qty));

                        DBHelper_New db = new DBHelper_New(getActivity());
                        //db.openDataBase();
                        long menuid = DeliveryListFragmentNew.Menu_ID.get(position);
                        System.out.println(menuid+"**************");
                        double menuprice = finalPrice1;
                        String menuname = DeliveryListFragmentNew.Menu_name.get(position);
                        String menuImage= DeliveryListFragmentNew.Menu_image.get(position);

                        if(DeliveryListFragmentNew.Menu_Deal_price.get(position) > 0)
                        {
                            *//*if(DeliveryListFragmentNew.Menu_delivery.get(position).toString().toLowerCase().contains("delivery"))
                            {
                                if (db.isDataExist(menuid,"DEAL","DELIVERY")) {
                                    db.updateData(menuid, qty, (menuprice * qty),"DEAL","Home Chef","DELIVERY");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty),"DEAL","Home Chef","DELIVERY",menuImage);
                                }
                            }
                            else if(DeliveryListFragmentNew.Menu_delivery.get(position).toString().toLowerCase().contains("pickup"))
                            {
                                if (db.isDataExist(menuid, "DEAL", "PICKUP")) {
                                    db.updateData(menuid, qty, (menuprice * qty), "DEAL", "Home Chef", "PICKUP");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty), "DEAL", "Home Chef", "PICKUP",menuImage);
                                }
                            }*//*
                        }
                        else
                        {
                           *//* if (db.isDataExist(menuid,"Home Chef","DELIVERY")) {
                                db.updateData(menuid, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY");
                            } else {
                                db.addData(menuid, menuname, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY",menuImage);
                            }*//*
                        }


                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        double ChangedPrice = currentCartPrice + menuprice;

                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        db.close();
                    }
                    //txtItems.setText(String.valueOf(ItemCount));
                    txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));
                }
            });
            final Double finalPrice2 = finalPrice;
            holder.btndrc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    qty--;
*//*
                    if (qty > 0)
                        ItemCount--;*//*

                    if (qty <= 0) {


                        double price = finalPrice2;
                        //	holder.txtSubText.setText(price+" "+DeliveryListFragment.Currency);
                        holder.txtQty.setText("0");

                        long menuid = DeliveryListFragmentNew.Menu_ID.get(position);
                        DBHelper_New db = new DBHelper_New(getActivity());
                        //db.openDataBase();
                        db.deleteData(menuid);
                        db.close();

                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        if (currentCartPrice > 0) {
                            double ChangedPrice = currentCartPrice - price;
                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        }

                    } else {
                        ItemCount--;
                        if (qty == 1) {
                            double price = finalPrice2;
                            holder.txtQty.setText("0");
                            //holder.txtSubText.setText(price+" "+DeliveryListFragment.Currency);
                            DBHelper_New db = new DBHelper_New(getActivity());
                            //db.openDataBase();
                            long menuid = DeliveryListFragmentNew.Menu_ID.get(position);
                            double menuprice = finalPrice2;
                            String menuname = DeliveryListFragmentNew.Menu_name.get(position);
                            String menuimage= DeliveryListFragmentNew.Menu_image.get(position);
                            *//*if(DeliveryListFragmentNew.Menu_Deal_price.get(position) > 0)
                            {
                                if (db.isDataExist(menuid,"DEAL","DELIVERY")) {
                                    db.updateData(menuid, qty, (menuprice * qty),"DEAL","Home Chef","DELIVERY");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty),"DEAL","Home Chef","DELIVERY",menuimage);
                                }
                            }
                            else
                            {
                                if (db.isDataExist(menuid,"Home Chef","DELIVERY")) {
                                    db.updateData(menuid, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY",menuimage);
                                }
                            }*//*
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        } else {

                            double price = finalPrice2;
                            double changedPrice = price * qty;
                            //holder.txtSubText.setText(changedPrice+" "+DeliveryListFragment.Currency);
                            holder.txtQty.setText(String.valueOf(qty));
                            DBHelper_New db = new DBHelper_New(getActivity());
                            //db.openDataBase();
                            long menuid = DeliveryListFragmentNew.Menu_ID.get(position);
                            double menuprice = finalPrice2;
                            String menuname = DeliveryListFragmentNew.Menu_name.get(position);
                            String menuImage= DeliveryListFragmentNew.Menu_image.get(position);
                           *//* if(DeliveryListFragmentNew.Menu_Deal_price.get(position) > 0)
                            {
                                if (db.isDataExist(menuid,"DEAL","DELIVERY")) {
                                    db.updateData(menuid, qty, (menuprice * qty),"DEAL","Home Chef","DELIVERY");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty),"DEAL","Home Chef","DELIVERY",menuImage);
                                }
                            }
                            else
                            {
                                if (db.isDataExist(menuid,"Home Chef","DELIVERY")) {
                                    db.updateData(menuid, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY",menuImage);
                                }
                            }*//*
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        }
                    }
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));
                    txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());
                    //txtItems.setText(String.valueOf(ItemCount));
                }
            });

        }
    }

    class ViewHolderSlider extends RecyclerView.ViewHolder {
        ViewPager pager;
        InfiniteViewPager mViewPager;
        CirclePageIndicator indicator;
        Button btnDelivery,btnPickup,btnDeals,btnCombos;


        public ViewHolderSlider(View itemView) {
            super(itemView);
            pager = (ViewPager) itemView.findViewById(R.id.pager);
            pager.setClipToPadding(false);
            // set padding manually, the more you set the padding the more you see of prev & next page
            pager.setPadding(60, 0, 60, 0);
            // sets a margin b/w individual pages to ensure that there is a gap b/w them
            pager.setPageMargin(20);
            //indicator = (CirclePageIndicator) itemView.findViewById(R.id.indicator);
            btnPickup = (Button) itemView.findViewById(R.id.btnPickup);
            btnCombos=(Button) itemView.findViewById(R.id.btnCombos);
            btnCombos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setComboFragment();
                }
            });
            btnPickup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setPickUpFragment();
                }
            });
            btnDeals=(Button) itemView.findViewById(R.id.btnDeals);
          *//*  mViewPager = (InfiniteViewPager) itemView.findViewById(R.id.infpager);
            mViewPager.setClipToPadding(false);
            // set padding manually, the more you set the padding the more you see of prev & next page
            mViewPager.setPadding(60, 0, 60, 0);
            // sets a margin b/w individual pages to ensure that there is a gap b/w them
            mViewPager.setPageMargin(20);*//*

            if(dealsApply)
                btnDeals.setBackgroundResource(R.drawable.borderbackgroundselected);

            btnDeals.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e("dealsApply"," "+dealsApply);

                    if(dealsApply)
                    {
                        dealsApply=false;
                        loadSliderImages();
                    }
                    else
                    {
                        dealsApply=true;
                        loadSliderImages();
                    }
                }
            });

            btnDelivery=(Button) itemView.findViewById(R.id.btnDelivery);
            btnDelivery.setBackgroundResource(R.drawable.borderbackgroundselected);
            btnDelivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            btnCombos=(Button) itemView.findViewById(R.id.btnDeals);
            btnCombos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dealsApply=true;
                    loadSliderImages();
                }
            });
        }
    }

    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty, txtDeliverytime, txtweigthingms, txtfoodtype,txtMainValue,txtviewdelivery,txtAreaNamePickup;
        ImageView imgThumb,ivDealBanner,pickupimage;
        LinearLayout itemLayout;
        Button btninc, btndrc;
        RatingBar rb;

        public ViewHolderRow(View convertView) {
            super(convertView);
            txtText = (TextView) convertView.findViewById(R.id.txtText);
            txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
            imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
            pickupimage = (ImageView) convertView.findViewById(R.id.pickupimage);
            txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgms);
            txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQty);
            txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtype);
            txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtime);
            txtQty = (TextView) convertView.findViewById(R.id.txtQty);
            ivDealBanner=(ImageView) convertView.findViewById(R.id.ivDealBanner);
            btninc = (Button) convertView.findViewById(R.id.btninc);
            btndrc = (Button) convertView.findViewById(R.id.btndrc);
            txtMainValue=(TextView) convertView.findViewById(R.id.txtMainValue);
            txtviewdelivery=(TextView) convertView.findViewById(R.id.txtviewdelivery);
            txtAreaNamePickup=(TextView) convertView.findViewById(R.id.txtAreaNamePickup);
            itemLayout=(LinearLayout) convertView.findViewById(R.id.itemLayout);



        }
    }

    // clear arraylist variables before used
    void clearData() {
        Menu_ID.clear();
        Menu_name.clear();
        Menu_price.clear();
        Menu_image.clear();
        Menu_stock_qty.clear();
        Menu_delivery.clear();
        Menu_delivery_time.clear();
        Menu_food_type.clear();
        Menu_serves.clear();
        Menu_weigth_grms.clear();
        Menu_Deal_price.clear();
        Menu_pickup_image.clear();
        Menu_pickup_location.clear();
        Menu_pickup_distance.clear();
    }


    ProgressDialog pd;

    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
        if (pd == null)
            pd = new ProgressDialog(getActivity());
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();
    }

    public void closeDialog() {
        // do something wih the result
        if (pd != null)
            pd.dismiss();
    }

    public void getDataFromServer(String keyword) {
        //
        showProgressDialog();

        String mUrl="";



         if(ApplicationLoader.getProchefID().isEmpty() && keyword.isEmpty())
             mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=deliverynpickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        else if(ApplicationLoader.getProchefID().isEmpty() && keyword.length() > 0)
             mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&keyword="+keyword+"&category_id=00&deliverytype=deliverynpickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        else
            mUrl="http://128.199.173.98/api/get-menu-prochef.php?accesskey=12345&category_id=00&chefid="+ApplicationLoader.getProchefID()+"&deliverytype=deliverynpickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        //

        System.out.println(mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                closeDialog();

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });


    }


    // method to parse json data from server
    public void parseJSONData(String strResponse) {

        clearData();

        try {
            ///dummy data
            Menu_ID.add(null);
            Menu_name.add(null);
            Menu_price.add(null);
            Menu_image.add(null);//Menu_image
            Menu_stock_qty.add(null);
            Menu_delivery.add(null);
            Menu_delivery_time.add(null);
            Menu_food_type.add(null);
            Menu_serves.add(null);
            Menu_weigth_grms.add(null);
            Menu_Deal_price.add(null);
            Menu_pickup_image.add(null);
            Menu_pickup_location.add(null);
            Menu_pickup_distance.add(null);



            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int ii = 0; ii < data.length(); ii++) {

                JSONObject object = data.getJSONObject(ii);

                JSONObject menu = object.getJSONObject("MenuActivity");

                Log.e("MenuActivity", menu.toString());

                Log.e("dealPrice"," "+menu.getString("deal_price"));
                if(dealsApply)
                {
                    if(Integer.parseInt(menu.getString("deal_price"))>0)
                    {
                        Menu_ID.add(Long.parseLong(menu.getString("Menu_ID")));
                        Menu_name.add(menu.getString("Menu_name"));
                        Menu_price.add(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                        Menu_image.add(menu.getString("main_image"));//Menu_image
                        Menu_stock_qty.add(menu.getString("Quantity"));
                        Menu_delivery.add(menu.getString("delivery_type"));
                        Menu_delivery_time.add(menu.getString("delivery_time"));
                        Menu_food_type.add(menu.getString("type_of_food"));
                        Menu_serves.add(menu.getString("no_of_serves"));
                        Menu_weigth_grms.add(menu.getString("weight_in_grams"));
                        Menu_Deal_price.add(Double.valueOf(menu.getString("deal_price")));
                        Menu_pickup_location.add(menu.getString("pickuplocation"));
                        Menu_pickup_image.add(menu.getString("pickupImageUrl"));
                        Menu_pickup_distance.add(menu.getString("distance"));

                    }
                }
                else
                {
                    Menu_ID.add(Long.parseLong(menu.getString("Menu_ID")));
                    Menu_name.add(menu.getString("Menu_name"));
                    Menu_price.add(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                    Menu_image.add(menu.getString("main_image"));//Menu_image
                    Menu_stock_qty.add(menu.getString("Quantity"));
                    Menu_delivery.add(menu.getString("delivery_type"));
                    Menu_delivery_time.add(menu.getString("delivery_time"));
                    Menu_food_type.add(menu.getString("type_of_food"));
                    Menu_serves.add(menu.getString("no_of_serves"));
                    Menu_weigth_grms.add(menu.getString("weight_in_grams"));
                    Menu_Deal_price.add(Double.valueOf(menu.getString("deal_price")));
                    Menu_pickup_location.add(menu.getString("pickuplocation"));
                    Menu_pickup_image.add(menu.getString("pickupImageUrl"));
                    Menu_pickup_distance.add(menu.getString("distance"));
                }
            }
            dla.notifyDataSetChanged();
            recycler_view.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // create arraylist variables to store data from server
    static ArrayList<Long> Menu_ID = new ArrayList<Long>();
    static ArrayList<String> Menu_name = new ArrayList<String>();
    static ArrayList<Double> Menu_price = new ArrayList<Double>();
    static ArrayList<String> Menu_image = new ArrayList<String>();
    static ArrayList<String> Menu_stock_qty = new ArrayList<String>();
    static ArrayList<String> Menu_food_type = new ArrayList<String>();
    static ArrayList<String> Menu_delivery = new ArrayList<String>();
    static ArrayList<String> Menu_weigth_grms = new ArrayList<String>();
    static ArrayList<String> Menu_serves = new ArrayList<String>();
    static ArrayList<String> Menu_delivery_time = new ArrayList<String>();
    static ArrayList<Double> Menu_Deal_price = new ArrayList<Double>();
    static ArrayList<String> Menu_pickup_location = new ArrayList<String>();
    static ArrayList<String> Menu_pickup_image = new ArrayList<String>();
    static ArrayList<String> Menu_pickup_distance = new ArrayList<String>();


    static String Currency="₹";

    @Override
    public void onResume() {
        super.onResume();
        loadSliderImages();


    }

    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper_New dbhelper = new DBHelper_New(ctx);
        //dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }




    public void loadSliderImages() {
        showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        Request request = new Builder().url("http://chefmonster.com/api/getSliders.php").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {

            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                // Log.i("response",response.body());
                //Log.i("response",response.body().string());
                closeDialog();
                if (response.isSuccessful()) {
                    //processSliderImages(response.body().string());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processSliderImages(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });

    }

    public void processSliderImages(String response) {
        //{"slides": ["slide.jpg","slide.jpg","slide.jpg"]}
        sliderImages.clear();
        try {
            JSONObject jobj = new JSONObject(response);
            JSONArray jSlides = jobj.getJSONArray("slides");
            for (int i = 0; i < jSlides.length(); i++) {
                sliderImages.add(jSlides.getString(i));
            }
            //
            //slidingImageAdapter = new SlidingImageAdapter(getActivity(), sliderImages, imageLoader);
            recycler_view.setAdapter(dla);
            //

            getDataFromServer("");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    ArrayList<String> sliderImages = new ArrayList<>();
    SlidingImageAdapter slidingImageAdapter;
*/
}
