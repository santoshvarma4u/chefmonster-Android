package com.android.chefmonster.UI.OTP;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.android.chefmonster.Utills.Constants;

import java.io.IOException;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class OTPActivity extends AppCompatActivity {
    EditText et_UMobile;

    TextInputLayout til_UMobile;

    Button btnAcCont;

    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        pd=new ProgressDialog(OTPActivity.this);
        pd.setMessage("Loading....");
        til_UMobile = (TextInputLayout) findViewById(R.id.til_UMobile);
        et_UMobile = (EditText) findViewById(R.id.et_UMobile);

        btnAcCont=(Button)findViewById(R.id.btnAcCont);
        btnAcCont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptContinue();
            }
        });
        et_UMobile.addTextChangedListener(new MyTextWatcher(et_UMobile));

        et_UMobile.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do something
                    generatePin();
                }
                return false;
            }
        });
    }

    public void acceptContinue() {
        generatePin();
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1,
                                      int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1,
                                  int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_UMobile:
                    validateMobile(et_UMobile, til_UMobile);
                    break;

            }
        }
    }

    private boolean validateMobile(EditText et, TextInputLayout til) {
        if (et.getText().toString().trim().isEmpty()) {
            til.setError("Enter Mobile Number");
            requestFocus(et);
            return false;
        } else if (et.getText().toString().trim().length() != 10) {
            til.setError("Enter 10 Digit Mobile Number");
            requestFocus(et);
            return false;
        } else {
            til.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void generatePin() {

        if (et_UMobile.length() != 10) {
            til_UMobile.setError("Enter 10 digit mobile number");
            requestFocus(et_UMobile);
        }
     /*   else if (!et_UMobile.getText().startsWith("9")
                || cHelper.getETValue(et_UMobile).startsWith("8") || cHelper
                .getETValue(et_UMobile).startsWith("7"))) {
            til_UMobile.setError("Number should start with 9|8|7 only");
            requestFocus(et_UMobile);
        }*/

        else {

            if(getIntent().getExtras().containsKey("AvailabilityStatus") )
            {
                if(getIntent().getExtras().getString("AvailabilityStatus").equalsIgnoreCase("Success"))
                {
                    pd.show();
                    Random random=new Random();

                    final String randomNumber=String.format("%04d", random.nextInt(10000));
                    FormBody.Builder formBody;
                    formBody = new FormBody.Builder()
                            .add("smsApi","true")
                            .add("to", "91"+et_UMobile.getText().toString())
                            .add("otp",randomNumber);

                    HttpLoggingInterceptor interceptor;
                    interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .addInterceptor(interceptor)
                            .build();
                    RequestBody formBodyA = formBody.build();
                    Request request = new Request.Builder()
                            .url(Constants.OtpURL)
                            .post(formBodyA)
                            .build();

                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {

                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if (response.isSuccessful()) {
                             //   pd.hide();
                                Intent intent = new Intent(OTPActivity.this, PinValidation.class);
                                intent.putExtra("pin", randomNumber);
                                intent.putExtra("phone", et_UMobile.getText().toString());
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }

            }
            else
            {
                Toast.makeText(OTPActivity.this, "Thank you for your interest, We are not serving in your area.", Toast.LENGTH_LONG).show();
                OTPActivity.this.finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }


            //otpsccucess

        /*    ApplicationLoader.setUserPhone(et_UMobile.getText().toString()) ;
            Intent intent = new Intent(OTPActivity.this, PinValidation.class);
            intent.putExtra("pin", "7859");
            startActivity(intent);
            finish();*/

        }

}
