package com.android.chefmonster.UI.Menu.Search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.chefmonster.R;
import com.android.chefmonster.Searchbar.MaterialSearchBar;
import com.android.chefmonster.Searchbar.SearchIndex;
import com.android.chefmonster.Utills.Constants;
import com.ivankocijan.magicviews.views.MagicTextView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    protected MagicTextView noresults;
    List<SearchIndex> listSearch;
    MaterialSearchBar searchBar;
    SearchView mainSearch;
    RecyclerView rvSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

      /*  searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        searchBar.setHint("Custom hint");
        searchBar.setSpeechMode(true);
        //enable searchbar callbacks
        searchBar.setOnSearchActionListener(this);
        //restore last queries from disk
        lastSearches = loadSearchSuggestionFromDisk();
        searchBar.setLastSuggestions(lastSearches);*/

        initView();
    }

    public void initView() {
        mainSearch = (SearchView) findViewById(R.id.main_search);
        mainSearch.setIconified(false);
        // mainSearch.performClick();
        listSearch = new ArrayList<>();
        mainSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                finish();
                return false;
            }
        });
        mainSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 1) {
                    getDataFromServer(newText);
                } else {
                    listSearch.clear();
                    sml.notifyDataSetChanged();
                }
                return true;
            }
        });
        rvSearch = (RecyclerView) findViewById(R.id.rvSearch);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchActivity.this);
        rvSearch.setLayoutManager(mLayoutManager);
        rvSearch.setItemAnimator(new DefaultItemAnimator());

        sml = new SearchListAdapter();
        noresults = (MagicTextView) findViewById(R.id.noresults);
    }

    SearchListAdapter sml;

    public class SearchListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.search_item, parent, false);
            viewHolder = new ViewHolderRow(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderRow vh2 = (ViewHolderRow) holder;
            configureViewHolderRow(vh2, position);
        }

        @Override
        public int getItemCount() {
            return listSearch.size();
        }

        SearchIndex searchIndexList;

        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {
            searchIndexList = listSearch.get(position);
            holder.txtSearchMenuName.setText(searchIndexList.getMenuName());
            holder.txtSearchCategoryName.setText(searchIndexList.getCategoryName());
        }
    }

    class ViewHolderRow extends RecyclerView.ViewHolder {

        protected TextView txtSearchMenuName;
        protected TextView txtSearchCategoryName;


        public ViewHolderRow(View itemView) {
            super(itemView);
            txtSearchMenuName = (TextView) itemView.findViewById(R.id.searchMenuName);
            txtSearchCategoryName = (TextView) itemView.findViewById(R.id.searchCategoryName);
            txtSearchMenuName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("MenuId", listSearch.get(getAdapterPosition()).getMenuID());
                    intent.putExtra("MenuName", listSearch.get(getAdapterPosition()).getMenuName());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
        }
    }

    public void getDataFromServer(String query) {

        String mUrl = Constants.searchUrl + "?searchKeyword=" + query;
        Log.d("murl", mUrl);

        HttpLoggingInterceptor interceptor;
        interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });
    }


    public void parseJSONData(String strResponse) {

        try {
            listSearch.clear();
            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            System.out.println(data.length() + "***************");
            for (int ii = 0; ii < data.length(); ii++) {
                SearchIndex si = new SearchIndex();
                JSONObject object = data.getJSONObject(ii);
                JSONObject search = object.getJSONObject("Search");
                Log.e("Search", search.toString());
                si.setMenuID(search.getString("Menu_ID"));
                si.setMenuName(search.getString("Menu_name"));
                si.setCategoryName(search.getString("Category_name"));
                listSearch.add(si);
            }


            if(listSearch.size() <=0)
                noresults.setVisibility(View.VISIBLE);
            else
                noresults.setVisibility(View.GONE);

            rvSearch.setAdapter(sml);
            sml.notifyDataSetChanged();

        /*    InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);*/

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
