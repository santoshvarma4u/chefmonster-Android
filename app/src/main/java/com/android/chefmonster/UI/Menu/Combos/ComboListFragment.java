package com.android.chefmonster.UI.Menu.Combos;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.ActivityCartNew;
import com.android.chefmonster.Cart.CartTabbedActivity;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.InfinateViewpager.InfiniteViewPager;
import com.android.chefmonster.LoadingDrawble.loadingdrawable.LoadingView;
import com.android.chefmonster.R;
import com.android.chefmonster.Searchbar.MaterialSearchBar;
import com.android.chefmonster.UI.Menu.CombosFragment;
import com.android.chefmonster.UI.Menu.FilterFragment;
import com.android.chefmonster.UI.Menu.FilterListener;
import com.android.chefmonster.UI.Menu.Filters.Cusines;
import com.android.chefmonster.UI.Menu.Filters.FilterBottomSheet;
import com.android.chefmonster.UI.Menu.MapAddrView;
import com.android.chefmonster.UI.Menu.Menu;
import com.android.chefmonster.UI.Menu.OnLoadMoreListener;
import com.android.chefmonster.UI.Menu.PickUpListFragment;
import com.android.chefmonster.UI.Menu.SlidingImageAdapter;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.circlepageidicator.CirclePageIndicator;
import com.konifar.fab_transformation.FabTransformation;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ChakravartyG on 9/10/2016.
 */

public class ComboListFragment extends Fragment implements View.OnClickListener, FilterListener,MaterialSearchBar.OnSearchActionListener {
    View view;
    RecyclerView recycler_view;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public ImageLoader imageLoader;
    DecimalFormat formatData;
    private TextView mTxtAmountAdapter;
    private TextView txtItems;
    int ItemCount = 0;
    RelativeLayout checkout;
    LinearLayout optionsLayout;
    EditText mSearch;
    boolean dealsApply;
    TextView tv_checkout;
    //
    private List<String> lastSearches;
    private MaterialSearchBar searchBar;

    Button btnPickup,btnDeals,btnCombos;
    Button btnCartAdapter;
    Button btnFilter;

     List<Combo> menuList;
    SwipeRevealLayout swipeRevealLayout;

    Button mainFragFilter;

    TextView tv_type_delivery,tv_type_pickup,tv_type_deliverypickup;

    static String Currency="₹";

    FloatingActionButton fab;
    Toolbar toolbarFooter;
    BottomSheetDialogFragment myBottomSheet;
    LoadingView loading_view;

    BottomSheetDialog bottomSheetDialog ;
    JSONObject filterResult;
    View view1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.menu_deliverylist_frag, container, false);
        //
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        tv_checkout=(TextView)view.findViewById(R.id.tv_checkout);
        tv_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(getActivity(), CartTabbedActivity.class);
                startActivity(iMyOrder);
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.smoothScrollToPosition(0);
        loading_view=(LoadingView)view.findViewById(R.id.loading_view);
        //filter bottom sheet
        filterResult = new JSONObject();
        myBottomSheet = FilterBottomSheet.newInstance("Modal Bottom Sheet");
        mainFragFilter=(Button) view.findViewById(R.id.mainFragFilter);

        view1 = getActivity().getLayoutInflater().inflate(R.layout.fragment_filter_bottom_sheet, null);
        bottomSheetDialog = new BottomSheetDialog(getActivity());
        bottomSheetDialog.setContentView(view1);
        initFilterView(view1);

        mainFragFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottomSheetDialog.show();

            }
        });


        menuList=new ArrayList<>();

        imageLoader = new ImageLoader(getActivity());

        formatData = new DecimalFormat("#.##");
//

        toolbarFooter=(Toolbar) view.findViewById(R.id.toolbar_footer);
         fab=(FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent iMyOrder = new Intent(getActivity(), CartTabbedActivity.class);
                startActivity(iMyOrder);

            }
        });
        mTxtAmountAdapter = (TextView) view.findViewById(R.id.txtCartPriceAdapter);

        mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));

        checkout = (RelativeLayout) view.findViewById(R.id.lytCheckout);
        btnCartAdapter=(Button) view.findViewById(R.id.btnCartAdapter);

        searchBar = (MaterialSearchBar) view.findViewById(R.id.edtKeyword);
        searchBar.setOnSearchActionListener(this);

        lastSearches = loadSearchSuggestionFromDisk();
       // searchBar.setLastSuggestions(lastSearches);
        swipeRevealLayout=(SwipeRevealLayout) view.findViewById(R.id.swipeRevealLayout);

        tv_type_delivery=(TextView) view.findViewById(R.id.typedelivery);
        tv_type_pickup=(TextView) view.findViewById(R.id.typepickup);
        tv_type_deliverypickup=(TextView) view.findViewById(R.id.typepickupndelivery);

        tv_type_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tv_type_deliverypickup.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery");
                    tv_type_delivery.setText("Delivery & Pickup");
                    getDataFromServer("","Delivery");

                    //get only delivery items
                }
                else if(tv_type_deliverypickup.getText().equals("Delivery") && tv_type_delivery.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery & Pickup");
                    tv_type_delivery.setText("Delivery");
                    getDataFromServer("","Delivery & Pickup");

                    //get all items
                }
                else if(tv_type_deliverypickup.getText().equals("Pickup") && tv_type_delivery.getText().equals("Delivery"))
                {
                    tv_type_deliverypickup.setText("Delivery");
                    tv_type_delivery.setText("Pickup");
                    getDataFromServer("","Delivery");
                    //get delivery items only
                }
                else if(tv_type_deliverypickup.getText().equals("Delivery") && tv_type_delivery.getText().equals("Pickup"))
                {
                    tv_type_deliverypickup.setText("Pickup");
                    tv_type_delivery.setText("Delivery");
                    getDataFromServer("","Pickup");
                    //get pickup items only
                }
                else if(tv_type_deliverypickup.getText().equals("Pickup") && tv_type_delivery.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery & Pickup");
                    tv_type_pickup.setText("Pickup");
                    getDataFromServer("","Delivery & Pickup");
                    //get all items
                }
                swipeRevealLayout.close(true);
            }
        });

        tv_type_pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tv_type_deliverypickup.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Pickup");
                    tv_type_pickup.setText("Delivery & Pickup");
                    getDataFromServer("","Pickup");
                    //get only pickup items
                }
                else if(tv_type_deliverypickup.getText().equals("Pickup") && tv_type_pickup.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery & Pickup");
                    tv_type_pickup.setText("Pickup");
                    getDataFromServer("","Delivery & Pickup");
                    //get all items
                }
                else if(tv_type_deliverypickup.getText().equals("Delivery") && tv_type_pickup.getText().equals("Delivery & Pickup"))
                {
                    tv_type_deliverypickup.setText("Delivery & Pickup");
                    tv_type_pickup.setText("Delivery");
                    getDataFromServer("","Delivery & Pickup");
                    //get all items
                }
                else if(tv_type_deliverypickup.getText().equals("Delivery") && tv_type_pickup.getText().equals("Pickup"))
                {
                    tv_type_deliverypickup.setText("Pickup");
                    tv_type_pickup.setText("Delivery");
                    getDataFromServer("","Pickup");

                    //get pickup items only
                }
                else if(tv_type_deliverypickup.getText().equals("Pickup") && tv_type_pickup.getText().equals("Delivery"))
                {
                    tv_type_deliverypickup.setText("Delivery");
                    tv_type_delivery.setText("Pickup");
                    getDataFromServer("","Delivery");
                    //get delivery items only
                }
                swipeRevealLayout.close(true);
            }
        });


        //searchBar.getLastSuggestions();

      /*  mSearch=(EditText)view.findViewById(R.id.edtKeyword);
        mSearch.setFocusable(false);
        mSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearch.setFocusable(true);
            }
        });*/

        btnDeals=(Button) view.findViewById(R.id.btnDeals);
        btnDeals.setOnClickListener(this);
        btnCombos=(Button)view.findViewById(R.id.btnCombos);
        btnCombos.setOnClickListener(this);

        btnCartAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(getActivity(), ActivityCartNew.class);
                startActivity(iMyOrder);
            }
        });
        //
        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                //totalItemCount = linearLayoutManager.getItemCount();
                //lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (linearLayoutManager.findFirstVisibleItemPosition() >= 1) {
                   if(optionsLayout.getVisibility()==View.GONE)
                    optionsLayout.setVisibility(View.VISIBLE);
                } else {
                    optionsLayout.setVisibility(View.GONE);
                }

                if (fab.getVisibility() != View.VISIBLE) {
                    FabTransformation.with(fab).transformFrom(toolbarFooter);
                }
            }
        });
        linearLayoutManager = (LinearLayoutManager) recycler_view.getLayoutManager();

        //
        optionsLayout = (LinearLayout) view.findViewById(R.id.linearLayout10);
        //
        txtItems=(TextView) view.findViewById(R.id.txtItems);
        txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());

        btnPickup = (Button) view.findViewById(R.id.btnPickup);
        btnPickup.setOnClickListener(this);
        //
        dla = new DeliveryListAdapter();

        // recycler_view.setAdapter(dla);

        btnFilter=(Button)view.findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(this);
        //



        //taken from onresume

        loadSliderImages();
        getFilterDataFromServer();

        return view;
    }

    DeliveryListAdapter dla;

    public LinearLayoutManager linearLayoutManager;
    PickUpListFragment pickUpListFragment;
    CombosFragment combosFragment;

    public void setPickUpFragment() {
        pickUpListFragment = new PickUpListFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.main_activity_content_frame, pickUpListFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

    }
    public void setComboFragment() {
        combosFragment = new CombosFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.main_activity_content_frame, combosFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

    }

    RecyclerView rv_cusines;
    TextView filter_apply,filter_reset;
    CheckBox cb_distanceFilter,cb_ratingFilter,cb_vegFilter,cb_splRecipeFilter,cb_100Range,cb_200Range,cb_300Range,cb_AboveRange;


    public void initFilterView(View view)
    {
        rv_cusines=(RecyclerView)view.findViewById(R.id.rvcusines);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_cusines.setLayoutManager(mLayoutManager);
        rv_cusines.setItemAnimator(new DefaultItemAnimator());
        // rv_cusines.smoothScrollToPosition(0);

        // TextView filter_apply,filter_reset;
        //CheckBox cb_distanceFilter,cb_ratingFilter,cb_vegFilter,cb_splRecipeFilter,cb_100Range,cb_200Range,cb_300Range,cb_AboveRange;

        filter_apply=(TextView)view.findViewById(R.id.filter_apply);
        filter_reset=(TextView)view.findViewById(R.id.filter_reset);
        cb_distanceFilter=(CheckBox)view.findViewById(R.id.cb_distanceFilter);
        cb_ratingFilter=(CheckBox)view.findViewById(R.id.cb_ratingFilter);
        cb_vegFilter=(CheckBox)view.findViewById(R.id.cb_vegFilter);
        cb_splRecipeFilter=(CheckBox)view.findViewById(R.id.cb_splRecipeFilter);
        cb_100Range=(CheckBox)view.findViewById(R.id.cb_100Range);
        cb_200Range=(CheckBox)view.findViewById(R.id.cb_200Range);
        cb_300Range=(CheckBox)view.findViewById(R.id.cb_300Range);
        cb_AboveRange=(CheckBox)view.findViewById(R.id.cb_AboveRange);

        cb_distanceFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    if (isChecked) {

                        filterResult.put("dist", "true");
                              cb_distanceFilter.setButtonDrawable(R.drawable.filter_checked);
                    } else {

                        filterResult.put("dist", "false");
                        cb_distanceFilter.setButtonDrawable(R.drawable.filter_unchecked);
                    }
                }
                catch (JSONException ex)
                {

                }

            }
        });
        cb_ratingFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("rating", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_ratingFilter.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    try {
                        filterResult.put("rating", "false");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_ratingFilter.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_vegFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("veg", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_vegFilter.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    try {
                        filterResult.put("veg", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_vegFilter.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_splRecipeFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("spl", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_splRecipeFilter.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    try {
                        filterResult.put("spl", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_splRecipeFilter.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_100Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("100", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_100Range.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    try {
                        filterResult.put("100", "false");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_100Range.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_200Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("200", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_200Range.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    try {
                        filterResult.put("200", "false");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_200Range.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_300Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("300", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_300Range.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    try {
                        filterResult.put("300", "false");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_300Range.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_AboveRange.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    try {
                        filterResult.put("above", "true");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_AboveRange.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    try {
                        filterResult.put("above", "false");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cb_AboveRange.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });


        filter_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("*************"+filterResult.toString());
               bottomSheetDialog.dismiss();

            }
        });


        cusineList=new ArrayList<>();
        fla=new CuisineListAdapter();
    }

    List<Cusines> cusineList;


    CuisineListAdapter fla;


    public class CuisineListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_cusine, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return cusineList.size();
        }
        Cusines listCusine;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCusine=cusineList.get(position);
            holder.CusineName.setText(listCusine.getCuisineName());
            holder.CusineId=listCusine.getCuisineId();
            holder.cb_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        holder.cb_select.setButtonDrawable(R.drawable.filter_checked);
                    }
                    else {

                        holder.cb_select.setButtonDrawable(R.drawable.filter_unchecked);
                    }
                }
            });

        }
    }


    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView CusineName;
        String CusineId;
        CheckBox cb_select;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            CusineName=(TextView)itemView.findViewById(R.id.tv_cusine);
            cb_select=(CheckBox)itemView.findViewById(R.id.cb_cusineitem);
        }
    }



    public void getFilterDataFromServer()
    {
      /*  String mUrl="http://128.199.173.98/api/getCuisine.php?cuisine=true&accesskey=12345";
        System.out.println(mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {

                //closeDialog();
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                parseFilterJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });*/
    }
    public void parseFilterJSONData(String strResponse) {
        try
        {
            System.out.println("******"+strResponse.toString());
            JSONObject json = new JSONObject(strResponse.toString());
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            //  cusineList.clear();
            for (int ii = 0; ii < data.length(); ii++) {
                Cusines clist = new Cusines();
                JSONObject object = data.getJSONObject(ii);
                JSONObject cmenu = object.getJSONObject("cusine");
                clist.setCuisineId(cmenu.getString("id"));
                clist.setCuisineName(cmenu.getString("cuisine"));
                cusineList.add(clist);
            }
            rv_cusines.setAdapter(fla);
            fla.notifyDataSetChanged();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }






    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPickup:
                setPickUpFragment();
                break;
            case R.id.btnFilter:
                FilterFragment.setFilterListener(this);
                startActivity(new Intent(getActivity(),FilterFragment.class));
                break;
            case R.id.btnDeals:

                Log.e("dealsApply"," "+dealsApply);
                if(dealsApply)
                {
                    dealsApply=false;
                    loadSliderImages();
                }
                else
                {
                    dealsApply=true;
                    loadSliderImages();
                }

                break;
            case R.id.btnCombos:
                setComboFragment();
                break;
            default:
                break;
        }
    }

    @Override
    public void onApplyFilter(JSONObject result) {
        Log.i("result",result.toString());
        Toast.makeText(getActivity(), result.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSearchStateChanged(boolean enabled) {

        if(enabled)
        {
            recycler_view.setVisibility(View.GONE);
        }
        else
        {
            recycler_view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSearchConfirmed(CharSequence text) {

        //getdata with search

       //  getDataFromServer(text.toString());


    }

    @Override
    public void onButtonClicked(int buttonCode) {


    }
    public List<String> loadSearchSuggestionFromDisk()
    {
        List<String> ll=new ArrayList<>();
        ll.add(0,"Biryani");
        ll.add(1,"Chilli Chicken");
        ll.add(2,"Dum Biryani");


        return ll;
       /* DBHelper_New db=new DBHelper_New(getActivity());
        List<List<String>> results=db.getDataByQuery("SELECT "+db.FOOD_NAME+" "+db.TABLE_RECENTSEARCH);
        return results;*/
    }


    public class DeliveryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        final int SLIDER = 0, ROW = 1;
        private final int VIEW_TYPE_LOADING = 3;

        private OnLoadMoreListener mOnLoadMoreListener;

        private boolean isLoading;
        private int visibleThreshold = 5;
        private int lastVisibleItem, totalItemCount;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            switch (viewType) {
                case SLIDER:
                    View vSlider = inflater.inflate(R.layout.item_viewpager, viewGroup, false);
                    viewHolder = new ViewHolderSlider(vSlider);
                    break;

                case ROW:
                    View vRow = inflater.inflate(R.layout.menu_items_new, viewGroup, false);
                    viewHolder = new ViewHolderRow(vRow);
                    break;
                default:
                    break;
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {



            switch (viewHolder.getItemViewType()) {
                case SLIDER:
                    ViewHolderSlider vh1 = (ViewHolderSlider) viewHolder;
                    configureViewHolderSlider(vh1, position);
                    break;
                case ROW:
                    ViewHolderRow vh2 = (ViewHolderRow) viewHolder;
                    configureViewHolderRow(vh2, position);
                    break;
                default:

                    break;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return menuList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? SLIDER : ROW ;
        }


        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        public void configureViewHolderSlider(final ViewHolderSlider viewHolder, int position) {


                viewHolder.pager.setAdapter(new SlidingImageAdapter(getActivity(), sliderImages));
                viewHolder.minipager.setAdapter(new SlidingImageMenuAdapter(getActivity(),imageLoader));
                //viewHolder.indicator.setViewPager(viewHolder.pager);




           /* final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
            viewHolder.indicator.setRadius(2 * density);

           */

            viewHolder.pager.setCurrentItem(1, true);

          /*  NUM_PAGES = sliderImages.size();
            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }

                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 5000, 5000);*/

        }

        int ItemCount = 0;
        Combo listMenu;
        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {

            Double finalPrice=0.0;
            listMenu=menuList.get(position);
            holder.txtText.setText(listMenu.getComboName());


            holder.txtfoodtype.setText(listMenu.getComboFoodType());
            holder.txtStockQty.setText(listMenu.getComboQuantity());
            holder.txtweigthingms.setText(listMenu.getComboWeight()+" gms");
            holder.txtAreaNamePickup.setText(listMenu.getComboAddress());
            if(position > 1)
            {
                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0,40,0,0);
                holder.menuCard.setLayoutParams(params);
            }

            holder.txtSubText.setText(listMenu.getComboPrice() + " " + ComboListFragment.Currency);
            finalPrice=listMenu.getComboPrice();

          /*
            if(listMenu.getMenu_Deal_price() > 0)
            {
                holder.ivDealBanner.setVisibility(View.VISIBLE);
                holder.txtMainValue.setVisibility(View.VISIBLE);
                holder.txtMainValue.setText(listMenu.getMenu_price() + " " + ComboListFragment.Currency);
                holder.txtMainValue.setPaintFlags(holder.txtMainValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                Double afterDiscount=listMenu.getMenu_price()-listMenu.getMenu_Deal_price();
                holder.txtSubText.setText(String.valueOf(afterDiscount) + " " + ComboListFragment.Currency);
                finalPrice=afterDiscount;
            }
            else
            {
                holder.txtSubText.setText(listMenu.getMenu_price() + " " + ComboListFragment.Currency);
                finalPrice=listMenu.getMenu_price();

            }*/

            if(listMenu.getComboDeliveryType().toString().toLowerCase().contains("pickup"))
            {
                holder.itemLayout.setVisibility(View.VISIBLE);
                holder.pickupimage.setVisibility(View.VISIBLE);
                Picasso.with(getActivity()).load(listMenu.getComboPickupImageUrl().replaceAll("(?<!http:)//", "/")).placeholder(R.drawable.loading).into(holder.pickupimage);

                //imageLoader.DisplayImage(listMenu.getMenu_pickup_image(), holder.pickupimage);
            }

            holder.pickupimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getActivity(),MapAddrView.class);
                    i.putExtra("pimage",listMenu.getComboPickupImageUrl().replaceAll("(?<!http:)//", "/").toString());
                    i.putExtra("paddress",listMenu.getComboAddress().toString());
                    i.putExtra("platitude",listMenu.getComboLatitude());
                    i.putExtra("plongitude",listMenu.getComboLongitude());
                    startActivity(i);
                }
            });


            if(listMenu.getComboDeliveryType().toString().toLowerCase().contains("pickup"))
            {
                holder.txtDeliverytime.setText(listMenu.getComboDistance());
                holder.txtviewdelivery.setText("Distance");
            }
            else
            {
                holder.txtDeliverytime.setText(listMenu.getComboDeliveryTime());
                holder.txtviewdelivery.setText("Delivery Time");
            }

                Picasso.with(getActivity()).load(Constants.ImagesUrl + listMenu.getComboImage()).placeholder(R.drawable.loading).into(holder.imgThumb);


            //
            final Double finalPrice1 = finalPrice;
            holder.txtQty.setText(menuList.get(position).getItemCount()+"");
            holder.btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    if (qty < 0) {

                        //ItemCount--;
                    } else {
                        ItemCount++;

                    //    checkout.setVisibility(View.VISIBLE);
                        qty++;
                        double price = finalPrice1;
                        double changedPrice = price * qty;
                        //holder.txtSubText.setText(changedPrice+" "+ActivityMenuList.Currency);
                        holder.txtQty.setText(String.valueOf(qty));

                        DBHelper_New db = new DBHelper_New(getActivity());
                        //db.openDataBase();
                        long menuid = Long.parseLong(listMenu.getComboId());
                        System.out.println(menuid+"**************");
                        double menuprice = finalPrice1;
                        String menuname = listMenu.getComboName();
                        String menuImage=listMenu.getComboImage();



                            if (db.isDataExist(menuid,"Home Chef","DELIVERY","COMBO")) {
                             //   db.updateData(menuid, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY","COMBO");
                            } else {
                              //  db.addData(menuid, menuname, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY",menuImage,"COMBO");
                            }

                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        double ChangedPrice = currentCartPrice + menuprice;

                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        db.close();
                    }
                    //txtItems.setText(String.valueOf(ItemCount));
                    txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));

                    menuList.get(position).setItemCount(qty);

                    if (fab.getVisibility() == View.VISIBLE) {
                        FabTransformation.with(fab).transformTo(toolbarFooter);
                    }
                }
            });
            final Double finalPrice2 = finalPrice;
            holder.btndrc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    qty--;
/*
                    if (qty > 0)
                        ItemCount--;*/

                    if (qty <= 0) {


                        double price = finalPrice2;
                        //	holder.txtSubText.setText(price+" "+ComboListFragment.Currency);
                        holder.txtQty.setText("0");

                        long menuid = Long.parseLong(listMenu.getComboId());
                        DBHelper_New db = new DBHelper_New(getActivity());
                        //db.openDataBase();
                        db.deleteData(menuid);
                        db.close();

                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        if (currentCartPrice > 0) {
                            double ChangedPrice = currentCartPrice - price;
                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        }

                    } else {
                        ItemCount--;
                        if (qty == 1) {
                            double price = finalPrice2;
                            holder.txtQty.setText("0");
                            //holder.txtSubText.setText(price+" "+ComboListFragment.Currency);
                            DBHelper_New db = new DBHelper_New(getActivity());
                            //db.openDataBase();
                            long menuid = Long.parseLong(listMenu.getComboId());
                            double menuprice = finalPrice2;
                            String menuname = listMenu.getComboName();
                            String menuimage=listMenu.getComboImage();

                                if (db.isDataExist(menuid,"Home Chef","DELIVERY","COMBO")) {
                                   // db.updateData(menuid, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY","COMBO");
                                } else {
                                  //  db.addData(menuid, menuname, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY",menuimage,"COMBO");
                                }

                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        } else {

                            double price = finalPrice2;
                            double changedPrice = price * qty;
                            //holder.txtSubText.setText(changedPrice+" "+ComboListFragment.Currency);
                            holder.txtQty.setText(String.valueOf(qty));
                            DBHelper_New db = new DBHelper_New(getActivity());
                            //db.openDataBase();
                            long menuid = Long.parseLong(listMenu.getComboId());
                            double menuprice = finalPrice2;
                            String menuname =listMenu.getComboName();
                            String menuImage=listMenu.getComboImage();

                                if (db.isDataExist(menuid,"Home Chef","DELIVERY","COMBO")) {
                                  //  db.updateData(menuid, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY","COMBO");
                                } else {
                                  //  db.addData(menuid, menuname, qty, (menuprice * qty),"Home Chef","Home Chef","DELIVERY",menuImage,"COMBO");
                                }

                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        }
                    }
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));
                    txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());
                    //txtItems.setText(String.valueOf(ItemCount));
                    menuList.get(position).setItemCount(qty);
                    if (fab.getVisibility() == View.VISIBLE) {
                        FabTransformation.with(fab).transformTo(toolbarFooter);
                    }
                }
            });

        }
    }

    class ViewHolderSlider extends RecyclerView.ViewHolder {
        ViewPager pager,minipager;
        InfiniteViewPager mViewPager;
        CirclePageIndicator indicator;
        Button btnDelivery,btnPickup,btnDeals,btnCombos;
        SearchView searchViewPager;


        public ViewHolderSlider(View itemView) {
            super(itemView);
            pager = (ViewPager) itemView.findViewById(R.id.pager);
            pager.setClipToPadding(false);
            // set padding manually, the more you set the padding the more you see of prev & next page
            pager.setPadding(60, 0, 60, 20);
            // sets a margin b/w individual pages to ensure that there is a gap b/w them
            pager.setPageMargin(20);


            //menu pager

            minipager=(ViewPager) itemView.findViewById(R.id.minipager);

            pager.setClipToPadding(false);
            // set padding manually, the more you set the padding the more you see of prev & next page
            pager.setPadding(60, 0, 60, 0);
            // sets a margin b/w individual pages to ensure that there is a gap b/w them
            pager.setPageMargin(20);


            minipager.setClipToPadding(false);
            minipager.setPadding(20,0,70,0);
            minipager.setPageMargin(10);


            /*indicator = (CirclePageIndicator) itemView.findViewById(R.id.indicator);*/
            btnPickup = (Button) itemView.findViewById(R.id.btnPickup);
            btnCombos=(Button) itemView.findViewById(R.id.btnCombos);
            btnCombos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setComboFragment();
                }
            });
            btnPickup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setPickUpFragment();
                }
            });
            btnDeals=(Button) itemView.findViewById(R.id.btnDeals);

            if(dealsApply)
                btnDeals.setBackgroundResource(R.drawable.borderbackgroundselected);

            btnDeals.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e("dealsApply"," "+dealsApply);

                    if(dealsApply)
                    {
                        dealsApply=false;
                        loadSliderImages();
                    }
                    else
                    {
                        dealsApply=true;
                        loadSliderImages();
                    }
                }
            });

            btnDelivery=(Button) itemView.findViewById(R.id.btnDelivery);
            btnDelivery.setBackgroundResource(R.drawable.borderbackgroundselected);
            btnDelivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            btnCombos=(Button) itemView.findViewById(R.id.btnDeals);
            btnCombos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dealsApply=true;
                    loadSliderImages();
                }
            });
        }
    }

    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty, txtDeliverytime, txtweigthingms, txtfoodtype,txtMainValue,txtviewdelivery,txtAreaNamePickup;
        ImageView imgThumb,ivDealBanner,pickupimage;
        LinearLayout itemLayout;
        Button btninc, btndrc;
        RatingBar rb;
        CardView menuCard;

        public ViewHolderRow(View convertView) {
            super(convertView);
            txtText = (TextView) convertView.findViewById(R.id.txtText);
            txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
            imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
            pickupimage = (ImageView) convertView.findViewById(R.id.pickupimage);
            txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgms);
            txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQty);
            txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtype);
            txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtime);
            txtQty = (TextView) convertView.findViewById(R.id.txtQty);
            ivDealBanner=(ImageView) convertView.findViewById(R.id.ivDealBanner);
            btninc = (Button) convertView.findViewById(R.id.btninc);
            btndrc = (Button) convertView.findViewById(R.id.btndrc);
            txtMainValue=(TextView) convertView.findViewById(R.id.txtMainValue);
            txtviewdelivery=(TextView) convertView.findViewById(R.id.txtviewdelivery);
            txtAreaNamePickup=(TextView) convertView.findViewById(R.id.txtAreaNamePickup);
            itemLayout=(LinearLayout) convertView.findViewById(R.id.itemLayout);
            menuCard=(CardView) convertView.findViewById(R.id.menuCard);
        }
    }

    // clear arraylist variables before used
    void clearData() {
      menuList.clear();
    }


    ProgressDialog pd;



    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
        loading_view.setVisibility(View.VISIBLE);
      /*  if (pd == null)
            pd = new ProgressDialog(getActivity());
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();*/
    }

    public void closeDialog() {
        // do something wih the result
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(loading_view.getVisibility()==View.VISIBLE)
                            loading_view.setVisibility(View.GONE);
            }
        });

       /* if (pd != null)
            pd.dismiss();*/
    }

    public void getDataFromServer(String keyword,String DeliveryType) {
        //

        //showProgressDialog();

        String mUrl="";


        if(DeliveryType.equals("Delivery"))
        {
            if(ApplicationLoader.getProchefID().isEmpty() && keyword.isEmpty())
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=delivery&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else if(ApplicationLoader.getProchefID().isEmpty() && keyword.length() > 0)
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&keyword="+keyword+"&category_id=00&deliverytype=delivery&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else
                mUrl="http://128.199.173.98/api/get-menu-prochef.php?accesskey=12345&category_id=00&chefid="+ApplicationLoader.getProchefID()+"&deliverytype=delivery&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();

        }
        else if(DeliveryType.equals("Pickup"))
        {

            if(ApplicationLoader.getProchefID().isEmpty() && keyword.isEmpty())
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else if(ApplicationLoader.getProchefID().isEmpty() && keyword.length() > 0)
                mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&keyword="+keyword+"&category_id=00&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else
                mUrl="http://128.199.173.98/api/get-menu-prochef.php?accesskey=12345&category_id=00&chefid="+ApplicationLoader.getProchefID()+"&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        }
        else
        {
            if(ApplicationLoader.getProchefID().isEmpty() && keyword.isEmpty())
                mUrl="http://128.199.173.98/api/get-deals-combos.php?accesskey=12345&category_id=00&deliverytype=deliverynpickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else if(ApplicationLoader.getProchefID().isEmpty() && keyword.length() > 0)
                mUrl="http://128.199.173.98/api/get-deals-combos.php?accesskey=12345&category_id=00&deliverytype=deliverynpickup&keyword="+keyword+"&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else
                mUrl="http://128.199.173.98/api/get-deals-combos.php?accesskey=12345&category_id=00&deliverytype=deliverynpickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        }

        System.out.println(mUrl);
    /*    OkHttpClient client = new OkHttpClient();
        Request request = new Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {

                closeDialog();
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });*/
    }


    // method to parse json data from server
    public void parseJSONData(String strResponse) {

        clearData();

        try {

            menuList.add(null);


            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int ii = 0; ii < data.length(); ii++) {

                Combo comboItem=new Combo();
                JSONObject object = data.getJSONObject(ii);

                JSONObject menu = object.getJSONObject("MenuActivity");

                Log.e("MenuActivity", menu.toString());

                Log.e("dealPrice"," "+menu.getString("deal_price"));

                comboItem.setComboId(menu.getString("id"));
                comboItem.setComboAddress(menu.getString("address_1"));
                comboItem.setComboName(menu.getString("deal_name"));
                comboItem.setComboImage(menu.getString("deal_image"));
                comboItem.setComboPrice(Double.parseDouble(menu.getString("total")));
                comboItem.setComboQuantity(menu.getString("Quantity"));
                comboItem.setComboFoodType(menu.getString("food_type"));
                comboItem.setComboChefID(menu.getString("chefid"));
                comboItem.setComboWeight(menu.getString("weight_in_grams"));
                comboItem.setComboDeliveryType(menu.getString("delivery_type"));
                comboItem.setComboRating(menu.getString("rating"));
                comboItem.setComboDistance(menu.getString("distance"));
                comboItem.setComboDeliveryTime(menu.getString("delivery_time"));
                comboItem.setComboPickupImageUrl(menu.getString("pickupImageUrl"));
                comboItem.setComboLatitude(menu.getString("latitude"));
                comboItem.setComboLongitude(menu.getString("longitude"));
                menuList.add(comboItem);
            }


            dla.notifyDataSetChanged();
            recycler_view.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // create arraylist variables to store data from server
    static ArrayList<Long> Menu_ID = new ArrayList<Long>();
    static ArrayList<String> Menu_name = new ArrayList<String>();
    static ArrayList<Double> Menu_price = new ArrayList<Double>();
    static ArrayList<String> Menu_image = new ArrayList<String>();
    static ArrayList<String> Menu_stock_qty = new ArrayList<String>();
    static ArrayList<String> Menu_food_type = new ArrayList<String>();
    static ArrayList<String> Menu_delivery = new ArrayList<String>();
    static ArrayList<String> Menu_weigth_grms = new ArrayList<String>();
    static ArrayList<String> Menu_serves = new ArrayList<String>();
    static ArrayList<String> Menu_delivery_time = new ArrayList<String>();
    static ArrayList<Double> Menu_Deal_price = new ArrayList<Double>();
    static ArrayList<String> Menu_pickup_location = new ArrayList<String>();
    static ArrayList<String> Menu_pickup_image = new ArrayList<String>();
    static ArrayList<String> Menu_pickup_distance = new ArrayList<String>();



    @Override
    public void onResume() {
        super.onResume();

    }

    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper_New dbhelper = new DBHelper_New(ctx);
        //dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }




    public void loadSliderImages() {
  /*      showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        Request request = new Builder().url("http://chefmonster.com/api/getSliders.php").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {

            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                // Log.i("response",response.body());
                //Log.i("response",response.body().string());
                closeDialog();
                if (response.isSuccessful()) {
                    //processSliderImages(response.body().string());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processSliderImages(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });*/

    }

    public void processSliderImages(String response) {
        //{"slides": ["slide.jpg","slide.jpg","slide.jpg"]}
        sliderImages.clear();
        try {
            JSONObject jobj = new JSONObject(response);
            JSONArray jSlides = jobj.getJSONArray("slides");
            for (int i = 0; i < jSlides.length(); i++) {
                sliderImages.add(jSlides.getString(i));
            }
            //
            //slidingImageAdapter = new SlidingImageAdapter(getActivity(), sliderImages, imageLoader);
            recycler_view.setAdapter(dla);
            //
            showProgressDialog();
            new CountDownTimer(2000,1000) {


                /** This method will be invoked on finishing or expiring the timer */
                @Override
                public void onFinish() {
                    /** Creates an intent to start new activity */
                    getDataFromServer("","Delivery & Pickup");
                }

                /** This method will be invoked in every 1000 milli seconds until
                 * this timer is expired.Because we specified 1000 as tick time
                 * while creating this CountDownTimer
                 */
                @Override
                public void onTick(long millisUntilFinished) {

                }
            }.start();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    ArrayList<String> sliderImages = new ArrayList<>();
    SlidingImageAdapter slidingImageAdapter;




    public class SlidingImageMenuAdapter extends PagerAdapter {
        private ArrayList<String> IMAGES;
        private LayoutInflater inflater;
        private Context context;
        private ImageLoader imageLoader;

        public SlidingImageMenuAdapter(Context context, ImageLoader imageLoader) {
            this.context = context;
            inflater = LayoutInflater.from(context);
            this.imageLoader=imageLoader;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.sliding_category, view, false);

            //assert imageLayout != null;



            final TextView btnMiniSlider1=(TextView)imageLayout.findViewById(R.id.btnMiniSlider1);
            final TextView btnMiniSlider2=(TextView)imageLayout.findViewById(R.id.btnMiniSlider2);

            btnMiniSlider1.setText("Today's Unique");
            btnMiniSlider2.setText("Be more with Combos");


    /*    imageView1.setImageResource(R.drawable.dealsslider);
        imageView2.setImageResource(R.drawable.tranding);*/

            if(position!=0) {

                btnMiniSlider1.setText("Deals");
                btnMiniSlider2.setText("Tranding");
                btnMiniSlider1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("Deals clicked");
                    }
                });
                btnMiniSlider2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("Tranding clicked");
                    }
                });
        /*    imageView1.setImageResource(R.drawable.todayunique);
            imageView2.setImageResource(R.drawable.combomore);*/
            }
            else {
                btnMiniSlider1.setText("Today's Unique");
                btnMiniSlider2.setText("Be more with Combos");
                btnMiniSlider1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("Today's Unique");
                    }
                });
                btnMiniSlider2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("Be more with Combos");
                    }
                });
            }

            // imageView.setImageResource(IMAGES.get(position));
     /*   Log.i("img", Constants.ImagesSlideURL + IMAGES.get(position));
        //Picasso.with(context).load(Constants.ImagesSlideURL + IMAGES.get(position)).resize(200,200).into(imageView);

        imageLoader.DisplayImage(Constants.ImagesSlideURL + IMAGES.get(position), imageView);
        view.addView(imageLayout, 0);*/
            view.addView(imageLayout, 0);

            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }

}
