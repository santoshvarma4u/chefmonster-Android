package com.android.chefmonster.UI.Menu;

/**
 * Created by SantoshT on 11/21/2016.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
