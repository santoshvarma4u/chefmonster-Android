package com.android.chefmonster.UI.Menu.Filters;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.android.chefmonster.R;
import com.android.chefmonster.UI.Menu.FilterFragment;
import com.android.chefmonster.UI.Menu.FilterListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link } subclass.
 * Activities that contain this fragment must implement the
 * {@link FilterBottomSheet.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FilterBottomSheet#newInstance} factory method to
 * create an instance of this fragment.
 */

public class FilterBottomSheet extends BottomSheetDialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView rv_cusines;
    TextView filter_apply,filter_reset;
    CheckBox cb_distanceFilter,cb_ratingFilter,cb_vegFilter,cb_splRecipeFilter,cb_100Range,cb_200Range,cb_300Range,cb_AboveRange;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static View bottomSheetInternal;

    private static BottomSheetBehavior bottomSheetBehavior;

    private static FilterBottomSheet INSTANCE;

    private OnFragmentInteractionListener mListener;

    public FilterBottomSheet() {
        // Required empty public constructor
    }

    public static FilterBottomSheet newInstance(String string) {
        FilterBottomSheet f = new FilterBottomSheet();
        Bundle args = new Bundle();
        args.putString("string", string);
        f.setArguments(args);
        return f;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FilterBottomSheet.
     */
    // TODO: Rename and change types and number of parameters
    public static FilterBottomSheet newInstance(String param1, String param2) {
        FilterBottomSheet fragment = new FilterBottomSheet();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_filter_bottom_sheet, container, false);
        rv_cusines=(RecyclerView)view.findViewById(R.id.rvcusines);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_cusines.setLayoutManager(mLayoutManager);
        rv_cusines.setItemAnimator(new DefaultItemAnimator());
       // rv_cusines.smoothScrollToPosition(0);

        // TextView filter_apply,filter_reset;
        //CheckBox cb_distanceFilter,cb_ratingFilter,cb_vegFilter,cb_splRecipeFilter,cb_100Range,cb_200Range,cb_300Range,cb_AboveRange;

        filter_apply=(TextView)view.findViewById(R.id.filter_apply);
        filter_reset=(TextView)view.findViewById(R.id.filter_reset);
        cb_distanceFilter=(CheckBox)view.findViewById(R.id.cb_distanceFilter);
        cb_ratingFilter=(CheckBox)view.findViewById(R.id.cb_ratingFilter);
        cb_vegFilter=(CheckBox)view.findViewById(R.id.cb_vegFilter);
        cb_splRecipeFilter=(CheckBox)view.findViewById(R.id.cb_splRecipeFilter);
        cb_100Range=(CheckBox)view.findViewById(R.id.cb_100Range);
        cb_200Range=(CheckBox)view.findViewById(R.id.cb_200Range);
        cb_300Range=(CheckBox)view.findViewById(R.id.cb_300Range);
        cb_AboveRange=(CheckBox)view.findViewById(R.id.cb_AboveRange);

        cb_distanceFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                {
                    cb_distanceFilter.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    cb_distanceFilter.setButtonDrawable(R.drawable.filter_unchecked);
                }

            }
        });
        cb_ratingFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        cb_ratingFilter.setButtonDrawable(R.drawable.filter_checked);
                    }
                else
                    {
                        cb_ratingFilter.setButtonDrawable(R.drawable.filter_unchecked);
                    }
            }
        });
        cb_vegFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    cb_vegFilter.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    cb_vegFilter.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_splRecipeFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    cb_splRecipeFilter.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    cb_splRecipeFilter.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_100Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    cb_100Range.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    cb_100Range.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_200Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    cb_200Range.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    cb_200Range.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_300Range.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    cb_300Range.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    cb_300Range.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });
        cb_AboveRange.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    cb_AboveRange.setButtonDrawable(R.drawable.filter_checked);
                }
                else
                {
                    cb_AboveRange.setButtonDrawable(R.drawable.filter_unchecked);
                }
            }
        });


        filter_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });


        cusineList=new ArrayList<>();
        dla=new CuisineListAdapter();

        //dismiss sheet



        return view;
    }


    public static FilterListener filterListener;

    public static void setFilterListener(FilterListener filterListener){
        FilterBottomSheet.filterListener=filterListener;
    }

    public void onApplyFilter()
    {

    }

    JSONObject result = new JSONObject();
    @Override
    public void onResume() {
        super.onResume();
        getDataFromServer();
    }

    List<Cusines> cusineList;


    CuisineListAdapter dla;

    public class CuisineListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_cusine, parent, false);
            viewHolder = new ViewHolderRow(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderRow vh2 = (ViewHolderRow) holder;
            configureViewHolderRow(vh2, position);
        }

        @Override
        public int getItemCount() {
            return cusineList.size();
        }
        Cusines listCusine;

        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {
            listCusine=cusineList.get(position);
            holder.CusineName.setText(listCusine.getCuisineName());
            holder.CusineId=listCusine.getCuisineId();
            holder.cb_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                        holder.cb_select.setButtonDrawable(R.drawable.filter_checked);
                    else
                        holder.cb_select.setButtonDrawable(R.drawable.filter_unchecked);

                }
            });

        }
    }


    class ViewHolderRow extends RecyclerView.ViewHolder{

        TextView CusineName;
        String CusineId;
        CheckBox cb_select;

        public ViewHolderRow(View itemView) {
            super(itemView);
            CusineName=(TextView)itemView.findViewById(R.id.tv_cusine);
            cb_select=(CheckBox)itemView.findViewById(R.id.cb_cusineitem);

        }
    }

    public void getDataFromServer()
    {
        String mUrl="http://128.199.173.98/api/getCuisine.php?cuisine=true&accesskey=12345";
        System.out.println(mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {

                //closeDialog();
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });
    }
    public void parseJSONData(String strResponse) {
        try
        {
            System.out.println("******"+strResponse.toString());
            JSONObject json = new JSONObject(strResponse.toString());
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
          //  cusineList.clear();
            for (int ii = 0; ii < data.length(); ii++) {
                Cusines clist = new Cusines();
                JSONObject object = data.getJSONObject(ii);
                JSONObject cmenu = object.getJSONObject("cusine");
                clist.setCuisineId(cmenu.getString("id"));
                clist.setCuisineName(cmenu.getString("cuisine"));
                cusineList.add(clist);
            }
            rv_cusines.setAdapter(dla);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
