package com.android.chefmonster.UI.Menu.ProChef;

/**
 * Created by SantoshT on 11/25/2016.
 */

public class Resturant {
    String title,desc,cost,rating,time,imageUrl;
    int Foodtype;

    public int getFoodtype() {
        return Foodtype;
    }

    public void setFoodtype(int foodtype) {
        Foodtype = foodtype;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
