package com.android.chefmonster.UI.Menu.ProChef;

/**
 * Created by SantoshT on 11/25/2016.
 */

public class Branches {

    String BranchID ;
    String  BranchName;
    String BrnachAddr;
    String BranchDistance;
    String  BranchPaymentType;
    String  BranchMinOrder;
    String BranchDeliveryType;

    public String getBranchID() {
        return BranchID;
    }

    public void setBranchID(String branchID) {
        BranchID = branchID;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getBrnachAddr() {
        return BrnachAddr;
    }

    public void setBrnachAddr(String brnachAddr) {
        BrnachAddr = brnachAddr;
    }

    public String getBranchDistance() {
        return BranchDistance;
    }

    public void setBranchDistance(String branchDistance) {
        BranchDistance = branchDistance;
    }

    public String getBranchPaymentType() {
        return BranchPaymentType;
    }

    public void setBranchPaymentType(String branchPaymentType) {
        BranchPaymentType = branchPaymentType;
    }

    public String getBranchMinOrder() {
        return BranchMinOrder;
    }

    public void setBranchMinOrder(String branchMinOrder) {
        BranchMinOrder = branchMinOrder;
    }

    public String getBranchDeliveryType() {
        return BranchDeliveryType;
    }

    public void setBranchDeliveryType(String branchDeliveryType) {
        BranchDeliveryType = branchDeliveryType;
    }
}
