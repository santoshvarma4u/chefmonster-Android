package com.android.chefmonster.UI;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.ChefApp.ChefRegistration;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.Locations.SelectLocationActivity;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.Menu.DeliveryListFragment;
import com.android.chefmonster.UI.Menu.FilterInterface;
import com.android.chefmonster.UI.Menu.Search.SearchActivity;
import com.android.chefmonster.Utills.ImageLoader;

import studios.codelight.smartloginlibrary.manager.UserSessionManager;
import studios.codelight.smartloginlibrary.users.SmartUser;

/**
 * Created by SantoshT on 9/8/2016.
 */
public class MainActivity extends AppCompatActivity implements FilterInterface,AdapterView.OnItemSelectedListener, ChefRegistration.OnFragmentInteractionListener {

    private Toolbar mToolbar;
    private Context mContext;
    public int SEARCH_REQ_CODE=2454;

    private TextView tv_imchef, tv_username, tv_userEmail, tv_btmhomechef,tv_btmprochef,tv_btmcancel;
    private ImageView iv_profile_pic,imgSearch;
    public ImageLoader imageLoader;
    private ImageView ab_filter;

    private BottomSheetBehavior bottomSheetBehavior ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        init(savedInstanceState);
        dbhelper=new DBHelper_New(getApplicationContext());
    }

    Spinner spinner;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_spinner, menu);

        MenuItem item = menu.findItem(R.id.spinner);
        spinner = (Spinner) MenuItemCompat.getActionView(item);


        spinner.setOnItemSelectedListener(this);
        //spinner.setPopupBackgroundResource(getResources().getColor(R.color.white));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.cheftypes, android.R.layout.simple_spinner_item);
        spinner.setBackgroundColor(getResources().getColor(R.color.white));

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!ApplicationLoader.getProchefID().isEmpty())
                {
                    TextView text1=(TextView) spinner.getSelectedView();
                    text1.setText("ProChecf");
                }
            }
        },1*500);

        return true;
    }

    private void init(@Nullable final Bundle savedInstanceState) {
        bindResources();
        setUpToolbar();
        //setUpDrawer();
        restoreState(savedInstanceState);
        //
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isFirstTime = false;
            }
        }, 2 * 1000);

    }

    private BottomSheetBehavior mBottomSheetBehavior;

    private void bindResources() {
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);


        ab_filter=(ImageView) findViewById(R.id.ab_filter);
        tv_btmhomechef=(TextView) findViewById(R.id.btmhomechef);
        tv_btmprochef=(TextView) findViewById(R.id.btmprochef);
        tv_btmcancel=(TextView) findViewById(R.id.btmcancel);


        imageLoader = new ImageLoader(getApplicationContext());
        ab_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DeliveryListFragment dlf = (DeliveryListFragment)getSupportFragmentManager().findFragmentByTag("lastSMSFragment");
                dlf.filterHit();
            }
        });
        imgSearch=(ImageView)findViewById(R.id.imgSearch);
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this, SearchActivity.class),SEARCH_REQ_CODE);
            }
        });




        final View viewBottomSheet = findViewById(R.id.bottom_sheet) ;
        bottomSheetBehavior = BottomSheetBehavior.from(viewBottomSheet) ;
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

    }
    TextView txtChefSelection;

    private void setUpToolbar() {

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);
        TextView City=(TextView) findViewById(R.id.txtLocalCity);
        TextView AddressLocal=(TextView) findViewById(R.id.txtLocalAddress);
        ImageView iv_menu_nav=(ImageView) findViewById(R.id.iv_menu_nav);
        txtChefSelection=(TextView) findViewById(R.id.txtChefSelection);
        City.setText(ApplicationLoader.getUserCity());
        AddressLocal.setText(ApplicationLoader.getUserLocation());
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(MainActivity.this,MenuActivity.class);
                startActivity(intent);
               // mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
        City.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });
        AddressLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });
        txtChefSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivityForResult(new Intent(MainActivity.this, DeliveryTypeSelection.class),DEL_CODE);

                //mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
               /* DeliveryTypeSelectionBS deliveryTypeSelectionBS = new DeliveryTypeSelectionBS();
                deliveryTypeSelectionBS.show(getSupportFragmentManager(),"BottomSheet Fragment");*/



            }
        });
        tv_btmhomechef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                txtChefSelection.setText("Delivery");
            }
        });
        tv_btmprochef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                txtChefSelection.setText("Pickup");

            }
        });
        tv_btmcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });
    }
    DBHelper_New dbhelper;
    private void setUpDrawer()
    {
        dbhelper=new DBHelper_New(getApplicationContext());
        final SmartUser currentUser = UserSessionManager.getCurrentUser(getApplicationContext());

        // View v=mNavigationHeaderView;
       /* mNavigationView
                .getHeaderView(0)
                .findViewById(R.id.navigation_drawer_header_clickable)
                .setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                      mDrawerLayout.closeDrawer(GravityCompat.START);
                        //check is the user already logged in or not
                        if(ApplicationLoader.getUserEmail().equals(""))
                        {
                            Intent in=new Intent(mContext, Login.class);
                            in.putExtra("FromClass","MainActivity");
                            startActivityForResult(in,1234);

                        }
                        else{

                            startActivityWithDelay(UserProfile.class);
                        }

                    }
                });*/




      /*  mNavigationView.setNavigationItemSelectedListener
                (
                        new NavigationView.OnNavigationItemSelectedListener()
                        {
                            @Override
                            public boolean onNavigationItemSelected(final MenuItem item)
                            {
                                mDrawerLayout.closeDrawer(GravityCompat.START);

                                switch (item.getItemId())
                                {
                                    case R.id.navigation_view_item_home:
                                        item.setChecked(true);
                                       // setToolbarTitle("Home","");
                                      //  showImageFragment(ImageFragment.sIMAGE_NEO);
                                        break;
*//*
                                    case R.id.navigation_view_item_deals:
                                        startActivityWithDelay(DealsHome.class);
                                       // showImageFragment(ImageFragment.sIMAGE_MORPHEUS);
                                        item.setChecked(true);
                                        break;*//*

                                    case R.id.navigation_view_item_cart:
                                       // setToolbarTitle("Cart","");

                                       startActivityWithDelay(CartTabbedActivity.class);
                                        break;

                                    case R.id.navigation_view_item_orders:
                                       // setToolbarTitle("My Orders","");
                                        startActivityWithDelay(Myorders.class);
                                        break;

                                    case R.id.navigation_view_item_become_chef:
                                      setToolbarTitle("Chef Registration","");
                                        // startActivityWithDelay(AboutActivity.class);
                                        showFragmanet(1,new ChefRegistration());
                                        break;

                                    case R.id.navigation_view_item_exit:

                                        dbhelper.deleteAllData();
                                        dbhelper.close();
                                        MainActivity.this.finish();
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);

                                        break;
                                }

                                return true;
                            }
                        }
                );*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1234&& resultCode==RESULT_OK){


        }else if(requestCode == SEARCH_REQ_CODE && resultCode == RESULT_OK){
            //pass data to fragment from here
            String menuId= data.getExtras().getString("MenuId").toString();
            String menuName=data.getExtras().getString("MenuName").toString();
            DeliveryListFragment dlf = (DeliveryListFragment)getSupportFragmentManager().findFragmentByTag("lastSMSFragment");
            dlf.searchRequest(menuId,menuName);
        }
        else if(requestCode == DEL_CODE && resultCode == RESULT_OK){
            //pass data to fragment from here
            String typeofdelivery= data.getExtras().getString("typeofdelivery").toString();
            DeliveryListFragment dlf = (DeliveryListFragment)getSupportFragmentManager().findFragmentByTag("lastSMSFragment");
            dlf.TypeofDeliveryRequest(typeofdelivery);
            txtChefSelection.setText(typeofdelivery);

        }



    }

    int DEL_CODE=5;

    private void restoreState(final @Nullable Bundle savedInstanceState)
    {
        // This allow us to know if the activity was recreated
        // after orientation change and restore the Toolbar title
        if (savedInstanceState == null)
        {
            showDefaultFragment();
        }
        else
        {
            setToolbarTitle((String) savedInstanceState.getCharSequence("Chef Monster"),"");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dbhelper.deleteAllData();
        dbhelper.close();
        MainActivity.this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void showDeliveryTypeDialog()
    {
    }

    private void showDefaultFragment()
    {
       /// System.out.println((ApplicationLoader.getUserLatLon().toString()));

        if(ApplicationLoader.getUserLat().length()>0)
        {
            setToolbarTitle(ApplicationLoader.getUserLocation(),ApplicationLoader.getUserCity());
        }
        else
        {
            setToolbarTitle("Select Location","");
        }
       showFragmanet(1,new DeliveryListFragment());
       // showFragmanet(1,new ComboListFragment());
    }

    public void showFragmanet(int flag,Fragment fm)
    {

        DeliveryListFragment lastSMSFragment=DeliveryListFragment.newInstance();


        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        if(flag==1)
            tx.add(R.id.main_activity_content_frame,fm,"lastSMSFragment");
        else
            tx.replace(R.id.main_activity_content_frame,fm,"lastSMSFragment");

        tx.addToBackStack("ChefMonster");
        tx.commit();
    }

    private void setToolbarTitle(final String title,final String Subtitle)
    {
        Typeface font = Typeface.createFromAsset(
                getAssets(),
                "fonts/RNSSanz-Medium.otf");

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(title);

            if(Subtitle.length()>1)
            {
                getSupportActionBar().setSubtitle(title);
                getSupportActionBar().setTitle(Subtitle);
            }
        }
    }

    private void setToolbarTitle(@StringRes final int string)
    {
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(string);
        }
    }

    private String getToolbarTitle()
    {
        if (getSupportActionBar() != null)
        {
            return (String) getSupportActionBar().getTitle();
        }
        return getString(R.string.app_name);
    }

    /**
     * start this activities with delay to avoid junk while closing the drawer
     */
    private void startActivityWithDelay(@NonNull final Class activity)
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                startActivity(new Intent(mContext, activity));
            }
        }, 500);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    protected void onSaveInstanceState(final Bundle outState)
    {
        outState.putCharSequence("Chef Monster", getToolbarTitle());
        super.onSaveInstanceState(outState);
    }

    boolean isFirstTime=true;

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        if(!isFirstTime){
            if(position==0){
                Log.i("home","home");
                setPickUpFragment(new DeliveryListFragment());
               // setPickUpFragment(new ComboListFragment());
            }else{
                Log.i("home","prochef");
                //setPickUpFragment(new ProChefListFragment());
            }
        }
    }


    public void setPickUpFragment(Fragment fragnement) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_activity_content_frame, fragnement);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void filterClicked() {

    }
}
