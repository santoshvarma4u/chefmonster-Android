package com.android.chefmonster.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.CartTabbedActivity;
import com.android.chefmonster.Checkout.Checkout;
import com.android.chefmonster.ChefApp.ChefLauncher;
import com.android.chefmonster.Locations.SelectLocationActivity;
import com.android.chefmonster.Orders.OrdersTabbedActivity;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.Login.Login;
import com.android.chefmonster.UI.Login.UserProfile;
import com.ivankocijan.magicviews.views.MagicTextView;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    protected ImageView ivCloseMenu,editlocation;
    protected TextView tvMenuAddress;
    protected TextView tvHomeMenu;
    protected TextView tvWorkMenu;
    protected TextView tvHome;
    protected TextView tvCart;
    protected TextView tvCheckout;
    protected TextView tvOrders;
    protected TextView tvMessages;
    protected TextView tvSignout;
    protected TextView tv_becomechef;
    protected MagicTextView tvMyaccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_menu);
        initView();

    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        if (view.getId() == R.id.ivCloseMenu) {
            intent = new Intent(MenuActivity.this, HomePage.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tvMenuAddress) {
            intent = new Intent(MenuActivity.this, SelectLocationActivity.class);
            intent.putExtra("firstTime", "false");
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tvHomeMenu) {
            intent = new Intent(MenuActivity.this, MenuActivity.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tvWorkMenu) {
            intent = new Intent(MenuActivity.this, MenuActivity.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tv_home) {
            intent = new Intent(MenuActivity.this, HomePage.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tv_cart) {
            intent = new Intent(MenuActivity.this, CartTabbedActivity.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tv_checkout) {
            intent = new Intent(MenuActivity.this, Checkout.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tv_orders) {
            intent = new Intent(MenuActivity.this, OrdersTabbedActivity.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tv_messages) {
            intent = new Intent(MenuActivity.this, MenuActivity.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tv_signout) {

            if (tvSignout.getText().toString().equalsIgnoreCase("Sign In")) {
                Intent in = new Intent(MenuActivity.this, Login.class);
                startActivity(in);
                finish();
            } else {

                ApplicationLoader.removeFromSharedPreferences("UserName");
                ApplicationLoader.removeFromSharedPreferences("Email");
                ApplicationLoader.removeFromSharedPreferences("ProfilePic");
                intent = new Intent(MenuActivity.this, HomePage.class);
                startActivity(intent);
                finish();

            }


        } else if (view.getId() == R.id.tv_becomechef) {
            intent = new Intent(MenuActivity.this, ChefLauncher.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.tv_myaccount) {
            intent = new Intent(MenuActivity.this, UserProfile.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == R.id.editlocation) {
            intent = new Intent(MenuActivity.this, SelectLocationActivity.class);
            intent.putExtra("firstTime", "false");
            startActivity(intent);
            finish();
        }
    }

    private void initView() {
        ivCloseMenu = (ImageView) findViewById(R.id.ivCloseMenu);
        ivCloseMenu.setOnClickListener(MenuActivity.this);
        editlocation = (ImageView) findViewById(R.id.editlocation);
        editlocation.setOnClickListener(MenuActivity.this);
        tvMenuAddress = (TextView) findViewById(R.id.tvMenuAddress);
        tvMenuAddress.setOnClickListener(MenuActivity.this);
        tvMenuAddress.setText(ApplicationLoader.getUserCity() + "," + ApplicationLoader.getUserLocation());
        tvHomeMenu = (TextView) findViewById(R.id.tvHomeMenu);
        tvHomeMenu.setOnClickListener(MenuActivity.this);
        tvWorkMenu = (TextView) findViewById(R.id.tvWorkMenu);
        tvWorkMenu.setOnClickListener(MenuActivity.this);
        tvHome = (TextView) findViewById(R.id.tv_home);
        tvHome.setOnClickListener(MenuActivity.this);
        tvCart = (TextView) findViewById(R.id.tv_cart);
        tvCart.setOnClickListener(MenuActivity.this);
        tvCheckout = (TextView) findViewById(R.id.tv_checkout);
        tvCheckout.setOnClickListener(MenuActivity.this);
        tvOrders = (TextView) findViewById(R.id.tv_orders);
        tvOrders.setOnClickListener(MenuActivity.this);
        tvMessages = (TextView) findViewById(R.id.tv_messages);
        tvMessages.setOnClickListener(MenuActivity.this);
        tvSignout = (TextView) findViewById(R.id.tv_signout);
        tvSignout.setOnClickListener(MenuActivity.this);
        tv_becomechef = (TextView) findViewById(R.id.tv_becomechef);
        tv_becomechef.setOnClickListener(MenuActivity.this);


        if (ApplicationLoader.getChefId().isEmpty()) {
            tv_becomechef.setText("Become a Chef");
        } else {
            tv_becomechef.setText("Switch to Chef");
        }

        if (ApplicationLoader.getUserEmail().isEmpty()) {
            tvSignout.setText("Sign In");
        }

        tvMyaccount = (MagicTextView) findViewById(R.id.tv_myaccount);
        tvMyaccount.setOnClickListener(MenuActivity.this);
    }
}
