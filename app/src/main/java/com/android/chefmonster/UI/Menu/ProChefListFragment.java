package com.android.chefmonster.UI.Menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.UI.Model.ProChef;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ChakravartyG on 9/17/2016.
 */

public class ProChefListFragment extends Fragment {
    View view;
    RecyclerView recycler_view;
    ImageLoader imageLoader;
    List<ProChef> proChefs;
    ProchefAdapter prochefAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.menu_procheflist, container, false);
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        //
        imageLoader = new ImageLoader(getActivity());
        proChefs = new ArrayList<>();
        prochefAdapter = new ProchefAdapter(getActivity(), proChefs);
        recycler_view.setAdapter(prochefAdapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadProChefData();
    }

    public class ProchefAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        List<ProChef> proChefList;
        Context context;

        public ProchefAdapter(Context context, List<ProChef> proChefList) {
            this.proChefList = proChefList;
            this.context = context;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

            View view = layoutInflater.inflate(R.layout.prochefitem, parent, false);

            return new ViewHolderRow(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final ProChef proChef = proChefList.get(position);
//            retaurentname, logo, minorder, paymenttype, sepkitchen,
//                    username, userreview, deltype, latitude, longitude, distance,
//                    duration, delivery_time, pickupImageUrl
            ViewHolderRow viewHolderRow = (ViewHolderRow) holder;
            viewHolderRow.txtProChefRes.setText(proChef.getRetaurentname());
            imageLoader.DisplayImage(Constants.ImagesUrl + proChef.getLogo(), viewHolderRow.imgItemProchef);
            viewHolderRow.txtMinOrderProchef.setText(proChef.getMinorder());
            viewHolderRow.txtTypeofPayments.setText(proChef.getPaymenttype());
            if (proChef.getSepkitchen().equalsIgnoreCase("0"))
                viewHolderRow.txtKitchenType.setText("No Separate kitchen");
            viewHolderRow.txtCmUserReviewName.setText(proChef.getUsername());
            viewHolderRow.txtUserreview.setText(proChef.getUserreview());
            viewHolderRow.txtDeliveryType.setText(proChef.getDeltype());
            viewHolderRow.txtDelTimeProChef.setText(proChef.getDuration());
            viewHolderRow.txtFoodTypes.setText(proChef.getAvailable_food_types());

            viewHolderRow.imgItemProchef.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println(proChef.getChefid());

                    ApplicationLoader.setProchefID(proChef.getChefid());
                    Intent intent = new Intent(getActivity(), ProChefBranches.class);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return proChefList.size();
        }
    }

    public class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtProChefRes, txtDelTimeProChef, txtChefRatingProChef, txtMinOrderProchef, txtTypeofPayments, txtKitchenType;
        TextView txtFoodTypes, txtDeliveryType, txtUserreview, txtCmUserReviewName;
        ImageView imgItemProchef;

        public ViewHolderRow(View itemView) {
            super(itemView);
            imgItemProchef = (ImageView) itemView.findViewById(R.id.imgItemProchef);
            txtProChefRes = (TextView) itemView.findViewById(R.id.txtProChefRes);
            txtDelTimeProChef = (TextView) itemView.findViewById(R.id.txtDelTimeProChef);
            txtChefRatingProChef = (TextView) itemView.findViewById(R.id.txtChefRatingProChef);
            txtMinOrderProchef = (TextView) itemView.findViewById(R.id.txtMinOrderProchef);
            txtTypeofPayments = (TextView) itemView.findViewById(R.id.txtTypeofPayments);
            txtKitchenType = (TextView) itemView.findViewById(R.id.txtKitchenType);
            txtFoodTypes = (TextView) itemView.findViewById(R.id.txtFoodTypes);
            txtDeliveryType = (TextView) itemView.findViewById(R.id.txtDeliveryType);
            txtUserreview = (TextView) itemView.findViewById(R.id.txtUserreview);
            txtCmUserReviewName = (TextView) itemView.findViewById(R.id.txtCmUserReviewName);

            imgItemProchef.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });


        }
    }

    public void loadProChefData() {
/*
        String mURL="http://chefmonster.com/api/getProChefInfo.php?accesskey=12345&cheftype=prochef&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(mURL).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {

            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                // Log.i("response",response.body());
                //Log.i("response",response.body().string());
                closeDialog();
                if (response.isSuccessful()) {
                    

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    processProChefData(response.body().string());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });




                }
            }
        });*/

    }

    public void processProChefData(String response) throws JSONException {
        JSONObject jsonObject=new JSONObject(response);

        JSONArray jprochef=jsonObject.getJSONArray("data");
        for (int i = 0; i <jprochef.length() ; i++) {
            JSONObject jmenu=jprochef.getJSONObject(i).getJSONObject("MenuActivity");
            System.out.println(jmenu.toString());
            ProChef proChef=new ProChef();
            //retaurentname, logo, minorder, paymenttype, sepkitchen, username, userreview, deltype, latitude, longitude,
            // distance, duration, delivery_time, pickupImageUrl,available_food_types
            proChef.setRetaurentname(jmenu.getString("retaurentname"));
            proChef.setLogo(jmenu.getString("logo"));
            proChef.setMinorder(jmenu.getString("minorder"));
            proChef.setPaymenttype(jmenu.getString("paymenttype"));
            proChef.setSepkitchen(jmenu.getString("sepkitchen"));
            proChef.setUsername(jmenu.getString("username"));
            proChef.setUserreview(jmenu.getString("userreview"));
            proChef.setDeltype(jmenu.getString("deltype"));
            proChef.setDistance(jmenu.getString("distance"));
            proChef.setDuration(jmenu.getString("duration"));
            proChef.setDelivery_time(jmenu.getString("delivery_time"));
            proChef.setPickupImageUrl(jmenu.getString("pickupImageUrl"));
            proChef.setAvailable_food_types(jmenu.getString("available_food_types"));
            proChef.setChefid(jmenu.getString("chefid"));

            proChefs.add(proChef);

        }


        //
        prochefAdapter.notifyDataSetChanged();
    }

    ProgressDialog pd;

    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
        if (pd == null)
            pd = new ProgressDialog(getActivity());
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();
    }

    public void closeDialog() {
        // do something wih the result
        if (pd != null)
            pd.dismiss();
    }


}
