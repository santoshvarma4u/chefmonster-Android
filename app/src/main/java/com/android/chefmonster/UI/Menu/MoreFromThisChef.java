package com.android.chefmonster.UI.Menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.InfinateViewpager.InfiniteViewPager;
import com.android.chefmonster.LiveVideo.MenuLive;
import com.android.chefmonster.Locations.SelectLocationActivity;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.UI.Menu.Search.SearchActivity;
import com.android.chefmonster.UI.MenuActivity;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.circlepageidicator.CirclePageIndicator;
import com.konifar.fab_transformation.FabTransformation;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MoreFromThisChef extends AppCompatActivity {

    RecyclerView recycler_view;
    public static String Currency="₹";
    List<Menu> menuList;
    DecimalFormat formatData;
    boolean dealsApply;
    View view;
    int ItemCount = 0;
    private TextView mTxtAmountAdapter;
    private TextView txtItems;
    private TextView tv_imchef, tv_username, tv_userEmail, tv_btmhomechef,tv_btmprochef,tv_btmcancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_from_this_chef);
        initView();
    }

    private void initView() {
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MoreFromThisChef.this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.smoothScrollToPosition(0);
        dla=new DeliveryListAdapter();
        setUpToolbar();
    }

    private void setUpToolbar() {

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);
        TextView City=(TextView) findViewById(R.id.txtLocalCity);
        TextView AddressLocal=(TextView) findViewById(R.id.txtLocalAddress);
        ImageView iv_menu_nav=(ImageView) findViewById(R.id.iv_menu_nav);
        City.setText(ApplicationLoader.getUserCity());
        AddressLocal.setText(ApplicationLoader.getUserLocation());
        ImageView ab_filter=(ImageView) findViewById(R.id.ab_filter);
        ImageView imgSearch=(ImageView)findViewById(R.id.imgSearch);

        tv_btmhomechef=(TextView) findViewById(R.id.btmhomechef);
        tv_btmprochef=(TextView) findViewById(R.id.btmprochef);
        tv_btmcancel=(TextView) findViewById(R.id.btmcancel);


        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(MoreFromThisChef.this,MenuActivity.class);
                startActivity(intent);
                // mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
        City.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreFromThisChef.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });
        AddressLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreFromThisChef.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });






    }

    int DEL_CODE=5;

    public class DeliveryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        final int SLIDER = 0, ROW = 1;
        private final int VIEW_TYPE_LOADING = 3;

        private OnLoadMoreListener mOnLoadMoreListener;

        private boolean isLoading;
        private int visibleThreshold = 5;
        private int lastVisibleItem, totalItemCount;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            switch (viewType) {
                  case ROW:
                    View vRow = inflater.inflate(R.layout.menu_items_new, viewGroup, false);
                    viewHolder = new ViewHolderRow(vRow);
                    break;
                default:
                    break;
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

            switch (viewHolder.getItemViewType()) {
                case ROW:
                    ViewHolderRow vh2 = (ViewHolderRow) viewHolder;
                    configureViewHolderRow(vh2, position);
                    break;
                default:
                    break;
            }
        }
        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return menuList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return ROW ;
        }


        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }



        int ItemCount = 0;
        Menu listMenu;
        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {

            Double finalPrice=0.0;
            listMenu=menuList.get(position);
            holder.txtText.setText(listMenu.getMenu_name());

            holder.txtfoodtype.setText(listMenu.getMenu_food_type());
            holder.txtStockQty.setText(listMenu.getMenu_stock_qty());
            holder.txtweigthingms.setText(listMenu.getMenu_weigth_grms()+" gms");
            holder.txtAreaNamePickup.setText(listMenu.getMenu_pickup_location());
            if(position > 1)
            {
                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0,40,0,0);
                holder.menuCard.setLayoutParams(params);
            }
            if(listMenu.getMenu_Deal_price() > 0)
            {
                holder.ivDealBanner.setVisibility(View.VISIBLE);
                holder.txtMainValue.setVisibility(View.VISIBLE);
                holder.txtMainValue.setText(listMenu.getMenu_price() + " " + Currency);
                holder.txtMainValue.setPaintFlags(holder.txtMainValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                Double afterDiscount=listMenu.getMenu_price()-listMenu.getMenu_Deal_price();
                holder.txtSubText.setText(String.valueOf(afterDiscount) + " " + Currency);
                menuList.get(position).setFinal_price(afterDiscount);
                //finalPrice=afterDiscount;
            }
            else
            {
                holder.txtSubText.setText(listMenu.getMenu_price() + " " + Currency);
                finalPrice=listMenu.getMenu_price();
                menuList.get(position).setFinal_price(finalPrice);
            }

            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
//                holder.itemLayout.setVisibility(View.VISIBLE);//commented as per jii request
                holder.pickupimage.setVisibility(View.VISIBLE);

                String mapurl= Constants.googleMapUrl+listMenu.getMenu_pickup_image();

                ImageLoader iml=new ImageLoader(MoreFromThisChef.this);

                iml.DisplayImage(mapurl, holder.pickupimage);
                // Picasso.with(HomePage.this).load(mapurl).into(holder.pickupimage);

                Log.d("testUrl", mapurl);
                //imageLoader.DisplayImage(listMenu.getMenu_pickup_image(), holder.pickupimage);
            }

            holder.pickupimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(MoreFromThisChef.this,MapAddrView.class);
                    i.putExtra("pimage",Constants.googleMapUrl+listMenu.getMenu_pickup_image());
                    i.putExtra("paddress",listMenu.getMenu_pickup_location().toString());
                    i.putExtra("platitude",listMenu.getLatitude());
                    i.putExtra("plongitude",listMenu.getLongitude());
                    startActivity(i);
                }
            });


            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
                holder.txtDeliverytime.setText(listMenu.getMenu_pickup_distance());
                holder.txtviewdelivery.setText("Distance");
            }
            else
            {
                holder.txtDeliverytime.setText(listMenu.getMenu_delivery_time());
                holder.txtviewdelivery.setText("Delivery Time");
            }

            if(listMenu.isMenu_image_avail())
                Picasso.with(MoreFromThisChef.this).load(Constants.ImagesUrl + listMenu.getMenu_image()).placeholder(R.drawable.loading).into(holder.imgThumb);
            else
                Picasso.with(MoreFromThisChef.this).load(R.drawable.photonotavail).placeholder(R.drawable.loading).into(holder.imgThumb);


            final Double finalPrice1 = finalPrice;

            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
                holder.txtQty.setText(new DBHelper_New(MoreFromThisChef.this).getMenuQty(String.valueOf(listMenu.getMenu_ID()),"PICKUP"));

                if(Integer.parseInt(holder.txtQty.getText().toString())>0)
                    holder.morefromchef.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.txtQty.setText(new DBHelper_New(MoreFromThisChef.this).getMenuQty(String.valueOf(listMenu.getMenu_ID()),"DELIVERY"));

                if(Integer.parseInt(holder.txtQty.getText().toString())>0)
                    holder.morefromchef.setVisibility(View.VISIBLE);
            }
        }
    }


    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty, txtDeliverytime, txtweigthingms, txtfoodtype,txtMainValue,txtviewdelivery,txtAreaNamePickup,btn_live;
        ImageView imgThumb,ivDealBanner,pickupimage;
        LinearLayout itemLayout,morefromchef;
        Button btninc, btndrc;
        RatingBar rb;
        CardView menuCard;

        public ViewHolderRow(View convertView) {
            super(convertView);
            txtText = (TextView) convertView.findViewById(R.id.txtText);
            txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
            imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
            pickupimage = (ImageView) convertView.findViewById(R.id.pickupimage);
            txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgms);
            txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQty);
            txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtype);
            txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtime);
            txtQty = (TextView) convertView.findViewById(R.id.txtQty);
            ivDealBanner=(ImageView) convertView.findViewById(R.id.ivDealBanner);
            btninc = (Button) convertView.findViewById(R.id.btninc);
            btndrc = (Button) convertView.findViewById(R.id.btndrc);
            txtMainValue=(TextView) convertView.findViewById(R.id.txtMainValue);
            txtviewdelivery=(TextView) convertView.findViewById(R.id.txtviewdelivery);
            txtAreaNamePickup=(TextView) convertView.findViewById(R.id.txtAreaNamePickup);
            itemLayout=(LinearLayout) convertView.findViewById(R.id.itemLayout);
            menuCard=(CardView) convertView.findViewById(R.id.menuCard);
            morefromchef=(LinearLayout)convertView.findViewById(R.id.morefromchef);
            btn_live=(TextView)convertView.findViewById(R.id.btn_live);


            Log.e("Adapter Postion",getAdapterPosition()+""+menuList.size());
            // btninc.setOnClickListener(new btnIncListener(0.0,getAdapterPosition(),txtQty));

            btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Adapter Postion",getAdapterPosition()+""+menuList.size());


                    if(menuList.get(getAdapterPosition()).getMenu_delivery().toLowerCase().contains("pickup"))
                    {
                        int qty = Integer.parseInt(txtQty.getText().toString());

                        if (qty < Integer.parseInt(txtStockQty.getText().toString()) )
                        {

                            if(qty <=0){
                                View adv = MoreFromThisChef.this.getLayoutInflater().inflate(R.layout.layout_pickup_alert, null, false);
                                TextView tvdistance=(TextView)adv.findViewById(R.id.tvalertDistance);
                                TextView tvcost=(TextView)adv.findViewById(R.id.tvalertCost);
                                TextView tvTime=(TextView)adv.findViewById(R.id.tvalertTime);


                                tvdistance.setText(menuList.get(getAdapterPosition()).getMenu_pickup_distance());
                                tvcost.setText(menuList.get(getAdapterPosition()).getMenu_price()+" "+Currency);
                                tvTime.setText(menuList.get(getAdapterPosition()).getMenu_delivery_time());


                                new MaterialDialog.Builder(MoreFromThisChef.this)
                                        .autoDismiss(false)
                                        .title("Confirm Pickup")
                                        .customView(adv,true)
                                        .positiveText("Confirm")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                btnInc(getAdapterPosition(),txtQty,txtStockQty,morefromchef,btn_live);
                                                dialog.dismiss();
                                            }
                                        })
                                        .negativeText("Cancel")
                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                                dialog.dismiss();
                                            }
                                        })
                                        .show();
                            }
                            else {
                                btnInc(getAdapterPosition(),txtQty,txtStockQty,morefromchef,btn_live);
                            }

                        }
                        else
                        {
                            Toast.makeText(MoreFromThisChef.this, "No More Qunatity available", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        btnInc(getAdapterPosition(),txtQty,txtStockQty,morefromchef,btn_live);
                    }



                }
            });


            //   final Double finalPrice2 = finalPrice;
            btndrc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnDec(getAdapterPosition(),txtQty,txtStockQty,morefromchef);
                }
            });
        }
    }



    public void getDataFromServer(String keyword,String DeliveryType,String filterKey) {/*
        //

        showProgressDialog();

        String  mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php";

        Log.i("murl",mUrl);
        System.out.println(mUrl+"");
        RequestBody formBody;
        if( filterKey.length() > 0)
        {
            if(DeliveryType.equals("Delivery"))
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","delivery")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","pickup")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
            else  if(DeliveryType.equals("Pickup"))
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","pickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("chefid","00")
                            .add("deliverytype","delivery")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
            else
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","deliverynpickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("keyword",filterKey)
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","deliverynpickup")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }

        }
        else //not filters
        {
            if(DeliveryType.equals("Delivery"))
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","delivery")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","pickup")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
            else  if(DeliveryType.equals("Pickup"))
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","pickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("chefid","00")
                            .add("deliverytype","delivery")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
            else
            {
                if(keyword.isEmpty())
                {
                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","deliverynpickup")
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
                else
                {

                    formBody = new FormEncodingBuilder()
                            .add("accesskey","12345")
                            .add("category_id","00")
                            .add("deliverytype","deliverynpickup")
                            .add("searchkeyword",keyword)
                            .add("latitude", ApplicationLoader.getUserLat())
                            .add("longitude",ApplicationLoader.getUserLon())
                            .build();
                }
            }
        }




        System.out.println(mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .post(formBody)
                .build();




        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {


                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                //Log.e("response",response.body().string());
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });*/
    }

    void clearData() {
        menuList.clear();
    }


    public void parseJSONData(String strResponse) {

        clearData();

        try {
            menuList.add(null);

            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int ii = 0; ii < data.length(); ii++) {

                Menu menuItem=new Menu();
                JSONObject object = data.getJSONObject(ii);

                JSONObject menu = object.getJSONObject("Menu");

                Log.e("Menu", menu.toString());

                Log.e("dealPrice"," "+menu.getString("deal_price"));
                if(dealsApply)
                {
                    if(Integer.parseInt(menu.getString("deal_price"))>0)
                    {

                        menuItem.setMenu_ID(Long.parseLong(menu.getString("Menu_ID")));
                        menuItem.setMenu_name(menu.getString("Menu_name"));
                        menuItem.setMenu_Deal_price(Double.valueOf(menu.getString("deal_price")));
                        menuItem.setMenu_delivery(menu.getString("delivery_type"));
                        menuItem.setMenu_delivery_time(menu.getString("delivery_time"));
                        menuItem.setMenu_food_type(menu.getString("type_of_food"));
                        menuItem.setMenu_image(menu.getString("main_image"));
                        menuItem.setLatitude(menu.getString("latitude"));
                        menuItem.setLongitude(menu.getString("longitude"));
                        menuItem.setLive_url(menu.getString("live_url"));
                        menuItem.setLive_status(menu.getString("live_status"));
                        if(menu.getString("main_image").equals(""))
                        {
                            menuItem.setMenu_image_avail(false);
                        }
                        else
                        {
                            menuItem.setMenu_image_avail(true);
                        }

                        if(menu.getString("delivery_type").equals("Pickup"))
                        {
                            menuItem.setPickup_image_avail(true);
                            menuItem.setMenu_stock_qty(menu.getString("pickup_quantity"));
                            menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("pickup_total"))));
                        }
                        else
                        {
                            menuItem.setPickup_image_avail(false);
                            menuItem.setMenu_stock_qty(menu.getString("Quantity"));
                            menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                        }
                        menuItem.setMenu_pickup_distance(menu.getString("distance"));
                        menuItem.setMenu_pickup_location(menu.getString("pickuplocation"));
                        menuItem.setMenu_weigth_grms(menu.getString("weight_in_grams"));
                        menuItem.setMenu_serves(menu.getString("no_of_serves"));
                        menuItem.setMenu_pickup_image(menu.getString("pickupImageUrl"));

                    }
                }
                else
                {
                    menuItem.setMenu_ID(Long.parseLong(menu.getString("Menu_ID")));
                    menuItem.setMenu_name(menu.getString("Menu_name"));
                    menuItem.setMenu_Deal_price(Double.valueOf(menu.getString("deal_price")));
                    menuItem.setMenu_delivery(menu.getString("delivery_type"));
                    menuItem.setMenu_delivery_time(menu.getString("delivery_time"));
                    menuItem.setMenu_food_type(menu.getString("type_of_food"));
                    menuItem.setMenu_image(menu.getString("main_image"));
                    menuItem.setMenu_pickup_distance(menu.getString("distance"));
                    menuItem.setLatitude(menu.getString("latitude"));
                    menuItem.setLongitude(menu.getString("longitude"));
                    menuItem.setLive_url(menu.getString("live_url"));
                    menuItem.setLive_status(menu.getString("live_status"));

                    if(menu.getString("main_image").equals(""))
                    {
                        menuItem.setMenu_image_avail(false);
                    }
                    else
                    {
                        menuItem.setMenu_image_avail(true);
                    }

                    if(menu.getString("delivery_type").equals("Pickup"))
                    {
                        menuItem.setPickup_image_avail(true);
                        menuItem.setMenu_stock_qty(menu.getString("pickup_quantity"));
                        menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("pickup_total"))));
                    }
                    else
                    {
                        menuItem.setPickup_image_avail(false);
                        menuItem.setMenu_stock_qty(menu.getString("Quantity"));
                        menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                    }

                    menuItem.setMenu_pickup_location(menu.getString("pickuplocation"));
                    menuItem.setMenu_weigth_grms(menu.getString("weight_in_grams"));
                    menuItem.setMenu_serves(menu.getString("no_of_serves"));
                    menuItem.setMenu_pickup_image(menu.getString("pickupImageUrl"));

                }
                menuList.add(menuItem);
            }

            recycler_view.setAdapter(dla);
            dla.notifyDataSetChanged();
            recycler_view.setVisibility(View.VISIBLE);
       /*     InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow();*/
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        closeDialog();
    }
    ProgressDialog pd;

    DeliveryListAdapter dla;

    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
        //  loading_view.setVisibility(View.VISIBLE);
        if (pd == null)
            pd = new ProgressDialog(MoreFromThisChef.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();
    }

    public void closeDialog() {
        // do something wih the result
      /*  getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(loading_view.getVisibility()==View.VISIBLE)
                    loading_view.setVisibility(View.GONE);
            }
        });*/

        if (pd != null)
            pd.dismiss();
    }

    String typeOfDelivery="Delivery & Pickup";

    public void btnInc(final int adapterPos, TextView txtQty,TextView txtStock, LinearLayout liveLayout, TextView btnLive) {

        Log.e("apos",adapterPos+""+menuList.size());
        double finalPrice=menuList.get(adapterPos).getFinal_price();

        btnLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(MoreFromThisChef.this, MenuLive.class);
                intent.putExtra("customerLive","customerLive");
                intent.putExtra("menuid",menuList.get(adapterPos).getMenu_ID());
                intent.putExtra("menuname",menuList.get(adapterPos).getMenu_name());
                intent.putExtra("liveURL",menuList.get(adapterPos).getLive_url());
                startActivity(intent);


            }
        });

        int qty = Integer.parseInt(txtQty.getText().toString());
        if (qty < 0) {

        } else {

            if (qty < Integer.parseInt(txtStock.getText().toString())) {
                ItemCount++;
                liveLayout.setVisibility(View.VISIBLE);

                qty++;
                double price = finalPrice;
                double changedPrice = ApplicationLoader.round(price * qty,2);

                txtQty.setText(String.valueOf(qty));

                txtQty.setText(String.valueOf(qty));

                DBHelper_New db = new DBHelper_New(MoreFromThisChef.this);
                //db.openDataBase();
                long menuid = menuList.get(adapterPos).getMenu_ID();
                System.out.println(menuid + "**************" + menuList.get(adapterPos).getMenu_delivery().toString());
                double menuprice = finalPrice;
                String menuname = menuList.get(adapterPos).getMenu_name();
                String menuImage = menuList.get(adapterPos).getMenu_image();

                if (menuList.get(adapterPos).getMenu_Deal_price() > 0) {
                    if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                        if (db.isDataExist(menuid, "DEAL", "DELIVERY", "Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "DELIVERY", "Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "DELIVERY", menuImage, "Single",txtStock.getText().toString());
                        }
                    } else if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {
                        if (db.isDataExist(menuid, "DEAL", "PICKUP", "Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "PICKUP", "Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "DEAL", "Home Chef", "PICKUP", menuImage, "Single",txtStock.getText().toString());
                        }
                    }
                } else {
                    if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                        if (db.isDataExist(menuid, "Home Chef", "DELIVERY", "Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "DELIVERY", "Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "DELIVERY", menuImage, "Single",txtStock.getText().toString());
                        }
                    } else if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {
                        if (db.isDataExist(menuid, "Home Chef", "PICKUP", "Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "PICKUP", "Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2), "Home Chef", "Home Chef", "PICKUP", menuImage, "Single",txtStock.getText().toString());
                        }
                    }
                }




                double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());
                double ChangedPrice = currentCartPrice + menuprice;

                txtItems.setText(new DBHelper_New(MoreFromThisChef.this).getItemsCount());
                mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(MoreFromThisChef.this)));

                menuList.get(adapterPos).setItemCount(qty);
              /*  if (fab.getVisibility() == View.VISIBLE) {
                    FabTransformation.with(fab).transformTo(toolbarFooter);
                }
                else
                {
                    fab.setVisibility(View.VISIBLE);
                }
*/

            } else {
                Toast.makeText(MoreFromThisChef.this, "No More Qunatity available", Toast.LENGTH_SHORT).show();
            }

        }
    }


    public void btnDec(int adapterPos,TextView txtQty,TextView txtStock, LinearLayout liveLayout) {

        double finalPrice=menuList.get(adapterPos).getFinal_price();


        int qty = Integer.parseInt(txtQty.getText().toString());

        if (qty <= 0) {

            liveLayout.setVisibility(View.GONE);

            double price = finalPrice;

            txtQty.setText("0");

            long menuid =  menuList.get(adapterPos).getMenu_ID();
            DBHelper_New db = new DBHelper_New(MoreFromThisChef.this);

            if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery"))
                db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY' and id="+menuid);
            else
                db.deleteDataByQuery("tbl_order","delivery_type='PICKUP' and id="+menuid);

            db.close();

            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

            if (currentCartPrice > 0) {
                double ChangedPrice = currentCartPrice - price;
                //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
            }

        } else {
            ItemCount--;
            if (qty == 1) {
                double price = finalPrice;
                txtQty.setText("0");

                liveLayout.setVisibility(View.GONE);


                long menuid =  menuList.get(adapterPos).getMenu_ID();
                DBHelper_New db = new DBHelper_New(MoreFromThisChef.this);
                //db.openDataBase();

                if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery"))
                    db.deleteDataByQuery("tbl_order","delivery_type='DELIVERY' and id="+menuid);
                else
                    db.deleteDataByQuery("tbl_order","delivery_type='PICKUP' and id="+menuid);

                System.out.println("delivery_type='PICKUP' and id="+menuid);


                double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                if (currentCartPrice > 0) {
                    double ChangedPrice = currentCartPrice - price;

                }

            } else {
                qty--;
                double price = finalPrice;
                double changedPrice = price * qty;

                txtQty.setText(String.valueOf(qty));
                DBHelper_New db = new DBHelper_New(MoreFromThisChef.this);
                //db.openDataBase();
                long menuid =  menuList.get(adapterPos).getMenu_ID();
                double menuprice = finalPrice;
                String menuname = menuList.get(adapterPos).getMenu_name();
                String menuImage= menuList.get(adapterPos).getMenu_image();
                if( menuList.get(adapterPos).getMenu_Deal_price() > 0)
                {
                    if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                        if (db.isDataExist(menuid,"DEAL","DELIVERY","Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","DELIVERY","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty,ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","DELIVERY",menuImage,"Single",txtStock.getText().toString());
                        }
                    }
                    else  if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {

                        if (db.isDataExist(menuid,"DEAL","PICKUP","Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","PICKUP","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty,ApplicationLoader.round((menuprice * qty),2),"DEAL","Home Chef","PICKUP",menuImage,"Single",txtStock.getText().toString());
                        }
                    }
                }
                else
                {
                    if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("delivery")) {
                        if (db.isDataExist(menuid,"Home Chef","DELIVERY","Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","DELIVERY","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","DELIVERY",menuImage,"Single",txtStock.getText().toString());
                        }
                    }
                    else  if (menuList.get(adapterPos).getMenu_delivery().toString().toLowerCase().contains("pickup")) {
                        if (db.isDataExist(menuid,"Home Chef","PICKUP","Single")) {
                            db.updateData(menuid, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","PICKUP","Single",txtStock.getText().toString());
                        } else {
                            db.addData(menuid, menuname, qty, ApplicationLoader.round((menuprice * qty),2),"Home Chef","Home Chef","PICKUP",menuImage,"Single",txtStock.getText().toString());
                        }
                    }
                }
                double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                double ChangedPrice = currentCartPrice - price;

                //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                //db.close();
            }
        }
        mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(MoreFromThisChef.this)));
        txtItems.setText(new DBHelper_New(MoreFromThisChef.this).getItemsCount());
        //txtItems.setText(String.valueOf(ItemCount));
        menuList.get(adapterPos).setItemCount(qty);
      /*  if (fab.getVisibility() == View.VISIBLE) {
            FabTransformation.with(fab).transformTo(toolbarFooter);
        }*/

    }

    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper_New dbhelper = new DBHelper_New(ctx);
        //dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }
        // count total order
        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }
}
