package com.android.chefmonster.UI.Menu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.Menu.ProChef.Resturants.ResturantHome;
import com.android.chefmonster.Utills.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class ProChefBranches extends Activity {


    RecyclerView recycler_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pro_chef_branches);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        bla=new BranchesListAdapter();
    }
    BranchesListAdapter bla;

    public class BranchesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vRow = inflater.inflate(R.layout.prochef_branches, parent, false);
            viewHolder = new ViewHolderRow(vRow);
            return viewHolder;
        }
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            ViewHolderRow vh2 = (ViewHolderRow) holder;
            configureViewHolderRow(vh2, position);
        }
        @Override
        public int getItemCount() {
            Log.i("size",BranchName.size()+"");
            return BranchName.size();
        }
    }

    // create arraylist variables to store data from server

    static ArrayList<Long> BranchID = new ArrayList<Long>();
    static ArrayList<String> BranchName = new ArrayList<String>();
    static ArrayList<String> BrnachAddr = new ArrayList<String>();
    static ArrayList<String> BranchDistance = new ArrayList<String>();
    static ArrayList<String> BranchPaymentType = new ArrayList<String>();
    static ArrayList<String> BranchMinOrder = new ArrayList<String>();
    static ArrayList<String> BranchDeliveryType = new ArrayList<String>();

    public void configureViewHolderRow(final ViewHolderRow holder, final int position) {

        holder.tvbrnachname.setText(BranchName.get(position).toString());
        holder.tvaddr.setText(BrnachAddr.get(position).toString());
        holder.distanceChef.setText(BranchDistance.get(position).toString());
        holder.paymentType.setText(BranchPaymentType.get(position).toString());
        holder.minorder.setText(BranchMinOrder.get(position).toString());
        holder.deliveryType.setText(BranchDeliveryType.get(position).toString());
        holder.layoutProchefBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(ProChefBranches.this, ResturantHome.class);

               // String branchId=BranchID.get(position).toString();
               // Toast.makeText(ProChefBranches.this, branchId, Toast.LENGTH_SHORT).show();
                Intent i=getIntent();
                String prochefID=i.getExtras().getString("ProchefID").toString();
                String branchId=BranchID.get(position).toString();
                intent.putExtra("ProchefID",prochefID);
                intent.putExtra("BranchId",branchId);
                startActivity(intent);
            }
        });
        holder.btnGetFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

    }
    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView tvbrnachname,distanceChef,tvaddr,minorder,paymentType,deliveryType;
        Button btnGetFood;
        LinearLayout layoutProchefBranch;


        public ViewHolderRow(View convertView) {
            super(convertView);
            tvbrnachname=(TextView)convertView.findViewById(R.id.tvbrnachname);
            distanceChef=(TextView)convertView.findViewById(R.id.distanceChef);
            tvaddr=(TextView)convertView.findViewById(R.id.tvaddr);
            minorder=(TextView)convertView.findViewById(R.id.minorder);
            paymentType=(TextView)convertView.findViewById(R.id.paymentType);
            btnGetFood=(Button)convertView.findViewById(R.id.btnGetFood);
            deliveryType=(TextView)convertView.findViewById(R.id.deliveryType);
            layoutProchefBranch=(LinearLayout)convertView.findViewById(R.id.layoutProchefBranch);
        }
    }

    // clear arraylist variables before used
    void clearData() {
        BranchID.clear();
        BranchName.clear();
        BrnachAddr.clear();
        BranchDistance.clear();
        BranchPaymentType.clear();
        BranchMinOrder.clear();
        BranchDeliveryType.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent i=getIntent();
        String prochefID=i.getExtras().getString("ProchefID").toString();
        getDataFromServer(prochefID);
    }

    ProgressDialog pd;
    public void getDataFromServer(String chefId) {
/*
        showProgressDialog("Getting chef branches....");
        String mUrl= Constants.ProchefBranchesAPI+"?accesskey="+Constants.AccessKey+"&prochefid="+chefId+"&latitude="+ApplicationLoader.getUserLat()+"&longitude="+ApplicationLoader.getUserLon();
        Log.d("murl",mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    closeDialog();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

            }
        });*/
    }
    public void parseJSONData(String strResponse) {


        clearData();

        try {
            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            System.out.println(data.length()+"***************");
            for (int ii = 0; ii < data.length(); ii++) {
                //int i = 0;

                JSONObject object = data.getJSONObject(ii);

                JSONObject branch = object.getJSONObject("MenuActivity");

                Log.e("Branches", branch.toString());


                BranchID.add(Long.valueOf(branch.getString("branchid")));
                BranchName.add(branch.getString("retaurentname"));
                BrnachAddr.add(branch.getString("address")+"\n"+branch.getString("branchloc"));
                BranchDistance.add(branch.getString("distance").toString().replace(",","."));
                BranchPaymentType.add(branch.getString("paymenttype"));
                BranchMinOrder.add(branch.getString("minorder"));
                BranchDeliveryType.add(branch.getString("deltype"));
            }

            recycler_view.setAdapter(bla);

            bla.notifyDataSetChanged();
            recycler_view.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void showProgressDialog(String msg){

        if (pd == null)
            pd = new ProgressDialog(ProChefBranches.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();

    }
    public void closeDialog(){
        // do something wih the result
        if (pd != null)
            pd.dismiss();
    }
}
