package com.android.chefmonster.UI.Menu.Combos;

/**
 * Created by tskva on 12/22/2016.
 */

public class Combo {

    private String ComboId;
    private String ComboName;
    private String ComboImage;
    private Double ComboPrice;
    private String ComboQuantity;
    private String ComboFoodType;
    private String ComboChefID;
    private String ComboRating;
    private String ComboWeight;
    private String ComboDeliveryType;
    private String ComboAddress;
    private String ComboDistance;
    private String ComboDeliveryTime;
    private String ComboPickupImageUrl;
    private String ComboLatitude;
    private String ComboLongitude;

    public String getComboLatitude() {
        return ComboLatitude;
    }

    public void setComboLatitude(String comboLatitude) {
        ComboLatitude = comboLatitude;
    }

    public String getComboLongitude() {
        return ComboLongitude;
    }

    public void setComboLongitude(String comboLongitude) {
        ComboLongitude = comboLongitude;
    }

    public String getComboId() {
        return ComboId;
    }


    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }


    private int itemCount;


    public void setComboId(String comboId) {
        ComboId = comboId;
    }

    public String getComboName() {
        return ComboName;
    }

    public void setComboName(String comboName) {
        ComboName = comboName;
    }

    public String getComboImage() {
        return ComboImage;
    }

    public void setComboImage(String comboImage) {
        ComboImage = comboImage;
    }

    public Double getComboPrice() {
        return ComboPrice;
    }

    public void setComboPrice(Double comboPrice) {
        ComboPrice = comboPrice;
    }

    public String getComboQuantity() {
        return ComboQuantity;
    }

    public void setComboQuantity(String comboQuantity) {
        ComboQuantity = comboQuantity;
    }

    public String getComboFoodType() {
        return ComboFoodType;
    }

    public void setComboFoodType(String comboFoodType) {
        ComboFoodType = comboFoodType;
    }

    public String getComboChefID() {
        return ComboChefID;
    }

    public void setComboChefID(String comboChefID) {
        ComboChefID = comboChefID;
    }

    public String getComboRating() {
        return ComboRating;
    }

    public void setComboRating(String comboRating) {
        ComboRating = comboRating;
    }

    public String getComboWeight() {
        return ComboWeight;
    }

    public void setComboWeight(String comboWeight) {
        ComboWeight = comboWeight;
    }

    public String getComboDeliveryType() {
        return ComboDeliveryType;
    }

    public void setComboDeliveryType(String comboDeliveryType) {
        ComboDeliveryType = comboDeliveryType;
    }

    public String getComboAddress() {
        return ComboAddress;
    }

    public void setComboAddress(String comboAddress) {
        ComboAddress = comboAddress;
    }

    public String getComboDistance() {
        return ComboDistance;
    }

    public void setComboDistance(String comboDistance) {
        ComboDistance = comboDistance;
    }

    public String getComboDeliveryTime() {
        return ComboDeliveryTime;
    }

    public void setComboDeliveryTime(String comboDeliveryTime) {
        ComboDeliveryTime = comboDeliveryTime;
    }

    public String getComboPickupImageUrl() {
        return ComboPickupImageUrl;
    }

    public void setComboPickupImageUrl(String comboPickupImageUrl) {
        ComboPickupImageUrl = comboPickupImageUrl;
    }
}
