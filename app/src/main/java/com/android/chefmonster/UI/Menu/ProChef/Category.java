package com.android.chefmonster.UI.Menu.ProChef;

import com.android.chefmonster.UI.Menu.ProChef.Resturants.Cat_Resturant;

import java.util.List;

/**
 * Created by SantoshT on 11/30/2016.
 */

public class Category {

    String categoryId,categoryName,categoryImage,categoryDiscription;
    List<Cat_Resturant> cat_resturants;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryDiscription() {
        return categoryDiscription;
    }

    public void setCategoryDiscription(String categoryDiscription) {
        this.categoryDiscription = categoryDiscription;
    }

    public List<Cat_Resturant> getCat_resturants() {
        return cat_resturants;
    }

    public void setCat_resturants(List<Cat_Resturant> cat_resturants) {
        this.cat_resturants = cat_resturants;
    }
}
