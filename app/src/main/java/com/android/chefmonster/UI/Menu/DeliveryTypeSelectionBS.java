package com.android.chefmonster.UI.Menu;

/**
 * Created by SantoshT on 5/23/2017.
 */

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.View;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.ivankocijan.magicviews.views.MagicTextView;


public class DeliveryTypeSelectionBS extends BottomSheetDialogFragment {

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            switch (newState) {

                case BottomSheetBehavior.STATE_COLLAPSED:{

                    Log.d("BSB","collapsed") ;
                }
                case BottomSheetBehavior.STATE_SETTLING:{

                    Log.d("BSB","settling") ;
                }
                case BottomSheetBehavior.STATE_EXPANDED:{

                    Log.d("BSB","expanded") ;
                }
                case BottomSheetBehavior.STATE_HIDDEN: {

                    Log.d("BSB" , "hidden") ;
                    dismiss();
                }
                case BottomSheetBehavior.STATE_DRAGGING: {

                    Log.d("BSB","dragging") ;
                }
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            Log.d("BSB","sliding " + slideOffset ) ;
        }
    };

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.deliverytype_bottomsheet, null);
        dialog.setContentView(contentView);

        btndelivery = (MagicTextView)contentView.findViewById(R.id.btndelivery);
        btnpickup = (MagicTextView)contentView. findViewById(R.id.btnpickup);
        btmcancel = (MagicTextView)contentView.findViewById(R.id.btmcancel);

        if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
        {
            btndelivery.setText("Delivery");
            btnpickup.setText("Pickup");
        }
        else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
        {
            btndelivery.setText("Delivery & Pickup");
            btnpickup.setText("Pickup");
        }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
        {
            btndelivery.setText("Delivery");
            btnpickup.setText("Delivery & Pickup");
        }

        btndelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                {
                  ApplicationLoader.setDeliveryType("delivery");

                }
                else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
                {
                    ApplicationLoader.setDeliveryType("deliverynpickup");

                }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
                {
                    ApplicationLoader.setDeliveryType("delivery");
                }
                dialog.cancel();
            }
        });
        btnpickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
                {
                    ApplicationLoader.setDeliveryType("pickup");

                }
                else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
                {
                    ApplicationLoader.setDeliveryType("pickup");

                }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
                {
                    ApplicationLoader.setDeliveryType("deliverynpickup");
                }
                dialog.cancel();
            }
        });
        btmcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    protected MagicTextView btndelivery;
    protected MagicTextView btnpickup;
    protected MagicTextView btmcancel;



}
