package com.android.chefmonster.UI;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.CartTabbedActivity;
import com.android.chefmonster.Cart.Orders;
import com.android.chefmonster.Checkout.Checkout;
import com.android.chefmonster.R;
import com.android.chefmonster.Utills.Constants;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicEditText;
import com.ivankocijan.magicviews.views.MagicTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener {

    protected Toolbar toolbar;
    protected MagicTextView tvUsername;
    protected MagicTextView tvOrderid;
    protected MagicTextView tvOrdercost;
    protected LinearLayout orderInfo;
    protected MagicTextView orderserved;
    protected MagicTextView orderdelivertime;
    protected LinearLayout serveLayout;
    protected RatingBar rbdelivery;
    protected RatingBar rbchef;
    protected MagicEditText etLeaveCommetn;
    protected LinearLayout rateLayout;
    protected MagicButton btnSubmitFeedback;
    protected RelativeLayout activityFeedback;

    private String orderid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_feedback);
        initView();
        if(getIntent().getExtras().containsKey("orderid"))
        {
            orderid=getIntent().getExtras().getString("orderid");
            tvOrderid.setText(orderid);
            getOrderDetails(orderid);
        }
        setUpToolbar();
    }


    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarr);
        setSupportActionBar(toolbar);
        ImageView iv_menu_nav = (ImageView) findViewById(R.id.iv_menu_nav);
        TextView tv_title = (TextView) findViewById(R.id.txtTitle);
        tv_title.setText("Order Feedback");
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(FeedbackActivity.this, HomePage.class));
                finish();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSubmitFeedback) {

            HttpLoggingInterceptor interceptor;
            interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build();

            FormBody.Builder formBody;

            formBody = new FormBody.Builder()
                    .add("orderid", orderid)
                    .add("submitFeedback", "true")
                    .add("chef_rating", rbchef.getRating()+"")
                    .add("delivery_rating", rbdelivery.getRating()+"")
                    .add("comments", etLeaveCommetn.getText().toString());

            RequestBody formBodyA = formBody.build();
            Request request = new Request.Builder()
                    .url(Constants.SubmitFeedback)
                    .post(formBodyA)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    //  Toast.makeText(FeedbackActivity.this, "Thank you . Feedback submitted", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvUsername = (MagicTextView) findViewById(R.id.tv_username);
        tvOrderid = (MagicTextView) findViewById(R.id.tv_orderid);
        tvOrdercost = (MagicTextView) findViewById(R.id.tv_ordercost);
        orderInfo = (LinearLayout) findViewById(R.id.orderInfo);
        orderserved = (MagicTextView) findViewById(R.id.orderserved);
        orderdelivertime = (MagicTextView) findViewById(R.id.orderdelivertime);
        serveLayout = (LinearLayout) findViewById(R.id.serveLayout);
        rbdelivery = (RatingBar) findViewById(R.id.rbdelivery);
        rbchef = (RatingBar) findViewById(R.id.rbchef);
        etLeaveCommetn = (MagicEditText) findViewById(R.id.et_leaveCommetn);
        rateLayout = (LinearLayout) findViewById(R.id.rateLayout);
        btnSubmitFeedback = (MagicButton) findViewById(R.id.btnSubmitFeedback);
        btnSubmitFeedback.setOnClickListener(FeedbackActivity.this);
        activityFeedback = (RelativeLayout) findViewById(R.id.activity_feedback);
    }

    public void getOrderDetails(String orderid)
    {
       // showProgressDialog("Getting your orders....");
        String mUrl = Constants.GetOrders + "?orderforfeedback=true&order_id="+orderid;
        Log.d("murl", mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }

    public void parseJSONData(String strResponse)
    {
        try {
            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            System.out.println(data.length() + "***************");
            if (data.length() <= 0) {
                Toast.makeText(FeedbackActivity.this, "Sorry , Order not found for feedback", Toast.LENGTH_SHORT).show();
                finish();

            } else {
                for (int ii = 0; ii < data.length(); ii++) {
                    //int i = 0;
                    JSONObject object = data.getJSONObject(ii);
                    JSONObject order = object.getJSONObject("Order");

                    tvOrderid.setText(order.getString("order_id"));
                    orderdelivertime.setText(order.getString("date_time"));
                    tvOrdercost.setText("Price : "+order.getString("order_price")+"  ₹");
                }
            }

            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
