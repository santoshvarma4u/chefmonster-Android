package com.android.chefmonster.UI.Menu.ProChef;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.LoadingDrawble.loadingdrawable.LoadingView;
import com.android.chefmonster.Locations.SelectLocationActivity;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.UI.Menu.ProChefBranches;
import com.android.chefmonster.UI.Model.ProChef;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ProChefResturants extends AppCompatActivity {

    View view;
    RecyclerView rv_resturants;
    ImageLoader imageLoader;
    List<ProChef> proChefs;
    List<Branches> brachesList;
    ProchefAdapter prochefAdapter;
    LoadingView loadingView;
    private BottomSheetBehavior mBottomSheetBehavior;
    private TextView tv_imchef, tv_username, tv_userEmail, tv_btmhomechef,tv_btmprochef,tv_btmcancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pro_chef_resturants);
        rv_resturants=(RecyclerView)findViewById(R.id.rv_resturants);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProChefResturants.this);
        rv_resturants.setLayoutManager(mLayoutManager);
        rv_resturants.setItemAnimator(new DefaultItemAnimator());
        loadingView=(LoadingView)findViewById(R.id.loading_view);
        imageLoader = new ImageLoader(ProChefResturants.this);
        proChefs = new ArrayList<>();
        prochefAdapter = new ProchefAdapter(ProChefResturants.this, proChefs);
        rv_resturants.setAdapter(prochefAdapter);

        View bottomSheet = findViewById( R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        setUpToolbar();

    }


    private void setUpToolbar() {

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);
        tv_btmhomechef=(TextView) findViewById(R.id.btmhomechef);
        tv_btmprochef=(TextView) findViewById(R.id.btmprochef);
        tv_btmcancel=(TextView) findViewById(R.id.btmcancel);

        TextView City=(TextView) findViewById(R.id.txtLocalCity);
        TextView AddressLocal=(TextView) findViewById(R.id.txtLocalAddress);
        final TextView txtChefSelection=(TextView) findViewById(R.id.txtChefSelection);
        City.setText(ApplicationLoader.getUserCity());
        AddressLocal.setText(ApplicationLoader.getUserLocation());

        City.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProChefResturants.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });
        AddressLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProChefResturants.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });
        txtChefSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        txtChefSelection.setText("Professional Chef");

        tv_btmhomechef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                txtChefSelection.setText("Home Chef");
                startActivity(new Intent(ProChefResturants.this,MainActivity.class));
            }
        });
        tv_btmprochef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                txtChefSelection.setText("Professional Chef");
                startActivity(new Intent(ProChefResturants.this,ProChefHome.class));
            }
        });
        tv_btmcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        loadProChefData();
    }

    public class ProchefAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        List<ProChef> proChefList;
        Context context;

        public ProchefAdapter(Context context, List<ProChef> proChefList) {
            this.proChefList = proChefList;
            this.context = context;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

            View view = layoutInflater.inflate(R.layout.resturant, parent, false);
            return new ViewHolderRow(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final ProChef proChef = proChefList.get(position);
//            retaurentname, logo, minorder, paymenttype, sepkitchen,
//                    username, userreview, deltype, latitude, longitude, distance,
//                    duration, delivery_time, pickupImageUrl

           /* viewHolderRow.txtTypeofPayments.setText(proChef.getPaymenttype());
            if (proChef.getSepkitchen().equalsIgnoreCase("0"))
                viewHolderRow.txtKitchenType.setText("No Separate kitchen");
            viewHolderRow.txtCmUserReviewName.setText(proChef.getUsername());
            viewHolderRow.txtUserreview.setText(proChef.getUserreview());*/
            ViewHolderRow viewHolderRow = (ViewHolderRow) holder;
            viewHolderRow.txtProChefRes.setText(proChef.getRetaurentname());
            imageLoader.DisplayImage(Constants.ImagesUrl + proChef.getLogo(), viewHolderRow.imgItemProchef);
            viewHolderRow.txtMinOrderProchef.setText(proChef.getMinorder());
            viewHolderRow.txtDeliveryType.setText(proChef.getDeltype());
            viewHolderRow.txtDelTimeProChef.setText(proChef.getDuration());
            viewHolderRow.txtFoodTypes.setText(proChef.getAvailable_food_types());

              //getDataFromServer(proChef.getChefid());

            viewHolderRow.imgItemProchef.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println(proChef.getChefid());

                  /*  ApplicationLoader.setProchefID(proChef.getChefid());
                    Intent intent = new Intent(getActivity(), ProChefBranches.class);
                    startActivity(intent);*/
                }
            });

            viewHolderRow.txtProChefRes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //ApplicationLoader.setProchefID(proChef.getChefid());
                    Intent intent = new Intent(ProChefResturants.this, ProChefBranches.class);
                    intent.putExtra("ProchefID",proChef.getChefid());
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return proChefList.size();
        }
    }

    public class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtProChefRes, txtDelTimeProChef, txtChefRatingProChef, txtMinOrderProchef, txtTypeofPayments, txtKitchenType;
        TextView txtFoodTypes, txtDeliveryType, txtUserreview, txtCmUserReviewName;
        TextView noofresturants;
        ImageView imgItemProchef;
        RecyclerView rv_branches;

        public ViewHolderRow(View itemView) {
            super(itemView);
            imgItemProchef = (ImageView) itemView.findViewById(R.id.iv_resturant);
            txtProChefRes = (TextView) itemView.findViewById(R.id.tvFoodtitle);
            txtDelTimeProChef = (TextView) itemView.findViewById(R.id.tvTime);
            txtChefRatingProChef = (TextView) itemView.findViewById(R.id.tvRate);
            txtMinOrderProchef = (TextView) itemView.findViewById(R.id.tvminorder);
          //  txtTypeofPayments = (TextView) itemView.findViewById(R.id.txtTypeofPayments);
           // txtKitchenType = (TextView) itemView.findViewById(R.id.txtKitchenType);
           txtFoodTypes = (TextView) itemView.findViewById(R.id.tvFoodDesc);
            txtDeliveryType = (TextView) itemView.findViewById(R.id.tv_prochefdeltype);
           /* txtUserreview = (TextView) itemView.findViewById(R.id.txtUserreview);
            txtCmUserReviewName = (TextView) itemView.findViewById(R.id.txtCmUserReviewName);*/
            //rv_branches=(RecyclerView) itemView.findViewById(R.id.rv_branches);
            noofresturants=(TextView) itemView.findViewById(R.id.noofresturants);

          /*  RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            rv_branches.setLayoutManager(mLayoutManager);
            rv_branches.setItemAnimator(new DefaultItemAnimator());
            brachesList= new ArrayList<>();
*/

            imgItemProchef.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

    }


    BranchesListAdapter bla;

    public class BranchesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vRow = inflater.inflate(R.layout.prochef_branches, parent, false);
            viewHolder = new ViewHolderRow(vRow);
            return viewHolder;
        }
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            ViewHolderBranchRow vh2 = (ViewHolderBranchRow) holder;
            configureViewHolderBranchRow(vh2, position);
        }
        @Override
        public int getItemCount() {
            return brachesList.size();
        }
    }



    public void configureViewHolderBranchRow(final ViewHolderBranchRow holder, final int position) {

        holder.tvbrnachname.setText(brachesList.get(position).getBranchName());
        holder.tvaddr.setText(brachesList.get(position).getBrnachAddr());
        holder.distanceChef.setText(brachesList.get(position).getBranchDistance());
        holder.paymentType.setText(brachesList.get(position).getBranchPaymentType());
        holder.minorder.setText(brachesList.get(position).getBranchMinOrder());
        holder.deliveryType.setText(brachesList.get(position).getBranchDeliveryType());


    }
    class ViewHolderBranchRow extends RecyclerView.ViewHolder {
        TextView tvbrnachname,distanceChef,tvaddr,minorder,paymentType,deliveryType;

        public ViewHolderBranchRow(View convertView) {
            super(convertView);
            tvbrnachname=(TextView)convertView.findViewById(R.id.tvbrnachname);
            distanceChef=(TextView)convertView.findViewById(R.id.distanceChef);
            tvaddr=(TextView)convertView.findViewById(R.id.tvaddr);
            minorder=(TextView)convertView.findViewById(R.id.minorder);
            paymentType=(TextView)convertView.findViewById(R.id.paymentType);
            deliveryType=(TextView)convertView.findViewById(R.id.deliveryType);
        }
    }



    public void loadProChefData() {

        String mURL="http://chefmonster.com/api/getProChefInfo.php?accesskey=12345&cheftype=prochef&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        Log.i("murl",mURL);
        showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(mURL).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
            }
            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                // Log.i("response",response.body());
                //Log.i("response",response.body().string());
                closeDialog();
                if (response.isSuccessful()) {
                   runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processProChefData(response.body().string());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }
    public void processProChefData(String response) throws JSONException {
        proChefs.clear();
        JSONObject jsonObject=new JSONObject(response);

        JSONArray jprochef=jsonObject.getJSONArray("data");
        for (int i = 0; i <jprochef.length() ; i++) {
            JSONObject jmenu=jprochef.getJSONObject(i).getJSONObject("MenuActivity");
            System.out.println(jmenu.toString());
            ProChef proChef=new ProChef();
            //retaurentname, logo, minorder, paymenttype, sepkitchen, username, userreview, deltype, latitude, longitude,
            // distance, duration, delivery_time, pickupImageUrl,available_food_types
            proChef.setRetaurentname(jmenu.getString("retaurentname"));
            proChef.setLogo(jmenu.getString("logo"));
            proChef.setMinorder(jmenu.getString("minorder"));
            proChef.setPaymenttype(jmenu.getString("paymenttype"));
            proChef.setSepkitchen(jmenu.getString("sepkitchen"));
            proChef.setUsername(jmenu.getString("username"));
            proChef.setUserreview(jmenu.getString("userreview"));
            proChef.setDeltype(jmenu.getString("deltype"));
            proChef.setDistance(jmenu.getString("distance"));
            proChef.setDuration(jmenu.getString("duration"));
            proChef.setDelivery_time(jmenu.getString("delivery_time"));
            proChef.setPickupImageUrl(jmenu.getString("pickupImageUrl"));
            proChef.setAvailable_food_types(jmenu.getString("available_food_types"));
            proChef.setChefid(jmenu.getString("chefid"));

           // proChef.setBranchsList();

            proChefs.add(proChef);
        }
        //
        prochefAdapter.notifyDataSetChanged();
    }

    ProgressDialog pd;

    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
        loadingView.setVisibility(View.VISIBLE);
       /* if (pd == null)
            pd = new ProgressDialog(ProChefResturants.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();*/
    }

    public void closeDialog() {
        // do something wih the result
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingView.setVisibility(View.GONE);
            }
        });
        /*if (pd != null)
            pd.dismiss();*/
    }


    public void getDataFromServer(String chefId) {

        showProgressDialog("Getting chef resturents....");
        String mUrl= Constants.ProchefBranchesAPI+"?accesskey="+Constants.AccessKey+"&prochefid="+chefId+"&latitude="+ApplicationLoader.getUserLat()+"&longitude="+ApplicationLoader.getUserLon();
        Log.d("murl",mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    closeDialog();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

            }
        });
    }

    public void parseJSONData(String strResponse) {

            brachesList.clear();
        try {
            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            //System.out.println(data.length()+"***************");
            for (int ii = 0; ii < data.length(); ii++) {
                //int i = 0;
                JSONObject object = data.getJSONObject(ii);
                JSONObject branch = object.getJSONObject("MenuActivity");

                Log.e("Branches", branch.toString());
                Branches proBranches=new Branches();

                proBranches.setBranchID(branch.getString("branchid"));
                proBranches.setBranchName(branch.getString("retaurentname"));
                proBranches.setBrnachAddr(branch.getString("address")+"\n"+branch.getString("branchloc"));
                proBranches.setBranchDistance(branch.getString("distance").toString().replace(",","."));
                proBranches.setBranchPaymentType(branch.getString("paymenttype"));
                proBranches.setBranchMinOrder(branch.getString("minorder"));
                proBranches.setBranchDeliveryType(branch.getString("deltype"));

                brachesList.add(proBranches);
            }
            bla.notifyDataSetChanged();
            InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
