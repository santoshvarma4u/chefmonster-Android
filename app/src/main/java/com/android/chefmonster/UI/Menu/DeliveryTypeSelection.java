package com.android.chefmonster.UI.Menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.ivankocijan.magicviews.views.MagicTextView;

public class DeliveryTypeSelection extends AppCompatActivity implements View.OnClickListener {

    protected MagicTextView magicTextView5;
    protected MagicTextView btndelivery;
    protected MagicTextView btnpickup;
    protected MagicTextView btmcancel;
    protected CoordinatorLayout crlytFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.deliverytype_bottomsheet);
        initView();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btndelivery) {
            Intent intent=new Intent();
            intent.putExtra("typeofdelivery",btndelivery.getText().toString());
            setResult(RESULT_OK,intent);
            finish();
        } else if (view.getId() == R.id.btnpickup) {
            Intent intent=new Intent();
            intent.putExtra("typeofdelivery",btnpickup.getText().toString());
            setResult(RESULT_OK,intent);
            finish();
        } else if (view.getId() == R.id.btmcancel) {
            Intent intent=new Intent();
            intent.putExtra("typeofdelivery","Delivery & Pickup");
            setResult(RESULT_OK,intent);
            finish();
        }
    }

    private void initView() {
        magicTextView5 = (MagicTextView) findViewById(R.id.magicTextView5);
        btndelivery = (MagicTextView) findViewById(R.id.btndelivery);
        btndelivery.setOnClickListener(DeliveryTypeSelection.this);
        btnpickup = (MagicTextView) findViewById(R.id.btnpickup);
        btnpickup.setOnClickListener(DeliveryTypeSelection.this);
        btmcancel = (MagicTextView) findViewById(R.id.btmcancel);
        btmcancel.setOnClickListener(DeliveryTypeSelection.this);
        crlytFilter = (CoordinatorLayout) findViewById(R.id.crlyt_filter);

        if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("deliverynpickup"))
        {
            btndelivery.setText("Delivery");
            btnpickup.setText("Pickup");
        }
        else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("delivery"))
        {
            btndelivery.setText("Delivery & Pickup");
            btnpickup.setText("Pickup");
        }else if(ApplicationLoader.getDeliveryType().equalsIgnoreCase("pickup"))
        {
            btndelivery.setText("Delivery");
            btnpickup.setText("Delivery & Pickup");
        }
    }
}
