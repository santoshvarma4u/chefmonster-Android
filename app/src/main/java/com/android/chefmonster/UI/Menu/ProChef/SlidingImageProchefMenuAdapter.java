package com.android.chefmonster.UI.Menu.ProChef;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.chefmonster.R;
import com.android.chefmonster.Utills.ImageLoader;

import java.util.ArrayList;

/**
 * Created by SantoshT on 11/28/2016.
 */

public class SlidingImageProchefMenuAdapter  extends PagerAdapter {
    private ArrayList<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    private ImageLoader imageLoader;

    public SlidingImageProchefMenuAdapter(Context context, ImageLoader imageLoader) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.imageLoader=imageLoader;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.sliding_category, view, false);

        //assert imageLayout != null;



        final TextView btnMiniSlider1=(TextView)imageLayout.findViewById(R.id.btnMiniSlider1);
        final TextView btnMiniSlider2=(TextView)imageLayout.findViewById(R.id.btnMiniSlider2);

        btnMiniSlider1.setText("New Restaurants");
        btnMiniSlider2.setText("Combo Foodies");


    /*    imageView1.setImageResource(R.drawable.dealsslider);
        imageView2.setImageResource(R.drawable.tranding);*/

        if(position!=0) {

            btnMiniSlider1.setText("Deals");
            btnMiniSlider2.setText("Tranding");

        /*    imageView1.setImageResource(R.drawable.todayunique);
            imageView2.setImageResource(R.drawable.combomore);*/
        }
        else {
            btnMiniSlider1.setText("New Restaurants");
            btnMiniSlider2.setText("Combo Foodies");

        }
        // imageView.setImageResource(IMAGES.get(position));
     /*   Log.i("img", Constants.ImagesSlideURL + IMAGES.get(position));
        //Picasso.with(context).load(Constants.ImagesSlideURL + IMAGES.get(position)).resize(200,200).into(imageView);

        imageLoader.DisplayImage(Constants.ImagesSlideURL + IMAGES.get(position), imageView);
        view.addView(imageLayout, 0);*/
        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}

