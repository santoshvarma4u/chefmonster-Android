package com.android.chefmonster.UI.Menu.Deals;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.ActivityCart;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.Menu.FilterFragment;
import com.android.chefmonster.UI.Menu.FilterListener;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class DealsPickup extends AppCompatActivity  implements View.OnClickListener,FilterListener {

    RecyclerView recycler_view;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public ImageLoader imageLoader;
    DecimalFormat formatData;
    //
    private TextView mTxtAmountAdapter;
    private TextView txtItems;
    int ItemCount = 0;
    RelativeLayout checkout;
    LinearLayout optionsLayout;
    Button btnDelivery;
    Button btnCartAdapter;
    Button btnFilter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals_pickup);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setUpToolbar();
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(DealsPickup.this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());

        imageLoader = new ImageLoader(DealsPickup.this);

        formatData = new DecimalFormat("#.##");
//
        mTxtAmountAdapter = (TextView) findViewById(R.id.txtCartPriceAdapter);
        txtItems=(TextView) findViewById(R.id.txtItems);
        checkout = (RelativeLayout) findViewById(R.id.lytCheckout);

        btnCartAdapter=(Button) findViewById(R.id.btnCartAdapter);

        btnCartAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(DealsPickup.this, ActivityCart.class);
                startActivity(iMyOrder);
            }
        });

        linearLayoutManager = (LinearLayoutManager) recycler_view.getLayoutManager();

        btnDelivery = (Button) findViewById(R.id.btnDealDelivery);
        btnDelivery.setOnClickListener(this);
        //
        dla = new DeliveryListAdapter();

        //recycler_view.setAdapter(dla);

        btnFilter=(Button)findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(this);

        //
        recycler_view.setAdapter(dla);
        getDataFromServer();


    }
    private Toolbar mToolbar;
    private void setUpToolbar()
    {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null)
        {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left_black_48dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
    public LinearLayoutManager linearLayoutManager;
    DeliveryListAdapter dla;



    public class DeliveryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        final int ROW = 1;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            switch (viewType) {
                case ROW:
                    View vRow = inflater.inflate(R.layout.menulistpickupitem, viewGroup, false);
                    viewHolder = new ViewHolderRow(vRow);
                    break;
                default:

                    break;
            }
            return viewHolder;


        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            switch (viewHolder.getItemViewType()) {
                case ROW:
                    ViewHolderRow vh2 = (ViewHolderRow) viewHolder;
                    configureViewHolderRow(vh2, position);
                    break;
                default:

                    break;
            }
        }

        @Override
        public int getItemCount() {
            return DealsPickup.Menu_ID.size();
        }

        @Override
        public int getItemViewType(int position) {

            return ROW;
        }


        int ItemCount = 0;

        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {

            holder.txtText.setText(DealsPickup.Menu_name.get(position));
            holder.txtSubText.setText(DealsPickup.Menu_price.get(position) + " " + DealsPickup.Currency);
            holder.txtDeliverytime.setText(DealsPickup.Menu_delivery_time.get(position));
            holder.txtfoodtype.setText(DealsPickup.Menu_food_type.get(position));
            holder.txtStockQty.setText(DealsPickup.Menu_stock_qty.get(position));
            holder.txtweigthingms.setText(DealsPickup.Menu_weigth_grms.get(position));
            holder.txtPickupLocation.setText(DealsPickup.Menu_pickup_location.get(position));
            imageLoader.DisplayImage(Constants.AdminPageURL + DealsPickup.Menu_image.get(position), holder.imgThumb);
            imageLoader.DisplayImage(DealsPickup.Menu_pickup_image.get(position), holder.pickupimage);

            holder.btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    if (qty < 0) {


                        // checkout.setVisibility(View.GONE);
                    } else {

                        ItemCount++;
                        qty++;
                        checkout.setVisibility(View.VISIBLE);
                        double price=DealsPickup.Menu_price.get(position);
                        double changedPrice=price*qty;
                        holder.txtSubText.setText(changedPrice+" "+DealsPickup.Currency);
                        holder.txtQty.setText(String.valueOf(qty));

                        DBHelper_New db=new DBHelper_New(getApplicationContext());
                        //db.openDataBase();
                        long menuid=DealsPickup.Menu_ID.get(position);
                        double menuprice=DealsPickup.Menu_price.get(position);
                        String menuname=DealsPickup.Menu_name.get(position);
                       /* if(db.isDataExist(menuid,"DEAL","PICKUP")){
                            db.updateData(menuid, qty, (menuprice*qty),"DEAL","Home Chef","PICKUP");
                        }else{
                            db.addData(menuid, menuname, qty, (menuprice*qty),"DEAL","Home Chef","PICKUP","");
                        }*/

                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        double ChangedPrice=currentCartPrice+menuprice;

                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        db.close();
                    }

                    txtItems.setText(new DBHelper_New(DealsPickup.this).getItemsCount());
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getApplicationContext())));
                }
            });
            holder.btndrc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty=Integer.parseInt(holder.txtQty.getText().toString());

                    if(qty<=0)
                    {

                        double price=DealsPickup.Menu_price.get(position);
                        holder.txtSubText.setText(price+" "+DealsPickup.Currency);
                        holder.txtQty.setText("0");

                        long menuid=DealsPickup.Menu_ID.get(position);
                        DBHelper_New db=new DBHelper_New(getApplicationContext());
                        //db.openDataBase();
                        db.deleteData(menuid);
                        db.close();

                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        if(currentCartPrice>0)
                        {
                            double ChangedPrice=currentCartPrice-price;
                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        }

                    }
                    else
                    {
                        qty--;

                        if(qty==1)
                        {
                            ItemCount--;
                            double price=DealsPickup.Menu_price.get(position);
                            holder.txtQty.setText("0");
                            holder.txtSubText.setText(price+" "+DealsPickup.Currency);
                            DBHelper_New db=new DBHelper_New(getApplicationContext());
                            //1111db.openDataBase();
                            long menuid=DealsPickup.Menu_ID.get(position);
                            double menuprice=DealsPickup.Menu_price.get(position);
                            String menuname=DealsPickup.Menu_name.get(position);
                          /*  if(db.isDataExist(menuid,"DEAL","PICKUP")){
                                db.updateData(menuid, qty, (menuprice * qty),"DEAL","Home Chef","PICKUP");
                            }else{
                                db.addData(menuid, menuname, qty, (menuprice*qty),"DEAL","Home Chef","PICKUP","");
                            }*/
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice=currentCartPrice-price;

                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        }
                        else
                        {

                            ItemCount--;
                            double price=DealsPickup.Menu_price.get(position);
                            double changedPrice=price*qty;
                            holder.txtSubText.setText(changedPrice+" "+DealsPickup.Currency);
                            holder.txtQty.setText(String.valueOf(qty));
                            DBHelper_New db=new DBHelper_New(getApplicationContext());
                            //db.openDataBase();
                            long menuid=DealsPickup.Menu_ID.get(position);
                            double menuprice=DealsPickup.Menu_price.get(position);
                            String menuname=DealsPickup.Menu_name.get(position);
                           /* if(db.isDataExist(menuid,"DEAL","PICKUP")){
                                db.updateData(menuid, qty, (menuprice * qty),"DEAL","Home Chef","PICKUP");
                            }else{
                                db.addData(menuid, menuname, qty, (menuprice*qty),"DEAL","Home Chef","PICKUP","");
                            }*/
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice=currentCartPrice-price;

                            //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        }
                    }

                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getApplicationContext())));
                    txtItems.setText(new DBHelper_New(DealsPickup.this).getItemsCount());
                }
            });

        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDealDelivery   :
                startActivity(new Intent(DealsPickup.this,DealsHome.class));
                finish();
                break;
            case R.id.btnFilter:
                FilterFragment.setFilterListener(this);
                startActivity(new Intent(DealsPickup.this,FilterFragment.class));
                break;
            default:
                break;
        }
    }
    @Override
    public void onApplyFilter(JSONObject result) {
        Log.i("result",result.toString());
        Toast.makeText(getApplicationContext(), result.toString(), Toast.LENGTH_SHORT).show();
    }


    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty, txtDeliverytime, txtweigthingms, txtfoodtype, txtPickupLocation;
        ImageView imgThumb, pickupimage;
        Button btninc, btndrc;
        RatingBar rb;

        public ViewHolderRow(View convertView) {
            super(convertView);
            txtText = (TextView) convertView.findViewById(R.id.txtTextPickup);
            txtSubText = (TextView) convertView.findViewById(R.id.txtSubTextPickup);
            imgThumb = (ImageView) convertView.findViewById(R.id.imgThumbPickup);
            txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgmsPickup);
            txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQtyPickup);
            txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtypePickup);
            txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtimePickup);
            txtPickupLocation = (TextView) convertView.findViewById(R.id.txtAreaNamePickup);
            pickupimage = (ImageView) convertView.findViewById(R.id.pickupimage);
            txtQty = (TextView) convertView.findViewById(R.id.txtQtyPickup);
            btninc = (Button) convertView.findViewById(R.id.btnincPickup);
            btndrc = (Button) convertView.findViewById(R.id.btndrcPickup);


        }
    }
    void clearData() {
        Menu_ID.clear();
        Menu_name.clear();
        Menu_price.clear();
        Menu_image.clear();
        Menu_stock_qty.clear();
        Menu_delivery.clear();
        Menu_delivery_time.clear();
        Menu_food_type.clear();
        Menu_serves.clear();
        Menu_weigth_grms.clear();
        Menu_pickup_location.clear();
        Menu_pickup_image.clear();
    }


    ProgressDialog pd;

    public void getDataFromServer() {
        //
        showProgressDialog();

        String mUrl="";
        if(ApplicationLoader.getProchefID().isEmpty())
            mUrl="http://128.199.173.98/api/get-menu-data-by-category-id.php?accesskey=12345&category_id=00&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        else
            mUrl="http://128.199.173.98/api/get-menu-prochef.php?accesskey=12345&category_id=00&chefid="+ApplicationLoader.getProchefID()+"&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        //


       /* OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    closeDialog();
                   runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });
*/
    }


    // method to parse json data from server
    public void parseJSONData(String strResponse) {

        clearData();

        try {
            ///dummy data

            Menu_ID.add(null);
            Menu_name.add(null);
            Menu_price.add(null);
            Menu_image.add(null);//Menu_image
            Menu_stock_qty.add(null);
            Menu_delivery.add(null);
            Menu_delivery_time.add(null);
            Menu_food_type.add(null);
            Menu_serves.add(null);
            Menu_weigth_grms.add(null);
            Menu_pickup_location.add(null);
            Menu_pickup_image.add(null);


            ///


            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int ii = 0; ii < data.length(); ii++) {
                int i = 0;
                JSONObject object = data.getJSONObject(i);

                JSONObject menu = object.getJSONObject("MenuActivity");

                Log.e("MenuActivity", menu.toString());

                Menu_ID.add(Long.parseLong(menu.getString("Menu_ID")));
                Menu_name.add(menu.getString("Menu_name"));
                Menu_price.add(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                Menu_image.add(menu.getString("main_image"));//Menu_image
                Menu_stock_qty.add(menu.getString("Quantity"));
                Menu_delivery.add(menu.getString("delivery_type"));
                Menu_delivery_time.add(menu.getString("delivery_time"));
                Menu_food_type.add(menu.getString("type_of_food"));
                Menu_serves.add(menu.getString("no_of_serves"));
                Menu_weigth_grms.add(menu.getString("weight_in_grams"));
                Menu_pickup_location.add(menu.getString("pickuplocation"));
                Menu_pickup_image.add(menu.getString("pickupImageUrl"));
            }


            dla.notifyDataSetChanged();


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // create arraylist variables to store data from server
    static ArrayList<Long> Menu_ID = new ArrayList<Long>();
    static ArrayList<String> Menu_name = new ArrayList<String>();
    static ArrayList<Double> Menu_price = new ArrayList<Double>();
    static ArrayList<String> Menu_image = new ArrayList<String>();
    static ArrayList<String> Menu_stock_qty = new ArrayList<String>();
    static ArrayList<String> Menu_food_type = new ArrayList<String>();
    static ArrayList<String> Menu_delivery = new ArrayList<String>();
    static ArrayList<String> Menu_weigth_grms = new ArrayList<String>();
    static ArrayList<String> Menu_serves = new ArrayList<String>();
    static ArrayList<String> Menu_delivery_time = new ArrayList<String>();
    static ArrayList<String> Menu_pickup_location = new ArrayList<String>();
    static ArrayList<String> Menu_pickup_image = new ArrayList<String>();


    static String Currency="₹";

    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper_New dbhelper = new DBHelper_New(ctx);
        //dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }

    public void showProgressDialog(){
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg){
        if (pd == null)
            pd = new ProgressDialog(DealsPickup.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();
    }
    public void closeDialog(){
        // do something wih the result
        if (pd != null)
            pd.dismiss();
    }


}
