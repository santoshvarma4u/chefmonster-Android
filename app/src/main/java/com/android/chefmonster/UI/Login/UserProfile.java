package com.android.chefmonster.UI.Login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class UserProfile extends AppCompatActivity {

    private Toolbar mToolbar;
    private Context mContext;
    private TextView username,useremail,phoneNumber;
    private ImageView userImage,homeImage;
    private RecyclerView rv_trans;
    List<Trans> oList;
    TransListAdapter ola;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        setUpToolbar();
        setupResources();

    }
    private void setupResources()
    {
        username=(TextView)findViewById(R.id.user_profile_name);
        useremail=(TextView)findViewById(R.id.user_profile_short_bio);
        phoneNumber=(TextView)findViewById(R.id.userPhone);
        userImage=(ImageView)findViewById(R.id.user_profile_photo);
        homeImage=(ImageView)findViewById(R.id.add_friend);
        ImageLoader imageLoader = new ImageLoader(getApplicationContext());
        if(!ApplicationLoader.getUserName().isEmpty())
            username.setText(ApplicationLoader.getUserName().toString());
        if(!ApplicationLoader.getUserEmail().isEmpty())
            useremail.setText(ApplicationLoader.getUserEmail().toString());
        /*if(!ApplicationLoader.getUserProfilePic().isEmpty())
            imageLoader.DisplayImage(ApplicationLoader.getUserProfilePic(), userImage);*/
        if(!ApplicationLoader.getUserPhone().isEmpty())
            phoneNumber.setText(ApplicationLoader.getUserPhone().toString());

        homeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserProfile.this, HomePage.class));
            }
        });
        rv_trans=(RecyclerView)findViewById(R.id.rv_trans);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(UserProfile.this);
        rv_trans.setLayoutManager(mLayoutManager);
        rv_trans.setItemAnimator(new DefaultItemAnimator());
        oList = new ArrayList<>();
        ola = new TransListAdapter();
    }

    private void setUpToolbar()
    {
       /* mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.include);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("My Profile");
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_left_black_48dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserProfile.this, HomePage.class));
            }
        });*/
    }

    public class TransListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vRow = inflater.inflate(R.layout.my_trans_item, parent, false);
            viewHolder = new ViewHolderRow(vRow);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            ViewHolderRow vh2 = (ViewHolderRow) holder;
            configureViewHolderRow(vh2, position);
        }

        @Override
        public int getItemCount() {
            return oList.size();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataFromServer();
    }

    public void configureViewHolderRow(final ViewHolderRow holder, int position) {
        // position=position-1;

        Trans mTrans = new Trans();
        mTrans = oList.get(position);
        holder.tv_trans_id.setText("Transaction ID: "+mTrans.getTransactionID());
        holder.tv_trans_date.setText("Date: "+mTrans.getTransDate());
        holder.tv_done_by.setText("Payment Mode: "+mTrans.getModeofpayment());
        holder.tv_amount.setText("Transaction Amount: "+mTrans.getTransAmount());
        holder.tv_status.setText("Staus: "+mTrans.getTransStatus());

    }

    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView tv_trans_id, tv_trans_date, tv_done_by, tv_amount, tv_status;


        public ViewHolderRow(View convertView) {
            super(convertView);
            tv_trans_id=(TextView)convertView.findViewById(R.id.tv_trans_id);
            tv_trans_date=(TextView)convertView.findViewById(R.id.tv_trans_date);
            tv_done_by=(TextView)convertView.findViewById(R.id.tv_done_by);
            tv_amount=(TextView)convertView.findViewById(R.id.tv_amount);
            tv_status=(TextView)convertView.findViewById(R.id.tv_status);
        }
    }

    ProgressDialog pd;

    public void getDataFromServer() {

        showProgressDialog("Getting your Transactions....");
        String mUrl = "http://128.199.173.98/api/getMyAccounts.php?getMyAccount=true&custemail="+ApplicationLoader.getUserEmail();
        Log.d("murl", mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    closeDialog();
                  runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }
    public void parseJSONData(String strResponse) {

        // clearData();
        oList.clear();
        try {
            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            System.out.println(data.length() + "***************");
            if (data.length() <= 0) {

                  Toast.makeText(UserProfile.this, "Not found any payment transactions. Keep order delicious foods.", Toast.LENGTH_LONG).show();

            } else {
                for (int ii = 0; ii < data.length(); ii++) {
                    //int i = 0;
                    JSONObject object = data.getJSONObject(ii);
                    JSONObject trans = object.getJSONObject("Trans");
                    Log.e("Trans", trans.toString());
                    Trans mTransactions = new Trans();
                    mTransactions.setTransactionID(trans.getString("txn_id"));
                    mTransactions.setModeofpayment(trans.getString("cardnum"));
                    mTransactions.setTransAmount(trans.getString("amount"));
                    mTransactions.setTransDate(trans.getString("date_time"));
                    mTransactions.setTransStatus(trans.getString("status"));
                    oList.add(mTransactions);
                }
            }

            rv_trans.setAdapter(ola);

            ola.notifyDataSetChanged();
            rv_trans.setVisibility(View.VISIBLE);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void showProgressDialog(String msg) {

        if (pd == null)
            pd = new ProgressDialog(UserProfile.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();

    }

    public void closeDialog() {
        // do something wih the result
        if (pd != null)
            pd.dismiss();
    }
}
