package com.android.chefmonster.UI.Menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.ActivityCartNew;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.R;
import com.android.chefmonster.Searchbar.MaterialSearchBar;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.circlepageidicator.CirclePageIndicator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CombosFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CombosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CombosFragment extends Fragment  implements View.OnClickListener, FilterListener, MaterialSearchBar.OnSearchActionListener  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    View view;
    RecyclerView recycler_view;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES = {R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    public ImageLoader imageLoader;
    DecimalFormat formatData;
    //
    private TextView mTxtAmountAdapter;
    private TextView txtItems;
    int ItemCount = 0;
    RelativeLayout checkout;
    LinearLayout optionsLayout;
    Button btnDelivery;
    Button btnCartAdapter;
    Button btnFilter;

    ComboListAdapter cla;

    private List<String> lastSearches;
    private MaterialSearchBar searchBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public LinearLayoutManager linearLayoutManager;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CombosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CombosFragment newInstance(String param1, String param2) {
        CombosFragment fragment = new CombosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_combos, container, false);
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        //
        for (int i = 0; i < IMAGES.length; i++)
            ImagesArray.add(IMAGES[i]);
        //
        imageLoader = new ImageLoader(getActivity());
        //
        formatData = new DecimalFormat("#.##");
//
        searchBar = (MaterialSearchBar) view.findViewById(R.id.edtKeyword);
        searchBar.setOnSearchActionListener(this);


        lastSearches = loadSearchSuggestionFromDisk();
       // searchBar.setLastSuggestions(lastSearches);


        mTxtAmountAdapter = (TextView) view.findViewById(R.id.txtCartPriceAdapter);
        txtItems = (TextView) view.findViewById(R.id.txtItems);
        checkout = (RelativeLayout) view.findViewById(R.id.lytCheckout);

        btnCartAdapter = (Button) view.findViewById(R.id.btnCartAdapter);

        btnCartAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iMyOrder = new Intent(getActivity(), ActivityCartNew.class);
                startActivity(iMyOrder);
            }
        });

        //
        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                //totalItemCount = linearLayoutManager.getItemCount();
                //lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (linearLayoutManager.findFirstVisibleItemPosition() > 0) {
                    if (optionsLayout.getVisibility() == View.GONE)
                        optionsLayout.setVisibility(View.VISIBLE);
                } else {
                    optionsLayout.setVisibility(View.GONE);
                }
            }
        });
        linearLayoutManager = (LinearLayoutManager) recycler_view.getLayoutManager();

        //
        optionsLayout = (LinearLayout) view.findViewById(R.id.linearLayout10);

        //
        btnDelivery = (Button) view.findViewById(R.id.btnDelivery);
        btnDelivery.setOnClickListener(this);
        //
        cla = new ComboListAdapter();

        //recycler_view.setAdapter(cla);

        btnFilter = (Button) view.findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(this);
        return view;
    }


    public class ComboListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        final int SLIDER = 0, ROW = 1;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            switch (viewType) {
                case SLIDER:
                    View vSlider = inflater.inflate(R.layout.item_viewpager, viewGroup, false);
                    viewHolder = new ViewHolderSlider(vSlider);
                    break;
                case ROW:
                    View vRow = inflater.inflate(R.layout.menu_list_items_combos, viewGroup, false);
                    viewHolder = new ViewHolderRow(vRow);
                    break;
                default:

                    break;
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            switch (viewHolder.getItemViewType()) {
                case SLIDER:
                    ViewHolderSlider vh1 = (ViewHolderSlider) viewHolder;
                    configureViewHolderSlider(vh1, position);
                    break;
                case ROW:
                    ViewHolderRow vh2 = (ViewHolderRow) viewHolder;
                    configureViewHolderRow(vh2, position);
                    break;
                default:

                    break;
            }
        }

        @Override
        public int getItemCount() {
            return PickUpListFragment.Menu_ID.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? SLIDER : ROW;
        }

        public void configureViewHolderSlider(final ViewHolderSlider viewHolder, int position) {

            viewHolder.pager.setAdapter(new SlidingImageAdapter(getActivity(), sliderImages));


           // viewHolder.indicator.setViewPager(viewHolder.pager);

            viewHolder.pager.setClipToPadding(false);
            // set padding manually, the more you set the padding the more you see of prev & next page
            viewHolder.pager.setPadding(60, 0, 60, 0);
            // sets a margin b/w individual pages to ensure that there is a gap b/w them
            viewHolder.pager.setPageMargin(20);
            final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
          //  viewHolder.indicator.setRadius(2 * density);

            NUM_PAGES = IMAGES.length;

        }

        int ItemCount = 0;

        public void configureViewHolderRow(final ViewHolderRow holder, final int position) {

            Double finalPrice = 0.0;

            holder.txtText.setText(PickUpListFragment.Menu_name.get(position));
            //  holder.txtSubText.setText(PickUpListFragment.Menu_price.get(position) + " " + PickUpListFragment.Currency);
            holder.txtDeliverytime.setText(PickUpListFragment.Menu_delivery_time.get(position));
            holder.txtfoodtype.setText(PickUpListFragment.Menu_food_type.get(position));
            holder.txtStockQty.setText(PickUpListFragment.Menu_stock_qty.get(position));
            holder.txtweigthingms.setText(PickUpListFragment.Menu_weigth_grms.get(position));
            holder.txtComboItems.setText(PickUpListFragment.Menu_pickup_location.get(position));

            if (PickUpListFragment.Menu_Deal_price.get(position) > 0) {
                holder.ivDealBanner.setVisibility(View.VISIBLE);
                holder.txtMainValue.setVisibility(View.VISIBLE);
                holder.txtMainValue.setText(PickUpListFragment.Menu_price.get(position) + " " + PickUpListFragment.Currency);
                holder.txtMainValue.setPaintFlags(holder.txtMainValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                Double afterDiscount = PickUpListFragment.Menu_price.get(position) - PickUpListFragment.Menu_Deal_price.get(position);
                holder.txtSubText.setText(String.valueOf(afterDiscount) + " " + DeliveryListFragment.Currency);
                finalPrice = afterDiscount;
            } else {
                holder.txtSubText.setText(PickUpListFragment.Menu_price.get(position) + " " + PickUpListFragment.Currency);
                finalPrice = 0.0;
            }


            imageLoader.DisplayImage(Constants.AdminPageURL + PickUpListFragment.Menu_image.get(position), holder.imgThumb);
            imageLoader.DisplayImage(PickUpListFragment.Menu_pickup_image.get(position), holder.pickupimage);

            //
            final Double finalPrice1 = finalPrice;

            holder.btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    if (qty < 0) {

                        // checkout.setVisibility(View.GONE);
                    } else {

                        ItemCount++;
                        qty++;
                        checkout.setVisibility(View.VISIBLE);
                        double price = finalPrice1;
                        double changedPrice = price * qty;
                        holder.txtSubText.setText(changedPrice + " " + PickUpListFragment.Currency);
                        holder.txtQty.setText(String.valueOf(qty));

                        DBHelper_New db = new DBHelper_New(getActivity());
                        //db.openDataBase();
                        long menuid = PickUpListFragment.Menu_ID.get(position);
                        System.out.println(menuid + "**************");
                        double menuprice = finalPrice1;
                        String menuname = PickUpListFragment.Menu_name.get(position);
                        String menuImage=PickUpListFragment.Menu_image.get(position);
                       /* if (PickUpListFragment.Menu_Deal_price.get(position) > 0) {
                            if (db.isDataExist(menuid, "DEAL", "PICKUP")) {
                                db.updateData(menuid, qty, (menuprice * qty), "DEAL", "Home Chef", "PICKUP");
                            } else {
                                db.addData(menuid, menuname, qty, (menuprice * qty), "DEAL", "Home Chef", "PICKUP",menuImage);
                            }
                        } else {
                            if (db.isDataExist(menuid, "Home Chef", "PICKUP")) {
                                db.updateData(menuid, qty, (menuprice * qty), "Home Chef", "Home Chef", "PICKUP");
                            } else {
                                db.addData(menuid, menuname, qty, (menuprice * qty), "Home Chef", "Home Chef", "PICKUP",menuImage);
                            }
                        }*/

                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        double ChangedPrice = currentCartPrice + menuprice;

                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        db.close();
                    }

                    txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));
                }
            });
            final Double finalPrice2 = finalPrice;
            holder.btndrc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());

                    if (qty <= 0) {

                        double price = finalPrice2;
                        holder.txtSubText.setText(price + " " + PickUpListFragment.Currency);
                        holder.txtQty.setText("0");

                        long menuid = PickUpListFragment.Menu_ID.get(position);
                        DBHelper_New db = new DBHelper_New(getActivity());
                        //db.openDataBase();
                        db.deleteData(menuid);
                        db.close();

                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        if (currentCartPrice > 0) {
                            double ChangedPrice = currentCartPrice - price;
                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        }

                    } else {
                        qty--;

                        if (qty == 1) {
                            ItemCount--;
                            double price = finalPrice2;
                            holder.txtQty.setText("0");
                            holder.txtSubText.setText(price + " " + PickUpListFragment.Currency);
                            DBHelper_New db = new DBHelper_New(getActivity());
                            //1111db.openDataBase();
                            long menuid = PickUpListFragment.Menu_ID.get(position);
                            double menuprice = finalPrice2;
                            String menuname = PickUpListFragment.Menu_name.get(position);
                            String menuImage=PickUpListFragment.Menu_image.get(position);
                          /*  if (PickUpListFragment.Menu_Deal_price.get(position) > 0) {
                                if (db.isDataExist(menuid, "DEAL", "PICKUP")) {
                                    db.updateData(menuid, qty, (menuprice * qty), "DEAL", "Home Chef", "PICKUP");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty), "DEAL", "Home Chef", "PICKUP",menuImage);
                                }
                            } else {
                                if (db.isDataExist(menuid, "Home Chef", "PICKUP")) {
                                    db.updateData(menuid, qty, (menuprice * qty), "Home Chef", "Home Chef", "PICKUP");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty), "Home Chef", "Home Chef", "PICKUP",menuImage);
                                }
                            }
*/
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        } else {

                            ItemCount--;
                            double price = finalPrice2;
                            double changedPrice = price * qty;
                            holder.txtSubText.setText(changedPrice + " " + PickUpListFragment.Currency);
                            holder.txtQty.setText(String.valueOf(qty));
                            DBHelper_New db = new DBHelper_New(getActivity());
                            //db.openDataBase();
                            long menuid = PickUpListFragment.Menu_ID.get(position);
                            double menuprice = finalPrice2;
                            String menuname = PickUpListFragment.Menu_name.get(position);
                            String menuImage=PickUpListFragment.Menu_image.get(position);
                           /* if (PickUpListFragment.Menu_Deal_price.get(position) > 0) {
                                if (db.isDataExist(menuid, "DEAL", "PICKUP")) {
                                    db.updateData(menuid, qty, (menuprice * qty), "DEAL", "Home Chef", "PICKUP");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty), "DEAL", "Home Chef", "PICKUP",menuImage);
                                }
                            } else {
                                if (db.isDataExist(menuid, "Home Chef", "PICKUP")) {
                                    db.updateData(menuid, qty, (menuprice * qty), "Home Chef", "Home Chef", "PICKUP");
                                } else {
                                    db.addData(menuid, menuname, qty, (menuprice * qty), "Home Chef", "Home Chef", "PICKUP",menuImage);
                                }
                            }*/
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        }
                    }

                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(getActivity())));
                    txtItems.setText(new DBHelper_New(getActivity()).getItemsCount());
                }
            });
        }
    }


    class ViewHolderSlider extends RecyclerView.ViewHolder {
        ViewPager pager;
        CirclePageIndicator indicator;
        Button btnDelivery;

        public ViewHolderSlider(View itemView) {
            super(itemView);
            pager = (ViewPager) itemView.findViewById(R.id.pager);
            //indicator = (CirclePageIndicator) itemView.findViewById(R.id.indicator);
            btnDelivery = (Button) itemView.findViewById(R.id.btnDelivery);
            btnDelivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setDeliveryFragment();
                }
            });
        }
    }
    DeliveryListFragment deliveryListFragment;
    public void setDeliveryFragment() {
        deliveryListFragment = new DeliveryListFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.main_activity_content_frame, deliveryListFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty, txtDeliverytime, txtweigthingms, txtfoodtype, txtComboItems, txtMainValue;
        ImageView imgThumb, pickupimage, ivDealBanner;

        Button btninc, btndrc;
        RatingBar rb;

        public ViewHolderRow(View convertView) {
            super(convertView);
            txtText = (TextView) convertView.findViewById(R.id.tv_comboname);
            txtSubText = (TextView) convertView.findViewById(R.id.txtSubTextPickup);
            imgThumb = (ImageView) convertView.findViewById(R.id.imgThumbPickup);
            txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgmsPickup);
            txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQtyPickup);
            txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtypePickup);
            txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtimePickup);
            txtComboItems = (TextView) convertView.findViewById(R.id.tv_combo_items);
            pickupimage = (ImageView) convertView.findViewById(R.id.pickupimage);
            txtQty = (TextView) convertView.findViewById(R.id.txtQtyPickup);
            btninc = (Button) convertView.findViewById(R.id.btnincPickup);
            btndrc = (Button) convertView.findViewById(R.id.btndrcPickup);
            txtMainValue = (TextView) convertView.findViewById(R.id.txtMainValue);
            ivDealBanner = (ImageView) convertView.findViewById(R.id.ivDealBanner);
        }
    }


    void clearData() {
        Menu_ID.clear();
        Menu_name.clear();
        Menu_price.clear();
        Menu_image.clear();
        Menu_stock_qty.clear();
        Menu_delivery.clear();
        Menu_delivery_time.clear();
        Menu_food_type.clear();
        Menu_serves.clear();
        Menu_weigth_grms.clear();
        Menu_pickup_location.clear();
        Menu_pickup_image.clear();
        Menu_Deal_price.clear();
    }


    ProgressDialog pd;


    public void getDataFromServer(String keyword) {
        //
        showProgressDialog();

        String mUrl = "";
        if (ApplicationLoader.getProchefID().isEmpty() && keyword.isEmpty())

            mUrl = " http://128.199.173.98/api/get-deals-combos.php?accesskey=12345&deliverytype=delivery&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        else if (ApplicationLoader.getProchefID().isEmpty() && keyword.length() > 0)
            mUrl = " http://128.199.173.98/api/get-deals-combos.php?accesskey=12345&deliverytype=delivery&keyword=" + keyword + "&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        else
            mUrl = " http://128.199.173.98/api/get-deals-combos.php?accesskey=12345&chefid=" + ApplicationLoader.getProchefID() + "&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();


        System.out.println("***************" + mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    closeDialog();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });
    }



//rewrite for combos
    // method to parse json data from server
    public void parseJSONData(String strResponse) {

        clearData();

        try {

            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int ii = 0; ii < data.length(); ii++) {

                JSONObject object = data.getJSONObject(ii);

                JSONObject menu = object.getJSONObject("MenuActivity");

                Log.e("MenuActivity", menu.toString());

                Menu_ID.add(Long.parseLong(menu.getString("id")));
                Menu_name.add(menu.getString("deal_name"));
                Menu_price.add(Double.valueOf(formatData.format(menu.getDouble("deal_price"))));
                Menu_image.add(menu.getString("main_image"));//Menu_image
                Menu_stock_qty.add(menu.getString("Quantity"));
                Menu_delivery.add(menu.getString("delivery_type"));
                Menu_delivery_time.add(menu.getString("distance"));
                Menu_food_type.add(menu.getString("type_of_food"));
                Menu_serves.add(menu.getString("no_of_serves"));
                Menu_weigth_grms.add(menu.getString("weight_in_grams"));
                Menu_pickup_location.add(menu.getString("pickupLocation"));
                Menu_pickup_image.add(menu.getString("pickupImageUrl"));
                Menu_Deal_price.add(Double.valueOf(menu.getString("deal_price")));
            }

            cla.notifyDataSetChanged();
            recycler_view.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    // create arraylist variables to store data from server
    static ArrayList<Long> Menu_ID = new ArrayList<Long>();
    static ArrayList<String> Menu_name = new ArrayList<String>();
    static ArrayList<Double> Menu_price = new ArrayList<Double>();
    static ArrayList<String> Menu_image = new ArrayList<String>();
    static ArrayList<String> Menu_stock_qty = new ArrayList<String>();
    static ArrayList<String> Menu_food_type = new ArrayList<String>();
    static ArrayList<String> Menu_delivery = new ArrayList<String>();
    static ArrayList<String> Menu_weigth_grms = new ArrayList<String>();
    static ArrayList<String> Menu_serves = new ArrayList<String>();
    static ArrayList<String> Menu_delivery_time = new ArrayList<String>();
    static ArrayList<String> Menu_pickup_location = new ArrayList<String>();
    static ArrayList<String> Menu_pickup_image = new ArrayList<String>();
    static ArrayList<Double> Menu_Deal_price = new ArrayList<Double>();


    static String Currency = "₹";

    @Override
    public void onResume() {
        super.onResume();
        loadSliderImages();
        //getDataFromServer();
    }

    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper_New dbhelper = new DBHelper_New(ctx);
        //dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }

    public void loadSliderImages() {
        showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://chefmonster.com/api/getSliders.php").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {

            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                // Log.i("response",response.body());
                //Log.i("response",response.body().string());
                closeDialog();
                if (response.isSuccessful()) {
                    //processSliderImages(response.body().string());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processSliderImages(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });

    }

    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
        if (pd == null)
            pd = new ProgressDialog(getActivity());
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();
    }

    public void closeDialog() {
        // do something wih the result
        if (pd != null)
            pd.dismiss();
    }


    public void processSliderImages(String response) {
        //{"slides": ["slide.jpg","slide.jpg","slide.jpg"]}
        sliderImages.clear();
        try {
            JSONObject jobj = new JSONObject(response);
            JSONArray jSlides = jobj.getJSONArray("slides");
            for (int i = 0; i < jSlides.length(); i++) {
                sliderImages.add(jSlides.getString(i));
            }
            //
            recycler_view.setAdapter(cla);
            //
            getDataFromServer("");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    ArrayList<String> sliderImages = new ArrayList<>();

    public List<String> loadSearchSuggestionFromDisk() {
        List<String> ll = new ArrayList<>();
        ll.add(0, "Biryani");
        ll.add(1, "Chilli Chicken");
        ll.add(2, "Dum Biryani");


        return ll;
       /* DBHelper_New db=new DBHelper_New(getActivity());
        List<List<String>> results=db.getDataByQuery("SELECT "+db.FOOD_NAME+" "+db.TABLE_RECENTSEARCH);
        return results;*/
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onSearchStateChanged(boolean enabled) {

    }

    @Override
    public void onSearchConfirmed(CharSequence text) {

    }

    @Override
    public void onButtonClicked(int buttonCode) {

    }

    @Override
    public void onApplyFilter(JSONObject result) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
