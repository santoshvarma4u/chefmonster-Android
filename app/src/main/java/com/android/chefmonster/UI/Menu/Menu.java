package com.android.chefmonster.UI.Menu;

/**
 * Created by SantoshT on 11/15/2016.
 */

public class Menu {

    private Long Menu_ID;
    private String Menu_name;
    private Double Menu_price;
    private String Menu_image;
    private String Menu_stock_qty;
    private String Menu_delivery;
    private String Menu_delivery_time;
    private String Menu_food_type;
    private String Menu_serves;
    private String Menu_weigth_grms;
    private Double Menu_Deal_price;
    private String Menu_pickup_image;
    private String Menu_pickup_location;
    private String Menu_pickup_distance;
    private boolean Menu_image_avail;
    private boolean pickup_image_avail;
    private String latitude;
    private String longitude;
    private String live_status;
    private String live_url;
    private String timetocook;
    private String distanceValue;


    public String getDistanceValue() {
        return distanceValue;
    }

    public void setDistanceValue(String distanceValue) {
        this.distanceValue = distanceValue;
    }

    public String getTimetocook() {
        return timetocook;
    }

    public void setTimetocook(String timetocook) {
        this.timetocook = timetocook;
    }

    public String getMenu_Chef_id() {
        return Menu_Chef_id;
    }

    public void setMenu_Chef_id(String menu_Chef_id) {
        Menu_Chef_id = menu_Chef_id;
    }

    private String Menu_Chef_id;


    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }

    public String getLive_url() {
        return live_url;
    }

    public void setLive_url(String live_url) {
        this.live_url = live_url;
    }

    public double getFinal_price() {
        return final_price;
    }

    public void setFinal_price(double final_price) {
        this.final_price = final_price;
    }

    private double final_price;


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public boolean isPickup_image_avail() {
        return pickup_image_avail;
    }

    public void setPickup_image_avail(boolean pickup_image_avail) {
        this.pickup_image_avail = pickup_image_avail;
    }

    public boolean isMenu_image_avail() {
        return Menu_image_avail;
    }

    public void setMenu_image_avail(boolean menu_image_avail) {
        Menu_image_avail = menu_image_avail;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }


    private int itemCount;


    public Long getMenu_ID() {
        return Menu_ID;
    }

    public void setMenu_ID(Long menu_ID) {
        Menu_ID = menu_ID;
    }

    public String getMenu_name() {
        return Menu_name;
    }

    public void setMenu_name(String menu_name) {
        Menu_name = menu_name;
    }

    public Double getMenu_price() {
        return Menu_price;
    }

    public void setMenu_price(Double menu_price) {
        Menu_price = menu_price;
    }

    public String getMenu_image() {
        return Menu_image;
    }

    public void setMenu_image(String menu_image) {
        Menu_image = menu_image;
    }

    public String getMenu_stock_qty() {
        return Menu_stock_qty;
    }

    public void setMenu_stock_qty(String menu_stock_qty) {
        Menu_stock_qty = menu_stock_qty;
    }

    public String getMenu_delivery() {
        return Menu_delivery;
    }

    public void setMenu_delivery(String menu_delivery) {
        Menu_delivery = menu_delivery;
    }

    public String getMenu_delivery_time() {
        return Menu_delivery_time;
    }

    public void setMenu_delivery_time(String menu_delivery_time) {
        Menu_delivery_time = menu_delivery_time;
    }

    public String getMenu_food_type() {
        return Menu_food_type;
    }

    public void setMenu_food_type(String menu_food_type) {
        Menu_food_type = menu_food_type;
    }

    public String getMenu_serves() {
        return Menu_serves;
    }

    public void setMenu_serves(String menu_serves) {
        Menu_serves = menu_serves;
    }

    public String getMenu_weigth_grms() {
        return Menu_weigth_grms;
    }

    public void setMenu_weigth_grms(String menu_weigth_grms) {
        Menu_weigth_grms = menu_weigth_grms;
    }

    public Double getMenu_Deal_price() {
        return Menu_Deal_price;
    }

    public void setMenu_Deal_price(Double menu_Deal_price) {
        Menu_Deal_price = menu_Deal_price;
    }

    public String getMenu_pickup_image() {
        return Menu_pickup_image;
    }

    public void setMenu_pickup_image(String menu_pickup_image) {
        Menu_pickup_image = menu_pickup_image;
    }

    public String getMenu_pickup_location() {
        return Menu_pickup_location;
    }

    public void setMenu_pickup_location(String menu_pickup_location) {
        Menu_pickup_location = menu_pickup_location;
    }

    public String getMenu_pickup_distance() {
        return Menu_pickup_distance;
    }

    public void setMenu_pickup_distance(String menu_pickup_distance) {
        Menu_pickup_distance = menu_pickup_distance;
    }
}
