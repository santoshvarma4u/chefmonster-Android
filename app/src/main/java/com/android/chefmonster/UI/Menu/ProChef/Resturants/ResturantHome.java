package com.android.chefmonster.UI.Menu.ProChef.Resturants;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.R;
import com.android.chefmonster.Searchbar.MaterialSearchBar;
import com.android.chefmonster.UI.Menu.Menu;
import com.android.chefmonster.UI.Menu.ProChef.Category;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ResturantHome extends AppCompatActivity {

    RecyclerView recycler_view;
    RecyclerView rv_category;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public ImageLoader imageLoader;
    DecimalFormat formatData;
    private TextView mTxtAmountAdapter;
    private TextView txtItems;
    int ItemCount = 0;
    RelativeLayout checkout;
    LinearLayout optionsLayout;
    EditText mSearch;
    boolean dealsApply;
    //
    private List<String> lastSearches;
    private MaterialSearchBar searchBar;

    Button btnPickup,btnDeals,btnCombos;
    Button btnCartAdapter;
    Button btnFilter;

    List<Menu> menuList;
    List<Category> categoryList;
    SwipeRevealLayout swipeRevealLayout;

    TextView resturantNameTv,resturantTagTv;

    static String Currency="₹";
    ImageView resturantLogoImgView;
    private RelativeLayout parallaxView;


    private AppBarLayout appBarLayout = null;
    private CollapsingToolbarLayout collapsingToolbar = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resturant_home);

        recycler_view = (RecyclerView) findViewById(R.id.resturantRv);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ResturantHome.this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.smoothScrollToPosition(0);
        recycler_view.addItemDecoration(new SpacesItemDecoration(10));
        recycler_view.setHasFixedSize(true);

        rv_category=(RecyclerView) findViewById(R.id.categoryRv);

        RecyclerView.LayoutManager mLayoutManager1 =new LinearLayoutManager(ResturantHome.this, LinearLayoutManager.HORIZONTAL, false);
        rv_category.setLayoutManager(mLayoutManager1);
        rv_category.setItemAnimator(new DefaultItemAnimator());
        rv_category.smoothScrollToPosition(0);



        parallaxView = ((RelativeLayout)findViewById(R.id.parallaxView));
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar_container);
        appBarLayout.getLayoutParams().height = (int) (getWindowSize(ResturantHome.this).y * 0.60);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_container);

        resturantNameTv=(TextView)findViewById(R.id.resturantNameTv);
        resturantTagTv=(TextView)findViewById(R.id.resturantTagTv);
        resturantLogoImgView=(ImageView)findViewById(R.id.resturantLogoImgView);


      /*  findViewById(R.id.counterView).
                getLayoutParams().height = (int) (getWindowSize(ResturantHome.this).y * 0.10);*/

        menuList=new ArrayList<>();
        categoryList=new ArrayList<>();

        imageLoader = new ImageLoader(ResturantHome.this);

        txtItems=(TextView) findViewById(R.id.txtItems);
        txtItems.setText(new DBHelper_New(ResturantHome.this).getItemsCount());

        //
        formatData = new DecimalFormat("#.##");
//
        mTxtAmountAdapter = (TextView) findViewById(R.id.txtCartPriceAdapter);

        mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(ResturantHome.this)));

        checkout = (RelativeLayout) findViewById(R.id.lytCheckout);
        btnCartAdapter=(Button) findViewById(R.id.btnCartAdapter);
        dla=new ResturantListAdapter();
        cla=new CategoryListAdapter();
    }


    public static Point getWindowSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int verticalSpace;

        public SpacesItemDecoration(int verticalSpace) {
            this.verticalSpace = dpToPx(ResturantHome.this,verticalSpace);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
//            outRect.left = 45;
            if(parent.getChildAdapterPosition(view) == 0) {
                outRect.top = verticalSpace;
                outRect.bottom = verticalSpace;
            } else if(parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount()-1) {
                outRect.bottom = verticalSpace;
            } else {
                outRect.bottom = verticalSpace;
            }
        }
    }


    ResturantListAdapter dla;
    CategoryListAdapter cla;

    public class CategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            View vRow = inflater.inflate(R.layout.category_item_resturant_home, viewGroup, false);
            viewHolder = new ResturantHome.ViewHolderCategoryRow(vRow);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

            ResturantHome.ViewHolderCategoryRow vh2 = (ResturantHome.ViewHolderCategoryRow) viewHolder;
            configureViewHolderCategoryRow(vh2, position);

        }

        Category listCategory;

        public void configureViewHolderCategoryRow(final ResturantHome.ViewHolderCategoryRow holder, final int position) {
            listCategory=categoryList.get(position);
            holder.CategoryName.setText(listCategory.getCategoryName());

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return categoryList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position =0 ;
        }


    }


    public class ResturantListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        final int SLIDER = 0, ROW = 1;
        private final int VIEW_TYPE_LOADING = 3;


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            View vRow = inflater.inflate(R.layout.menu_items_new, viewGroup, false);
            viewHolder = new ResturantHome.ViewHolderRow(vRow);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

            ResturantHome.ViewHolderRow vh2 = (ResturantHome.ViewHolderRow) viewHolder;
            configureViewHolderRow(vh2, position);

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return menuList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position =0 ;
        }






        int ItemCount = 0;
        Menu listMenu;
        public void configureViewHolderRow(final ResturantHome.ViewHolderRow holder, final int position) {

            Double finalPrice=0.0;
            listMenu=menuList.get(position);
            holder.txtText.setText(listMenu.getMenu_name());
            holder.txtfoodtype.setText(listMenu.getMenu_food_type());
            holder.txtStockQty.setText(listMenu.getMenu_stock_qty());
            holder.txtweigthingms.setText(listMenu.getMenu_weigth_grms()+" gms");
            holder.txtAreaNamePickup.setText(listMenu.getMenu_pickup_location());
            if(position > 1)
            {
                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0,40,0,0);
                holder.menuCard.setLayoutParams(params);
            }
            if(listMenu.getMenu_Deal_price() > 0)
            {
                holder.ivDealBanner.setVisibility(View.VISIBLE);
                holder.txtMainValue.setVisibility(View.VISIBLE);
                holder.txtMainValue.setText(listMenu.getMenu_price() + " " + ResturantHome.Currency);
                holder.txtMainValue.setPaintFlags(holder.txtMainValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                Double afterDiscount=listMenu.getMenu_price()-listMenu.getMenu_Deal_price();
                holder.txtSubText.setText(String.valueOf(afterDiscount) + " " + ResturantHome.Currency);
                finalPrice=afterDiscount;
            }
            else
            {
                holder.txtSubText.setText(listMenu.getMenu_price() + " " + ResturantHome.Currency);
                finalPrice=listMenu.getMenu_price();

            }

            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
                holder.itemLayout.setVisibility(View.VISIBLE);
                holder.pickupimage.setVisibility(View.VISIBLE);
                imageLoader.DisplayImage(listMenu.getMenu_pickup_image(), holder.pickupimage);
            }


            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
            {
                holder.txtDeliverytime.setText(listMenu.getMenu_pickup_distance());
                holder.txtviewdelivery.setText("Distance");
            }
            else
            {
                holder.txtDeliverytime.setText(listMenu.getMenu_delivery_time());
                holder.txtviewdelivery.setText("Delivery Time");
            }

            if(listMenu.isMenu_image_avail())
                imageLoader.DisplayImage(Constants.ImagesUrl + listMenu.getMenu_image(), holder.imgThumb);
            else
                holder.imgThumb.setImageResource(R.drawable.photonotavail);



            //
            final Double finalPrice1 = finalPrice;
            holder.txtQty.setText(menuList.get(position).getItemCount()+"");
            holder.btninc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    if (qty < 0) {

                        //ItemCount--;
                    } else {
                        ItemCount++;

                        checkout.setVisibility(View.VISIBLE);
                        qty++;
                        double price = finalPrice1;
                        double changedPrice = price * qty;
                        //holder.txtSubText.setText(changedPrice+" "+ActivityMenuList.Currency);
                        holder.txtQty.setText(String.valueOf(qty));

                        DBHelper_New db = new DBHelper_New(ResturantHome.this);
                        //db.openDataBase();
                        long menuid = listMenu.getMenu_ID();
                        System.out.println(menuid+"**************");
                        double menuprice = finalPrice1;
                        String menuname = listMenu.getMenu_name();
                        String menuImage=listMenu.getMenu_image();

                        if(listMenu.getMenu_Deal_price() > 0)
                        {
                            if(listMenu.getMenu_delivery().toString().toLowerCase().contains("delivery"))
                            {
                                if (db.isDataExist(menuid,"DEAL","DELIVERY","Single")) {
                                 //   db.updateData(menuid, qty, (menuprice * qty),"DEAL","Pro Chef","DELIVERY","Single");
                                } else {
                                   // db.addData(menuid, menuname, qty, (menuprice * qty),"DEAL","Pro Chef","DELIVERY",menuImage,"Single");
                                }
                            }
                            else if(listMenu.getMenu_delivery().toString().toLowerCase().contains("pickup"))
                            {
                                if (db.isDataExist(menuid, "DEAL", "PICKUP","Single")) {
                                  //  db.updateData(menuid, qty, (menuprice * qty), "DEAL", "Pro Chef", "PICKUP","Single");
                                } else {
                                 //   db.addData(menuid, menuname, qty, (menuprice * qty), "DEAL", "Pro Chef", "PICKUP",menuImage,"Single");
                                }
                            }
                        }
                        else
                        {
                            if (db.isDataExist(menuid,"Home Chef","DELIVERY","Single")) {
                                //db.updateData(menuid, qty, (menuprice * qty),"Pro Chef","Pro Chef","DELIVERY","Single");
                            } else {
                              //  db.addData(menuid, menuname, qty, (menuprice * qty),"Pro Chef","Pro Chef","DELIVERY",menuImage,"Single");
                            }
                        }


                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        double ChangedPrice = currentCartPrice + menuprice;

                        //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        db.close();
                    }
                    //txtItems.setText(String.valueOf(ItemCount));
                    txtItems.setText(new DBHelper_New(ResturantHome.this).getItemsCount());
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(ResturantHome.this)));

                    menuList.get(position).setItemCount(qty);
                }
            });
            final Double finalPrice2 = finalPrice;
            holder.btndrc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(holder.txtQty.getText().toString());
                    qty--;
/*
                    if (qty > 0)
                        ItemCount--;*/

                    if (qty <= 0) {


                        double price = finalPrice2;
                        //	holder.txtSubText.setText(price+" "+DeliveryListFragment.Currency);
                        holder.txtQty.setText("0");

                        long menuid = listMenu.getMenu_ID();
                        DBHelper_New db = new DBHelper_New(ResturantHome.this);
                        //db.openDataBase();
                        db.deleteData(menuid);
                        db.close();

                        double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                        if (currentCartPrice > 0) {
                            double ChangedPrice = currentCartPrice - price;
                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                        }

                    } else {
                        ItemCount--;
                        if (qty == 1) {
                            double price = finalPrice2;
                            holder.txtQty.setText("0");
                            //holder.txtSubText.setText(price+" "+DeliveryListFragment.Currency);
                            DBHelper_New db = new DBHelper_New(ResturantHome.this);
                            //db.openDataBase();
                            long menuid = listMenu.getMenu_ID();
                            double menuprice = finalPrice2;
                            String menuname = listMenu.getMenu_name();
                            String menuimage=listMenu.getMenu_image();
                            if(listMenu.getMenu_Deal_price() > 0)
                            {
                                if (db.isDataExist(menuid,"DEAL","DELIVERY","Single")) {
                                   // db.updateData(menuid, qty, (menuprice * qty),"DEAL","Pro Chef","DELIVERY","Single");
                                } else {
                                 //   db.addData(menuid, menuname, qty, (menuprice * qty),"DEAL","Pro Chef","DELIVERY",menuimage,"Single");
                                }
                            }
                            else
                            {
                                if (db.isDataExist(menuid,"Home Chef","DELIVERY","Single")) {
                                 //   db.updateData(menuid, qty, (menuprice * qty),"Pro Chef","Pro Chef","DELIVERY","Single");
                                } else {
                                 //   db.addData(menuid, menuname, qty, (menuprice * qty),"Pro Chef","Pro Chef","DELIVERY",menuimage,"Single");
                                }
                            }
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        } else {

                            double price = finalPrice2;
                            double changedPrice = price * qty;
                            //holder.txtSubText.setText(changedPrice+" "+DeliveryListFragment.Currency);
                            holder.txtQty.setText(String.valueOf(qty));
                            DBHelper_New db = new DBHelper_New(ResturantHome.this);
                            //db.openDataBase();
                            long menuid = listMenu.getMenu_ID();
                            double menuprice = finalPrice2;
                            String menuname =listMenu.getMenu_name();
                            String menuImage=listMenu.getMenu_image();
                            if(listMenu.getMenu_Deal_price() > 0)
                            {
                                if (db.isDataExist(menuid,"DEAL","DELIVERY","Single")) {
                               //     db.updateData(menuid, qty, (menuprice * qty),"DEAL","Pro Chef","DELIVERY","Single");
                                } else {
                                  //  db.addData(menuid, menuname, qty, (menuprice * qty),"DEAL","Pro Chef","DELIVERY",menuImage,"Single");
                                }
                            }
                            else
                            {
                                if (db.isDataExist(menuid,"Home Chef","DELIVERY","Single")) {
                                  //  db.updateData(menuid, qty, (menuprice * qty),"Pro Chef","Pro Chef","DELIVERY","Single");
                                } else {
                                 //   db.addData(menuid, menuname, qty, (menuprice * qty),"Pro Chef","Pro Chef","DELIVERY",menuImage,"Single");
                                }
                            }
                            double currentCartPrice = Double.parseDouble(mTxtAmountAdapter.getText().toString());

                            double ChangedPrice = currentCartPrice - price;

                            //	mTxtAmountAdapter.setText(String.valueOf(ChangedPrice));
                            db.close();
                        }
                    }
                    mTxtAmountAdapter.setText(String.valueOf(getDataFromDatabase(ResturantHome.this)));
                    txtItems.setText(new DBHelper_New(ResturantHome.this).getItemsCount());
                    //txtItems.setText(String.valueOf(ItemCount));
                    menuList.get(position).setItemCount(qty);
                }
            });

        }
    }


    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtText, txtSubText, txtQty, txtStockQty, txtDeliverytime, txtweigthingms, txtfoodtype,txtMainValue,txtviewdelivery,txtAreaNamePickup;
        ImageView imgThumb,ivDealBanner,pickupimage;
        LinearLayout itemLayout;
        Button btninc, btndrc;
        RatingBar rb;
        CardView menuCard;

        public ViewHolderRow(View convertView) {
            super(convertView);
            txtText = (TextView) convertView.findViewById(R.id.txtText);
            txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
            imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
            pickupimage = (ImageView) convertView.findViewById(R.id.pickupimage);
            txtweigthingms = (TextView) convertView.findViewById(R.id.txtweightgms);
            txtStockQty = (TextView) convertView.findViewById(R.id.txtStockQty);
            txtfoodtype = (TextView) convertView.findViewById(R.id.txtfoodtype);
            txtDeliverytime = (TextView) convertView.findViewById(R.id.txtDtime);
            txtQty = (TextView) convertView.findViewById(R.id.txtQty);
            ivDealBanner=(ImageView) convertView.findViewById(R.id.ivDealBanner);
            btninc = (Button) convertView.findViewById(R.id.btninc);
            btndrc = (Button) convertView.findViewById(R.id.btndrc);
            txtMainValue=(TextView) convertView.findViewById(R.id.txtMainValue);
            txtviewdelivery=(TextView) convertView.findViewById(R.id.txtviewdelivery);
            txtAreaNamePickup=(TextView) convertView.findViewById(R.id.txtAreaNamePickup);
            itemLayout=(LinearLayout) convertView.findViewById(R.id.itemLayout);
            menuCard=(CardView) convertView.findViewById(R.id.menuCard);
        }
    }

    class ViewHolderCategoryRow extends RecyclerView.ViewHolder{

        TextView CategoryName;
        public ViewHolderCategoryRow(View itemView) {
            super(itemView);
            CategoryName=(TextView)itemView.findViewById(R.id.categoryName);
        }
    }

    // clear arraylist variables before used
    void clearData() {
        menuList.clear();
    }


    ProgressDialog pd;

    public void showProgressDialog() {
        showProgressDialog("loading...");
    }

    public void showProgressDialog(String msg) {
        if (pd == null)
            pd = new ProgressDialog(ResturantHome.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();
    }

    public void closeDialog() {
        // do something wih the result
        if (pd != null)
            pd.dismiss();
    }

    public void getDataFromServer(String keyword,String DeliveryType,String ChefId,String BranchID) {
        //
        showProgressDialog();

        String mUrl="";


        if(DeliveryType.equals("Delivery"))
        {
            if(keyword.isEmpty())
                mUrl="http://128.199.173.98/api/get-menu-data-bychefid.php?accesskey=12345&category_id=00&deliverytype=delivery&chefid="+ChefId+"&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else if(keyword.length() > 0)
                mUrl="http://128.199.173.98/api/get-menu-data-bychefid.php?accesskey=12345&keyword="+keyword+"&category_id=00&deliverytype=delivery&chefid="+ChefId+"&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        }
        else if(DeliveryType.equals("Pickup"))
        {

            if(keyword.isEmpty())
                mUrl="http://128.199.173.98/api/get-menu-data-bychefid.php?accesskey=12345&category_id=00&deliverytype=pickup&chefid="+ChefId+"&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else if(keyword.length() > 0)
                mUrl="http://128.199.173.98/api/get-menu-data-bychefid.php?accesskey=12345&keyword="+keyword+"&category_id=00&chefid="+ChefId+"&deliverytype=pickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();

        }
        else
        {
            if(keyword.isEmpty())
                mUrl="http://128.199.173.98/api/get-menu-data-bychefid.php?accesskey=12345&category_id=00&deliverytype=deliverynpickup&chefid="+ChefId+"&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
            else if(keyword.length() > 0)
                mUrl="http://128.199.173.98/api/get-menu-data-bychefid.php?accesskey=12345&keyword="+keyword+"&category_id=00&chefid="+ChefId+"&deliverytype=deliverynpickup&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        }

        System.out.println(mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                closeDialog();

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            }
        });


    }


    // method to parse json data from server
    public void parseJSONData(String strResponse) {

        clearData();

        try {



            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part

            for (int ii = 0; ii < data.length(); ii++) {

                Menu menuItem=new Menu();
                JSONObject object = data.getJSONObject(ii);

                JSONObject menu = object.getJSONObject("MenuActivity");

                Log.e("MenuActivity", menu.toString());

                Log.e("dealPrice"," "+menu.getString("deal_price"));
                if(dealsApply)
                {
                    if(Integer.parseInt(menu.getString("deal_price"))>0)
                    {

                        menuItem.setMenu_ID(Long.parseLong(menu.getString("Menu_ID")));
                        menuItem.setMenu_name(menu.getString("Menu_name"));
                        menuItem.setMenu_Deal_price(Double.valueOf(menu.getString("deal_price")));
                        menuItem.setMenu_delivery(menu.getString("delivery_type"));
                        menuItem.setMenu_delivery_time(menu.getString("delivery_time"));
                        menuItem.setMenu_food_type(menu.getString("type_of_food"));
                        menuItem.setMenu_image(menu.getString("main_image"));
                        if(menu.getString("main_image").equals(""))
                        {
                            menuItem.setMenu_image_avail(false);
                        }
                        else
                        {
                            menuItem.setMenu_image_avail(true);
                        }

                        if(menu.getString("delivery_type").equals("Pickup"))
                        {
                            menuItem.setPickup_image_avail(true);
                        }
                        else
                        {
                            menuItem.setPickup_image_avail(false);
                        }
                        menuItem.setMenu_pickup_distance(menu.getString("distance"));
                        menuItem.setMenu_pickup_location(menu.getString("pickuplocation"));
                        menuItem.setMenu_weigth_grms(menu.getString("weight_in_grams"));
                        menuItem.setMenu_serves(menu.getString("no_of_serves"));
                        menuItem.setMenu_stock_qty(menu.getString("Quantity"));
                        menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                        menuItem.setMenu_pickup_image(menu.getString("pickupImageUrl"));

                    }
                }
                else
                {
                    menuItem.setMenu_ID(Long.parseLong(menu.getString("Menu_ID")));
                    menuItem.setMenu_name(menu.getString("Menu_name"));
                    menuItem.setMenu_Deal_price(Double.valueOf(menu.getString("deal_price")));
                    menuItem.setMenu_delivery(menu.getString("delivery_type"));
                    menuItem.setMenu_delivery_time(menu.getString("delivery_time"));
                    menuItem.setMenu_food_type(menu.getString("type_of_food"));
                    menuItem.setMenu_image(menu.getString("main_image"));
                    menuItem.setMenu_pickup_distance(menu.getString("distance"));
                    if(menu.getString("main_image").equals(""))
                    {
                        menuItem.setMenu_image_avail(false);
                    }
                    else
                    {
                        menuItem.setMenu_image_avail(true);
                    }

                    if(menu.getString("delivery_type").equals("Pickup"))
                    {
                        menuItem.setPickup_image_avail(true);
                    }
                    else
                    {
                        menuItem.setPickup_image_avail(false);
                    }

                    menuItem.setMenu_pickup_location(menu.getString("pickuplocation"));
                    menuItem.setMenu_weigth_grms(menu.getString("weight_in_grams"));
                    menuItem.setMenu_serves(menu.getString("no_of_serves"));
                    menuItem.setMenu_stock_qty(menu.getString("Quantity"));
                    menuItem.setMenu_price(Double.valueOf(formatData.format(menu.getDouble("Price"))));
                    menuItem.setMenu_pickup_image(menu.getString("pickupImageUrl"));

                }
                menuList.add(menuItem);
            }

            dla.notifyDataSetChanged();
            recycler_view.setAdapter(dla);
            recycler_view.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            //imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public double getDataFromDatabase(Context ctx) {

        DecimalFormat formatData = new DecimalFormat("#.##");
        ArrayList<ArrayList<Object>> data;
        ArrayList<Double> Sub_total_price = new ArrayList<Double>();
        double Total_price = 0;
        DBHelper_New dbhelper = new DBHelper_New(ctx);
        //dbhelper.openDataBase();
        data = dbhelper.getAllData();

        // store data to arraylist variables
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Object> row = data.get(i);
            Sub_total_price.add(Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString()))));
            Total_price += Sub_total_price.get(i);
        }

        // count total order

        //Total_price -= (Total_price * (Tax/100));
        Total_price = Double.parseDouble(formatData.format(Total_price));
        dbhelper.close();
        return Total_price;
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent i=getIntent();
        String prochefID=i.getExtras().getString("ProchefID").toString();
        String branchID=i.getExtras().getString("BranchId").toString();
        loadResturentData(prochefID,branchID);
    }

    public void loadResturentData(final String ProchefId, final String BrachId) {
        showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        String mUrl="http://128.199.173.98/api/getProChefInfo.php?prochefid="+ProchefId+"&branchid="+BrachId+"&getResInfo=true";
        Log.i("mUrl",mUrl);
        Request request = new Request.Builder().url(mUrl).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
            }
            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                // Log.i("response",response.body());

                closeDialog();
                if (response.isSuccessful()) {
                    //processSliderImages(response.body().string());
                     runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONOp(response.body().string(),ProchefId,BrachId);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });

    }

    public void parseJSONOp(String strResponse,String ProchefId,String BranchId) {

        try {

            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            for (int ii = 0; ii < 1; ii++) {

                JSONObject object = data.getJSONObject(ii);

                JSONObject menu = object.getJSONObject("restarents");

                Log.e("restarents", menu.toString());


                mResturentLogo=  menu.getString("restaurant_logo");
                mResturentName=  menu.getString("retaurentname");
                mResturentAddress=   menu.getString("branchloc")+" "+menu.getString("address");

                resturantNameTv.setText(mResturentName);
                resturantTagTv.setText(mResturentAddress);
                collapsingToolbar.setTitle(mResturentName);

                ImageLoader iml=new ImageLoader(ResturantHome.this);
                iml.DisplayImage(Constants.ImagesUrl+mResturentLogo,resturantLogoImgView);

                collapsingToolbar.setTitleEnabled(true);
            }

            loadResturentCategories(ProchefId,BranchId);
            //getDataFromServer("","",ProchefId,BranchId);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void loadResturentCategories(final String ProchefId, final String BranchId) {
        showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        String mUrl="http://chefmonster.com/api/getCategoryResturant.php?accesskey=12345&getCatsonly=true";
        Log.i("mUrl",mUrl);
        Request request = new Request.Builder().url(mUrl).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
            }
            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                // Log.i("response",response.body());

                closeDialog();
                if (response.isSuccessful()) {
                    //processSliderImages(response.body().string());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONCats(response.body().string(),ProchefId,BranchId);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });

    }


    public void parseJSONCats(String strResponse,String ProchefId,String BranchId) {

        try {

            Category catItem;
            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            for (int ii = 0; ii < data.length(); ii++) {

                catItem=new Category();
                JSONObject object = data.getJSONObject(ii);

                JSONObject cats = object.getJSONObject("Categories");

                catItem.setCategoryName(cats.getString("Category_name"));

                Log.e("cats", cats.toString());

                categoryList.add(catItem);
            }
            cla.notifyDataSetChanged();
            rv_category.setAdapter(cla);
            rv_category.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            getDataFromServer("","",ProchefId,BranchId);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    String mResturentName,mResturentLogo,mResturentAddress;
}
