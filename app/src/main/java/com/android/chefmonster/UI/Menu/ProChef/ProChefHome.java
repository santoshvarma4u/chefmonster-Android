package com.android.chefmonster.UI.Menu.ProChef;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.InfinateViewpager.InfiniteViewPager;
import com.android.chefmonster.LoadingDrawble.loadingdrawable.LoadingView;
import com.android.chefmonster.Locations.SelectLocationActivity;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.UI.Menu.ProChef.Resturants.Cat_Resturant;
import com.android.chefmonster.UI.Menu.ProChef.Resturants.ResturantHome;
import com.android.chefmonster.UI.Menu.SlidingImageAdapter;
import com.android.chefmonster.UI.QRCodeReader.Qrcodereader;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import com.android.chefmonster.Utills.RoundedCornersTransformation;
import com.circlepageidicator.CirclePageIndicator;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProChefHome extends AppCompatActivity {

    RecyclerView recycler_view;
    public LinearLayoutManager linearLayoutManager;
    LinearLayout optionsLayout;
    private TextView tv_imchef, tv_username, tv_userEmail, tv_btmhomechef,tv_btmprochef,tv_btmcancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pro_chef_home);

        imageLoader = new ImageLoader(ProChefHome.this);

        loading_view=(LoadingView) findViewById(R.id.loading_view);

        recycler_view = (RecyclerView)findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProChefHome.this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.smoothScrollToPosition(0);

        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (linearLayoutManager.findFirstVisibleItemPosition() >= 1) {
                    if(optionsLayout.getVisibility()== View.GONE)
                        optionsLayout.setVisibility(View.VISIBLE);
                } else {
                    optionsLayout.setVisibility(View.GONE);
                }
            }
        });
        linearLayoutManager = (LinearLayoutManager) recycler_view.getLayoutManager();

        //
        optionsLayout = (LinearLayout) findViewById(R.id.linearLayout10);
        categoryList=new ArrayList<>();
        categoryList.add(null);
        pla = new ProChefHomeAdapter();

        View bottomSheet = findViewById( R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        setUpToolbar();
    }
    private BottomSheetBehavior mBottomSheetBehavior;


    private void setUpToolbar() {

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);
        tv_btmhomechef=(TextView) findViewById(R.id.btmhomechef);
        tv_btmprochef=(TextView) findViewById(R.id.btmprochef);
        tv_btmcancel=(TextView) findViewById(R.id.btmcancel);

        TextView City=(TextView) findViewById(R.id.txtLocalCity);
        TextView AddressLocal=(TextView) findViewById(R.id.txtLocalAddress);
        final TextView txtChefSelection=(TextView) findViewById(R.id.txtChefSelection);
        City.setText(ApplicationLoader.getUserCity());
        AddressLocal.setText(ApplicationLoader.getUserLocation());

        City.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProChefHome.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });
        AddressLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProChefHome.this, SelectLocationActivity.class);
                intent.putExtra("firstTime","false");
                startActivity(intent);
                finish();
            }
        });
        txtChefSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        txtChefSelection.setText("Professional Chef");

        tv_btmhomechef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                txtChefSelection.setText("Home Chef");
                startActivity(new Intent(ProChefHome.this,MainActivity.class));
            }
        });
        tv_btmprochef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                txtChefSelection.setText("Professional Chef");

            }
        });
        tv_btmcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });
    }

    public class ProChefHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        final int SLIDER = 0, ROW = 1;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            switch (viewType) {
                case SLIDER:
                    View vSlider = inflater.inflate(R.layout.item_prochef_viewpager, viewGroup, false);
                    viewHolder = new ViewHolderSlider(vSlider);
                    break;

                case ROW:
                    View vRow = inflater.inflate(R.layout.prochef_home_category_item, viewGroup, false);
                    viewHolder = new ViewHolderRow(vRow);
                    break;
                default:
                    break;
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            switch (viewHolder.getItemViewType()) {
                case SLIDER:
                    ViewHolderSlider vh1 = (ViewHolderSlider) viewHolder;
                    configureViewHolderSlider(vh1, position);
                    break;
                case ROW:
                    ViewHolderRow vh2 = (ViewHolderRow) viewHolder;
                    configureViewHolderRow(vh2, position);
                    break;
                default:

                    break;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount(){
            return categoryList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? SLIDER : ROW ;
        }


        public void configureViewHolderSlider(final ProChefHome.ViewHolderSlider viewHolder, int position) {


            viewHolder.pager.setAdapter(new SlidingImageAdapter(ProChefHome.this, sliderImages));
            viewHolder.minipager.setAdapter(new SlidingImageProchefMenuAdapter(ProChefHome.this,imageLoader));

            viewHolder.pager.setCurrentItem(1, true);

            viewHolder.tv_scanqrcode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                        startActivityForResult(new Intent(ProChefHome.this, Qrcodereader.class),1456);
                }
            });
            viewHolder.searchresturents.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(ProChefHome.this, ProChefResturants.class));
                }
            });


        }
        Category mainCategory;
        public void configureViewHolderRow(final ViewHolderRow viewHolder,int position){

           mainCategory=categoryList.get(position);
             int radius =10;
             int margin = 5;
             Transformation transformation = new RoundedCornersTransformation(radius, margin, RoundedCornersTransformation.CornerType.ALL);

            Picasso.with(ProChefHome.this).load(Constants.ImagesUrl + mainCategory.getCategoryImage()).transform(transformation).into(viewHolder.categoryImage);




           // imageLoader.DisplayImage(Constants.ImagesUrl + mainCategory.getCategoryImage(), viewHolder.categoryImage);
            viewHolder.CategoryName.setText(mainCategory.getCategoryName());
            viewHolder.CategoryDescription.setText(mainCategory.getCategoryDiscription());
            viewHolder.expandableLayout1.collapse();
            viewHolder.CategoryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(viewHolder.expandableLayout1.isExpanded())
                    {
                        Log.e("expandes","expanded");
                    }
                    else
                    {
                        Log.e("collapsed","collapsed");
                    }
                    if(!viewHolder.expandableLayout1.isExpanded())
                    {
                        viewHolder.expandableLayout1.setVisibility(View.VISIBLE);
                        viewHolder.expandableLayout1.setDuration(600);
                        viewHolder.expandableLayout1.toggle();
                        viewHolder.mainViewProChef.setBackgroundColor(Color.parseColor("#fdfaebaf"));
                    }
                    else
                    {
                        viewHolder.expandableLayout1.setVisibility(View.GONE);
                        viewHolder.expandableLayout1.setDuration(600);
                        viewHolder.expandableLayout1.collapse();
                        viewHolder.mainViewProChef.setBackgroundColor(Color.parseColor("#ffffffff"));
                    }
                }
            });

            if(mainCategory.getCat_resturants().size() > 0)
            {
                ProChefResturantsAdapter prAdapter=new ProChefResturantsAdapter(mainCategory.getCat_resturants());
                viewHolder.rv_resturants.setAdapter(prAdapter);
            }
            else
            {
                viewHolder.llLayoutRests.setVisibility(View.GONE);
                viewHolder.tv_no_rests.setVisibility(View.VISIBLE);
            }
        }
    }

    public class ProChefResturantsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        List<Cat_Resturant> catRes=new ArrayList<>();
        public ProChefResturantsAdapter(List<Cat_Resturant> catRes) {
            this.catRes=catRes;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

            View vSlider = inflater.inflate(R.layout.prochef_home_resturant_item, viewGroup, false);
            viewHolder = new ViewHolderResturant(vSlider);

            return viewHolder;
        }

        Cat_Resturant catRests;

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            ViewHolderResturant vh2 = (ViewHolderResturant) viewHolder;
            catRests=catRes.get(position);
            Picasso.with(ProChefHome.this).load(Constants.ImagesUrl +catRests.getResturantLogo()).into(vh2.iv_resturant_image);
            //imageLoader.DisplayImage(Constants.ImagesUrl +catRests.getResturantLogo(),vh2.iv_resturant_image);
            vh2.tv_prohome_returentNme.setText(catRests.getResturantName());

            vh2.iv_resturant_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent= new Intent(ProChefHome.this, ResturantHome.class);
                    intent.putExtra("ProchefID",catRests.getChefId());
                    intent.putExtra("BranchId",catRests.getResturantName());

                    startActivity(intent);
                }
            });
        }


        @Override
        public int getItemCount() {
            return catRes.size();
        }

    }

    class ViewHolderResturant extends RecyclerView.ViewHolder{

        ImageView iv_resturant_image;
        TextView tv_prohome_returentNme;

        public ViewHolderResturant(View itemView) {
            super(itemView);
            iv_resturant_image=(ImageView) itemView.findViewById(R.id.iv_resturant_image);
            tv_prohome_returentNme=(TextView) itemView.findViewById(R.id.tv_prohome_returentNme);
        }
    }




    ArrayList<String> sliderImages = new ArrayList<>();
    public ImageLoader imageLoader;
    ProChefHomeAdapter pla;
    class ViewHolderSlider extends RecyclerView.ViewHolder {
        ViewPager pager,minipager;
        InfiniteViewPager mViewPager;
        CirclePageIndicator indicator;
        Button btnDelivery,btnPickup,btnDeals,btnCombos;
        SearchView searchViewPager;
        TextView tv_scanqrcode,chooseresturents,searchresturents;

        public ViewHolderSlider(View itemView) {
            super(itemView);
            pager = (ViewPager) itemView.findViewById(R.id.pager);
            pager.setClipToPadding(false);
            // set padding manually, the more you set the padding the more you see of prev & next page
            pager.setPadding(60, 0, 60, 20);
            // sets a margin b/w individual pages to ensure that there is a gap b/w them
            pager.setPageMargin(20);

            //menu pager

            minipager=(ViewPager) itemView.findViewById(R.id.minipager);
            tv_scanqrcode=(TextView) itemView.findViewById(R.id.tv_scanqrcode);
            chooseresturents=(TextView) itemView.findViewById(R.id.chooseresturents);
            searchresturents=(TextView) itemView.findViewById(R.id.searchresturents);

        }
    }

    class ViewHolderRow extends RecyclerView.ViewHolder{

        ImageView categoryImage;
        TextView CategoryName,CategoryDescription,tv_no_rests;
        ExpandableRelativeLayout expandableLayout1;
        RecyclerView rv_resturants;
        LinearLayout llLayoutRests,mainViewProChef;


        public ViewHolderRow(View itemView) {
            super(itemView);
            expandableLayout1 = (ExpandableRelativeLayout) itemView.findViewById(R.id.expandableLayout1);
            categoryImage=(ImageView) itemView.findViewById(R.id.category_image);
            CategoryName=(TextView) itemView.findViewById(R.id.tv_categoryName);
            CategoryDescription=(TextView) itemView.findViewById(R.id.tv_categroyDescription);
            rv_resturants=(RecyclerView) itemView.findViewById(R.id.rv_resturants);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(ProChefHome.this,4);
            rv_resturants.setLayoutManager(mLayoutManager);
            rv_resturants.setItemAnimator(new DefaultItemAnimator());
            rv_resturants.smoothScrollToPosition(0);
            llLayoutRests=(LinearLayout)itemView.findViewById(R.id.llLayoutRests);
            mainViewProChef=(LinearLayout)itemView.findViewById(R.id.mainViewProChef);
            tv_no_rests=(TextView)itemView.findViewById(R.id.tv_no_rests);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadSliderImages();
    }

    public void loadSliderImages() {/*
        showProgressDialog();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://chefmonster.com/api/getSliders.php").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {

            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                // Log.i("response",response.body());
                //Log.i("response",response.body().string());
                closeDialog();
                if (response.isSuccessful()) {
                    //processSliderImages(response.body().string());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processSliderImages(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
*/
    }

    public void processSliderImages(String response) {
        //{"slides": ["slide.jpg","slide.jpg","slide.jpg"]}
        sliderImages.clear();
        try {
            JSONObject jobj = new JSONObject(response);
            JSONArray jSlides = jobj.getJSONArray("slides");
            for (int i = 0; i < jSlides.length(); i++) {
                sliderImages.add(jSlides.getString(i));
            }
            //
            //slidingImageAdapter = new SlidingImageAdapter(getActivity(), sliderImages, imageLoader);

            //
            recycler_view.setAdapter(pla);
           getDataFromServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getDataFromServer() {/*
        //
        showProgressDialog();

        String mUrl="";
        mUrl="http://128.199.173.98/api/getCategoryResturant.php?accesskey=12345&cheftype=prochef&latitude=" + ApplicationLoader.getUserLat() + "&longitude=" + ApplicationLoader.getUserLon();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call request ,final Response response) throws IOException {
                closeDialog();

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });
*/
    }

    List<Category> categoryList;
    public void parseJSONData(String strResponse) {

        categoryList.clear();
        categoryList.add(null);
        try{


            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data");

            Log.e("MenuActivity", data.toString());


            for (int ii = 0; ii < data.length(); ii++) {
                Category category=new Category();
                JSONObject object = data.getJSONObject(ii);

                JSONObject cats = object.getJSONObject("Categories");
                category.setCategoryId(cats.getString("Category_ID"));
                category.setCategoryName(cats.getString("Category_name"));
                category.setCategoryImage(cats.getString("Category_image"));
                category.setCategoryDiscription(cats.getString("Category_discription"));
                List<Cat_Resturant>  cat_resturantList=new ArrayList<>();
                JSONArray restData=cats.getJSONArray("resturants");
                cat_resturantList.clear();
                for(int jj=0;jj<restData.length();jj++)
                {
                      //  System.out.println(restData.length()+"");
                         Cat_Resturant catrest=new Cat_Resturant();
                         //JSONObject Resturant = object.getJSONObject("Resturant");
                        JSONObject restObject = restData.getJSONObject(jj).getJSONObject("Resturant");
                        catrest.setChefId(restObject.getString("chefid"));
                        catrest.setResturantLogo(restObject.getString("restaurant_logo"));
                        catrest.setLatitude(restObject.getString("latitude"));
                        catrest.setLongitude(restObject.getString("longitude"));
                        catrest.setResturantName(restObject.getString("restaurant_name"));
                        cat_resturantList.add(catrest);

                }
                category.setCat_resturants(cat_resturantList);

                categoryList.add(category);

                Log.e("Cats", cats.toString());
            }

            pla.notifyDataSetChanged();


        }
        catch (JSONException jse)
        {
            jse.printStackTrace();
        }

    }
    LoadingView loading_view;
    public void showProgressDialog() {

        showProgressDialog("loading...");
    }
    ProgressDialog pd;
    public void showProgressDialog(String msg) {
        loading_view.setVisibility(View.VISIBLE);
      /*  if (pd == null)
            pd = new ProgressDialog(ProChefHome.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();*/
    }

    public void closeDialog() {
        // do something wih the result

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loading_view.setVisibility(View.GONE);
            }
        });

    }

    String responseQrCode="0";
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1456&& resultCode==RESULT_OK){

            responseQrCode=data.getExtras().getString("qrcode").toString();

            //validate prochef code to move into resturant
            Toast.makeText(getApplicationContext(),"Resturant Not Found",Toast.LENGTH_LONG).show();

        }else{

        }
    }
}
