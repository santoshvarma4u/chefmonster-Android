package com.android.chefmonster;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.Manifest;
import android.telephony.TelephonyManager;
import android.view.Window;
import android.widget.Toast;

import com.android.chefmonster.AppIntro.IntroActivity;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.Locations.SelectLocationActivity;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.UI.OTP.OTPActivity;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.NoConnection;
import com.github.paolorotolo.appintro.AppIntro;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


     new CountDownTimer(4000,1000) {

            /** This method will be invoked on finishing or expiring the timer */
            @Override
            public void onFinish() {
                /** Creates an intent to start new activity */

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
                    {
                        ActivityCompat.requestPermissions(Splash.this, new String[]{Manifest.permission.CAMERA,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_PHONE_STATE}, 1257);
                    }
                    else
                    {
                        init();
                    }
                }
                else
                {
                    init();
                }

            }

            /** This method will be invoked in every 1000 milli seconds until
             * this timer is expired.Because we specified 1000 as tick time
             * while creating this CountDownTimer
             */
            @Override
            public void onTick(long millisUntilFinished) {

            }
        }.start();

    }


    String result, username;
    String tmDevice;

    public void init()
    {

        //getDBBackUp("com.android.chefmonster", DBHelper_New.DB_NAME, DBHelper_New.DB_NAME);

        if(Constants.isNetworkAvailable(Splash.this))
        {
            if(ApplicationLoader.getUserLat().equals(""))
            {
                //Intent to location
                Intent intent=new Intent(Splash.this, SelectLocationActivity.class);

                if(ApplicationLoader.getUserPhone().equals(""))
                {
                    intent.putExtra("forOtp","true");
                }
                intent.putExtra("firstTime","true");
                startActivity(intent);
                finish();
            }
            else if(ApplicationLoader.getUserPhone().equals(""))
            {
                Intent intent=new Intent(Splash.this, OTPActivity.class);
                intent.putExtra("AvailabilityStatus","Success");
                startActivity(intent);
                finish();
            }

            else {
                //Intent to menu
                startActivity(new Intent(Splash.this, HomePage.class));
                finish();
            }

            final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
            tmDevice = tm.getDeviceId();

            //update fc token to server

            OkHttpClient client = new OkHttpClient();

            if (!ApplicationLoader.getUserName().isEmpty())
                username = ApplicationLoader.getUserName();
            else
                username = "Guest";

            FormBody.Builder formBody = new FormBody.Builder()
                    .add("token", ApplicationLoader.getFCMToken())
                    .add("deviceid", tmDevice)
                    .add("username", username)
                    .add("saveDevice","saveDevice");

            RequestBody formBodyA = formBody.build();
            Request request = new Request.Builder()
                    .url(Constants.SaveDevice)
                    .post(formBodyA)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {

                        result = response.toString();
                        System.out.println("Device Registerd Sucessfully"+result);
                    }
                }
            });



/*
            final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
            tmDevice = tm.getDeviceId();

            //update fc token to server

            OkHttpClient client = new OkHttpClient();

            if (!ApplicationLoader.getUserName().isEmpty())
                username = ApplicationLoader.getUserName();
            else
                username = "Guest";

            RequestBody formBody = new FormEncodingBuilder()
                    .add("token", ApplicationLoader.getFCMToken())
                    .add("deviceid", tmDevice)
                    .add("username", username)
                    .add("saveDevice","saveDevice")
                    .build();

            Request request = new Request.Builder()
                    .url(Constants.SaveDevice)
                    .post(formBody)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call request, IOException e) {

                }

                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {

                        result = response.toString();
                        System.out.println("Device Registerd Sucessfully"+result);
                    }
                }
            });*/

        }
        else
        {
            //No internet error
            startActivityForResult(new Intent(Splash.this,NoConnection.class),2452);
        }

 /*       ApplicationLoader.setDeliveryType("deliverynpickup");
        Intent intent=new Intent(Splash.this, IntroActivity.class);
        startActivity(intent);
        finish();*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 2452:
                init();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    init();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Allow Permissions to get your fav food", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void getDBBackUp(String packageName, String DBName, String outDBName) {

        String fileName = "/data/data/" + packageName.trim() + "/databases/"
                + DBName.trim();
        String outfileName = Environment.getExternalStorageDirectory() + "/" + outDBName.trim()+".db";
        File f = new File(fileName);
        File fo = new File(outfileName);
        FileInputStream fis = null;
        FileOutputStream fos = null;

        try {
            if (!fo.exists())
                fo.createNewFile();

            fis = new FileInputStream(f);
            fos = new FileOutputStream(outfileName);
            while (true) {
                int i = fis.read();
                if (i != -1) {
                    fos.write(i);
                } else {
                    break;
                }
            }
            fos.flush();
            fos.close();
            fis.close();
            // Toast.makeText(this, "NABARD DB saved at :" +
            // outfileName,Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
            // Toast.makeText(this, "DB BackUp ERROR",
            // Toast.LENGTH_LONG).show();
        }
    }

}