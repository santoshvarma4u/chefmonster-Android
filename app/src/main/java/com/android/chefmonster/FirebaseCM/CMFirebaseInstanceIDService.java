package com.android.chefmonster.FirebaseCM;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Manifest;
import com.android.chefmonster.Utills.Constants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okio.BufferedSink;

/**
 * Created by SantoshT on 9/30/2016.
 */

public class CMFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "CMFirebaseIIDService";

    //Context ctx=this.getApplicationContext();
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    String result, username;
    String tmDevice;
    private void sendRegistrationToServer(String token) {

        ApplicationLoader.setFCMToken(token);
        // TODO: Implement this method to send token to your app server.
    /*    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            ActivityCompat.requestPermissions((Activity) getApplicationContext(),new String[]{android.Manifest.permission.READ_PHONE_STATE},1245);
        }
        else
        {
            final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
            tmDevice = tm.getDeviceId();

        }
        */



        /*OkHttpClient client = new OkHttpClient();

        if (!ApplicationLoader.getUserName().isEmpty())
            username = ApplicationLoader.getUserName();
        else
            username = "Guest";


        RequestBody formBody = new FormEncodingBuilder()
                .add("token", token)
                .add("deviceid", tmDevice)
                .add("username", username)
                .build();


        Request request = new Request.Builder()
                .url(Constants.SaveDevice)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (response.isSuccessful()) {

                    result = response.toString();
                    System.out.println("Device Registerd Sucessfully"+result);
                }
            }
        });*/


    }
}
