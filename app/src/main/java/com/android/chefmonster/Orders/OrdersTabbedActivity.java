package com.android.chefmonster.Orders;

import android.content.Intent;
import android.os.StrictMode;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.UI.Login.Login;

public class OrdersTabbedActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_tabbed);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setUpToolbar();

        TextView txtTitle=(TextView)findViewById(R.id.txtTitle);
        txtTitle.setText("My Orders");


        ImageView iv_menu_nav=(ImageView) findViewById(R.id.iv_menu_nav);
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(OrdersTabbedActivity.this,HomePage.class));
                finish();
            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Ongoing Orders"));
        tabLayout.addTab(tabLayout.newTab().setText("Past Orders"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        LinearLayout tabOne = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.tab_ongoing_orders, null);
     /*   tabOne.setText("Delivery Items");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.delivery,0, 0, 0);*/
        tabLayout.getTabAt(0).setCustomView(tabOne);
        LinearLayout tabTwo = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.tab_past_orders, null);
        tabLayout.getTabAt(1).setCustomView(tabTwo);



        viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerOrderAdapter adapter = new PagerOrderAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarr);
        setSupportActionBar(toolbar);
        ImageView iv_menu_nav = (ImageView) findViewById(R.id.iv_menu_nav);
        TextView tv_title = (TextView) findViewById(R.id.txtTitle);
        tv_title.setText("My Orders");
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrdersTabbedActivity.this, HomePage.class));
                finish();
            }
        });
    }



    void selectPage(int pageIndex){
        tabLayout.setScrollPosition(pageIndex,0f,true);
        viewPager.setCurrentItem(pageIndex);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 7451 && resultCode == RESULT_OK) {
            onResume();

        } else {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ApplicationLoader.getUserEmail().isEmpty()) {
            Intent in = new Intent(OrdersTabbedActivity.this, Login.class);
            in.putExtra("FromClass", "Myorders");
            startActivityForResult(in, 7451);
        } else {
            //check whether last order feedback is submitted or not
            
        }
    }
}
