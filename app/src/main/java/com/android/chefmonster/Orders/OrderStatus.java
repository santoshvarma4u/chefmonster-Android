package com.android.chefmonster.Orders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.ActivityCartNew;
import com.android.chefmonster.Checkout.Checkout;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.Utills.Constants;
import com.android.chefmonster.Utills.ImageLoader;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class OrderStatus extends AppCompatActivity {

    TextView txtOrderInfo,txtOrderTime,txtPaymentMethod,txtDeliveryContact;
    ImageView iv_orderImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);
        initUI();
    }
    public void initUI()
    {
        txtOrderInfo=(TextView)findViewById(R.id.txtOrderInfo);
        txtOrderTime=(TextView)findViewById(R.id.txtOrderedOn);
        txtPaymentMethod=(TextView)findViewById(R.id.txtPaymentMode);
        txtDeliveryContact=(TextView)findViewById(R.id.txtDeliveryContact);
        iv_orderImage=(ImageView)findViewById(R.id.iv_orderTracking);

        //load order info

        Intent i=getIntent();
    /*    String orderInfo=i.getStringExtra("Orderinfo").toString();
        String orderDate=i.getStringExtra("OrderDate").toString();*/
        String orderId=i.getStringExtra("OrderID").toString();
        setUpToolbar(orderId);
              /*  txtOrderInfo.setText(orderInfo);
        txtOrderTime.setText("ChefMonster @ "+orderDate);*/

   /*   getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowTitleEnabled(true);
        getActionBar().setTitle("Order Tracking");*/
        getOrderInfo(orderId);
       imageLoader = new ImageLoader(OrderStatus.this);

    }

    private void setUpToolbar(String orderID)
    {
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbarr) ;
        setSupportActionBar(toolbar);
        ImageView iv_menu_nav=(ImageView) findViewById(R.id.iv_menu_nav);
        TextView tv_title=(TextView) findViewById(R.id.txtTitle);
        tv_title.setText("Order("+orderID+")");
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrderStatus.this,OrdersTabbedActivity.class));
                finish();
            }
        });
    }
    public void getOrderInfo(String orderid)
    {
        OkHttpClient client = new OkHttpClient();

        String mUrl= Constants.GetOrders+"?getAllOrders=true&OrderID="+orderid;
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {

            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                if (response.isSuccessful()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }
    public ImageLoader imageLoader;

    public void parseJSONData(String strResponse) {

        try {
            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data");
            for (int ii = 0; ii < 1; ii++) {

                JSONObject object = data.getJSONObject(ii);
                JSONObject order = object.getJSONObject("Order");

                Log.e("Order", order.toString());

                String OrderInformation = "Food: "+order.getString("item_name")+".\n"+"Quantity: "+order.getString("item_quantity")+". \n"+"Status: "+order.getString("order_status");
                txtOrderInfo.setText(OrderInformation);
                txtOrderTime.setText("Chef Monster @ "+order.getString("chefname")+"--"+order.getString("date_time"));
                String ImageURL = "https://maps.googleapis.com/maps/api/staticmap?size=250x250&maptype=roadmap\\&path=color:0xff0000ff|weight:2|"+ ApplicationLoader.getUserLat()+","+ApplicationLoader.getUserLon()+"|"+order.getString("latitude")+","+order.getString("longitude")+"&key=AIzaSyCI_e36oRpU-iHWYt0-1GY77k9GGKI51BY";
                imageLoader.DisplayImage(ImageURL.replace(" ","").toString(), iv_orderImage);
                txtPaymentMethod.setText("Cash on Delivery");
                txtDeliveryContact.setText("admin@chefmonster.com,7894624510");
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
