package com.android.chefmonster.Orders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.Cart.Orders;
import com.android.chefmonster.R;
import com.android.chefmonster.UI.HomePage;
import com.android.chefmonster.UI.Login.Login;
import com.android.chefmonster.UI.MainActivity;
import com.android.chefmonster.Utills.Constants;
import com.ivankocijan.magicviews.views.MagicTextView;import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Myorders extends AppCompatActivity {

    protected TextView tvOngoingOrders;
    protected TextView tvPastOrders;
    protected MagicTextView tvNoOrders;
    protected LinearLayout linearLayout2;
    AdapterMyOrders moa;
    ListView ordersList;
    RecyclerView recycler_view;
    List<Orders> oList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_myorders);
/*

        DBHelper_New db=new DBHelper_New(getApplicationContext());
        ArrayList<Orders> Order=db.getAllOrders();
        moa=new AdapterMyOrders(this,Order);
        ordersList=(ListView)findViewById(R.id.myorderlist);
        ordersList.setAdapter(moa);
        moa.notifyDataSetChanged();
*/

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        recycler_view = (RecyclerView) findViewById(R.id.ordersView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Myorders.this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        ola = new OrdersListAdapter();
        if (ApplicationLoader.getUserEmail().isEmpty()) {
            Intent in = new Intent(Myorders.this, Login.class);
            in.putExtra("FromClass", "Myorders");
            startActivityForResult(in, 7451);
        }

        mContext = Myorders.this;
        setUpToolbar();
        initView();
        oList = new ArrayList<>();
    }

    Context mContext;
    Toolbar mToolbar;

    /*private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null)
        {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("My Orders");


        }
    }*/

    /*  private void setUpToolbar()
      {
          mContext = this;
          mToolbar = (Toolbar) findViewById(R.id.toolbar);
          setSupportActionBar(mToolbar);
          mToolbar.setTitle("Checkout");
          mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
          mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  startActivity(new Intent(Myorders.this, MainActivity.class));
                  finish();
              }
          });
      }*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 7451 && resultCode == RESULT_OK) {
            onResume();

        } else {

        }
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarr);
        setSupportActionBar(toolbar);
        ImageView iv_menu_nav = (ImageView) findViewById(R.id.iv_menu_nav);
        TextView tv_title = (TextView) findViewById(R.id.txtTitle);
        tv_title.setText("My Orders");
        iv_menu_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Myorders.this, HomePage.class));
                finish();
            }
        });
    }

    private void initView() {
        tvOngoingOrders = (TextView) findViewById(R.id.tv_ongoing_orders);
        tvPastOrders = (TextView) findViewById(R.id.tv_past_orders);
        tvOngoingOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvOngoingOrders.getText().toString().equalsIgnoreCase("Ongoing Orders")) {
                    getDataFromServer(ApplicationLoader.getUserEmail(), "NotDispatched");
                    tvOngoingOrders.setText("Ongoing Orders");
                    tvPastOrders.setText("Past Orders");
                } else {
                    getDataFromServer(ApplicationLoader.getUserEmail(), "Dispatched");
                    tvOngoingOrders.setText("Past Orders");
                    tvPastOrders.setText("Ongoing Orders");
                }
            }
        });
        tvPastOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tvPastOrders.getText().toString().equalsIgnoreCase("Past Orders")) {
                    getDataFromServer(ApplicationLoader.getUserEmail(), "Dispatched");
                    tvOngoingOrders.setText("Past Orders");
                    tvPastOrders.setText("Ongoing Orders");
                } else {
                    getDataFromServer(ApplicationLoader.getUserEmail(), "NotDispatched");
                    tvOngoingOrders.setText("Ongoing Orders");
                    tvPastOrders.setText("Past Orders");
                }
            }
        });
        tvNoOrders = (MagicTextView) findViewById(R.id.tvNoOrders);
        linearLayout2 = (LinearLayout) findViewById(R.id.linearLayout2);
    }

    public class OrdersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vRow = inflater.inflate(R.layout.orderitem, parent, false);
            viewHolder = new ViewHolderRow(vRow);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            ViewHolderRow vh2 = (ViewHolderRow) holder;
            configureViewHolderRow(vh2, position);
        }

        @Override
        public int getItemCount() {
            return oList.size();
        }
    }


    public void configureViewHolderRow(final ViewHolderRow holder, int position) {
        // position=position-1;

        Orders morders = new Orders();
        morders = oList.get(position);
        holder.txtOrderID.setText(morders.getOrder_ID());
        holder.txtOrderDate.setText(morders.getOrderDate());
        holder.txtOrderInfo.setText(morders.getOrderInfo());
        holder.orderItemName.setText(morders.getOrderItemName());
        holder.orderItemQty.setText(morders.getOrderItemQty());
        holder.txtOrderStatus.setText("Order Status: " + morders.getOrderStatus());
        holder.orderItemCost.setText(morders.getOrderItemInvCost() + " x " + morders.getOrderItemQty() + " = " + morders.getOrderItemCost());
        holder.txtOrderInfo.setMovementMethod(new ScrollingMovementMethod());
        holder.txtOrderInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Myorders.this, OrderStatus.class);
                intent.putExtra("OrderID", holder.txtOrderID.getText().toString());
                startActivity(intent);
            }
        });
        double subValue = Double.parseDouble(morders.getOrderItemCost());
        double taxValue = (subValue * 14.5 / 100) + subValue;

        holder.totalValue.setText(String.valueOf(ApplicationLoader.round(taxValue, 2)));
        holder.taxValue.setText(String.valueOf(ApplicationLoader.round(taxValue, 2)));
    }

    class ViewHolderRow extends RecyclerView.ViewHolder {
        TextView txtOrderID, txtOrderDate, txtOrderInfo, totalValue, taxValue, txtOrderStatus;
        View statusView;


        TextView orderItemName;
        TextView orderItemQty;
        TextView orderItemCost;

        TextView txtOrderCancel;


        public ViewHolderRow(View convertView) {
            super(convertView);
            txtOrderID = (TextView) convertView.findViewById(R.id.txtMyOrderID);
            txtOrderDate = (TextView) convertView.findViewById(R.id.txtOrderDate);
            txtOrderInfo = (TextView) convertView.findViewById(R.id.expand_text_view);
            statusView = (View) convertView.findViewById(R.id.statusColor);
            orderItemName = (TextView) convertView.findViewById(R.id.orderItemName);
            orderItemQty = (TextView) convertView.findViewById(R.id.orderItemQty);
            orderItemCost = (TextView) convertView.findViewById(R.id.orderItemCost);
            totalValue = (TextView) convertView.findViewById(R.id.totalValue);
            taxValue = (TextView) convertView.findViewById(R.id.taxValue);
            txtOrderCancel = (TextView) convertView.findViewById(R.id.cancel_order_btn);
            txtOrderStatus = (TextView) convertView.findViewById(R.id.order_status);
            txtOrderCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(Myorders.this, "Sorry! We are unable to cancel this order", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!ApplicationLoader.getUserEmail().isEmpty()) {
            getDataFromServer(ApplicationLoader.getUserEmail(), "NotDispatched");
        } else {
            Intent in = new Intent(Myorders.this, Login.class);
            in.putExtra("FromClass", "Myorders");
            startActivityForResult(in, 7451);
        }
    }

    ProgressDialog pd;

    public void getDataFromServer(String email, String status) {

        showProgressDialog("Getting your orders....");
        String mUrl = Constants.GetOrders + "?getAllOrders=true&custemail=" + email + "&status=" + status;
        Log.d("murl", mUrl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    closeDialog();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                parseJSONData(response.body().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }


            }
        });
    }

    OrdersListAdapter ola;

    int mOngoingLength = 0;

    public void parseJSONData(String strResponse) {

        // clearData();
        oList.clear();
        try {
            // parse json data and store into arraylist variables
            JSONObject json = new JSONObject(strResponse);
            JSONArray data = json.getJSONArray("data"); // this is the "items: [ ] part
            System.out.println(data.length() + "***************");
            if (data.length() <= 0) {
                if (mOngoingLength == 0) {
                    mOngoingLength = 1;
                    getDataFromServer(ApplicationLoader.getUserEmail(), "Dispatched");
                    tvOngoingOrders.setText("Past Orders");
                    tvPastOrders.setText("Ongoing Orders");
                    tvPastOrders.setVisibility(View.GONE);
                } else {
                    tvNoOrders.setVisibility(View.VISIBLE);
                }

            } else {
                for (int ii = 0; ii < data.length(); ii++) {
                    //int i = 0;
                    JSONObject object = data.getJSONObject(ii);
                    JSONObject order = object.getJSONObject("Order");
                    Log.e("Order", order.toString());
                    Orders mOrder = new Orders();
                    mOrder.setOrder_ID(order.getString("orderid"));
                    mOrder.setOrderDate(order.getString("date_time"));
                    String OrderInformation = "";
                    OrderInformation += "Food: " + order.getString("item_name") + ".\n" + "Quantity: " + order.getString("item_quantity") + ". \n" + "Status: " + order.getString("order_status") + "\n Click to track food live";
                    mOrder.setOrderInfo(OrderInformation);
                    mOrder.setOrderItemName(order.getString("item_name"));
                    mOrder.setOrderItemQty(order.getString("item_quantity"));
                    double ItemCost = Double.parseDouble(order.getString("item_price")) * Double.parseDouble(order.getString("item_quantity"));
                    mOrder.setOrderItemCost(String.valueOf(ItemCost));
                    mOrder.setOrderStatus(order.getString("order_status"));
                    mOrder.setOrderItemInvCost(order.getString("item_price"));
                    oList.add(mOrder);
                }
            }


            recycler_view.setAdapter(ola);

            ola.notifyDataSetChanged();
            recycler_view.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void showProgressDialog(String msg) {

        if (pd == null)
            pd = new ProgressDialog(Myorders.this);
        pd.setMessage("loading...");
        pd.setCancelable(false);
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
        pd.show();

    }

    public void closeDialog() {
        // do something wih the result
        if (pd != null)
            pd.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(Myorders.this,HomePage.class);
        startActivity(intent);
        finish();
    }

}
