package com.android.chefmonster.Orders;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.android.chefmonster.Cart.DeliveryCart;
import com.android.chefmonster.Cart.PickupCart;

/**
 * Created by SantoshT on 6/6/2017.
 */

public class PagerOrderAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PagerOrderAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                OngoingOrdersFragment tab1 = new OngoingOrdersFragment();
                return tab1;
            case 1:
                PastOrdersFragment tab2 = new PastOrdersFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
