package com.android.chefmonster.Payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.chefmonster.ApplicationLoader;
import com.android.chefmonster.ChefApp.WebAppInterface;
import com.android.chefmonster.Database.DBHelper_New;
import com.android.chefmonster.Orders.OrdersTabbedActivity;
import com.android.chefmonster.R;
import com.ivankocijan.magicviews.views.MagicTextView;

public class PaymentRequest extends AppCompatActivity implements View.OnClickListener {

    protected ImageView ivMenuNav;
    protected MagicTextView txtTitle;
    protected Toolbar toolbar;
    protected WebView paymentwebkit;
    static DBHelper_New dbhelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_payment_request);
        initView();

        dbhelper = new DBHelper_New(getApplicationContext());

        //Enable Javascript
        paymentwebkit.getSettings().setJavaScriptEnabled(true);
        paymentwebkit.getSettings().setUseWideViewPort(true);
        paymentwebkit.getSettings().setLoadWithOverviewMode(true);
        paymentwebkit.setWebViewClient(new WebViewClient());

       // paymentwebkit.addJavascriptInterface(new WebAppInterface(this), "Android");
        paymentwebkit.addJavascriptInterface(this,"Android");

        String email= ApplicationLoader.getUserEmail();
        String txn_id=getIntent().getExtras().getString("txnid");
        String name=ApplicationLoader.getUserName();
        String phone=ApplicationLoader.getUserPhone();
        String amount=getIntent().getExtras().getString("totalamount");

        String paymentUrl="http://anyemi.com/chefmonster/services/pay1.php?txn_id="+txn_id+"&amount="+amount+"&name="+name+"&mail="+email+"&phone="+phone+"&description=ChefMonster&rurl=http://128.199.173.98/api/paymentResponse.php";
        System.out.println(paymentUrl);
        paymentwebkit.loadUrl(paymentUrl);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_menu_nav) {

        }
    }

    @JavascriptInterface
    public void paymentSuccess()
    {
        Toast.makeText(PaymentRequest.this, "Payment Success, Confirming Order", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
        intent.putExtra("status","success");
        setResult(RESULT_OK, intent);
        finish();

    }

 @JavascriptInterface
    public void paymentFail()
    {
        Toast.makeText(PaymentRequest.this, "Payment Failed, Try Again", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
        intent.putExtra("status","fail");
        setResult(RESULT_OK, intent);
        finish();

    }

    private void initView() {
        ivMenuNav = (ImageView) findViewById(R.id.iv_menu_nav);
        ivMenuNav.setOnClickListener(PaymentRequest.this);
        txtTitle = (MagicTextView) findViewById(R.id.txtTitle);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        paymentwebkit = (WebView) findViewById(R.id.paymentwebkit);
    }
}
