package com.android.chefmonster.Payment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.chefmonster.ChefApp.WebAppInterface;
import com.android.chefmonster.R;
import com.ivankocijan.magicviews.views.MagicTextView;

public class PaymentResponse extends AppCompatActivity implements View.OnClickListener {

    protected ImageView ivMenuNav;
    protected MagicTextView txtTitle;
    protected Toolbar toolbar;
    protected WebView paymentwebkit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_payment_response);
        initView();

        paymentwebkit.getSettings().setJavaScriptEnabled(true);
        //Inject WebAppInterface methods into Web page by having Interface name 'Android'
        paymentwebkit.addJavascriptInterface(new WebAppInterface(this),"Android");
        //Load URL inside WebView
        paymentwebkit.loadUrl("http://128.199.173.98/api/paymentResponse.php");
    }

    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /**
         * Show Toast Message
         * @param toast
         */
        @JavascriptInterface
        public void showToast(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        }

        /**
         * Show Dialog
         * @param dialogMsg
         */
        @JavascriptInterface
        public void showDialog(String dialogMsg){
            AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();

            // Setting Dialog Title
            alertDialog.setTitle("JS triggered Dialog");

            // Setting Dialog Message
            alertDialog.setMessage(dialogMsg);

            // Setting alert dialog icon
            //alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

            // Setting OK Button
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(mContext, "Dialog dismissed!", Toast.LENGTH_SHORT).show();
                }
            });

            // Showing Alert Message
            alertDialog.show();
        }

        /**
         * Intent - Move to next screen
         */
        @JavascriptInterface
        public void moveToNextScreen(){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
            // Setting Dialog Title
            alertDialog.setTitle("Alert");
            // Setting Dialog Message
            alertDialog.setMessage("Are you sure you want to leave to next screen?");
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //Move to Next screen
                        }
                    });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Cancel Dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Message
            alertDialog.show();
        }

        @JavascriptInterface
        public void paymentSucess()
        {
            Toast.makeText(PaymentResponse.this, "Payment Success, Confirming Order", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_menu_nav) {

        }
    }

    private void initView() {
        ivMenuNav = (ImageView) findViewById(R.id.iv_menu_nav);
        ivMenuNav.setOnClickListener(PaymentResponse.this);
        txtTitle = (MagicTextView) findViewById(R.id.txtTitle);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        paymentwebkit = (WebView) findViewById(R.id.paymentwebkit);
    }
}
