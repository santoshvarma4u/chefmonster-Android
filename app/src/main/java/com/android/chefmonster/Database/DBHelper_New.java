package com.android.chefmonster.Database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.chefmonster.Orders.Orders;
import com.android.chefmonster.Utills.Constants;

public class DBHelper_New extends SQLiteOpenHelper{

	String DB_PATH;
	public final static String DB_NAME = "db_order";
	public final static int DB_VERSION = 2;
	public static SQLiteDatabase db;

	public final Context context;

	public final String TABLE_NAME = "tbl_order";
	public final String TABLE_ADDR="tbl_addr";
	public final String TABLE_ORDR="tbl_myorder";
	public final String TABLE_RECENTSEARCH="tbl_search";
	public final String TABLE_CHECKOUT_ADDR="tbl_checkout_addr";





	public static final String LOCATION_TABLE = "location";

	public final String ID = "id";
	public final String MENU_NAME = "Menu_name";
	public final String QUANTITY = "Quantity";
	public final String TOTAL_PRICE = "Total_price";
	public final String ORDER_FROM="order_from";
	public final String DELIVERYTYPE="delivery_type";
	public final String CHEF_TYPE="chef_type";

	public final String MENU_IMAGE="menu_image";


	public final String NAME="name";
	public final String ADDR="address";
	public final String CITY="city";
	public final String SHOPCODE="code";
	public final String PHONE="phone";
	public final String EMAIL="email";
	public final String ADDRTYPE="addr_type";
	public final String LATITUDE="latitude";
	public final String LONGITUDE="longitude";
	public final String PROFILEPIC="profilepic";
	public final String BUILDING="building";


	public final String ORDERID="orderid";
	public final String ORDERDATE="order_date";
	public final String ORDERINFO="info_order";
	public final String ORDERPRICE="order_price";
	public final String ORDERTYPE="order_type";
	public final String ORDERSTATUS="order_status";
	public final String ORDERMODE="order_mode";
	public final String ORDERFEEDBACK="order_feedback";


	public final String LANDMARK="landmark";


	public final String MENUTYPE="menutype";

	public static final String ID_COLUMN = "id";
	public static final String NAME_LOCATION_COLUMN = "name";
	public static final String LATITUDE_COLOMN = "decription";
	public static final String LONGITUDE_COLOMN = "image";

	public static final String MENU_STOCK_QTY="menustockqty";



	public static final String FOOD_NAME="foodname";


	public static final String CREATE_USER_LOCATION_TABLE = "CREATE TABLE "
			+ LOCATION_TABLE + "(" + ID_COLUMN + " INTEGER PRIMARY KEY, "
			+ NAME_LOCATION_COLUMN + " TEXT, " + LATITUDE_COLOMN + " TEXT, "
			+ LONGITUDE_COLOMN + " TEXT )";


	public DBHelper_New(Context context) {

		super(context, DB_NAME, null, DB_VERSION);
		this.context = context;
	}


	@Override
	public void close() {
	/*	db=this.getWritableDatabase();
		db.close();*/
	}

	@Override
	public void onCreate(SQLiteDatabase db) {


		String CREATE_TABLE_NAME="CREATE TABLE tbl_order (id INTEGER NOT NULL , Menu_name VARCHAR, Quantity INTEGER, Total_price NUMERIC,"+DELIVERYTYPE+" VARCHAR, "+ORDER_FROM+" VARCHAR, "+CHEF_TYPE+" VARCHAR, "+MENU_IMAGE+" VARCHAR, "+MENUTYPE+" VARCHAR, "+MENU_STOCK_QTY+" VARCHAR )";
		db.execSQL(CREATE_TABLE_NAME);

		String CREATE_TABLE_USERS = "CREATE TABLE "
				+ TABLE_ADDR  + "(" + ID + " INTEGER PRIMARY KEY, "
				+ NAME + " TEXT, " + ADDR + " TEXT," + CITY + " TEXT ," + SHOPCODE + " TEXT , "
				+ PHONE + " TEXT, " + EMAIL + " TEXT, "+ LATITUDE+" TEXT,"+LONGITUDE+" TEXT ,"+PROFILEPIC+" TEXT)";
		String CREATE_TABLE_CHECKOUT_ADDR = "CREATE TABLE "
				+ TABLE_CHECKOUT_ADDR  + "(" + ID + " INTEGER PRIMARY KEY, "
				+ LANDMARK + " TEXT, "+BUILDING+" TEXT)";

		String CREATE_TABLE_MYORDER="CREATE TABLE "
				+ TABLE_ORDR + "(" + ORDERID + " INTEGER PRIMARY KEY, "
				+ ORDERDATE + " TEXT, " + ORDERINFO + " TEXT," + ORDERPRICE + " TEXT, "+ORDERTYPE+" TEXT, "+ORDERSTATUS+" TEXT, "+ORDERMODE+" TEXT, "+ ORDERFEEDBACK +" TEXT)";
		String CREATE_TABLE_RECENT="CREATE TABLE "+TABLE_RECENTSEARCH+" ("+ID+" INTEGER PRIMARY KEY, "+FOOD_NAME+" TEXT)";
		db.execSQL(CREATE_TABLE_RECENT);
		db.execSQL(CREATE_TABLE_MYORDER);
		db.execSQL(CREATE_TABLE_USERS);

		db.execSQL(CREATE_TABLE_CHECKOUT_ADDR);
		db.execSQL(CREATE_USER_LOCATION_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDR);
		String CREATE_TABLE_USERS = "CREATE TABLE "
				+ TABLE_ADDR  + "(" + ID + " INTEGER PRIMARY KEY, "
				+ NAME + " TEXT, " + ADDR + " TEXT," + CITY + " TEXT ," + SHOPCODE + " TEXT , "
				+ PHONE + " TEXT, " + EMAIL + " TEXT, "+ LATITUDE+" TEXT,"+LONGITUDE+" TEXT,"+PROFILEPIC+" TEXT)";
		db.execSQL(CREATE_TABLE_USERS);
		db.execSQL("DROP TABLE IF EXISTS " + LOCATION_TABLE);
		db.execSQL(CREATE_USER_LOCATION_TABLE);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDR);
		String CREATE_TABLE_MYORDER="CREATE TABLE "
				+ TABLE_ORDR + "(" + ORDERID + " INTEGER PRIMARY KEY, "
				+ ORDERDATE + " TEXT, " + ORDERINFO + " TEXT," + ORDERPRICE + " TEXT)";
		db.execSQL(CREATE_TABLE_MYORDER);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		String CREATE_TABLE_NAME="CREATE TABLE tbl_order (id INTEGER NOT NULL , Menu_name VARCHAR, Quantity INTEGER, Total_price NUMERIC,"+DELIVERYTYPE+" VARCHAR, "+ORDER_FROM+" VARCHAR, "+CHEF_TYPE+" VARCHAR, "+MENU_IMAGE+" VARCHAR, "+MENUTYPE+" VARCHAR , "+MENU_STOCK_QTY+" VARCHAR)";
		db.execSQL(CREATE_TABLE_NAME);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENTSEARCH);
		String CREATE_TABLE_RECENT="CREATE TABLE "+TABLE_RECENTSEARCH+" ("+ID+" INTEGER PRIMARY KEY, "+FOOD_NAME+" TEXT)";
		db.execSQL(CREATE_TABLE_RECENT);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHECKOUT_ADDR);
		String CREATE_TABLE_CHECKOUT_ADDR = "CREATE TABLE "
				+ TABLE_CHECKOUT_ADDR  + "(" + ID + " INTEGER PRIMARY KEY, "
				+ LANDMARK + " TEXT , "+BUILDING+" TEXT)";
		db.execSQL(CREATE_TABLE_CHECKOUT_ADDR);

	}

	/** this code is used to get all data from database */
	public ArrayList<ArrayList<Object>> getAllData(){
		ArrayList<ArrayList<Object>> dataArrays = new ArrayList<ArrayList<Object>>();

		Cursor cursor = null;
		db=this.getWritableDatabase();
		try{
			cursor = db.query(
					TABLE_NAME,
					new String[]{ID, MENU_NAME, QUANTITY, TOTAL_PRICE,DELIVERYTYPE,ORDER_FROM,MENU_IMAGE,MENUTYPE,MENU_STOCK_QTY},
					null,null, null, null, null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					ArrayList<Object> dataList = new ArrayList<Object>();

					dataList.add(cursor.getLong(0));
					dataList.add(cursor.getString(1));
					dataList.add(cursor.getString(2));
					dataList.add(cursor.getString(3));
					dataList.add(cursor.getString(4));
					dataList.add(cursor.getString(5));
					dataList.add(cursor.getString(6));
					dataList.add(cursor.getString(7));
					dataList.add(cursor.getString(8));
					dataArrays.add(dataList);
				}

				while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}

		return dataArrays;
	}
	public String getItemsCount()
	{
		int totalCount=0;

		String selectQuery = "SELECT SUM("+QUANTITY+") FROM " + TABLE_NAME ;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				System.out.println("********************"+cursor.getInt(0));
				totalCount=cursor.getInt(0);
			} while (cursor.moveToNext());
		}
		return String.valueOf(totalCount);
	}

	/** this code is used to get all data from database */
	public boolean isDataExist(long id,String from,String del_type,String menu_type){
		boolean exist = false;
	//Cursor cursor = null;

		db=this.getWritableDatabase();
		try{


			int Count=0;
			String selectQuery = "SELECT Count(*) FROM " + TABLE_NAME +" WHERE "+ID +"="+id+" and "+ORDER_FROM+"='"+from+"' and "+DELIVERYTYPE+"='"+del_type+"' and "+MENUTYPE+"='"+menu_type+"'" ;
			//System.out.println("***********"+selectQuery);
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					Count=Integer.parseInt(cursor.getString(0));
				} while (cursor.moveToNext());
			}
			//return Count;


			/*cursor = db.query(
					TABLE_NAME,
					new String[]{ID},
					ID +"="+id+" and "+ORDER_FROM+"="+from+" and "+DELIVERYTYPE+"="+del_type,
					null, null, null, null);*/
			if(Count > 0){
				exist = true;
			}

			cursor.close();
		}catch (SQLException e){
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}

		return exist;
	}

	/** this code is used to get all data from database */
	public boolean isPreviousDataExist(){
		boolean exist = false;





		Cursor cursor = null;

		try{
			cursor = db.query(
					TABLE_NAME,
					new String[]{ID},
					null,null, null, null, null);
			if(cursor.getCount() > 0){
				exist = true;
			}

			cursor.close();
		}catch (SQLException e){
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}

		return exist;
	}

	public void addData(long id, String menu_name, int quantity, double total_price,String from,String chef_type,String del_type,String image,String menutype,String menuStockQty){
		// this is a key value pair holder used by android's SQLite functions
		ContentValues values = new ContentValues();
		values.put(ID, id);
		values.put(MENU_NAME, menu_name);
		values.put(QUANTITY, quantity);
		values.put(TOTAL_PRICE, total_price);
		values.put(ORDER_FROM,from);
		values.put(CHEF_TYPE,chef_type);
		values.put(DELIVERYTYPE,del_type);
		values.put(MENU_IMAGE,image);
		values.put(MENUTYPE,menutype);
		values.put(MENU_STOCK_QTY,menuStockQty);

		// ask the database object to insert the new data
		try{db.insert(TABLE_NAME, null, values);}
		catch(Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}

	public void addMyorders(String orderDate,String orderInfo,String orderPrice,String ordertype,String orderStatus)
	{
		ContentValues values = new ContentValues();
		values.put(ORDERDATE,orderDate);
		values.put(ORDERINFO,orderInfo);
		values.put(ORDERPRICE,orderPrice);
		values.put(ORDERTYPE,ordertype);
		values.put(ORDERSTATUS,orderStatus);

		try{db.insert(TABLE_ORDR, null, values);}
		catch(Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}

	public ArrayList<String> getMyorders(){
		ArrayList<String> dataArrays = new ArrayList<String>();

		Cursor cursor = null;

		try{


			cursor = db.query(
					TABLE_ORDR,
					new String[]{ID, ORDERDATE, ORDERINFO, ORDERPRICE},
					null,null, null, null, null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{

					dataArrays.add(String.valueOf(cursor.getLong(0)));
					dataArrays.add(cursor.getString(1));
					dataArrays.add(cursor.getString(2));
					dataArrays.add(cursor.getString(3));

				}

				while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}

		return dataArrays;
	}

	public ArrayList<Orders>  getAllOrders(){

		ArrayList<Orders> data = new ArrayList<Orders>();


		// Select All Query

		String selectQuery = "SELECT " + ORDERID+","+ ORDERDATE+","+ ORDERINFO+","+ORDERPRICE+" FROM " + TABLE_ORDR +" ORDER BY orderid DESC";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				data.add(new Orders(cursor.getString(0),cursor.getString(1),cursor.getString(2)));
				Log.d("Orders",cursor.getString(0)+","+cursor.getString(1)+","+cursor.getString(2));
			} while (cursor.moveToNext());
		}
		cursor.close();

		return data;
	}
/*
	public ArrayList<AddressPojo> getAllAddress()
	{
		ArrayList<AddressPojo> addrData=new ArrayList<AddressPojo>();
		String select_num="SELECT "+NAME+","+ADDR+","+CITY+","+SHOPCODE+","+PHONE+","+EMAIL+","+LATITUDE+","+LONGITUDE+" FROM "+ TABLE_ADDR  ;
		SQLiteDatabase db=this.getReadableDatabase();
		Cursor cursor=db.rawQuery(select_num,null);
		if(cursor.moveToFirst())
		{
			do
			{
				AddressPojo ap=new AddressPojo();
				ap.setTxtAddress(cursor.getString(1));
				ap.setTxtBNFN(cursor.getString(2));
				ap.setTxtPhone(cursor.getString(4));
				ap.setTxtLat(cursor.getString(6));
				ap.setTxtLon(cursor.getString(7));

				addrData.add(ap);
			}while (cursor.moveToNext());
		}

		return addrData;
	}*/

	public void addAddr(String name,String address,String city,String shopcode,String phone,String email,String latitude,String longitude,String profilepic)
	{
		ContentValues values = new ContentValues();
		values.put(NAME,name);
		values.put(ADDR,address);
		values.put(CITY,city);
		values.put(SHOPCODE,shopcode);
		values.put(PHONE,phone);
		values.put(EMAIL,email);
		values.put(LATITUDE,latitude);
		values.put(LONGITUDE,longitude);
		values.put(PROFILEPIC,profilepic);
		try{db.insert(TABLE_ADDR, null, values);}
		catch(Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}
	public void addCheckoutAddr(String landmark,String building)
	{
		if(addrCheckoutCount(landmark,building)<=0)
		{
			ContentValues values = new ContentValues();
			values.put(LANDMARK,landmark);
			values.put(BUILDING,building);
			try{db.insert(TABLE_CHECKOUT_ADDR, null, values);}
			catch(Exception e)
			{
				Log.e("DB ERROR", e.toString());
				e.printStackTrace();
			}
		}
	}

	public int addrCheckoutCount(String landmark,String building)
	{
		int Count=0;
		String selectQuery = "SELECT Count(*) FROM " + TABLE_CHECKOUT_ADDR +" WHERE "+LANDMARK+" = '"+landmark+"' AND "+BUILDING
				+" = '"+building+"'";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Count=Integer.parseInt(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		return Count;
	}

	public ArrayList<String> getCheckoutLandmark(){
		ArrayList<String> dataArrays = new ArrayList<String>();

		Cursor cursor = null;

		try{

			cursor = db.query(
					TABLE_CHECKOUT_ADDR,
					new String[]{LANDMARK},
					null,null, null, null, null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					dataArrays.add(cursor.getString(0));
				}

				while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}

		return dataArrays;
	}

	public ArrayList<String> getCheckoutBuilding(){
		ArrayList<String> dataArrays = new ArrayList<String>();

		Cursor cursor = null;

		try{

			cursor = db.query(
					TABLE_CHECKOUT_ADDR,
					new String[]{BUILDING},
					null,null, null, null, null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					dataArrays.add(cursor.getString(0));
				}

				while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}

		return dataArrays;
	}




	public int addrCount()
	{
		int Count=0;
		String selectQuery = "SELECT Count(*) FROM " + TABLE_ADDR ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Count=Integer.parseInt(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		return Count;
	}

	public String getAddress()
	{
		String data="";
		String select_num="SELECT "+NAME+","+ADDR+","+CITY+","+SHOPCODE+","+PHONE+","+EMAIL+" FROM "+ TABLE_ADDR  ;
		SQLiteDatabase db=this.getReadableDatabase();
		Cursor cursor=db.rawQuery(select_num,null);
		if(cursor.moveToFirst())
		{
			do
			{
				data =cursor.getString(0)+","+cursor.getString(1)+","+cursor.getString(2)+","+cursor.getString(3)+","+cursor.getString(4)+","+cursor.getString(5);

			}while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return data;

	}

	public void deleteData(long id){
		// ask the database manager to delete the row of given id
		try {db.delete(TABLE_NAME, ID + "=" + id, null);}
		catch (Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}

	public void deleteAllData(){
		// ask the database manager to delete the row of given id
	//	SQLiteDatabase db=getWritableDatabase();

		try {db.delete(TABLE_NAME, null, null);}
		catch (Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}

	public void deleteDataByQuery(String tabName,String query){
		// ask the database manager to delete the row of given id
		try {db.delete(tabName, query, null);}
		catch (Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}



	public void updateData(long id, int quantity, double total_price,String from,String chef_type,String del_type,String menu_type,String menuStockQty){
		// this is a key value pair holder used by android's SQLite functions
		ContentValues values = new ContentValues();
		values.put(QUANTITY, quantity);
		values.put(TOTAL_PRICE, total_price);
		values.put(ORDER_FROM,from);
		values.put(CHEF_TYPE,chef_type);
		values.put(DELIVERYTYPE,del_type);
		values.put(MENUTYPE,menu_type);
		values.put(MENU_STOCK_QTY,menuStockQty);

		// ask the database object to update the database row of given rowID
		try {db.update(TABLE_NAME, values, ID + "=" + id+ " and "+ORDER_FROM+" = '"+from+"' and "+DELIVERYTYPE+" = '"+del_type+"' and "+MENUTYPE+" = '"+menu_type+"'", null);}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}
	}

	public String getMenuQty(String menuId,String typeOfDelivery){

		SQLiteDatabase db=this.getWritableDatabase();
	    String selectQuery = "SELECT "+QUANTITY+" FROM " + TABLE_NAME+" WHERE "+ID+" = "+menuId+" AND "+DELIVERYTYPE+" = '"+typeOfDelivery+"'";

		Cursor cursor = db.rawQuery(selectQuery, null);
		String qty="";
		if (cursor.moveToFirst()) {
			do {
				qty=cursor.getString(0).toString();
			} while (cursor.moveToNext());
		}
		else
		{
			qty="0";
		}
		return qty;
	}


	public String DelMenuQty(String menuId){

		SQLiteDatabase db=this.getWritableDatabase();
		String selectQuery = "SELECT "+QUANTITY+" FROM " + TABLE_NAME+" WHERE "+ID+" = "+menuId;

		Cursor cursor = db.rawQuery(selectQuery, null);
		String qty="";
		if (cursor.moveToFirst()) {
			do {
				qty=cursor.getString(0).toString();
			} while (cursor.moveToNext());
		}
		return qty;
	}


	public void insertLocation(String Name,String Latitude,String Longitude)
	{
		SQLiteDatabase db=this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(NAME_LOCATION_COLUMN,Name);
		values.put(LATITUDE_COLOMN, Latitude);
		values.put(LONGITUDE_COLOMN,Longitude);
		db.insert(DBHelper_New.LOCATION_TABLE, null, values);
	}
	public long updateLocation(String Name,String Latitude,String Longitude)
	{
		String WHERE_ID_EQUALS = DBHelper_New.ID_COLUMN
				+ " !=?";
		SQLiteDatabase db=this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(NAME_LOCATION_COLUMN,Name);
		values.put(LATITUDE_COLOMN, Latitude);
		values.put(LONGITUDE_COLOMN,Longitude);
		long result = db.update(DBHelper_New.LOCATION_TABLE, values,
				WHERE_ID_EQUALS,
				null);
		return result;
	}
	public String getLocationName()
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String selectQuery = "SELECT "+NAME_LOCATION_COLUMN+" FROM " + LOCATION_TABLE;

		Cursor cursor = db.rawQuery(selectQuery, null);
		String Location="";
		if (cursor.moveToFirst()) {
			do {
				Location=cursor.getString(0).toString();
			} while (cursor.moveToNext());
		}
		return Location;
	}
	public String getLocationLatitude()
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String selectQuery = "SELECT "+LATITUDE_COLOMN+" FROM " + LOCATION_TABLE;

		Cursor cursor = db.rawQuery(selectQuery, null);
		String Location="";
		if (cursor.moveToFirst()) {
			do {
				Location=cursor.getString(0).toString();
			} while (cursor.moveToNext());
		}
		return Location;
	}

	public String getLocationLongitude()
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String selectQuery = "SELECT "+LONGITUDE_COLOMN+" FROM " + LOCATION_TABLE;

		Cursor cursor = db.rawQuery(selectQuery, null);
		String Location="";
		if (cursor.moveToFirst()) {
			do {
				Location=cursor.getString(0).toString();
			} while (cursor.moveToNext());
		}
		return Location;
	}
	public void deleteLocations()
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String sql="DELETE FROM " + LOCATION_TABLE ;
		db.execSQL(sql);
	}
	// checks the value already there int he table
	public boolean checkAlreadyExists(Context context, String tablename,
									  String columnname, String columnvalue) {
		boolean value = false;
		String countQuery = "SELECT  * FROM " + tablename + " WHERE "
				+ columnname + "='" + columnvalue + "'";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		if (count > 0)
			value = true;
		cursor.close();
		db.close();
		// return count
		return value;
	}

	public static int getCount(Context context, final String tableName) {
		DBHelper_New dbhelper = new DBHelper_New(context);
		SQLiteDatabase db = dbhelper.getReadableDatabase();
		int cnt = 0;
		Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + tableName, null);
		cursor.moveToFirst();
		cnt = Integer.parseInt(cursor.getString(0));
		cursor.close();
		db.close();
		return cnt;
	}

	public List<String[]> getTableDataByValue(Context context,
											  String TableName, String columname, String columnValue) {
		DBHelper_New dbhelper = new DBHelper_New(context);
		SQLiteDatabase db = dbhelper.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + TableName + " WHERE "
				+ columname + "='" + columnValue + "'", null);
		List<String[]> list = new ArrayList<String[]>();
		// System.out.println("cur:" + cur.getCount());
		if (cur.getCount() > 0)
			list = cursortoListArr(cur);
		cur.close();
		db.close();
		dbhelper.close();

		return list;
	}

	public List<String[]> cursortoListArr(Cursor c) {
		List<String[]> rowList = new ArrayList<String[]>();
		while (c.moveToNext()) {
			String[] arr = new String[c.getColumnCount()];
			for (int i = 0; i < c.getColumnCount(); i++) {
				arr[i] = c.getString(i);
			}
			rowList.add(arr);
		}
		c.close();
		return rowList;
	}

	public static boolean updateRowData(Context context,
										final String tablename, String[] columnNames,
										String[] columnValues, String[] whereColumn, String[] whereValue) {
		System.out.println("In Update...");
		DBHelper_New dbhelper = new DBHelper_New(context);
		SQLiteDatabase db = dbhelper.getReadableDatabase();
		ContentValues cv = new ContentValues();
		for (int i = 0; i < columnNames.length; i++) {
			cv.put(columnNames[i], columnValues[i]);
		}
		boolean flag = false;
		if (whereColumn != null) {
			String whereClause = "";
			for (int k = 0; k < whereColumn.length; k++) {
				whereClause = whereClause + whereColumn[k] + "='"
						+ whereValue[k] + "'" + " AND ";
			}
			whereClause = whereClause.substring(0, whereClause.length() - 5);
			System.out.println("whereclause......" + whereClause);
			flag = db.update(tablename, cv, whereClause, null) > 0;
		} else {
			flag = db.update(tablename, cv, null, null) > 0;
		}

		db.close();
		System.out.println("flag.................." + flag);
		return flag;
	}

	public static long insertintoTable(Context context, final String tableName,
									   String[] colNames, String[] values) {

		DBHelper_New dbhelper = new DBHelper_New(context);
		SQLiteDatabase db = dbhelper.getReadableDatabase();
		ContentValues cv = new ContentValues();
		for (int i = 0; i < colNames.length; i++) {
			cv.put(colNames[i], values[i]);
		}
		long cnt = db.insert(tableName, null, cv);
		db.close();
		return cnt;
	}

	public long insertintoTable1(String tableName, String[] colNames,
								 String[] colVals) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		for (int i = 0; i < colNames.length; i++) {
			cv.put(colNames[i], colVals[i]);
		}
		long result = db.insert(tableName, null, cv);
		db.close();
		return result;
	}

	public boolean updateByQuery(String tablename, String[] columnNames,
								 String[] columnValues, String Condition) {

		SQLiteDatabase db = this.getReadableDatabase();
		ContentValues cv = new ContentValues();
		for (int i = 0; i < columnNames.length; i++) {
			cv.put(columnNames[i], columnValues[i]);
		}
		boolean flag = db.update(tablename, cv, Condition, null) > 0;
		db.close();
		return flag;

	}
	public boolean updateByQuery(String tablename, String columnNames,
								 String columnValues, String Condition) {

		SQLiteDatabase db = this.getReadableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(columnNames, columnValues);

		boolean flag = db.update(tablename, cv, Condition, null) > 0;
		db.close();
		return flag;

	}

	public List<List<String>> getDataByQuery(String query) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery(query, null);
		List<List<String>> list = new ArrayList<List<String>>();
		if (cur.getCount() > 0)
			list = cursorToListArr(cur);
		cur.close();
		db.close();
		return list;
	}
	public List<List<String>> cursorToListArr(Cursor c) {
		List<List<String>> rowList = new ArrayList<List<String>>();
		while (c.moveToNext()) {
			List<String> arr = new ArrayList<String>();
			for (int i = 0; i < c.getColumnCount(); i++) {
				arr.add(c.getString(i));
			}
			rowList.add(arr);
		}
		c.close();
		return rowList;
	}




}