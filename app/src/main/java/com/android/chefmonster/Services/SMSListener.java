package com.android.chefmonster.Services;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.android.chefmonster.AndroidUtilities;

public class SMSListener extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction()
				.equals("android.provider.Telephony.SMS_RECEIVED")) {
			if (!AndroidUtilities.isWaitingForSms()) {
				return;
			}
			Bundle bundle = intent.getExtras();
			SmsMessage[] msgs;
			if (bundle != null) {
				try {
					Object[] pdus = (Object[]) bundle.get("pdus");
					msgs = new SmsMessage[pdus.length];
					String wholeString = "";
					for (int i = 0; i < msgs.length; i++) {
						msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
						wholeString += msgs[i].getMessageBody();
					}

					try {
						Pattern pattern = Pattern.compile("[0-9]+");
						Matcher matcher = pattern.matcher(wholeString);

						if (matcher.find()) {
							String str = matcher.group(0);
							if (str.length() == 4) {

								if(smsotpListener!=null)
									smsotpListener.onSmsRecieved(str);

//								PinValidation pin = PinValidation.getInstance();
//								if (pin != null)
//									pin.updatePin(str);
							}
						}
					} catch (Throwable e) {
						e.printStackTrace();
					}

				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static SMSOTPListener smsotpListener;
	public interface SMSOTPListener{
		public void onSmsRecieved(String otp);
	}
	public static void addSMSOTPListener(SMSOTPListener smsotpListener){
		SMSListener.smsotpListener=smsotpListener;
	}
	public static void removeSMSOTPListener(SMSOTPListener smsotpListener){
		SMSListener.smsotpListener=null;
	}
}
